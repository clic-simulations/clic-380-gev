#
# Generates a lattice file with displaced elements after tuning
#

BeamlineNew
source 1000_db_ml.tcl
BeamlineSet -name ml

set save_all_file [lindex $argv 0]

ReadAllPositions -file ./results/$save_all_file -cav_bpm 1

set quad_strength_file [open ./results/quad_strengths.dat r]
set strengths [read $quad_strength_file]
set strengths [split $strengths "\n"]
foreach strength $strengths {
    if {$strength != ""} {
        set i [lindex $strength 0]
        set k [lindex $strength 1]
        QuadrupoleSetStrength $i $k
    }
}
close $quad_strength_file

BeamlineList -file lattice.$save_all_file.tcl
