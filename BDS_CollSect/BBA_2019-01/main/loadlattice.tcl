source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set scale 1.0

set synrad $sr
set quad_synrad $sr
set mult_synrad $sr
set sbend_synrad $sr

puts "Load lattice"

SetReferenceEnergy $e0

source $script_dir/lattices/2006_bds.tcl

BeamlineSet -name test

# Slice the sbends for more accurate sr in tracking
Octave {
  SI = placet_get_number_list("test", "sbend");
  placet_element_set_attribute("test", SI, "thin_lens", int32(100));
}
