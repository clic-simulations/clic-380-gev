set tcl_precision 15
# ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir ./Scripts
set paramsdir  ./Parameters
set beamdir    ./Beams
set wakedir    ./Wakefields

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl

create_particles_file $beamdir/particles.in beamparams

source $scriptdir/beamline_and_wake.tcl

make_particle_beam beam1 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

source $scriptdir/load_mag.tcl

Octave {
    Start = placet_get_name_number_list("rtml","Marker-SR-start");
    End = placet_get_name_number_list("rtml","Marker-TAL2-start");
    [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1, Start, End);
    printf("\nEmittance entering TAL2\n");
    printf("Emittance: %10.2f %10.2f nm\n", E(end,2)*100,E(end,6)*100);
    save -text $beamdir/beam-$machine-8.dat B;
}
