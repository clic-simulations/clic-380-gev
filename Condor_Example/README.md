--------------------------------------------------------------------------------------
Example of how to run PLACET simulations of CLIC on CERN's HTCondor service.
--------------------------------------------------------------------------------------
This example was based on the slides:
https://twiki.cern.ch/twiki/pub/ABPComputing/Placet/Run_placet_on_HTCondor.pdf 

Most simulations in this repository take command line arguments. This example will 
submit several jobs with different command line arguments.

1) Put simulation files into the folder Simulation Files.
   E.g. I have put the files from RTML_ML_BDS/Plain_OneBeam_2019-01 into
   Simulation_Files.

2) Set the number of cores you want in job.sub:
       line 7: request_cpus = 4
   This number be the same as "ParallelThreads -num 4" in your placet scripts.

3) Generate an args.in file. This should be a comma delimited file.
   Each line corresponds to its own job. This example submits two jobs.

4) Make sure the submission script job.sub reads the correct number of arguments
   from args.in:
       line 14: arguments = $(arg1) $(arg2)
       line 16: queue arg1,arg2 from args.in

5) Modify the path to the simulations files in the executable script exec.sh:
       line 7: export PATH_TO_SIMULATION_FILES=/afs/cern.ch/user/c/cgohil/condor_example/Simulation_Files
   This should be changed to whichever folder you have made in your AFS directory
   to submit the jobs from.

6) To submit execute: condor_submit job.sub
   This command must be executed from a directory in AFS (and not EOS).

--------------------------------------------------------------------------------------
Additional comments:
- To view your jobs execute: condor_q

- To remove all jobs: condor_rm -all

- To change the queue you need to change the following line in job.sub:
      line 6: +JobFlavour = "espresso"
  See: https://twiki.cern.ch/twiki/bin/view/ABPComputing/LxbatchHTCondor#Queue_Flavours

- Some PLACET simulation may require command line arguments in the form:
  placet run.tcl machine 1 bpm_roll 100
  To run these scripts the job.sub scripts must be changed to:
      line 14: arguments = machine $(arg1) bpm_roll $(arg2)
      line 16: queue arg1,arg2 from args.in remains the same.
  and exec.sh must be changed to:
      line 13: placet run.tcl ${1} ${2} ${3} ${4}
