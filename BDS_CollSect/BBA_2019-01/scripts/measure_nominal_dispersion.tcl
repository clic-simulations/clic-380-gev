puts " "
puts "Measuring nominal dispersion profile"
Octave {
   # Measure nominal dispersion with and without multipoles active
   eta0_arr = MeasureDispersion("test", "beam1", "beam2", $deltae, BI);
   eta0 = [eta0_arr(:,1); eta0_arr(:,2)];

   save -text $script_dir/matrices/Dispersion_model_sr_${sr}_deltae_${deltae}.dat eta0_arr eta0
}
