#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <tcl.h>
#include <tk.h>

#define PLACET
//#include "placet.h"
//#include "structures_def.h"

/* Function prototypes */

#include "brent.h"

double r_sign(double x,double s)
{
  if (s>0.0) return fabs(x);
  if (s<0.0) return -fabs(x);
  return 0.0;
}

double brent(double ax,double bx,double cx,double (*f)(double),
		 double tol,double *xmin)
{
    /* System generated locals */
    double r__1;

    /* Local variables */
    static int iter;
    static double a, b, d__, e, p, q, r__, u, v, w, x, etemp, fv, fw, fx, fu, 
	    xm, tol1, tol2;

    if (ax<cx) {a=ax;} else {a=cx; }
    if (ax>cx) {b=ax;} else {b=cx; }
    v = bx;
    w = v;
    x = v;
    e = (double)0.;
    fx = (*f)(x);
    fv = fx;
    fw = fx;
    for (iter = 1; iter <= 100; ++iter) {
	xm = (a + b) * (double).5;
	tol1 = tol * fabs(x) + (double)1e-10;
	tol2 = tol1 * (double)2.;
	if ((r__1 = x - xm, fabs(r__1)) <= tol2 - (b - a) * (double).5) {
	    goto L3;
	}
	if (fabs(e) > tol1) {
	    r__ = (x - w) * (fx - fv);
	    q = (x - v) * (fx - fw);
	    p = (x - v) * q - (x - w) * r__;
	    q = (q - r__) * (double)2.;
	    if (q > (double)0.) {
		p = -p;
	    }
	    q = fabs(q);
	    etemp = e;
	    e = d__;
	    if (fabs(p) >= (r__1 = q * (double).5 * etemp, fabs(r__1)) || p <= 
		    q * (a - x) || p >= q * (b - x)) {
		goto L1;
	    }
	    d__ = p / q;
	    u = x + d__;
	    if (u - a < tol2 || b - u < tol2) {
		r__1 = xm - x;
		d__ = r_sign(tol1,r__1);
	    }
	    goto L2;
	}
L1:
	if (x >= xm) {
	    e = a - x;
	} else {
	    e = b - x;
	}
	d__ = e * (double).381966;
L2:
	if (fabs(d__) >= tol1) {
	    u = x + d__;
	} else {
	    u = x + r_sign(tol1,d__);
	}
	fu = (*f)(u);
	if (fu <= fx) {
	    if (u >= x) {
		a = x;
	    } else {
		b = x;
	    }
	    v = w;
	    fv = fw;
	    w = x;
	    fw = fx;
	    x = u;
	    fx = fu;
	} else {
	    if (u < x) {
		a = u;
	    } else {
		b = u;
	    }
	    if (fu <= fw || w == x) {
		v = w;
		fv = fw;
		w = u;
		fw = fu;
	    } else if (fu <= fv || v == x || v == w) {
		v = u;
		fv = fu;
	    }
	}
    }
    printf("Brent exceed maximum iterations.\n");
L3:
    *xmin = x;
    return fx;
} /* brent_ */

