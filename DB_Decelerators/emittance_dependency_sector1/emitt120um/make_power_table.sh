#!/bin/bash
c=2.9979246e+08
ofile="power_table.dat"

if [ -f $ofile ]; then
    rm $ofile
fi

for file in PETS_SavePower_*; do
	t=$(head "$file" | tail -n 1 | cut -f4)
	s=$(awk "BEGIN {printf \"%.2f\", $c * $t }" | sed 's/,/./g')
	P=$(tail -n 1 "$file" | cut -f6)
	echo "$s	$P" >> $ofile
done
