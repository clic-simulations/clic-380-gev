#
# CLIC 380 GeV: Main e- Beam (RTML)
#
# This code was developed by C. Gohil from a code inherited from A. Latina and Y. Han.
#
# Before executing this script wakefields should be generated with
# "placet Wakefields/create_wake.tcl" - this only needs to be done once to generate
# the .dat files in ./Wakefields.
#
# To run: "placet run.tcl <beam_seed>", if no beam seed is set, the default is 0.
#
set latticedir ./Lattices_e+
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set wakedir    ./Wakefields
set beamdir    ./Beams
set resultsdir ./Results

# Initialise a timer
Octave {initial = tic();}

# Set the number of cores to run in parallel
ParallelThreads -num 4

# Load variables and functions
source $scriptdir/set_parameters_e+.tcl
source $scriptdir/lattice_functions.tcl
source $scriptdir/octave_functions.tcl
source $scriptdir/beam_setup.tcl

puts "Generating initial particle distribution"
source $scriptdir/set_beam_seed.tcl
create_particles_file $beamdir/particles.in beamparams $beam_seed

puts "Setting up lattice"
source $scriptdir/beamline_and_wake.tcl

# Setup the beam
make_particle_beam beam0 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

# Tracking
Octave {
    [E, B] = placet_test_no_correction("rtml", "beam0", "None",
                                       "%s %ex %ey %x %y %xp %yp %z %sz %E %dE");
    disp_results(B);
    save -text $resultsdir/emitt.dat E;
    save_beam("$beamdir/particles.out", B);

    # Display how long the simulation took to run
    toc(initial);
}

exit
