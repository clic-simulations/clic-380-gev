# Mar 31, 2020
# xingguang.liu@cern.ch

# [default] select starting from the first one 
function [x_list, y_list]=select_simple(select_num,select_name, beamline_name="rtml",start_id=1)
    Multipole_all = placet_get_name_number_list(beamline_name,select_name);
    x_list=Multipole_all(start_id:start_id+select_num-1);
    y_list=x_list;
endfunction

# [option] select_case: selet those most effective, not in use (May 8, 2020)
function [x_list, y_list]=select_case(select_num,select_name, marker_start, marker_end, beamline_name="rtml",beam_name="beam4")
    Multipole_all = placet_get_name_number_list(beamline_name,select_name);
    if (select_num > length(Multipole_all))
        error ("select_num is larger than available number!");
    endif

    R=[];
    Start = placet_get_name_number_list(beamline_name,marker_start);
    End = placet_get_name_number_list(beamline_name,marker_end);
    for i=1:length(Multipole_all)
        placet_element_vary_attribute(beamline_name, [Multipole_all(i)], "x", [20]);
        [E,B] = placet_test_no_correction(beamline_name, beam_name, "None", 1, Start, End);
        merit=E(end,2)/7.0 + E(end,6)/0.05;
        placet_element_vary_attribute(beamline_name, [Multipole_all(i)], "x", [-20]);
        R=[R;Multipole_all(i) merit];
    endfor

    R_=sortrows(R,[-2]);
    x_list=sort(R_(1:select_num));

    R=[];
    for i=1:length(Multipole_all)
        placet_element_vary_attribute(beamline_name, [Multipole_all(i)], "y", [20]);
        [E,B] = placet_test_no_correction(beamline_name, beam_name, "None", 1, Start, End);
        merit=E(end,2)/7.0 + E(end,6)/0.05;
        placet_element_vary_attribute(beamline_name, [Multipole_all(i)], "y", [-20]);
        R=[R;Multipole_all(i) merit];
    endfor
    R_=sortrows(R,[-2]);
    y_list=sort(R_(1:select_num));

endfunction
    
# define a offset function
function add_offset(x_list,x_offset, y_list,y_offset,beamline_name="rtml")
    #  round to 0.1 precision 
    offset_X = round(100*x_offset)/10;
    offset_Y = round(100*y_offset)/10;
    placet_element_vary_attribute(beamline_name, x_list, "x", offset_X);
    placet_element_vary_attribute(beamline_name, y_list, "y", offset_Y);
endfunction

function [ex, ey]=test_sextupole_tuning(x_list,x_offset, y_list,y_offset,marker_start, marker_end, beamline_name="rtml",beam_name="beam4",uncerntainty=0.01)
    # num=length(X);
    add_offset(x_list,x_offset, y_list,y_offset,beamline_name);
    # track to the LTL-start
    Start = placet_get_name_number_list(beamline_name,marker_start);
    End = placet_get_name_number_list(beamline_name,marker_end);
    [E,B] = placet_test_no_correction(beamline_name,beam_name, "None", 1, Start, End);
    # add 1% uncerntainty to the 
    ex = E(end,2)*(1 + uncerntainty*randn());
    ey = E(end,6)*(1 + uncerntainty*randn());
    
endfunction