#
# CLIC 380 GeV: Main Beam RTML
#
# This code with developed by C. Gohil from the simulation inherited from
# Y. Han.
#
# The order of the lines up until the Octave code must not be changed.
# The seeds of the random number generated of this simulation have been set
# so that the result does not vary from run to run.
#
ParallelThreads -num 10

proc FF1 {} {}
proc FF2 {} {}

# Directories containing files needed by this simulation
set latticedir ./Lattices_e-
set scriptdir ./Scripts
set paramsdir ./Parameters

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $scriptdir/setParam_matching.tcl
source $scriptdir/beamsetup.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/lattice_functions.tcl
source $scriptdir/octave_functions.tcl

# Generate wakefields
source create_wake.tcl
if {$beamparams(usewakefields)==1} {
   source rf_parameteres_ww.tcl
}
create_particles_file particles.in beamparams
create_zero_wakes_file zero_wake.dat beamparams rfparamsbc1 particles.in

# Set up the RTML lattice
BeamlineNew
source $scriptdir/beamline_and_wake_matching.tcl
BeamlineSet -name rtml

# Set up the ML lattice
BeamlineNew
source $latticedir/ml_lattice.tcl
BeamlineSet -name ml

# Generate the beam at the start of the RTML
make_particle_beam beam1 beamparams beam_before_matching.in zero_wake.dat

# Turn on coherent synchrotron radiation
source $scriptdir/setCSR.m

# Twiss parameters at the end of the RTML to match to
array set twiss1 {
  beta_x 8.09953
  beta_y 1.19147
  alpha_x -8.4047e-17
  alpha_y -2.80157e-17
}

FirstOrder 1
Octave {
    pkg load optim;

    # Elements to use to match
    global Drift_Correctors = placet_get_name_number_list("rtml", "matching-drift");
    global Quad_Correctors = placet_get_name_number_list("rtml", "matching-quad");

    global twiss1 = zeros(4,4);
    twiss1(1:2,1:2) = [ $twiss1(beta_x), -($twiss1(alpha_x)); -($twiss1(alpha_x)), (1+($twiss1(alpha_x))*($twiss1(alpha_x)))/($twiss1(beta_x)) ];
    twiss1(3:4,3:4) = [ $twiss1(beta_y), -($twiss1(alpha_y)); -($twiss1(alpha_y)), (1+($twiss1(alpha_y))*($twiss1(alpha_y)))/($twiss1(beta_y)) ];

    # Sum of squares function to minimise
    function merit = TwissMatching(beamline)
        global Correctors;
        global Drift_Correctors;
        global Quad_Correctors;

        # Target Twiss parameters at the end of the RTML
        global twiss1;

        # Display the current parameters
        Quads=placet_element_get_attribute("rtml",Quad_Correctors,"strength")./2.7
        Drifts = placet_element_get_attribute("rtml", Drift_Correctors, "length")

        # Calculate the Twiss parameters at the end of the RTML
        [emitt,beam] = placet_test_no_correction("rtml", "beam1", "None");
        twiss = placet_get_twiss_matrix(beam);
        beta_x = twiss(1,1)/twiss1(1,1)
        beta_y = twiss(3,3)/twiss1(3,3)
        alpha_x = -twiss(1,2)/twiss1(1,2)
        alpha_y = -twiss(3,4)/twiss1(3,4)
        x = mean(beam(:,2))
        y = mean(beam(:,3))
        xp = mean(beam(:,5))
        yp = mean(beam(:,6))

        # Calculate the sum of squares
        #merit = sum((1.0 - [beta_x, beta_y, alpha_x, alpha_y]).**2);
        merit = sum((1.0 - [beta_x, beta_y]).**2) + sqrt((5*x-1e-2)**2) + sqrt((y-1e-2)**2) + sqrt((3*xp-1e-2)**2) + sqrt((yp-1e-2)**2);
        #merit = sum((1.0 - [beta_x, beta_y]).**2);

    endfunction

    # Properties to vary to find optimum
    Drift_Leverages = ["length"; "length"; "length"; "length"; "length"];
    Quad_Leverages   = ["strength"; "strength"; "strength"; "strength"];
    Drift_Constraints = [0  20; 0  20; 0  20; 0  20; 0  20];
    Quad_Constraints = [0  0; 0  0; 0  0; 0  0];
    #Drift_Leverages = ["length"; "length"; "length"];
    #Quad_Leverages   = ["strength"; "strength"; "strength"];
    #Drift_Constraints = [0  25; 0  25; 0  25];
    #Quad_Constraints = [0  0; 0  0; 0  0];

    # Combine the Drifts and Quads for optimisation
    Correctors = [Drift_Correctors Quad_Correctors];
    Leverages = [Drift_Leverages; Quad_Leverages];
    Constraints = [Drift_Constraints; Quad_Constraints];

    # Perform the optimisation and display the parameters
    [optimum, merit] = placet_optimize_constraint("rtml", "TwissMatching",
                           Correctors, Leverages, Constraints);
    Quads = placet_element_get_attribute("rtml",Quad_Correctors,"strength")./2.7
    Drifts = placet_element_get_attribute("rtml", Drift_Correctors, "length")

    # Track the beam through the RTML
    [emitt, beam] = placet_test_no_correction("rtml", "beam1", "None", "%ex %ey %sex %sey %x %y %xp %yp %Env %sx %sy %sxp %syp %E %dE %s %n %name");
    save -text emitt_matching.dat emitt;

    # Display the emittance at the end of the RMTL
    rtml = placet_get_emittance(beam)
    
    # Display the twiss parameters at the end of the RTML
    twiss = placet_get_twiss_matrix(beam);
    beta_x = twiss(1,1)
    beta_y = twiss(3,3)
    alpha_x = -twiss(1,2)
    alpha_y = -twiss(3,4)
    disp("");

    # Display the offset and angle of the beam at the end of the RTML
    x = mean(beam(:,2))
    y = mean(beam(:,3))
    xp = mean(beam(:,5))
    yp = mean(beam(:,6))

    # Remove the offset and angle of the beam
    beam(:,2) -= x;
    beam(:,3) -= y;
    beam(:,5) -= xp;
    beam(:,6) -= yp;

    # Track the beam through the ML
    placet_set_beam("beam1", beam);
    [emitt, beam] = placet_test_no_correction("ml", "beam1", "None", "%ex %ey %sex %sey %x %y %xp %yp %Env %sx %sy %sxp %syp %E %dE %s %n %name");
    
    # Display the emittance at the end of the ML
    ml = placet_get_emittance(beam)

    # Display the twiss parameters at the end of the RTML
    twiss = placet_get_twiss_matrix(beam);
    beta_x = twiss(1,1)
    beta_y = twiss(3,3)
    alpha_x = -twiss(1,2)
    alpha_y = -twiss(3,4)
    disp("");

    # Display the offset and angle of the beam at the end of the RTML
    x = mean(beam(:,2))
    y = mean(beam(:,3))
    xp = mean(beam(:,5))
    yp = mean(beam(:,6))

}

exit
