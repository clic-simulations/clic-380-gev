set rfparamsbooster(gradientww)   1.834483399745e+07
set rfparamsbc1(gradientww)   2.230394470628e+07
set rfparamsbc2(gradientww)   7.454231660871e+07
set energy_wake_bc1  -3.529015605001e-05
set energy_wake_booster  -5.773992532602e-05
set energy_wake_bc2  -7.432733777479e-05
