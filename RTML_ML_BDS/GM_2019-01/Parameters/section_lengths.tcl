#
# This script contains the length in metres of each section of CLIC.
#
set sec_len(rtml_e-) 9525.44
set sec_len(rtml_e+) 9234.1
set sec_len(ml)      3483.97
if {[info exists bds_type]} {
    if {$bds_type == "43"} {
        set sec_len(bds) 1727.84
    } elseif {$bds_type == "6"} {
        set sec_len(bds) 1949.11
    }
}
