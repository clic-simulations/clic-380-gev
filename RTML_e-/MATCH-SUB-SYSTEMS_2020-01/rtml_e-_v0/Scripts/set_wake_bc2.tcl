SplineCreate "rtml_Wt_bc2" -file "$wakedir/Wt_bc2.dat"
SplineCreate "rtml_Wl_bc2" -file "$wakedir/Wl_bc2.dat"
ShortRangeWake "rtml_SR_bc2" -type 2 \
-wx "rtml_Wt_bc2" \
-wy "rtml_Wt_bc2" \
-wz "rtml_Wl_bc2"
WakeSet rtml_Wlong_bc2 {}

TclCall -script {
       Octave {
               CAV = placet_get_name_number_list('0210','210C');
               placet_element_set_attribute("0210", CAV, "short_range_wake", "rtml_SR_bc2");
               placet_element_set_attribute("0210", CAV, "six_dim", true);

               lambda_bc2 = placet_element_get_attribute("0210", CAV(1),"lambda");
               Tcl_SetVar("lambda_bc2",lambda_bc2);
       }
       InjectorCavityDefine -lambda $lambda_bc2 -wakelong rtml_Wlong_bc2
}
