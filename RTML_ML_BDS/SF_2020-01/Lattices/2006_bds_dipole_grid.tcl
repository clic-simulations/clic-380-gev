Girder
Bpm
Quadrupole -name "FQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.1593135711*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007168470890909433 -strength_y -0.0005477827619991011
Dipole
Girder
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007043401852350846 -strength_y -0.0005339397274752659
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007122102889187948 -strength_y -0.0007287232435102544
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071716700376501605 -strength_y -0.0007758157455324038
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007152604056414348 -strength_y -0.0007187031606988491
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007205548828699483 -strength_y 0.0007959344886559758
Drift -name "FDD" -length 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQD" -synrad $quad_synrad -length 0.5 -strength [expr -0.06708030646*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "FDD" -length 0.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006912182800729551 -strength_y -0.0005860910723428126
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069101142319020654 -strength_y -0.0005200304013108794
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006955230505072634 -strength_y -0.0004733993575654685
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069319214176212915 -strength_y -0.0004133137993963151
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006869449776349699 -strength_y -0.0003422479595482167
Drift -name "FDD" -length 0.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQF2" -synrad $quad_synrad -length 0.5 -strength [expr -0.1182922355*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006853822445782181 -strength_y -0.0002330791145319602
Dipole
Girder
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006728778490726519 -strength_y -0.00022553251427248852
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006640676996112968 -strength_y -0.00030531450117233665
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006398368498963762 -strength_y -0.0002670493613194288
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0065281140830851046 -strength_y -0.00049048800235902
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006483807736084985 -strength_y 0.00028410010223047226
Drift -name "FDD" -length 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQD2" -synrad $quad_synrad -length 0.5 -strength [expr 0.02788484518*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Bpm
Quadrupole -name "TQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.1149808129*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006780697923772983 -strength_y -0.0007366316445158171
Dipole
Girder
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006777625696192406 -strength_y -0.0006945495869857045
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067491494094699955 -strength_y -0.0007332228339137523
Drift -name "DD" -length 0.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.78125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006809929012110049 -strength_y -0.0008483140489672277
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006690353738174398 -strength_y -0.0007997986514967259
Drift -name "DD" -length 0.4375 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.5625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006624013361424402 -strength_y -0.000855890653354296
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066663277993374865 -strength_y -0.0009126971921188692
Drift -name "DD" -length 0.65625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.34375 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006680360856884855 -strength_y -0.0006462750839242385
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00663565506443359 -strength_y -0.0005129450753039677
Drift -name "DD" -length 0.875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD" -synrad $quad_synrad -length 0.125 -strength [expr -0.01474936495*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006558927404371508 -strength_y -0.00023836252815379787
Quadrupole -name "TQD" -synrad $quad_synrad -length 0.875 -strength [expr -0.10324555465*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006658926463959371 -strength_y -0.00022870403079970115
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006740790274902633 -strength_y -0.00023253695206176982
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006737460307834209 -strength_y -0.00023810360948646142
Drift -name "DD" -length 0.09375 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.90625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006800207621459544 -strength_y -0.00025032507661427783
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006878913754648536 -strength_y -8.482575031376713e-05
Drift -name "DD" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.6875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006969445911318849 -strength_y -1.74831499183211e-05
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006860075614966182 -strength_y -4.358266079755573e-05
Drift -name "DD" -length 0.53125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.46875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006825872908827459 -strength_y -0.00018502973463561937
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006843632096408285 -strength_y -0.00024289028035553097
Drift -name "DD" -length 0.75 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF2A" -synrad $quad_synrad -length 0.25 -strength [expr 0.05168697685*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007121709426725326 -strength_y -4.828221909011445e-05
Quadrupole -name "TQF2A" -synrad $quad_synrad -length 0.25 -strength [expr 0.05168697685*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF2B" -synrad $quad_synrad -length 0.5 -strength [expr 0.1033739537*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 0.25 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007062082168846163 -strength_y 0.00013657114134204656
Drift -name "DD2" -length 0.774479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.225520833 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006913929867284103 -strength_y 8.839664629589927e-05
Drift -name "DD2" -length 0.798958334 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.201041666 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067800702631034306 -strength_y 9.098456489772039e-05
Drift -name "DD2" -length 0.823437501 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD2A" -synrad $quad_synrad -length 0.176562499 -strength [expr -0.0300137259639*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00664718150065187 -strength_y 0.0002243877776699533
Quadrupole -name "TQD2A" -synrad $quad_synrad -length 0.323437501 -strength [expr -0.0549808967161*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD2B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08499462268*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 0.176562499 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006695337996860274 -strength_y 0.00023909909445969318
Drift -name "DD2" -length 0.847916668 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.152083332 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006723224898528831 -strength_y 0.00025530579699945157
Drift -name "DD2" -length 0.872395835 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.127604165 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006607232062559099 -strength_y 0.00035117368984106197
Drift -name "DD2" -length 0.896875002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF3A" -synrad $quad_synrad -length 0.103124998 -strength [expr 0.0116745914015*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066106768422393766 -strength_y 0.0001943637681330411
Quadrupole -name "TQF3A" -synrad $quad_synrad -length 0.396875002 -strength [expr 0.0449294892185*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF3B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05660408062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 0.103124998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067311496579253085 -strength_y 0.00013339105987502682
Drift -name "DD2" -length 0.921354169 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.078645831 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006868432344377309 -strength_y 2.2468706291876664e-05
Drift -name "DD2" -length 0.945833336 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.054166664 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006904977948821913 -strength_y -0.00014705313521206
Drift -name "DD2" -length 0.970312503 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD3A" -synrad $quad_synrad -length 0.029687497 -strength [expr -0.00470596565645*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006968621791411488 -strength_y -0.00025782299601876573
Quadrupole -name "TQD3A" -synrad $quad_synrad -length 0.470312503 -strength [expr -0.0745524113036*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD3B" -synrad $quad_synrad -length 0.5 -strength [expr -0.07925837696*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 0.029687497 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069339474075081784 -strength_y -0.00026328615454387173
Drift -name "DD2" -length 0.99479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.00520832999997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006961891113977715 -strength_y -0.0003254881503621322
Drift -name "DD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006885260708571421 -strength_y -0.0005386759204099989
Drift -name "DD2" -length 0.019270837 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.980729163 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068451090697444405 -strength_y -0.000617866940847654
Drift -name "DD2" -length 0.043750004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF4A" -synrad $quad_synrad -length 0.5 -strength [expr 0.1039004288*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF4B" -synrad $quad_synrad -length 0.456249996 -strength [expr 0.0948091404488*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006906461707522733 -strength_y -0.0005734482171838299
Quadrupole -name "TQF4B" -synrad $quad_synrad -length 0.043750004 -strength [expr 0.00909128835121*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 0.956249996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007068119914377886 -strength_y -0.0005099097009262135
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007083379247392427 -strength_y -0.0004827864231228415
Drift -name "DD" -length 0.262500004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.737499996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00707859035253411 -strength_y -0.00032376622437305314
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071114972387167534 -strength_y -4.9578170020136694e-05
Drift -name "DD" -length 0.481250004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.518749996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067893512381066775 -strength_y -0.00011039329320428214
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00680642715766877 -strength_y -0.00016872905006524265
Drift -name "DD" -length 0.700000004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.299999996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068219604016281745 -strength_y -2.2309472382267786e-05
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071014299294108385 -strength_y 2.136542592694176e-05
Drift -name "DD" -length 0.918750004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD4A" -synrad $quad_synrad -length 0.081249996 -strength [expr -0.014607599465*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007097183661466011 -strength_y 0.00012043455723615284
Quadrupole -name "TQD4A" -synrad $quad_synrad -length 0.418750004 -strength [expr -0.075285324745*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD4B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08989292421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 0.081249996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007056522385011975 -strength_y -5.2082949512387214e-05
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007096991785905129 -strength_y -8.718746200348281e-05
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007185215802743488 -strength_y -9.884422859619948e-05
Drift -name "DD" -length 0.137500004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.862499996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007237075737480429 -strength_y -0.00018676681575083742
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007212685595909867 -strength_y -0.00029845345254717477
Drift -name "DD" -length 0.356250004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.643749996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007102051411966663 -strength_y -0.00034647044480973487
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071101053131865 -strength_y -0.00030019994187166054
Drift -name "DD" -length 0.575000004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.424999996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006972879024771779 -strength_y -0.0003261803027527352
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007037367390651847 -strength_y -0.00036303463033292126
Drift -name "DD" -length 0.793750004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF5A" -synrad $quad_synrad -length 0.206249996 -strength [expr 0.0237791299565*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007076336237419452 -strength_y -0.00021411923039856205
Quadrupole -name "TQF5A" -synrad $quad_synrad -length 0.293750004 -strength [expr 0.0338672468135*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF5B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05764637677*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 0.206249996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070615223842280675 -strength_y -0.0001272275093443893
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006951180046325018 -strength_y -0.00013724992691713128
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006862757777853778 -strength_y -0.00015938537210050992
Drift -name "DD" -length 0.012500004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.987499996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00673296111103503 -strength_y -0.00012457332445003975
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00664628659117497 -strength_y -0.00027528598313589225
Drift -name "DD" -length 0.231250004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.768749996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006632282834245352 -strength_y -0.0004563758255176848
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006856968232331258 -strength_y -0.00042464278902992715
Drift -name "DD" -length 0.450000004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.549999996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070752879853908275 -strength_y -0.0004761466376631075
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007161902609273771 -strength_y -0.0005504004493272315
Drift -name "DD" -length 0.668750004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD5" -synrad $quad_synrad -length 0.331249996 -strength [expr -0.0366898972169*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007078568797251985 -strength_y -0.0006871834685219649
Quadrupole -name "TQD5" -synrad $quad_synrad -length 0.668750004 -strength [expr -0.0740720579831*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.331249996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007194289811599803 -strength_y -0.0007903358805752944
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071295142529204675 -strength_y -0.0007435300639840857
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071312926055961965 -strength_y -0.0007170485492843655
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007191731097478816 -strength_y -0.0007283442134031614
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007159553775878617 -strength_y -0.0007205388410560869
Drift -name "DDL" -length 0.213141778 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.786858222 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006914830766654606 -strength_y -0.0005891670737556666
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00695393655344051 -strength_y -0.0004761799067383192
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006873274274801712 -strength_y -0.0003471469947612326
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068684605456115514 -strength_y -0.00032269289589816806
Drift -name "DDL" -length 0.757533552 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.242466448 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006837837105405922 -strength_y -0.00038259845987188753
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006873284929218164 -strength_y -0.0002728701358585738
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067532027896110725 -strength_y -0.00020588872128315792
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066920583919172815 -strength_y -0.0002947396922091481
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0064375700975889336 -strength_y -0.00027313549253037253
Drift -name "DDL" -length 0.301925326 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.698074674 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006540416913130531 -strength_y -0.0005206585920424573
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006723092994973561 -strength_y -0.0008023561899135985
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006748811571708916 -strength_y -0.0007374971135012632
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00677383568465676 -strength_y -0.0008307216206461074
Drift -name "DDL" -length 0.8463171 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF6A" -synrad $quad_synrad -length 0.1536829 -strength [expr 0.0080300934576*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006813452146753256 -strength_y 0.0008511379291830848
Quadrupole -name "TQF6A" -synrad $quad_synrad -length 0.3463171 -strength [expr 0.0180954333824*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF6B" -synrad $quad_synrad -length 0.5 -strength [expr 0.02612552684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.1536829 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006666088520764381 -strength_y -0.0009096576682006707
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006664228931835414 -strength_y -0.000791365830031225
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006682607145883685 -strength_y -0.000631157033589072
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006685839279160329 -strength_y -0.000530383679276315
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006626585104575572 -strength_y -0.0005132926151460977
Drift -name "DDL" -length 0.390708874 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.609291126 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006475125380258785 -strength_y -0.00046782762370889495
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006509590970761761 -strength_y -0.0002630395966445229
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006708094114104873 -strength_y -0.00023252095929395
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067377090974384945 -strength_y -0.0002332517814492597
Drift -name "DDL" -length 0.935100648 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.0648993519999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006788006437891113 -strength_y -0.0002586173806954015
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006865150981966719 -strength_y -0.00011189974636085419
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006963272378705054 -strength_y -1.873018770093785e-05
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006889507442880862 -strength_y -3.0272552283361067e-05
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006818957045635641 -strength_y -0.0001563370011636197
Drift -name "DDL" -length 0.479492422 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.520507578 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006846598751605498 -strength_y -0.00024605565153466917
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007095644112438965 -strength_y -0.0002132587489311636
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007004847484505947 -strength_y 0.00013985238571914327
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006778928290834125 -strength_y 9.300194825622006e-05
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006854257949561712 -strength_y -6.000688684138318e-05
Drift -name "DDL" -length 0.0238841960001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD6A" -synrad $quad_synrad -length 0.5 -strength [expr -0.02002336697*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD6B" -synrad $quad_synrad -length 0.476115804 -strength [expr -0.0190668829274*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006726426831520155 -strength_y 0.00023930560511824808
Quadrupole -name "TQD6B" -synrad $quad_synrad -length 0.0238841960001 -strength [expr -0.000956484042586*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.976115804 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006716531538925929 -strength_y 0.0002606644667655799
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006645767675452939 -strength_y 0.0003142379841753231
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00660683385212141 -strength_y 0.00035199837075423375
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006627941027268642 -strength_y 0.0003213959744609276
Drift -name "DDL" -length 0.56827597 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.43172403 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006626108939844481 -strength_y 0.00021645086656884078
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006669131685216485 -strength_y 0.00015941238939021161
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006896151814090046 -strength_y -2.9150259177456722e-05
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069163035135917025 -strength_y -0.0001708297470906029
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006981027957957154 -strength_y -0.00024683694507841066
Drift -name "DDL" -length 0.112667744 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.887332256 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00692563207482704 -strength_y -0.0002573548255283799
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006960717114335625 -strength_y -0.00037315190959060694
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006867999397030182 -strength_y -0.0005657160685211641
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006839911923799882 -strength_y -0.0006245069155039875
Drift -name "DDL" -length 0.657059518 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.342940482 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006929456929499387 -strength_y -0.0005628345460664421
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070621138594077 -strength_y -0.000442132709214279
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007111244613874243 -strength_y -4.943697143520907e-05
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006770185837399993 -strength_y -0.0001729102413460048
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006823335361764564 -strength_y -2.1105735258423815e-05
Drift -name "DDL" -length 0.201451292 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF7A" -synrad $quad_synrad -length 0.5 -strength [expr 0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF7B" -synrad $quad_synrad -length 0.298548708 -strength [expr 0.0119149257808*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00708961538676922 -strength_y 2.126992143787785e-05
Quadrupole -name "TQF7B" -synrad $quad_synrad -length 0.201451292 -strength [expr 0.00803981771921*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.798548708 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007127664058867334 -strength_y 5.5402361027392595e-05
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007080271472828829 -strength_y 0.00011609872777972838
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007081652216462595 -strength_y 0.00014783678617994491
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007098030794120036 -strength_y 0.0001040069330179922
Drift -name "DDL" -length 0.745843066 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.254156934 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070672489766129545 -strength_y -9.046459668209933e-06
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007134563707926379 -strength_y -9.18649293741169e-05
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007239288461374419 -strength_y -0.0001601696411993132
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007220348259009404 -strength_y -0.000284979836535218
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007115095864163179 -strength_y -0.0003450167011003411
Drift -name "DDL" -length 0.29023484 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.70976516 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007108923944859838 -strength_y -0.00030840271454241297
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006994790361900468 -strength_y -0.00031378142361261223
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007013016259906267 -strength_y -0.00036936809324783975
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007071311938619247 -strength_y -0.0002832000714503661
Drift -name "DDL" -length 0.834626614 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.165373386 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007072874244397928 -strength_y -0.0002166325351341736
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006962472108820229 -strength_y -0.0001346511908669302
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006744847579379426 -strength_y -0.0001261664352791594
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006631497486320383 -strength_y -0.0003751565319439273
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068354964813883866 -strength_y -0.00042689110282410843
Drift -name "DDL" -length 0.379018388 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD7A" -synrad $quad_synrad -length 0.5 -strength [expr -0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD7B" -synrad $quad_synrad -length 0.120981612 -strength [expr -0.00482831407135*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007082785845876905 -strength_y -0.0004860160925698673
Quadrupole -name "TQD7B" -synrad $quad_synrad -length 0.379018388 -strength [expr -0.0151264294286*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.620981612 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007148056107410933 -strength_y -0.0005485837406596053
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007166854695959926 -strength_y -0.0005409297757594285
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007101075308644698 -strength_y -0.0005222858082189329
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007021096464193105 -strength_y -0.0005558514206209445
Drift -name "DDL" -length 0.923410162 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.076589838 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007064745547307882 -strength_y -0.0006720141353569355
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007200999036446773 -strength_y -0.0007943243073535219
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007122776723586466 -strength_y -0.0007183799047507123
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071807160038578 -strength_y -0.0007272548269799708
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006918815231329663 -strength_y -0.0005934564082866671
Drift -name "DDL" -length 0.467801936 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.532198064 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00695316495790594 -strength_y -0.0004775912380761869
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068725339864339095 -strength_y -0.0003462158315495696
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068689793007582385 -strength_y -0.00032505947300294654
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006837448689488217 -strength_y -0.00038134646269700296
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00687230284062885 -strength_y -0.0002666811796518266
Drift -name "DDL" -length 0.01219371 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.98780629 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006750969427839322 -strength_y -0.0002066886468202956
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006692614059590873 -strength_y -0.00029444558712910424
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006443909350569366 -strength_y -0.0002746419942417976
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006467864272299339 -strength_y -0.00037721941559888336
Drift -name "DDL" -length 0.556585484 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF8A" -synrad $quad_synrad -length 0.443414516 -strength [expr 0.0178130801973*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006760151511941816 -strength_y -0.0007044959882325576
Quadrupole -name "TQF8A" -synrad $quad_synrad -length 0.056585484 -strength [expr 0.00227318170272*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF8B" -synrad $quad_synrad -length 0.5 -strength [expr 0.0200862619*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.443414516 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006798146244013197 -strength_y -0.0008380517574843673
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00680646961857094 -strength_y -0.0008470937258735929
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067239855263872845 -strength_y -0.0008104284365013838
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006630797815168102 -strength_y -0.0007930229738033666
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066163337132383 -strength_y -0.0008372566398981147
Drift -name "DDL" -length 0.100977258 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.899022742 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006666934111063022 -strength_y -0.000925814043266861
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006673980053108222 -strength_y -0.0006909633844225979
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006659973165804695 -strength_y -0.0005137151565515208
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006480385611801723 -strength_y -0.00047403973733829913
Drift -name "DDL" -length 0.645369032 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.354630968 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006500002189718601 -strength_y -0.00027079926470803127
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067006981263441874 -strength_y -0.0002318936714272758
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067383018822275015 -strength_y -0.0002321648339963089
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006783307791724088 -strength_y -0.0002604644755618986
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006862714124769964 -strength_y -0.00011747267121709933
Drift -name "DDL" -length 0.189760806 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.810239194 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006963788482086064 -strength_y -1.774565126850158e-05
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006817490581749428 -strength_y -0.0001469466177728784
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006858878667455609 -strength_y -0.00025558224625812497
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007114834848414958 -strength_y -0.00014923909963398765
Drift -name "DDL" -length 0.73415258 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD8" -synrad $quad_synrad -length 0.26584742 -strength [expr -0.0275952029474*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007070055469140092 -strength_y 0.0002546707305378364
Quadrupole -name "TQD8" -synrad $quad_synrad -length 0.73415258 -strength [expr -0.0762057026526*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL89" -length 0.26584742 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006841059796675215 -strength_y 5.710884184804119e-05
Drift -name "DDL89" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006787710258706603 -strength_y 7.91037582644069e-05
Drift -name "DDL89" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006754961273939577 -strength_y 0.00013657201313730988
Drift -name "DDL89" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006702855585126786 -strength_y 0.0001878745285838788
Drift -name "DDL89" -length 0.528544354 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF9" -synrad $quad_synrad -length 0.471455646 -strength [expr -0.0718722937141*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006680909198897931 -strength_y 0.00028703735250779495
Quadrupole -name "TQF9" -synrad $quad_synrad -length 0.528544354 -strength [expr -0.0805753316859*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDLE" -length 0.471455646 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066140451949692584 -strength_y 0.00034505388042430004
Drift -name "DDLE" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006638570601166689 -strength_y 0.0002845139801512763
Drift -name "DDLE" -length 0.549011171 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD9" -synrad $quad_synrad -length 0.450988829 -strength [expr 0.0607891689544*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006731520270999887 -strength_y 0.0001332058125775557
Quadrupole -name "TQD9" -synrad $quad_synrad -length 0.549011171 -strength [expr 0.0740016840456*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988829 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006823525577189798 -strength_y 7.178611129938298e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00690528936563956 -strength_y -6.494648639503822e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006905684146622313 -strength_y -0.0001514871225305808
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006948605179867424 -strength_y -0.0001961059178433871
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006982270665680136 -strength_y -0.00024324485752795773
Drift -name "DUTIL" -length 0.549011171 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.450988829 -strength [expr 0.000806605433266*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006864514585191733 -strength_y -0.000571573759778764
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.799011171 -strength [expr 0.00142905258473*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.200988829 -strength [expr 0.000359473829666*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006835839384351722 -strength_y -0.0006255225339191033
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.0 -strength [expr 0.0017885264144*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006849814951151948 -strength_y -0.0006065221846821689
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.0490111709999 -strength [expr 8.76577739341e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "YGIRDER" -length 0.325988829 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007074799357811152 -strength_y -0.0005069519806860646
Drift -name "YGIRDER" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007072639052824722 -strength_y -0.00046690500655770044
Drift -name "YGIRDER" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007072222227887611 -strength_y -0.00034188515083774306
Drift -name "YGIRDER" -length 0.174011171 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.200988829 -angle 1.53787370981e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006865093640967202 -strength_y -6.609366471966235e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067636488752907545 -strength_y -0.00014831390138214003
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006794714464801884 -strength_y -0.00017943081678650576
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006806729710956546 -strength_y -0.00012496602329387194
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006810188119310865 -strength_y -3.5901963050506266e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006932521887749937 -strength_y 1.552712099364142e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 0.450052838 -angle 3.44359649752e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.549947162 -angle 4.20794173702e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007136798996113346 -strength_y 2.4541707664266234e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070775888130602035 -strength_y 0.0001222149411173547
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070971601374338325 -strength_y 9.16354868969623e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007062324138025669 -strength_y -7.153233506259955e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007212310631044118 -strength_y -0.0001082146480099959
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007231733271613175 -strength_y -0.00022508646667690694
Sbend -name "B3B" -synrad $sbend_synrad -length 0.101094505 -angle 7.73528470308e-07 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.273905495 -angle 2.09579836764e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007025285999875291 -strength_y -0.00030142223285084654
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006950668274216207 -strength_y -0.00034932997958976705
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00700040877573835 -strength_y -0.0003711633283422037
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007083477094615161 -strength_y -0.0003337350451674879
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007069135798295358 -strength_y -0.0002814513913589995
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007031297087486207 -strength_y -0.00025095855111907986
Sbend -name "B3A" -synrad $sbend_synrad -length 0.377136172 -angle 2.88567183968e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.622863828 -angle 4.76586639485e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007105432575114032 -strength_y -0.00018687251727664945
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070020162640474955 -strength_y -0.0001278771829558719
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068286946429954865 -strength_y -0.00015771283253713226
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00669867152243166 -strength_y -0.00013353506907094412
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006635494172244982 -strength_y -0.0003483774671095889
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006678243250969027 -strength_y -0.00045555286288137396
Sbend -name "B3B" -synrad $sbend_synrad -length 0.0281778389999 -angle 2.15603812474e-07 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.346822161 -angle 2.65372302548e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007134108689791114 -strength_y -0.0005263320476746562
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007034669780305575 -strength_y -0.0005396193475943552
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007040822572709276 -strength_y -0.0006412542888644881
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007162896709366732 -strength_y -0.0007626629957077055
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007200587561873921 -strength_y -0.0007940879426585958
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007131040767714105 -strength_y -0.0007449682155507905
Sbend -name "B3A" -synrad $sbend_synrad -length 0.304219506 -angle 2.32774718185e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.695780494 -angle 5.32379105269e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007175870688545661 -strength_y -0.0007236147687633281
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070656779182315665 -strength_y -0.0006835308802273796
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00690280270068504 -strength_y -0.000529487791034818
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006942168942867036 -strength_y -0.00042628711500040175
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006852177843190793 -strength_y -0.0003080493730487111
Sbend -name "B3B" -synrad $sbend_synrad -length 0.955261173 -angle 7.30921738918e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.0447388270001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006859737215695335 -strength_y -0.0003658940877320252
Drift -name "DMM" -length 0.580261173 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.419738827 -angle 3.21164768331e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006712080181690896 -strength_y -0.0002753303706475701
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006629628476612552 -strength_y -0.00030543051112260966
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006466981984367987 -strength_y -0.0002801706219894527
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006395463120484765 -strength_y -0.0002747173658952148
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0064761791862276135 -strength_y -0.0003904418247744527
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006569735045676792 -strength_y -0.0006101375242257151
Sbend -name "B3A" -synrad $sbend_synrad -length 0.23130284 -angle 1.76982252402e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.76869716 -angle 5.88171571052e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066628271163214885 -strength_y -0.0008252331230165027
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006788269864177664 -strength_y -0.0007116869905505331
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006750688522746215 -strength_y -0.0007598421817045809
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006811854164273244 -strength_y -0.0008501846700806347
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006645914831651262 -strength_y -0.0007920146111610768
Sbend -name "B3B" -synrad $sbend_synrad -length 0.882344507 -angle 6.75129273134e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.117655493 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066659849689086025 -strength_y -0.0009286976319142317
Drift -name "DMM" -length 0.507344507 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.492655493 -angle 3.76957234114e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006516440261338005 -strength_y -0.0005002851307250972
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006450518430624534 -strength_y -0.0004095990814911856
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006482558921557654 -strength_y -0.0002890558951556746
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006590221074368352 -strength_y -0.00023124293197092198
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066981439034027946 -strength_y -0.000231669978550585
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006741708267894693 -strength_y -0.00023215157895490544
Sbend -name "B3A" -synrad $sbend_synrad -length 0.158386174 -angle 1.21189786618e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.841613826 -angle 6.43964036835e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006742785618673935 -strength_y -0.0002481982008316122
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006821435771517561 -strength_y -0.000220990342468163
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006905929263961753 -strength_y -4.966120643488368e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00696170936289117 -strength_y -1.789367447316472e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006830202636361518 -strength_y -7.228137201986723e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 0.809427841 -angle 6.19336807351e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.190572159 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006851926084322864 -strength_y -0.00025055211698099224
Drift -name "DMM" -length 0.434427841 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.565572159 -angle 4.32749699898e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007087814428984092 -strength_y 0.00010630779218393834
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007004445127655774 -strength_y 0.00013970573776648831
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006898124502062034 -strength_y 7.925882983892324e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006812988151755419 -strength_y 5.911152405915781e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00677060951179377 -strength_y 0.00010879499828776815
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006724671871031889 -strength_y 0.0001712097993523273
Sbend -name "B3A" -synrad $sbend_synrad -length 0.0854695079999 -angle 6.53973208348e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.914530492 -angle 6.99756502619e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006644278884035397 -strength_y 0.00023402228420139166
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00673295588187292 -strength_y 0.00024227047178962803
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006623651592187904 -strength_y 0.0003334574606078093
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006637322055793322 -strength_y 0.00029211893140226706
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006604530715401105 -strength_y 0.00018156108420572543
Sbend -name "B3B" -synrad $sbend_synrad -length 0.736511175 -angle 5.63544341567e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.263488825 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006961239450523008 -strength_y -0.00020611167042497113
Drift -name "DMM2" -length 0.0490111749999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.638488825 -angle 4.88542165681e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006928308716045161 -strength_y -0.000258798091191327
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069390545694865465 -strength_y -0.0002686160964856726
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006962993018299665 -strength_y -0.00034511732130802916
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006929095446310619 -strength_y -0.0004652481269548926
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006871990813275962 -strength_y -0.0005592633973915496
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006849099854710719 -strength_y -0.0006061954230198105
Sbend -name "B3A" -synrad $sbend_synrad -length 0.0125528419999 -angle 9.60485505145e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.987447158 -angle 7.55548968402e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006837607199943557 -strength_y -0.0006179965945421674
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070084382477459675 -strength_y -0.000531664630839109
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007073322399851743 -strength_y -0.0004680376348381574
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007117615375863809 -strength_y -0.00023034340598079362
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007030482619350555 -strength_y -3.2310655074750745e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 0.663594509 -angle 5.07751875784e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.336405491 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006800853362361074 -strength_y -9.107318134356136e-05
Drift -name "DMM" -length 0.288594509 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.711405491 -angle 5.44334631464e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007143405084152652 -strength_y 3.514504914477293e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007094041589271348 -strength_y 9.3959627461935e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007076802163162575 -strength_y 0.00014627043105039885
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007097841267087337 -strength_y 0.0001140050226435347
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007075320243503996 -strength_y 1.272920305925637e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 0.939636176 -angle 7.18966212722e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.060363824 -angle 4.61876107319e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007059924821885547 -strength_y -6.793356562007961e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007206923319122365 -strength_y -0.00010563175489892742
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007232546314407073 -strength_y -0.0002191351017018808
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007188062251182042 -strength_y -0.0003194535128052991
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007089617836618907 -strength_y -0.0003415382785097527
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007092522118927457 -strength_y -0.0002899146553936322
Sbend -name "B3B" -synrad $sbend_synrad -length 0.590677843 -angle 4.51959410001e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.409322157 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007056891746633919 -strength_y -0.00027284568840261017
Drift -name "DMM" -length 0.215677843 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.784322157 -angle 6.00127097248e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007114546641605444 -strength_y -0.00016723680802463805
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007061746297093052 -strength_y -0.0001272562458749758
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006942009633386881 -strength_y -0.0001395350192365053
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006851718389000894 -strength_y -0.0001601964561500987
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006783473608481281 -strength_y -0.0001398460230069213
Sbend -name "B3A" -synrad $sbend_synrad -length 0.86671951 -angle 6.63173746938e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.13328049 -angle 1.01980076515e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00670386553236091 -strength_y -0.00013038215203513571
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00663720341244336 -strength_y -0.0003367642622815667
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663324134372665 -strength_y -0.00045817692464550065
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006956040599159089 -strength_y -0.000423312730637314
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007102682960565844 -strength_y -0.0005140094334217002
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007166639964137301 -strength_y -0.0005407023391440628
Sbend -name "B3B" -synrad $sbend_synrad -length 0.517761177 -angle 3.96166944217e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.482238823 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007176177719983832 -strength_y -0.0007788136554262221
Drift -name "DMM" -length 0.142761177 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.857238823 -angle 6.55919563031e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007186949242132681 -strength_y -0.000726764324271468
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007159340394506781 -strength_y -0.0007204668898237752
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006992482896120393 -strength_y -0.0006467384483677876
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006896632681000841 -strength_y -0.0005548402667291933
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006938310667539847 -strength_y -0.00049463191452126
Sbend -name "B3A" -synrad $sbend_synrad -length 0.793802844 -angle 6.07381281155e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.206197156 -angle 1.57772542299e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069405558513279935 -strength_y -0.00042411282653924393
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006852300202648083 -strength_y -0.0003074802400946542
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068610798954572075 -strength_y -0.0003637108977287849
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006851091977626959 -strength_y -0.0003498138356586006
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006837870652499543 -strength_y -0.0002203678915533531
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006723841749723043 -strength_y -0.00023720007093826892
Sbend -name "B3B" -synrad $sbend_synrad -length 0.444844511 -angle 3.40374478434e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.555155489 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0065681099250284 -strength_y -0.0006043446055225112
Drift -name "DMM" -length 0.069844511 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.930155489 -angle 7.11712028815e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006671933139898788 -strength_y -0.0008250503630644383
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006763267107293181 -strength_y -0.0007636925157501453
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006784356854390275 -strength_y -0.0006975205372554045
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00675071956386642 -strength_y -0.0007242953347412275
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006772888928939189 -strength_y -0.0008088179036535359
Sbend -name "B3A" -synrad $sbend_synrad -length 0.720886178 -angle 5.51588815372e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.279113822 -angle 2.13565008082e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006810876868400217 -strength_y -0.0008496094257896997
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006642544497949156 -strength_y -0.0007919632119946637
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066446286494969515 -strength_y -0.00089551949668305
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00666317950712551 -strength_y -0.0008574644494459628
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006690108496220409 -strength_y -0.0005684540758253835
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0065800191935467415 -strength_y -0.0005141224147217156
Sbend -name "B3B" -synrad $sbend_synrad -length 0.371927845 -angle 2.84582012651e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.00307215499993 -angle 2.35067114444e-08 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006738536499230194 -strength_y -0.00023186685882790382
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00674678892897198 -strength_y -0.00025227859547493925
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006789987137895668 -strength_y -0.00025762592240190376
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006834675309198236 -strength_y -0.00019131544778069987
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006877055727462683 -strength_y -8.80590508083213e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006936894220493725 -strength_y -2.789564777092184e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 0.647969512 -angle 4.95796349588e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.352030488 -angle 2.69357473865e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006964406483463034 -strength_y -1.770274939560682e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006833911388955187 -strength_y -6.692061591817537e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006834779521334834 -strength_y -0.0002125056393908864
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006857743818009283 -strength_y -0.0002548020662378037
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070421892016273935 -strength_y -0.00027886658576557484
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007113101563909737 -strength_y 3.0038623256109075e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 0.299011179 -angle 2.28789546867e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.0759888209999 -strength [expr -0.0019969997893*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006664262892756318 -strength_y 0.00021206450641724964
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.549011179 -strength [expr -0.0144281118507*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.450988821 -strength [expr -0.0118520667733*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006688166261220105 -strength_y 0.00023927324675737243
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.174011179 -strength [expr -0.0045730448667*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.200988821 -strength [expr -0.0052820221173*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00669268402029015 -strength_y 0.00027836487920649295
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.424011179 -strength [expr -0.0111430895227*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.575988821 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006609047920386952 -strength_y 0.0003508189231549773
Drift -name "DMM" -length 0.0490111790001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.950988821 -strength [expr -0.00170086862616*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006608226948488573 -strength_y 0.0001912738130097436
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.299011179 -strength [expr -0.000534789391843*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.700988821 -strength [expr -0.00125373702256*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006611398871343319 -strength_y 0.00017718232914673248
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.549011179 -strength [expr -0.000981920995443*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988821 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006898105341042474 -strength_y -3.475430242176159e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006905332256401051 -strength_y -0.0001253184169097165
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006917377255288722 -strength_y -0.000171932353883774
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00696416762962425 -strength_y -0.00020870987728111582
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006980836228595106 -strength_y -0.0002472271893407499
Drift -name "DUTIL" -length 0.549011179 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*463.8761985*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 0.450988821 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006865149653970115 -strength_y -0.0005704857461029308
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006848187742590327 -strength_y -0.0006092498957692178
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006838668445457467 -strength_y -0.0006252084486302416
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068405433131915735 -strength_y -0.0006146429846183946
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006907949379476774 -strength_y -0.0005727309580361426
Drift -name "DUTIL" -length 0.549011179 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.450988821 -strength [expr -0.000806605418957*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007087559470486771 -strength_y -3.974383170001032e-05
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.799011179 -strength [expr -0.00142905259904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.200988821 -strength [expr -0.000359473815357*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006773458962242332 -strength_y -0.000127572969173684
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.0 -strength [expr -0.0017885264144*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006808527408076459 -strength_y -0.0001626109850667948
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.0490111790001 -strength [expr -8.76577882426e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.325988821 -strength [expr -0.0085670444453*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006802276186521793 -strength_y -5.375682911015901e-05
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.299011179 -strength [expr -0.0078580671947*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.0759888209999 -strength [expr -0.0019969997893*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007049084052079978 -strength_y 2.1305733330374398e-05
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.549011179 -strength [expr -0.0144281118507*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.450988821 -strength [expr -0.0118520667733*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00712113174920687 -strength_y 6.238918615809593e-05
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.174011179 -strength [expr -0.0045730448667*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.200988821 -angle -3.41394910375e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007082232499663442 -strength_y 0.00014771936246017698
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070972686394134695 -strength_y 9.262823288852222e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007066248308651834 -strength_y -1.1839916146954999e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007068931605351984 -strength_y -7.748927561401425e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007154248496172601 -strength_y -9.391599971537576e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007228761217280943 -strength_y -0.0001214487004023098
Sbend -name "B4A" -synrad $sbend_synrad -length 0.450052846 -angle -7.64449237822e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.549947154 -angle -9.34127372939e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00723260993180282 -strength_y -0.00021867594223335822
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007188432992857553 -strength_y -0.0003192336988226544
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007089625570841755 -strength_y -0.00034165261787211053
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007092872267831771 -strength_y -0.0002899496838846886
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006949775686387601 -strength_y -0.0003529453678549259
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070792987772008206 -strength_y -0.00033867402558281155
Sbend -name "B4B" -synrad $sbend_synrad -length 0.101094513 -angle -1.71716775258e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.273905487 -angle -4.65249453777e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006915609697758542 -strength_y -0.0001467399162557849
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006835018306962804 -strength_y -0.00015899847012338874
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006765498907717626 -strength_y -0.00013231254722308847
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006694378538310795 -strength_y -0.00013682578257685467
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006652726171941586 -strength_y -0.00023741969522373757
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006630884030577873 -strength_y -0.00037925543626314936
Sbend -name "B4A" -synrad $sbend_synrad -length 0.37713618 -angle -6.4059469442e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.62286382 -angle -1.05798191634e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006662769414299176 -strength_y -0.0004582592095776073
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006955106824003614 -strength_y -0.00042323208585228297
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007102415519911484 -strength_y -0.0005136640070003721
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007166784486973098 -strength_y -0.0005408545180214693
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007021155581234206 -strength_y -0.0005557341954401166
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007172756890017775 -strength_y -0.0007704117401135156
Sbend -name "B4B" -synrad $sbend_synrad -length 0.0281778470002 -angle -4.7862231856e-08 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.346822153 -angle -5.89103997179e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006961346734874801 -strength_y -0.0006276206479787056
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006897753673209956 -strength_y -0.0005404945590188339
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006948117664441274 -strength_y -0.00048471056778672455
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006938525869396874 -strength_y -0.00042146166664281054
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006869096822388458 -strength_y -0.00034178373483914344
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006855530268737494 -strength_y -0.0003051499118991277
Sbend -name "B4A" -synrad $sbend_synrad -length 0.304219514 -angle -5.16740151018e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.695780486 -angle -1.18183645974e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006859937130014344 -strength_y -0.00036558042936737705
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006852518253810361 -strength_y -0.00034710635209610907
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006834656362751474 -strength_y -0.00021841375885461338
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006723125056354945 -strength_y -0.0002395445912938304
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006605534868079139 -strength_y -0.00030437959899864037
Sbend -name "B4B" -synrad $sbend_synrad -length 0.955261181 -angle -1.62258429921e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.0447388189998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00639554707097265 -strength_y -0.0002749251458663631
Drift -name "DMM" -length 0.580261181 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.419738819 -angle -7.12958540581e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006778348776193247 -strength_y -0.0006946102111610705
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006748738028389716 -strength_y -0.0007394620851460716
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067839484943030244 -strength_y -0.000823102596650036
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006811952336759918 -strength_y -0.0008502426592999766
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006731809502663786 -strength_y -0.0008132791690086728
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006628718101320439 -strength_y -0.0007934964511432518
Sbend -name "B4A" -synrad $sbend_synrad -length 0.231302848 -angle -3.92885607615e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.768697152 -angle -1.30569100315e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006646390806912621 -strength_y -0.0008986693946319423
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066630607607293 -strength_y -0.0008515087789457982
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006690308461140355 -strength_y -0.0005638367278620931
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006575218708523787 -strength_y -0.0005138772194626856
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006449099569805913 -strength_y -0.00037562319172541246
Sbend -name "B4B" -synrad $sbend_synrad -length 0.882344515 -angle -1.49872975581e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.117655485 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066907938794165305 -strength_y -0.0002310242742539658
Drift -name "DMM" -length 0.507344515 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.492655485 -angle -8.36813083984e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068441346897161305 -strength_y -0.0001660248800740652
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006890371374754459 -strength_y -6.744201135366055e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006950485943740126 -strength_y -2.2314563440586248e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069648482157765815 -strength_y -1.7672410095450636e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006898079056446865 -strength_y -2.7682860212060346e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00682684419163303 -strength_y -7.807247175578775e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 0.158386182 -angle -2.69031064213e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.841613818 -angle -1.42954554655e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068346756189102895 -strength_y -0.00021219779890109278
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006857466887165331 -strength_y -0.00025460962011372983
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007041500459621169 -strength_y -0.0002792917655257374
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00711331884880117 -strength_y 2.890974986337003e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006970800428554507 -strength_y 0.00012334502670597065
Sbend -name "B4B" -synrad $sbend_synrad -length 0.809427849 -angle -1.37487521241e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.190572151 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067379246498742015 -strength_y 0.00015843352564693825
Drift -name "DMM" -length 0.434427849 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.565572151 -angle -9.60667627386e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00672257479106762 -strength_y 0.00025584755655735724
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006646731903093163 -strength_y 0.0003134496479051144
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006606953400657565 -strength_y 0.0003523689194243982
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006633562936223245 -strength_y 0.00030674704207102403
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006629273723581235 -strength_y 0.00022259268290598098
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00660446459381688 -strength_y 0.00018168215263715483
Sbend -name "B4A" -synrad $sbend_synrad -length 0.0854695160003 -angle -1.45176520811e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.914530484 -angle -1.55340008995e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006793317648596637 -strength_y 9.595114754338021e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006906100313607791 -strength_y -0.0001161351587300987
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006958823951562293 -strength_y -0.00020406910828722554
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006949609830009256 -strength_y -0.0002617937867773382
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006948776772456149 -strength_y -0.0002827991473579006
Sbend -name "B4B" -synrad $sbend_synrad -length 0.736511183 -angle -1.25102066901e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.263488817 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068389249694052485 -strength_y -0.0006163976818993946
Drift -name "DMM2" -length 0.0490111830003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.638488817 -angle -1.08452217079e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007071981510828741 -strength_y -0.0005082486319267781
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007080187303818938 -strength_y -0.00047827848874158215
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007059397197013944 -strength_y -0.0003973571242581273
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007112782422881135 -strength_y -0.00024172718520282818
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007144532052815716 -strength_y -8.75769178072224e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007014778133040067 -strength_y -3.256912517902764e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 0.0125528500003 -angle -2.13219774089e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.98744715 -angle -1.67725463335e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006763024370832109 -strength_y -0.00015314122866239263
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006808489674785249 -strength_y -0.00013572175396593536
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068778038353994745 -strength_y 5.0657818659017e-06
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007138767810232658 -strength_y 2.524372052584032e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007077365953232807 -strength_y 0.00012279462757428715
Sbend -name "B4B" -synrad $sbend_synrad -length 0.663594517 -angle -1.12716612561e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.336405483 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007094137035997178 -strength_y -8.663531420005195e-05
Drift -name "DMM" -length 0.288594517 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.711405483 -angle -1.20837671419e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007228757822742613 -strength_y -0.00024769639651308224
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007209415556068127 -strength_y -0.00030246255887980596
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071476409934129815 -strength_y -0.0003368674328448576
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007092505090091826 -strength_y -0.0003452991399805661
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007102772826218536 -strength_y -0.0003193401892383278
Sbend -name "B4A" -synrad $sbend_synrad -length 0.939636184 -angle -1.59604404477e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.0603638159997 -angle -1.02532565993e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007097195842317978 -strength_y -0.000290588287339683
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00695062968700468 -strength_y -0.00034943623360586215
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007074980556508431 -strength_y -0.00034276647988144723
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007036835622490715 -strength_y -0.0002595651132458393
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007104857627625945 -strength_y -0.0001876352186849406
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007003770528209881 -strength_y -0.00012767364240571778
Sbend -name "B4B" -synrad $sbend_synrad -length 0.590677851 -angle -1.0033115822e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.409322149 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006647162628025768 -strength_y -0.00026970426821912037
Drift -name "DMM" -length 0.215677851 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.784322149 -angle -1.33223125759e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006699740289414832 -strength_y -0.0004510992201879262
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006868750232945177 -strength_y -0.00042365307274285635
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007020610643478128 -strength_y -0.00043677520375331856
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007087910198783045 -strength_y -0.0004932963253875191
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007135329603019986 -strength_y -0.0005432697900354813
Sbend -name "B4A" -synrad $sbend_synrad -length 0.866719518 -angle -1.47218950137e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.133280482 -angle -2.26387109395e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007168238207358765 -strength_y -0.0005427066989759872
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070246269321317615 -strength_y -0.0005499944253433905
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007157947437690394 -strength_y -0.0007587181861914633
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007147719703287247 -strength_y -0.0007587941189033151
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007176451690063793 -strength_y -0.0007237661101168047
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007058594448416809 -strength_y -0.0006803215672987978
Sbend -name "B4B" -synrad $sbend_synrad -length 0.517761185 -angle -8.79457038801e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.482238815 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006854544277698176 -strength_y -0.0003185578899256166
Drift -name "DMM" -length 0.142761185 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.857238815 -angle -1.45608580099e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006854575840858981 -strength_y -0.0003728893990038943
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00683812683703851 -strength_y -0.0003766600061271927
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006866544963676333 -strength_y -0.0003165991943869209
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006858745802335592 -strength_y -0.00023855158137857255
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00677892081689601 -strength_y -0.00020246982488754937
Sbend -name "B4A" -synrad $sbend_synrad -length 0.793802852 -angle -1.34833495796e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.206197148 -angle -3.50241652798e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00672254050819385 -strength_y -0.00024160828457633717
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0065999753199654 -strength_y -0.00030393252952191365
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0063949486900981645 -strength_y -0.0002732261840855371
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006550458639503797 -strength_y -0.0005480936589001913
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006652684550215625 -strength_y -0.000823337024351753
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006787142751289085 -strength_y -0.0007192152375066949
Sbend -name "B4B" -synrad $sbend_synrad -length 0.444844519 -angle -7.55602495399e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.555155481 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006630206335754435 -strength_y -0.0007931461777614098
Drift -name "DMM" -length 0.0698445190004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.930155481 -angle -1.5799403444e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006650493600220982 -strength_y -0.0009059152969149145
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066667604287291935 -strength_y -0.0009187120747700287
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663893216365727 -strength_y -0.000798197026274493
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006684038116203514 -strength_y -0.0006213975058733244
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006680225864865595 -strength_y -0.0005222909031277745
Sbend -name "B4A" -synrad $sbend_synrad -length 0.720886186 -angle -1.22448041456e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.279113814 -angle -4.740961962e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006571170580287961 -strength_y -0.0005135968107108069
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006449483396385509 -strength_y -0.0003710602934985038
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066027209057132105 -strength_y -0.0002296538424510258
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006740519336767429 -strength_y -0.0002326247976880804
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006743420041131968 -strength_y -0.00024893437781815065
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006821342832520721 -strength_y -0.0002211673410950855
Sbend -name "B4B" -synrad $sbend_synrad -length 0.371927853 -angle -6.31747951997e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.00307214699956 -angle -5.21827703827e-09 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006818176953684535 -strength_y -0.00015170433359879638
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00683714151065901 -strength_y -0.00021964686360065834
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006842132483100556 -strength_y -0.00024059143705673248
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068767300206166665 -strength_y -0.0002664959480241054
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006981249900379608 -strength_y -0.0002960404416115396
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007089282560270613 -strength_y -0.0002262444444477986
Sbend -name "B4A" -synrad $sbend_synrad -length 0.64796952 -angle -1.10062587116e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.35203048 -angle -5.97950739602e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007115792068545778 -strength_y 1.4708330874289885e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006978882186928351 -strength_y 0.000127860429292456
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006808013309111629 -strength_y 6.120140585595629e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006729504528014254 -strength_y 0.00016686134655237328
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006644201754652649 -strength_y 0.0002339411436512408
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006732907463892685 -strength_y 0.0002421691140461603
Sbend -name "B4B" -synrad $sbend_synrad -length 0.299011187 -angle -5.07893408595e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.0759888129996 -strength [expr 0.000638530569209*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006666792266211842 -strength_y 0.0001602151532348614
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.549011187 -strength [expr 0.00461331624879*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.450988813 -strength [expr 0.00378963866001*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00688983867615453 -strength_y -1.3944856059889877e-05
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.174011187 -strength [expr 0.00146220815799*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.200988813 -strength [expr 0.00168889993281*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006914147528574705 -strength_y -0.00016842854130350612
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.424011187 -strength [expr 0.00356294688519*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.575988813 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006982652171574866 -strength_y -0.0002406803210863417
Drift -name "DMM" -length 0.0490111870004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.950988813 -strength [expr 0.00170086861185*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006935133855904626 -strength_y -0.000264416291227386
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.299011187 -strength [expr 0.000534789406151*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.700988813 -strength [expr 0.00125373700825*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006956859520433624 -strength_y -0.0003018721119357918
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.549011187 -strength [expr 0.000981921009751*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988813 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068651765317996 -strength_y -0.000570440087556946
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006848194084437702 -strength_y -0.0006092287544605261
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068386777582611 -strength_y -0.0006252042498179071
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00684052115185465 -strength_y -0.0006146658786624519
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006907862330811644 -strength_y -0.0005727728073098243
Drift -name "DUTIL" -length 0.549011187 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DUTIL" -length 0.450988813 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007062519327584893 -strength_y -0.0005121632393689432
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070591814071988114 -strength_y -0.0004259897004523027
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00714733698436318 -strength_y -0.00013833922944137274
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00691483199286695 -strength_y -4.906901041212626e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067746644339719015 -strength_y -0.0001765086114025494
Drift -name "DUTIL" -length 0.549011187 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.450988813 -strength [expr 0.000806605404649*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007110272655562289 -strength_y 7.422360655652949e-05
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.799011187 -strength [expr 0.00142905261335*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.200988813 -strength [expr 0.000359473801049*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007097636523445403 -strength_y 0.00011656014251912004
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.0 -strength [expr 0.0017885264144*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007056557563477138 -strength_y -5.086590780067686e-05
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.0490111870004 -strength [expr 8.76578025512e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.325988813 -strength [expr 0.00273926929641*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007132356867771398 -strength_y -9.164691501870429e-05
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.299011187 -strength [expr 0.00251257752159*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.0759888129996 -strength [expr 0.000638530569209*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007239282459746733 -strength_y -0.000160582312293445
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.549011187 -strength [expr 0.00461331624879*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.450988813 -strength [expr 0.00378963866001*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0072222147575980045 -strength_y -0.00028002899695280084
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.174011187 -strength [expr 0.00146220815799*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.200988813 -angle -3.41394896786e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007129867474300972 -strength_y -0.0003419085573130129
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007089643426979075 -strength_y -0.0003418331351756085
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007108047068393556 -strength_y -0.00031045463196546807
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007082144601067733 -strength_y -0.00028966515542401436
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006984602581652935 -strength_y -0.0003190467731846968
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006955841397680987 -strength_y -0.00036505257513758934
Sbend -name "B4A" -synrad $sbend_synrad -length 0.450052854 -angle -7.64449251411e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.549947146 -angle -9.3412735935e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007074573473287051 -strength_y -0.0003431158369754288
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007037040905145988 -strength_y -0.000259734089032657
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007104579822872968 -strength_y -0.0001879967560289717
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007004488583907566 -strength_y -0.00012759298703269624
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006830072080331279 -strength_y -0.00015803098047880397
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067000287799214315 -strength_y -0.00013262876545397137
Sbend -name "B4B" -synrad $sbend_synrad -length 0.101094521 -angle -1.71716788847e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.273905479 -angle -4.65249440188e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070436926501755235 -strength_y -0.00044796905875822175
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007098454238730706 -strength_y -0.0005083717540223475
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007147974718796037 -strength_y -0.000548559991253439
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007164628465616659 -strength_y -0.0005388803886296715
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070838973527633 -strength_y -0.0005228370019269438
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007016183161162381 -strength_y -0.0005777393230508455
Sbend -name "B4A" -synrad $sbend_synrad -length 0.377136188 -angle -6.40594708009e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.622863812 -angle -1.05798190275e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071573167482453575 -strength_y -0.0007582126465420452
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007148133608477448 -strength_y -0.0007591089712590968
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007176076986941751 -strength_y -0.0007236682720760992
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00705958865602403 -strength_y -0.0006807758307719423
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069055214820142015 -strength_y -0.0005255521342812839
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006932608511801398 -strength_y -0.0004141347574426475
Sbend -name "B4B" -synrad $sbend_synrad -length 0.0281778550002 -angle -4.78622454448e-08 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.346822145 -angle -5.89103983591e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006871327873567597 -strength_y -0.00030073515028364957
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00684670408611945 -strength_y -0.00022667089836592895
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006763374837456 -strength_y -0.0002034368042076708
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067220045566052985 -strength_y -0.0002436164175163849
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006688450068603633 -strength_y -0.0002964799011959119
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00655599542044389 -strength_y -0.00029840558003639686
Sbend -name "B4A" -synrad $sbend_synrad -length 0.304219522 -angle -5.16740164606e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.695780478 -angle -1.18183644615e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006395577327352225 -strength_y -0.00027499853331841193
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006553776047633043 -strength_y -0.0005578157424276809
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006656089796201584 -strength_y -0.0008242553624587367
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006787701462855197 -strength_y -0.0007164636621591136
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006749527323536393 -strength_y -0.0007531183790075514
Sbend -name "B4B" -synrad $sbend_synrad -length 0.955261189 -angle -1.6225843128e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.0447388109999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006811829333580641 -strength_y -0.0008501699279858745
Drift -name "DMM" -length 0.580261189 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.419738811 -angle -7.12958526993e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066660712332930775 -strength_y -0.0007636228234219321
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006687856773275991 -strength_y -0.0005930502575312565
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006670410753682476 -strength_y -0.0005162308923811279
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0065762287565184745 -strength_y -0.0005139364084146847
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006474426771892892 -strength_y -0.00046692891264222983
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006453791822401589 -strength_y -0.0003468305561860264
Sbend -name "B4A" -synrad $sbend_synrad -length 0.231302856 -angle -3.92885621204e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.768697144 -angle -1.30569098956e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006607794128819617 -strength_y -0.0002291797907740242
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006741087442446598 -strength_y -0.00023242975181794916
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006744418959848644 -strength_y -0.00025001639896000207
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006823092379438634 -strength_y -0.0002177588308016964
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006906478822492535 -strength_y -4.9133974987606586e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 0.882344523 -angle -1.4987297694e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.117655477 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006905363782633888 -strength_y -2.5804398741307536e-05
Drift -name "DMM" -length 0.507344523 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.492655477 -angle -8.36813070395e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00689650943518537 -strength_y -0.0002763906073989731
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070111815866854285 -strength_y -0.0002921150368653457
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007105331365312162 -strength_y -0.00018765173717314197
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007116252649394538 -strength_y 1.1727555622515873e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007059146891795973 -strength_y 0.00013843064928588378
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006962703894714097 -strength_y 0.00011858642144000051
Sbend -name "B4A" -synrad $sbend_synrad -length 0.15838619 -angle -2.69031077802e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.84161381 -angle -1.42954553296e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006808392140814552 -strength_y 6.101726512523344e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067298881401026515 -strength_y 0.00016650238629773554
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006644111242634449 -strength_y 0.00023384156647322614
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006732859046713149 -strength_y 0.00024207802061532936
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066244189619355815 -strength_y 0.00033274166010819386
Sbend -name "B4B" -synrad $sbend_synrad -length 0.809427857 -angle -1.374875226e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.190572143 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006605111523430098 -strength_y 0.00018673810486847828
Drift -name "DMM" -length 0.434427857 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.565572143 -angle -9.60667613798e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006905110241895042 -strength_y -0.00014813299216493328
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006939948197163633 -strength_y -0.00018981755933616926
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00698188400363262 -strength_y -0.00023345060408092657
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006957812298369384 -strength_y -0.0002609789233209511
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006925450827281713 -strength_y -0.0002574489580959261
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006948531771464124 -strength_y -0.0002823561152627621
Sbend -name "B4A" -synrad $sbend_synrad -length 0.0854695239999 -angle -1.45176534399e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.914530476 -angle -1.55340007636e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069233415327631465 -strength_y -0.0004762215905969378
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006849268116742049 -strength_y -0.0006056368038741281
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006837896593337753 -strength_y -0.0006176276874220468
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007014276725193263 -strength_y -0.0005295680206274325
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007070517096288547 -strength_y -0.00046317758941335097
Sbend -name "B4B" -synrad $sbend_synrad -length 0.736511191 -angle -1.2510206826e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.263488809 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006763061613690501 -strength_y -0.00015703976500561713
Drift -name "DMM2" -length 0.0490111909999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.638488809 -angle -1.0845221572e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006799844918485521 -strength_y -7.910358778016279e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006851083665949164 -strength_y -4.3002859507143006e-06
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007012796452972594 -strength_y 2.088835421740751e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071357196149310995 -strength_y 2.4228425235443604e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007121544822318747 -strength_y 6.194766745536568e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00707584733047716 -strength_y 0.00012728597391420553
Sbend -name "B4A" -synrad $sbend_synrad -length 0.0125528579999 -angle -2.13219909968e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.987447142 -angle -1.67725461976e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070964864531229375 -strength_y 8.638786814706828e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00706306069045273 -strength_y -7.241064610955093e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0072115063070200165 -strength_y -0.00010779485327551422
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007232183743030341 -strength_y -0.0002217694036592486
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007187809644893859 -strength_y -0.00031960227469355224
Sbend -name "B4B" -synrad $sbend_synrad -length 0.663594525 -angle -1.12716613919e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.336405475 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070483743233429996 -strength_y -0.0002947594567734393
Drift -name "DMM" -length 0.288594525 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.711405475 -angle -1.2083767006e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007089768176413638 -strength_y -0.0003201374471615562
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007056097173644477 -strength_y -0.0002723337042720398
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007034296802447838 -strength_y -0.00024453300580678866
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070906565429301495 -strength_y -0.00020260906471122826
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00710795930062527 -strength_y -0.00014591461019550586
Sbend -name "B4A" -synrad $sbend_synrad -length 0.939636192 -angle -1.59604405836e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.0603638080003 -angle -1.02532552406e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007014084518204467 -strength_y -0.00012666767868036553
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006835234943076648 -strength_y -0.00015903454915450538
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006705027718219526 -strength_y -0.0001297878189120405
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006637576238144136 -strength_y -0.0003342173768420281
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006661680597870997 -strength_y -0.0004584164302530468
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006953230490992174 -strength_y -0.0004230771684649694
Sbend -name "B4B" -synrad $sbend_synrad -length 0.590677859 -angle -1.00331159579e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.409322141 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070607703909704965 -strength_y -0.0005269896667533448
Drift -name "DMM" -length 0.215677859 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.784322141 -angle -1.332231244e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007188863980183737 -strength_y -0.0007828298249207285
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00718637097810163 -strength_y -0.0007854044805349303
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071203838704650075 -strength_y -0.0007332512673935327
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007151411767082845 -strength_y -0.0007185189455450531
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007196444548046409 -strength_y -0.0007311383168017026
Sbend -name "B4A" -synrad $sbend_synrad -length 0.866719526 -angle -1.47218951495e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.133280474 -angle -2.26387095808e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007073259025078433 -strength_y -0.0006868967554388879
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006902355019543354 -strength_y -0.0005302110574979582
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006941110310783344 -strength_y -0.00042485284964180974
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006852428410264891 -strength_y -0.0003070785342903754
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00685962523432328 -strength_y -0.00036606807474301624
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006854014125860824 -strength_y -0.00034423488039311747
Sbend -name "B4B" -synrad $sbend_synrad -length 0.517761193 -angle -8.79457052389e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.482238807 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006648364711832461 -strength_y -0.0003049472964675799
Drift -name "DMM" -length 0.142761193 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.857238807 -angle -1.45608578741e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006401701900938891 -strength_y -0.00028538510732242545
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006497107966195825 -strength_y -0.00042661263818304085
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006580460030918298 -strength_y -0.0006512026156707139
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006630022959555994 -strength_y -0.0008069522164589861
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006716339328929957 -strength_y -0.0008070364092154483
Sbend -name "B4A" -synrad $sbend_synrad -length 0.79380286 -angle -1.34833497155e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.20619714 -angle -3.5024163921e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00678803205311505 -strength_y -0.0007142620757702637
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006749999887770524 -strength_y -0.0007562038340017316
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006812680571920763 -strength_y -0.0008506765507350913
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066492352385641405 -strength_y -0.0007921772114019207
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006641341521851817 -strength_y -0.0008895808866707894
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00666349442022404 -strength_y -0.000867836398138011
Sbend -name "B4B" -synrad $sbend_synrad -length 0.444844527 -angle -7.55602508986e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.555155473 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006453084388184405 -strength_y -0.00034967081165181965
Drift -name "DMM" -length 0.0698445269995 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.930155473 -angle -1.57994033081e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066206238272286085 -strength_y -0.0002283758857485884
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006717153483386697 -strength_y -0.0002331911074627534
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006742475271171079 -strength_y -0.00023087978996253912
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067376938248497244 -strength_y -0.0002390982773357926
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006765153621908581 -strength_y -0.00026146756193306414
Sbend -name "B4A" -synrad $sbend_synrad -length 0.720886194 -angle -1.22448042815e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.279113806001 -angle -4.74096182613e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006824606305360839 -strength_y -0.00021467960819534175
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069087488000267785 -strength_y -4.702025533334939e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006961130630090349 -strength_y -1.793589343331413e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006830946514177799 -strength_y -7.113135681854233e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068352239903959845 -strength_y -0.00021382585061375967
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068575039201407345 -strength_y -0.0002546354585175808
Sbend -name "B4B" -synrad $sbend_synrad -length 0.371927860999 -angle -6.31747965584e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.00307213900055 -angle -5.21826345134e-09 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068738803663335206 -strength_y 6.716828855446521e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006799663632140607 -strength_y 6.647534397846288e-05
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006761986803693025 -strength_y 0.000124981330604055
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006708515686512715 -strength_y 0.00018391195156248816
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006649136358528485 -strength_y 0.00022262130741564816
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006662397909253251 -strength_y 0.000238868570307057
Sbend -name "B4A" -synrad $sbend_synrad -length 0.647969527999 -angle -1.10062588475e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.352030472001 -angle -5.97950726015e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006731893178139501 -strength_y 0.00024103741072968826
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006629011053176936 -strength_y 0.0003285509733783602
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006635515410303774 -strength_y 0.00030001847844520906
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006604051165012938 -strength_y 0.00018337033231200116
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006791550608842998 -strength_y 9.721910224910917e-05
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069061774254199374 -strength_y -0.0001151801606241194
Sbend -name "B4B" -synrad $sbend_synrad -length 0.299011194999 -angle -5.07893422182e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.0759888050006 -strength [expr -0.00199699936884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006961220587041594 -strength_y -0.0003696651681873251
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.549011194999 -strength [expr -0.0144281122712*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.450988805001 -strength [expr -0.0118520663528*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006871706527658847 -strength_y -0.0005597163129015119
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.174011194999 -strength [expr -0.00457304528716*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.200988805001 -strength [expr -0.00528202169684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006839799791200684 -strength_y -0.0006245816380714358
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.424011194999 -strength [expr -0.0111430899432*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.575988805001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006888466663474214 -strength_y -0.0005825465764527952
Drift -name "DMM" -length 0.0490111949994 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.950988805001 -strength [expr -0.00170086859754*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007071109356661825 -strength_y -0.0005086341664589971
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.299011194999 -strength [expr -0.000534789420458*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.700988805001 -strength [expr -0.00125373699394*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00706083445399923 -strength_y -0.00038661797116357676
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.549011194999 -strength [expr -0.000981921024058*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988805001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007134415528046702 -strength_y -6.887880408008565e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006993383759345381 -strength_y -3.406897208406333e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00681878756232478 -strength_y -8.908316138978699e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006763557874780379 -strength_y -0.00016053867310521894
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006798649251037635 -strength_y -0.0001776172185405578
Drift -name "DUTIL" -length 0.549011194999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC4" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-458.648884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 0.450988805001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007090183457328925 -strength_y 9.935653367797485e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00709692832827525 -strength_y 0.00012211724583103696
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007056576903526722 -strength_y -5.0434543582480825e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007181250372130419 -strength_y -9.799975784698315e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0072360890624782025 -strength_y -0.00019413557006117826
Drift -name "DUTIL" -length 0.549011194999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.450988805001 -strength [expr -0.000806605390342*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007204704291699209 -strength_y 0.00010470573260369626
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.799011194999 -strength [expr -0.00142905262766*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.200988805001 -strength [expr -0.000359473786742*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007099041154904409 -strength_y -0.00034647407624274637
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.0 -strength [expr -0.0017885264144*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007094253159315516 -strength_y -0.0003312010599586807
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.0490111949994 -strength [expr -8.76578168577e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.325988805001 -strength [expr -0.00856704402484*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070056372273127835 -strength_y -0.0003088702996741931
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.299011194999 -strength [expr -0.00785806761516*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.0759888050006 -strength [expr -0.00199699936884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007005164827788965 -strength_y -0.00037060603821037935
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.549011194999 -strength [expr -0.0144281122712*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.450988805001 -strength [expr -0.0118520663528*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007079915340594331 -strength_y -0.00029137389083718195
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.174011194999 -strength [expr -0.00457304528716*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.200988805001 -angle 1.53787352618e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007043150932158925 -strength_y -0.00023690200704439075
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00710420190654001 -strength_y -0.0001884816880048166
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007093898468451911 -strength_y -0.00013593973624527136
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069847097386976495 -strength_y -0.00013035203636507
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006878810565145801 -strength_y -0.00015655223552864627
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006808381960024298 -strength_y -0.00015093868161333648
Sbend -name "B3A" -synrad $sbend_synrad -length 0.450052861999 -angle 3.44359668115e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.549947138001 -angle 4.20794155339e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006705376869661362 -strength_y -0.00012961668847693406
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006637687389404876 -strength_y -0.0003334572291820147
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006661201350431809 -strength_y -0.0004584836368576793
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006952396380368374 -strength_y -0.0004230112829924006
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071016432886835176 -strength_y -0.000512657737725601
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071671808173101835 -strength_y -0.000541293842582326
Sbend -name "B3B" -synrad $sbend_synrad -length 0.101094528999 -angle 7.73528653941e-07 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.273905471001 -angle 2.09579818401e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007116996235463378 -strength_y -0.0007243298381661753
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00716953546857783 -strength_y -0.0007220519774551676
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007185999303469969 -strength_y -0.0007287548286991947
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00704612757959196 -strength_y -0.0006745151243332928
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006908495896580609 -strength_y -0.0005814021166006398
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069182771403797005 -strength_y -0.0005119434159479377
Sbend -name "B3A" -synrad $sbend_synrad -length 0.377136195999 -angle 2.88567202332e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.622863804001 -angle 4.76586621122e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006941431933558179 -strength_y -0.0004252855255380163
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006852391976283799 -strength_y -0.0003071805606170797
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006859784742914871 -strength_y -0.0003658198504194256
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006853814075866702 -strength_y -0.0003446212452914555
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006829100056177546 -strength_y -0.00021538456836192428
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006720635455167544 -strength_y -0.0002491438882005861
Sbend -name "B3B" -synrad $sbend_synrad -length 0.0281778629992 -angle 2.15603996106e-07 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.346822137001 -angle 2.65372284184e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006590175954852105 -strength_y -0.0006918299424031383
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00664340524547962 -strength_y -0.0008190893783487346
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006734987425755661 -strength_y -0.0007929658342258709
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067882358870263795 -strength_y -0.0007122060446299056
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006760457010696309 -strength_y -0.000704123016822812
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006756893087242442 -strength_y -0.0007798408337111706
Sbend -name "B3A" -synrad $sbend_synrad -length 0.304219529999 -angle 2.32774736548e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.695780470001 -angle 5.32379086905e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006811796461144281 -strength_y -0.0008501504534986692
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00664555849386406 -strength_y -0.0007920034362583191
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006643112264581846 -strength_y -0.0008927905363231689
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663312666273111 -strength_y -0.0008624180329935192
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006689847912963783 -strength_y -0.00057266126658383
Sbend -name "B3B" -synrad $sbend_synrad -length 0.955261196999 -angle 7.30921757281e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.0447388030009 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006575552336531042 -strength_y -0.0005138972873428223
Drift -name "DMM" -length 0.580261196999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.419738803001 -angle 3.21164749968e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006741405903036029 -strength_y -0.00023049058350020776
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006739671267191349 -strength_y -0.00024375500942672718
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006774467125643662 -strength_y -0.00026212963859428276
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006822775555613646 -strength_y -0.00021838798671534271
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006863731272456244 -strength_y -0.000115117946673701
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006920815578042624 -strength_y -3.735585852332304e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 0.231302863999 -angle 1.76982270765e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.768697136001 -angle 5.88171552689e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006959350698394732 -strength_y -1.8068625711239634e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006828982531828013 -strength_y -7.426326417167025e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006835892294370612 -strength_y -0.00021582537107511657
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006859487603520778 -strength_y -0.0002559954090774568
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00704276456507869 -strength_y -0.0002785060967869198
Sbend -name "B3B" -synrad $sbend_synrad -length 0.882344530999 -angle 6.75129291497e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.117655469001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070656395491662215 -strength_y 0.0001339563606290366
Drift -name "DMM" -length 0.507344530999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.492655469001 -angle 3.76957215751e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066916666421782105 -strength_y 0.00019522104665372267
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066436880521651 -strength_y 0.000228820997532443
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006677439144454218 -strength_y 0.0002393824633102604
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006731623657658703 -strength_y 0.000240856230054594
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00669668259025186 -strength_y 0.0002754360204698864
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006620353297956896 -strength_y 0.00033659394665794067
Sbend -name "B3A" -synrad $sbend_synrad -length 0.158386197999 -angle 1.21189804981e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.841613802001 -angle 6.43964018472e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006635381194341519 -strength_y 0.0003005270904451139
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006604052829109938 -strength_y 0.0001834914261698147
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006790697051833805 -strength_y 9.782649957359173e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006906212842921816 -strength_y -0.00011473550270846689
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006957899691614056 -strength_y -0.0002033078880214068
Sbend -name "B3B" -synrad $sbend_synrad -length 0.809427864999 -angle 6.19336825714e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.190572135001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006940289806896745 -strength_y -0.00027009587439332764
Drift -name "DMM" -length 0.434427864999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.565572135001 -angle 4.32749681535e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006844980019278555 -strength_y -0.0006181357977776514
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006834829687800671 -strength_y -0.0006240295727446088
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006871937466921463 -strength_y -0.0005917857570184732
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006991895467888309 -strength_y -0.0005377124371286878
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007083696105980875 -strength_y -0.0005018069171268249
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070707489335526275 -strength_y -0.0004636021254568255
Sbend -name "B3A" -synrad $sbend_synrad -length 0.085469531999 -angle 6.53973391978e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.914530468001 -angle 6.99756484256e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071198115319839345 -strength_y -0.00022508472950228008
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007018260528440359 -strength_y -3.24464055876599e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006762986345751264 -strength_y -0.00015410374920623557
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006807926492862275 -strength_y -0.000131958957958794
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00688979597318549 -strength_y 8.128344983745652e-06
Sbend -name "B3B" -synrad $sbend_synrad -length 0.736511198999 -angle 5.6354435993e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.263488801001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007095494010240724 -strength_y 8.02190264273341e-05
Drift -name "DMM2" -length 0.0490111989989 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.638488801001 -angle 4.88542147318e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007105314881182576 -strength_y -8.854974826542866e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007197258728897146 -strength_y -0.0001020530040143342
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007238579631872981 -strength_y -0.00014642666193333967
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007232831397202236 -strength_y -0.00021708448289427807
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007222346434365243 -strength_y -0.00027963687134365087
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007182922364333776 -strength_y -0.0003223378735321019
Sbend -name "B3A" -synrad $sbend_synrad -length 0.0125528659989 -angle 9.60487341433e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.987447134001 -angle 7.55548950039e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070896550415005845 -strength_y -0.0003405269048194022
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070914123910921344 -strength_y -0.0002898172959249821
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006949825471403252 -strength_y -0.0003525742571959256
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070771148617875205 -strength_y -0.0003408399188610107
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007036691181583301 -strength_y -0.00025944474366196426
Sbend -name "B3B" -synrad $sbend_synrad -length 0.663594532999 -angle 5.07751894147e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.336405467001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069401198214748775 -strength_y -0.00014002307605899062
Drift -name "DMM" -length 0.288594532999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.711405467001 -angle 5.44334613102e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006681241175855612 -strength_y -0.0001519128026254388
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066468423247143795 -strength_y -0.00027173185225222234
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00662700978127118 -strength_y -0.0004062239616351523
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006642412477757873 -strength_y -0.00045946380710070546
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006759990015962061 -strength_y -0.0004386535227553835
Sbend -name "B3A" -synrad $sbend_synrad -length 0.939636199999 -angle 7.18966231084e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.0603638000013 -angle 4.61875923691e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006940071562241273 -strength_y -0.0004222415354567077
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007098281572192154 -strength_y -0.0005081340293471539
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007168493673343009 -strength_y -0.0005431418694411727
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007025597814452847 -strength_y -0.0005486814215078129
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007155858135383443 -strength_y -0.0007570405853790012
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007149077257748009 -strength_y -0.0007598230278713686
Sbend -name "B3B" -synrad $sbend_synrad -length 0.590677866999 -angle 4.51959428363e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.409322133001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006898417309807634 -strength_y -0.0005627360390271782
Drift -name "DMM" -length 0.215677866999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.784322133001 -angle 6.00127078885e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069198665825532385 -strength_y -0.00039952647529599775
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006858214809270753 -strength_y -0.0003257282454912246
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006860361448111951 -strength_y -0.00030775537590516143
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006867287783190035 -strength_y -0.00034968225785539527
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068414672221978464 -strength_y -0.0003833420163824504
Sbend -name "B3A" -synrad $sbend_synrad -length 0.866719533999 -angle 6.63173765301e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.133280466001 -angle 1.01980058153e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006851163308419762 -strength_y -0.0003496788258116267
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006835245606259506 -strength_y -0.00021876012997725183
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006722718840180446 -strength_y -0.00024096421966764763
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006596660088428878 -strength_y -0.00030363457238631483
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006395788107875226 -strength_y -0.00027549027890674284
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006557035093495827 -strength_y -0.0005677226338572256
Sbend -name "B3B" -synrad $sbend_synrad -length 0.517761200999 -angle 3.9616696258e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.482238799001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006749869351124691 -strength_y -0.0007282548817129295
Drift -name "DMM" -length 0.142761200999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.857238799001 -angle 6.55919544669e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006805336587655485 -strength_y -0.0008464642800966726
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00670921127488759 -strength_y -0.0008054118734757324
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006618994803269981 -strength_y -0.0007980080640417956
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006627597057178432 -strength_y -0.0008633857945066336
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663547116991576 -strength_y -0.000926988850121532
Sbend -name "B3A" -synrad $sbend_synrad -length 0.793802867999 -angle 6.07381299517e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.206197132001 -angle 1.57772523936e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663181560863603 -strength_y -0.0008575504925906577
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006690102387519082 -strength_y -0.0005685671843197933
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006580179098481795 -strength_y -0.0005141287662077874
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0064488106702471895 -strength_y -0.00038123746864938783
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006593029541082696 -strength_y -0.00023083080514046521
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006739144948255872 -strength_y -0.0002329684658941781
Sbend -name "B3B" -synrad $sbend_synrad -length 0.444844534999 -angle 3.40374496797e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.555155465001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006919388426536156 -strength_y -3.8371963321918175e-05
Drift -name "DMM" -length 0.0698445349985 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.930155465001 -angle 7.11712010452e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006954188040281085 -strength_y -1.8480922245006467e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006874384112205265 -strength_y -3.608610983206739e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006818349575861176 -strength_y -0.00010211368720002124
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006827342869490071 -strength_y -0.0001899115220480915
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006840658602138899 -strength_y -0.00023340156809342983
Sbend -name "B3A" -synrad $sbend_synrad -length 0.720886201999 -angle 5.51588833734e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.279113798002 -angle 2.13564989719e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006861244408059574 -strength_y -0.0002571675960949339
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007046679120868896 -strength_y -0.00027592019878993213
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007112801642284616 -strength_y 3.15703218880448e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006972687320053266 -strength_y 0.00012442276176107242
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006806542177210352 -strength_y 6.19583986729283e-05
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006729862451652319 -strength_y 0.0001665263540340284
Sbend -name "B3B" -synrad $sbend_synrad -length 0.371927868998 -angle 2.84582031013e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.00307213100155 -angle 2.35065278198e-08 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006610280749943739 -strength_y 0.00034955607572681994
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006638318460556409 -strength_y 0.0002863144117946115
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006620740821246303 -strength_y 0.000207717029864538
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006610254074827948 -strength_y 0.0001776395748813309
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006713026669652479 -strength_y 0.0001420032140974623
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006856128856715202 -strength_y 3.8345700979232945e-05
Sbend -name "B3A" -synrad $sbend_synrad -length 0.647969535998 -angle 4.95796367951e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.352030464002 -angle 2.69357455503e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069066782301231006 -strength_y -0.00010830067602303583
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006953710781011845 -strength_y -0.00019997367962201404
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069540776163580205 -strength_y -0.00026148999602895387
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006945431370633395 -strength_y -0.0002771913121100091
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006924162344059191 -strength_y -0.0004746958952449652
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00684939135818075 -strength_y -0.0006052296180808657
Sbend -name "B3B" -synrad $sbend_synrad -length 0.299011202998 -angle 2.2878956523e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DSXL" -length 0.0759887970016 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007062694095355768 -strength_y -0.0003766305809096907
Drift -name "DSXL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071353924843634235 -strength_y -0.00018443620879345633
Drift -name "DSXL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007099749758675744 -strength_y -4.396230011033201e-05
Drift -name "DSXL" -length 0.424011202998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM" -length 0.575988797002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006802085332960663 -strength_y -0.00017493601591354616
Drift -name "DMM" -length 0.0490112029984 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.950988797002 -strength [expr 0.00170086858324*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006801107196170793 -strength_y -9.30587573370372e-05
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.299011202998 -strength [expr 0.000534789434764*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.700988797002 -strength [expr 0.00125373697964*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007027481712777743 -strength_y 2.1153839159701405e-05
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.549011202998 -strength [expr 0.000981921038364*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988797002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007111241140932838 -strength_y 7.31409160966848e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007074569656942496 -strength_y 0.00013278806260187537
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070895579758473515 -strength_y 0.00014201937803830626
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007094351495394943 -strength_y 7.427368053549982e-05
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007063281300122025 -strength_y -2.047906692474602e-05
Drift -name "DUTIL" -length 0.549011202998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D1BC" -length 0.1 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.350988797002 -strength [expr 0.0362585369069*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007206335536732211 -strength_y -0.0003057156655559153
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.149011202998 -strength [expr 0.0153934491631*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.225988797002 -strength [expr 0.0233455403894*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071006194069806045 -strength_y -0.0003464962675911605
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.274011202998 -strength [expr 0.0283064456806*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BC" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.100988797002 -strength [expr -0.00849869234111*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007108260917679512 -strength_y -0.000296110489058166
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.399011202998 -strength [expr -0.0335787092789*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.100988797002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006962188740707058 -strength_y -0.00033435657650951277
Drift -name "DMM0" -length 0.0240112029984 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.475988797002 -strength [expr -0.0400567435561*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00706003721158923 -strength_y -0.00035306209592235086
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.0240112029984 -strength [expr -0.00202065806389*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.350988797002 -strength [expr -0.0295373931511*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00707620307490841 -strength_y -0.00028753739347748295
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.149011202998 -strength [expr -0.0125400084689*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.225988797002 -strength [expr -0.0190180427461*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007086612340578889 -strength_y -0.00020609007470074792
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.274011202998 -strength [expr -0.0230593588739*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BC" -length 0.725988797002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007114869973412623 -strength_y -0.00016239858984121733
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007058100100577765 -strength_y -0.0001268275215961072
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006943820863567132 -strength_y -0.00013907235327127536
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006856419508470843 -strength_y -0.00015998916632183866
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006791749768159058 -strength_y -0.0001436104446885296
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006721472414310753 -strength_y -0.00012491014410453993
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006667603554364199 -strength_y -0.0001798479617125344
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006641219964770559 -strength_y -0.0003092048464000569
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006624943616981559 -strength_y -0.0004251618473642364
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066528751411567225 -strength_y -0.0004594079609853488
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006777686265773423 -strength_y -0.0004354226302127797
Drift -name "D3BC" -length 0.907526202998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.0924737970016 -strength [expr 0.00251406586419*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007146315680146108 -strength_y -0.0007178255048931406
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.407526202998 -strength [expr 0.0110793300258*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.0924737970016 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007126717468561937 -strength_y -0.0007087546181880561
Drift -name "DMM0" -length 0.0325262029984 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.467473797002 -strength [expr 0.0127091127817*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006896729281796179 -strength_y -0.000545201412287387
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.0325262029984 -strength [expr 0.000884283108311*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D4BC" -length 0.1925765 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SEXIT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -20*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.0588922970016 -strength [expr -0.00714559101428*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006952578536984061 -strength_y -0.0004785791925957684
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.441107702998 -strength [expr -0.0535210103757*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.558892297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006874991260134581 -strength_y -0.00034927896418091703
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006852699963392195 -strength_y -0.0003064971887072236
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006868480196851434 -strength_y -0.0003227715627925145
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006858828182661329 -strength_y -0.0003672737064483343
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068376838850094 -strength_y -0.00038228953088227343
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006854733869136543 -strength_y -0.0003428375628911932
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006873177474893914 -strength_y -0.00027195954648420544
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006827572925411415 -strength_y -0.00021462210906912486
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006753256668364381 -strength_y -0.00020587060576275648
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006721382572376201 -strength_y -0.00024606992555203727
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006692964037388942 -strength_y -0.0002942560909193536
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006583920194552902 -strength_y -0.00030229122238169205
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0064406664389769636 -strength_y -0.00027386843306373806
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006397661118854633 -strength_y -0.0002791034579848583
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006474241917438173 -strength_y -0.0003873084378585507
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006561592979390723 -strength_y -0.000582215233223338
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066079780322331975 -strength_y -0.0007604991784373181
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00645043782803083 -strength_y -0.00040908085980585567
Drift -name "D1BCOL" -length 0.0248677029984 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.475132297002 -strength [expr 0.0419020631952*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006516458999356471 -strength_y -0.0002582289078737255
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.0248677029983 -strength [expr 0.00219309036479*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.975132297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006630224209346118 -strength_y -0.00022810886673053194
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006718958014914779 -strength_y -0.00023330164453842678
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006742526879689628 -strength_y -0.0002309525139825768
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006737373568952381 -strength_y -0.00023761375550017955
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006759831580244721 -strength_y -0.0002599765157154161
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068062043380620815 -strength_y -0.00024415237219790675
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006846253405872199 -strength_y -0.00016018667350300956
Drift -name "D2BCOL" -length 0.935842702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.0641572970017 -strength [expr -0.00871159075131*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006847218633140245 -strength_y -0.00024662976464646106
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.435842702998 -strength [expr -0.0591808482887*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.0641572970017 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007015360039043256 -strength_y -0.00029094994892476476
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071201580305709465 -strength_y -2.1764927721880827e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007001819786392783 -strength_y 0.00013871291546350408
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006824402414464617 -strength_y 5.659709030587162e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006745651166431246 -strength_y 0.00014947045989767828
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00664465142056088 -strength_y 0.00022724002851898006
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006722904335691791 -strength_y 0.00023893854557372203
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006652761479375708 -strength_y 0.00030858781053590584
Drift -name "D2BCOL" -length 0.846817702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.153182297002 -strength [expr 0.0135091938179*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006654493685982517 -strength_y -0.00021850633308110524
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.346817702998 -strength [expr 0.0305859597421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.153182297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006613471452895421 -strength_y 0.00019785778651634542
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006615654960865134 -strength_y 0.00017570119828256772
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006717049469683534 -strength_y 0.00014016853758178137
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068493881815083636 -strength_y 4.612548958020802e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006905759577305507 -strength_y -6.837219594245222e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006904708703556688 -strength_y -0.00014378529156307547
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006930901102791927 -strength_y -0.000183248666146532
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069763295617816835 -strength_y -0.00022217525089610285
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006972513550900612 -strength_y -0.0002557969537919104
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006933437296973049 -strength_y -0.0002602925021563642
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006931409254384474 -strength_y -0.00026108176778999627
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006959910917339438 -strength_y -0.0003135052545721549
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006948522420418368 -strength_y -0.00042048727136546394
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006893859699342363 -strength_y -0.0005254256969597502
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068558096726365385 -strength_y -0.000588125778806982
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006845322779639603 -strength_y -0.0006174077883953283
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00683527381108778 -strength_y -0.0006251342767681842
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006856310678196199 -strength_y -0.0006017654570904717
Drift -name "D1BCOL" -length 0.430577702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.0694222970017 -strength [expr -0.00842322963956*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007097411920701351 -strength_y 0.00011867317756483544
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.430577702998 -strength [expr -0.0522433717504*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.137412297002 -strength [expr -0.0166726740966*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007056522084718279 -strength_y -5.210100422694247e-05
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.362587702998 -strength [expr -0.0439939272934*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.137412297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071294174685043845 -strength_y -9.135490643791588e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007212809607959103 -strength_y -0.00010848174041643787
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007239298768190751 -strength_y -0.00015886352655621242
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007231347314403982 -strength_y -0.00022796793798132673
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007220036721971695 -strength_y -0.0002857120091947625
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00717807534693441 -strength_y -0.00032481766207904527
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007112306005234731 -strength_y -0.0003454604934235237
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007090658776044436 -strength_y -0.000337263281278267
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007109804803943674 -strength_y -0.00030563345868621984
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007076577231097335 -strength_y -0.0002900002985094062
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069842618121868935 -strength_y -0.0003192356566749277
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006953472138357608 -strength_y -0.000363104700057235
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007027627117047122 -strength_y -0.00036603282087116636
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007089284084056037 -strength_y -0.00032218070146501485
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007061714478803968 -strength_y -0.000276045862631917
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007031558129640244 -strength_y -0.0002491714181108259
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007074972588453763 -strength_y -0.00021511827166189793
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007114841973021428 -strength_y -0.00016168765016740555
Drift -name "D1BCOL" -length 0.446347702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.0536522970017 -strength [expr 0.00473161255027*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007107794753634186 -strength_y -0.0007158505712804364
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.446347702998 -strength [expr 0.0393635410097*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.553652297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007179063767893131 -strength_y -0.000780705670398425
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007145732163908202 -strength_y -0.0007177567178452352
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071313520922338435 -strength_y -0.0007104988070464077
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006896705962972853 -strength_y -0.0005554001409019326
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006955822571409272 -strength_y -0.0004512784294229866
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006855111044283826 -strength_y -0.00031981509213134665
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006868197280523705 -strength_y -0.00034636610938025515
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006841569672872659 -strength_y -0.00036818549348503207
Drift -name "D2BCOL" -length 0.357322702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.142677297002 -strength [expr -0.0193734193782*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006863393109317174 -strength_y -0.0002449509236908465
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.357322702998 -strength [expr -0.0485190196618*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.642677297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006759816133093849 -strength_y -0.0002040974167331333
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006722930335951582 -strength_y -0.0002402167464797794
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006699397557564319 -strength_y -0.00029013599484222977
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006600965943863914 -strength_y -0.00030401685289159204
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006454996563964863 -strength_y -0.00027730393219253915
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006395087557366975 -strength_y -0.00027368079934894125
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006461285992913752 -strength_y -0.000367149801505805
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006552758885430669 -strength_y -0.0005547965966567204
Drift -name "D2BCOL" -length 0.268297702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.231702297002 -strength [expr 0.020433896733*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006734171980860612 -strength_y -0.0008141644203172961
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.268297702998 -strength [expr 0.023661256827*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.731702297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006611769059777601 -strength_y -0.0008165579030761513
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006639822108682918 -strength_y -0.0008868046337878222
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006665870949171209 -strength_y -0.0009287230242705478
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663601308368876 -strength_y -0.0008705892669361787
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006670013428147597 -strength_y -0.0007229820463161028
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006689384862132213 -strength_y -0.0005785002110332904
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066701787685989255 -strength_y -0.0005161445822542889
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006588265808103891 -strength_y -0.0005143444742361989
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006491576187778553 -strength_y -0.0004846931922996592
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0064487402419907145 -strength_y -0.0003886238288058146
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006488555447126138 -strength_y -0.0002820495941948049
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006587676761242946 -strength_y -0.0002316449514479119
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006688383987708442 -strength_y -0.00023081525004091177
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006738526283749024 -strength_y -0.0002330846800156105
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067396119212831005 -strength_y -0.0002309709132927858
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00674141648875714 -strength_y -0.0002464483277698466
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006774225575055029 -strength_y -0.0002621432118534628
Drift -name "D1BCOL" -length 0.852057702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.147942297002 -strength [expr -0.0179503127218*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006838669827688328 -strength_y 5.679348074249387e-05
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.352057702998 -strength [expr -0.0427162886682*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.147942297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00676145848448964 -strength_y 0.00012591042014362824
Drift -name "D3BCOL" -length 0.0680627029983 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.215932297002 -strength [expr -0.0261997571789*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006649337162519292 -strength_y 0.00022245121669393214
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.284067702998 -strength [expr -0.0344668442111*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.715932297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067240940948012475 -strength_y 0.0002390343350621057
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006649045410761248 -strength_y 0.00031157066654098403
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006627147448305174 -strength_y 0.0003231219502993333
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066085543133374445 -strength_y 0.0001916949404261123
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067403160714675165 -strength_y 0.00012868289674860232
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006907034974059931 -strength_y -8.330389436665666e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006937859633480104 -strength_y -0.00018831729487413796
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006967824927692662 -strength_y -0.00025816380141364904
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069345106252625705 -strength_y -0.0002638144539535032
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006943783926728323 -strength_y -0.00043302656823133973
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006854088931395349 -strength_y -0.0005920405518322497
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006834922239484365 -strength_y -0.0006245140070614607
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006960824170980961 -strength_y -0.0005496697159135306
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007084512118090179 -strength_y -0.0004844544458351665
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007092319741811982 -strength_y -0.00028934525408099755
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070865532412420235 -strength_y -3.945330509962717e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006776011595059745 -strength_y -0.0001242382200996961
Drift -name "D1BCOL" -length 0.867827702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.132172297002 -strength [expr 0.0116563154653*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006850528655961307 -strength_y 0.000601611745341685
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.367827702998 -strength [expr 0.0324388380947*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.132172297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006824867746339222 -strength_y -1.9826458882880282e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006955376631735101 -strength_y 1.7915725075767047e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007104505510783853 -strength_y 2.1418285045866902e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007141168808293409 -strength_y 3.930365293806127e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007093280054327654 -strength_y 9.499404008545017e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007075610227241314 -strength_y 0.000144637834312113
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007096158245681196 -strength_y 0.00012612494945402032
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070851312738801 -strength_y 4.046638451054189e-05
Drift -name "D2BCOL" -length 0.778802702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.221197297002 -strength [expr -0.030035248005*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007100255755144629 -strength_y -0.0003464961888995196
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.278802702998 -strength [expr -0.037857191035*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.221197297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071058168371245616 -strength_y -0.000314621892669061
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007092247411473204 -strength_y -0.0002898883570717044
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007001843090383039 -strength_y -0.00031051592668169934
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006950114339067494 -strength_y -0.0003576524586231828
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007015989073601092 -strength_y -0.0003687978665698571
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070875828861957325 -strength_y -0.000326835790753668
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007064345360363516 -strength_y -0.00027788074668429815
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007031464883582166 -strength_y -0.0002495388442004197
Drift -name "D2BCOL" -length 0.689777702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.310222297002 -strength [expr 0.0273585996481*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006650013183844969 -strength_y -0.00025242296841722285
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.189777702998 -strength [expr 0.0167365539119*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.310222297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006640435279996253 -strength_y -0.00045922515201265345
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006901150877729491 -strength_y -0.0004219384044014018
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007087631304488414 -strength_y -0.000492893379161202
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007168061054760129 -strength_y -0.0005482330666332368
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007046008267275743 -strength_y -0.0005325925987230297
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007118108821915938 -strength_y -0.0007251907907589708
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007174036861978626 -strength_y -0.0007773957087516023
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071503016294895425 -strength_y -0.0007183539674578688
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007122459475404101 -strength_y -0.0007071315178112812
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006896366407639857 -strength_y -0.0005513156159104144
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006954705425850283 -strength_y -0.0004480235750454974
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006854196711410001 -strength_y -0.0003177394549656411
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006867629245338217 -strength_y -0.00034852229521813204
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00684241419548488 -strength_y -0.0003664368869302349
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006856718478322306 -strength_y -0.00023616793324317283
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006730253627737553 -strength_y -0.00022309192112803983
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006647103314913491 -strength_y -0.0003050254333578759
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006400475089517319 -strength_y -0.0002667661994772553
Drift -name "D1BCOL" -length 0.273537702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.226462297002 -strength [expr -0.0274773958041*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0065037728239154755 -strength_y -0.00043915840008570213
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.273537702998 -strength [expr -0.0331892055859*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.294452297002 -strength [expr -0.0357268402611*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006619110078996957 -strength_y -0.0007892302705158175
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.205547702998 -strength [expr -0.0249397611289*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.294452297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006767234094653935 -strength_y -0.0007584915717633199
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067855103558254765 -strength_y -0.0006989119465169199
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006754380663698392 -strength_y -0.0007138437079732511
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006759888655136975 -strength_y -0.0007866068640767245
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006805766483344524 -strength_y -0.0008448947902374455
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006797919276937806 -strength_y -0.0008424813065362658
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006706320979646314 -strength_y -0.0008044900536599385
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006622877664976699 -strength_y -0.0007955984537426558
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006620160151802571 -strength_y -0.0008471787344103628
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006656049530626369 -strength_y -0.0009154798413867716
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00666656279226194 -strength_y -0.0009158098362700501
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663492393718454 -strength_y -0.0008081508760701187
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006680038535304997 -strength_y -0.0006484448636628289
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066883672862064385 -strength_y -0.0005377452246795325
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006638147293637628 -strength_y -0.0005128777038959333
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0065402436081943745 -strength_y -0.0005086705638552523
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0064617743257926125 -strength_y -0.0004462904672542194
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006457412752219817 -strength_y -0.00033481908597581824
Drift -name "D1BCOL" -length 0.289307702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.210692297002 -strength [expr 0.0185810183804*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00700518386828729 -strength_y -0.0002934996954451494
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.289307702998 -strength [expr 0.0255141351796*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.710692297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007085610297836185 -strength_y -0.00023276702968394713
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00712185499180214 -strength_y -5.329140195006581e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007085120656939768 -strength_y 0.00011094127759430255
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070033707231960335 -strength_y 0.00013930686064626585
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00690027095272645 -strength_y 8.045370220544324e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068162147908431515 -strength_y 5.8105634135944764e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006773401497513452 -strength_y 0.00010336652578696202
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006731592538291995 -strength_y 0.00016488061005854197
Drift -name "D2BCOL" -length 0.200282702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.299717297002 -strength [expr -0.0406970766318*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006616427345734401 -strength_y 0.0002016805663092642
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.200282702998 -strength [expr -0.0271953624082*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.799717297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006746425542701118 -strength_y 0.00012540506358033992
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006907181194781382 -strength_y -8.768758810438246e-05
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006940503629955865 -strength_y -0.00019021630993940872
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006965602839566323 -strength_y -0.0002590077001028443
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00693628509822666 -strength_y -0.0002655739422309596
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069406012402207055 -strength_y -0.00044070557195504946
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006852987163703098 -strength_y -0.0005947529395121308
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006834820072018774 -strength_y -0.0006237512119479845
Drift -name "D2BCOL" -length 0.111257702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.388742297002 -strength [expr 0.0342833025631*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006921359779542258 -strength_y -0.0005664625077681754
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.111257702998 -strength [expr 0.00981185099688*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.888742297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007075857026546438 -strength_y -0.0005064399798045631
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007080054538537114 -strength_y -0.0004780915288334568
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007058821301788638 -strength_y -0.00040429172756935186
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007103841262006994 -strength_y -0.00026244060066215176
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007148904660101084 -strength_y -0.00011024721680964129
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007061327251179322 -strength_y -3.4352646389676585e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006877184266818026 -strength_y -6.135046239368706e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067678961034917445 -strength_y -0.00013653867561647432
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067831985538195335 -strength_y -0.00017997088044689296
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006809712834815421 -strength_y -0.00015162775303992457
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006799753660151741 -strength_y -7.397187916388414e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006850600136501943 -strength_y -4.513495865359471e-06
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007001216561853246 -strength_y 2.056114591659632e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007127877219589777 -strength_y 2.2769933964356013e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007131538486399812 -strength_y 5.1185025873525726e-05
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007082507717046123 -strength_y 0.00011175831816581028
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007080233993166221 -strength_y 0.00014789771796483932
Drift -name "D1BCOL" -length 0.695017702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.304982297002 -strength [expr -0.0370044788864*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007047839502470438 -strength_y -0.0002671293135750682
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.195017702998 -strength [expr -0.0236621225036*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.0889772970017 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007079047070904327 -strength_y -0.00021209257791151705
Drift -name "D3BCOL" -length 0.127027702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.372972297002 -strength [expr -0.0452539233434*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007033459345002977 -strength_y -0.00012574759264343682
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.127027702998 -strength [expr -0.0154126780466*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.872972297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00696460928778689 -strength_y -0.00013418845672152332
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006870983453694034 -strength_y -0.00015813150714510637
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006806326116504904 -strength_y -0.00015008476187813952
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006738139040613509 -strength_y -0.00012505369204508446
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066783075219294125 -strength_y -0.00015661727185028456
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006647022978964721 -strength_y -0.0002705861608992615
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00662809722836087 -strength_y -0.00039828481549384026
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006635112351469674 -strength_y -0.0004578395390971151
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006726630807572915 -strength_y -0.0004453532968583981
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068912431781595 -strength_y -0.000422294718417831
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070264858771802675 -strength_y -0.000439139384921642
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007087464772977766 -strength_y -0.0004926532421032117
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00713125124175915 -strength_y -0.0005409121622064304
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007168674636655805 -strength_y -0.0005474954668748904
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007130177990082155 -strength_y -0.0005254816456633512
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070377716367669586 -strength_y -0.0005373604787705092
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007030785309108949 -strength_y -0.0006249715734638569
Drift -name "D1BCOL" -length 0.710787702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.289212297002 -strength [expr 0.0255057212955*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006866799714343648 -strength_y -0.0002509086640771578
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.210787702998 -strength [expr 0.0185894322645*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.289212297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067384278306283395 -strength_y -0.00021387753122049875
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067173772728239475 -strength_y -0.00026194090258770694
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006670907966310128 -strength_y -0.00030198930442549334
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006540306465096572 -strength_y -0.00029575101929396326
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0064139660300012065 -strength_y -0.0002680486679090098
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006410690504399057 -strength_y -0.0002976527697738471
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006502022391878807 -strength_y -0.0004358090665087551
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0065770596746374714 -strength_y -0.0006376594525178974
Drift -name "D2BCOL" -length 0.621762702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "BTFD0" -length 0.02536345 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 0.352873847002 -strength [expr -0.0170789953902*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066853353987349085 -strength_y -0.0007984781683438392
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 1.0 -strength [expr -0.04839972*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006626990124813214 -strength_y -0.0008621458693390088
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 1.0 -strength [expr -0.04839972*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006665511931712983 -strength_y -0.0009023385455961924
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 0.147126152998 -strength [expr -0.00712086460979*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD1" -length 0.852873847002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006664059507230065 -strength_y 0.0009275693580692958
Drift -name "BTFD1" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663342934469054 -strength_y -0.0008127709385720654
Drift -name "BTFD1" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066896809339524735 -strength_y -0.0005749278832734796
Drift -name "BTFD1" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006624189912398654 -strength_y -0.0005133990563628282
Drift -name "BTFD1" -length 0.503472652998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 0.496527347002 -strength [expr 0.00604580833813*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006708810828361012 -strength_y -0.00023257874317487415
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 1.0 -strength [expr 0.012176184*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067420943767692535 -strength_y -0.00023191426904308079
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 1.0 -strength [expr 0.012176184*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006809153987977043 -strength_y -0.00024054404236300992
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 0.0034726529982 -strength [expr 4.22836618742e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD2" -length 0.996527347002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006846417838593093 -strength_y -0.00015973368836970063
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068884950803194895 -strength_y -7.000915403691464e-05
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006943817682987715 -strength_y -2.480661276800368e-05
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006969149267537226 -strength_y -1.740777124101931e-05
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006924361044043081 -strength_y -2.2031492017411215e-05
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006849033198272683 -strength_y -5.134843955511758e-05
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068161161883413725 -strength_y -0.00012157039562416112
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006829667740646402 -strength_y -0.00019724359061667636
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006840652360200702 -strength_y -0.0002333584119957256
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006744692057261222 -strength_y 0.00015065941311415729
Drift -name "BTFD2" -length 0.0010226529982 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 0.998977347002 -strength [expr 0.020171598337*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006644035521172639 -strength_y 0.0002281771456282667
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 1.0 -strength [expr 0.020192248*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067254120295688604 -strength_y 0.00023917156784088644
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 0.501022652998 -strength [expr 0.010116773663*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD3" -length 0.498977347002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006676514328596501 -strength_y -0.0002393778968741255
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006733057891786763 -strength_y 0.00024253654306698468
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006675621682750348 -strength_y 0.00029097710683571333
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006607816726938445 -strength_y 0.0003502749703138326
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006631430717820849 -strength_y 0.0003129154721093012
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006627334731819278 -strength_y 0.0002187188183743538
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006608546465159326 -strength_y 0.0001784069094758607
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067264125199850726 -strength_y 0.00013572967628288444
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006877165797687525 -strength_y 9.407977066355286e-06
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006906080218591188 -strength_y -0.0001163813458411957
Drift -name "BTFD3" -length 0.489312652998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 0.510687347002 -strength [expr -0.0136105736149*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006838907857580502 -strength_y -0.0006250947734715822
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 1.0 -strength [expr -0.02665148*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006840008493570198 -strength_y -0.0006152027575204892
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 0.989312652998 -strength [expr -0.0263666463851*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD4" -length 0.0106873470017 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007083289614802888 -strength_y -0.0004826569577059665
Drift -name "BTFD4" -length 0.0155886029983 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "LMD4" -length 0.984411397002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070585895019032365 -strength_y -0.00041678890681745687
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007095618704248325 -strength_y -0.0002815488257323316
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007148764975026401 -strength_y -0.00012582038328692286
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007079792219380942 -strength_y -3.769596793983269e-05
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006898695877471112 -strength_y -5.390103635098208e-05
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006773392436720546 -strength_y -0.00012766458321811363
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006778029822509562 -strength_y -0.000178306863699771
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006809230354887701 -strength_y -0.00015876849982055435
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006800166581461665 -strength_y -8.437134924150362e-05
Drift -name "LMD4" -length 0.772149212998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD11" -synrad $quad_synrad -length 0.227850787002 -strength [expr 0.0140156121315*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007167287444549339 -strength_y -9.560123019255006e-05
Quadrupole -name "QMD11" -synrad $quad_synrad -length 1.0 -strength [expr 0.0615122392857*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007230233866397482 -strength_y -0.0001234226369370666
Quadrupole -name "QMD11" -synrad $quad_synrad -length 1.0 -strength [expr 0.0615122392857*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007237561426820685 -strength_y -0.00018285610946598756
Quadrupole -name "QMD11" -synrad $quad_synrad -length 0.572149212998 -strength [expr 0.0351941792971*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD5" -length 0.427850787002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007093984397183033 -strength_y -0.00034581612311160873
Drift -name "LMD5" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007106666924297981 -strength_y -0.0002945072175077067
Drift -name "LMD5" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006959023806681086 -strength_y -0.00033733657108371394
Drift -name "LMD5" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007056982291590551 -strength_y -0.0003547105961049603
Drift -name "LMD5" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007047071080505132 -strength_y -0.0002666452807458454
Drift -name "LMD5" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007090920633948364 -strength_y -0.00020237335712721953
Drift -name "LMD5" -length 0.514963940998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD12" -synrad $quad_synrad -length 0.485036059002 -strength [expr -0.0226711883495*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007089035127276908 -strength_y 0.000323033085987653
Quadrupole -name "QMD12" -synrad $quad_synrad -length 1.0 -strength [expr -0.0467412431071*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007076461921564226 -strength_y -0.0002140262636265067
Quadrupole -name "QMD12" -synrad $quad_synrad -length 1.0 -strength [expr -0.0467412431071*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006939566164296865 -strength_y -0.000140167043245564
Quadrupole -name "QMD12" -synrad $quad_synrad -length 0.314963940998 -strength [expr -0.0147218061362*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD6" -length 0.685036059002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006658924428595028 -strength_y -0.00020874398950388628
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00663695900886916 -strength_y -0.00033843122855014467
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006624594307450766 -strength_y -0.0004370802518606481
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006662581296311044 -strength_y -0.00045828659231966274
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006790335003756017 -strength_y -0.0004332774174427499
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069516848503000305 -strength_y -0.00042295626570660656
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007054643486360682 -strength_y -0.00045556015005373396
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007100573460925911 -strength_y -0.0005112421586564505
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071457224505597815 -strength_y -0.0005478403758549949
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007168019153908888 -strength_y -0.0005423724842971462
Drift -name "LMD6" -length 0.394963940998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD13" -synrad $quad_synrad -length 0.605036059002 -strength [expr 0.0260476987442*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006920985389911517 -strength_y -0.0005956543847088884
Quadrupole -name "QMD13" -synrad $quad_synrad -length 1.0 -strength [expr 0.0430514815714*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006905156239310227 -strength_y -0.0005260427779604673
Quadrupole -name "QMD13" -synrad $quad_synrad -length 1.0 -strength [expr 0.0430514815714*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006952471138056265 -strength_y -0.0004787535837674195
Quadrupole -name "QMD13" -synrad $quad_synrad -length 0.194963940998 -strength [expr 0.00839348651297*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD7" -length 0.805036059002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006867215304674291 -strength_y -0.0003499177817632395
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00684302037562949 -strength_y -0.00036521272475114057
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00685539329854587 -strength_y -0.00023471253838949145
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067296053332416865 -strength_y -0.0002241222588527634
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006644641568872499 -strength_y -0.00030515735998916334
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006399685663796827 -strength_y -0.0002668362621917282
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006524729300826574 -strength_y -0.00048274782732863925
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066327525712250295 -strength_y -0.0008101471741051086
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006779473960575558 -strength_y -0.0007390518564188171
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006749713637815515 -strength_y -0.0007291411934002283
Drift -name "LMD7" -length 0.274963940998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD14" -synrad $quad_synrad -length 0.725036059002 -strength [expr -0.0204350624339*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006560229993669751 -strength_y 0.0005778002219184549
Quadrupole -name "QMD14" -synrad $quad_synrad -length 1.0 -strength [expr -0.0281848911929*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006749038622315079 -strength_y -0.0007343209956140095
Quadrupole -name "QMD14" -synrad $quad_synrad -length 1.0 -strength [expr -0.0281848911929*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066181559606942365 -strength_y -0.0008422106228489689
Quadrupole -name "QMD14" -synrad $quad_synrad -length 0.0749639409978 -strength [expr -0.00211285052041*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD8" -length 0.925036059002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006633546102559224 -strength_y 0.000792556122291586
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00662307083951059 -strength_y -0.0008538351052253069
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006664108986271499 -strength_y -0.00092762403065487
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663123656775754 -strength_y -0.000854937699135202
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006678904879019738 -strength_y -0.0006561126621376387
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006682758538716655 -strength_y -0.0005251808260667186
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006589252056072057 -strength_y -0.0005143557064956119
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006472091880016065 -strength_y -0.00046378133894110177
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006461421405121781 -strength_y -0.00032441835265428786
Drift -name "LMD8" -length 0.741403940998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF8" -synrad $quad_synrad -length 0.258596059002 -strength [expr -0.00110735413518*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006968009051702297 -strength_y -1.776442049574874e-05
Quadrupole -name "QF8" -synrad $quad_synrad -length 1.0 -strength [expr -0.00428217715093*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006943895946367862 -strength_y -1.9452106990817677e-05
Quadrupole -name "QF8" -synrad $quad_synrad -length 1.0 -strength [expr -0.00428217715093*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006867122702680688 -strength_y -3.9596482124324443e-05
Quadrupole -name "QF8" -synrad $quad_synrad -length 0.515083940998 -strength [expr -0.00220568068295*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D8" -length 0.484916059002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006841895095504594 -strength_y -0.00024008240536615234
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006868079044331574 -strength_y -0.00026148623538237484
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006954038852525663 -strength_y -0.0002940557181449614
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007061227045330505 -strength_y -0.00026402116150980073
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00711904239749766 -strength_y -0.00012082394857875109
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007106527890555418 -strength_y 5.8283576693114145e-05
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007045051539376252 -strength_y 0.0001441057345375015
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006954392222278083 -strength_y 0.00011352727091102716
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006860003300500755 -strength_y 6.179486148040943e-05
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067970222713718284 -strength_y 6.86933988871316e-05
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006763368842190634 -strength_y 0.00012250873417848733
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006717178541364374 -strength_y 0.00017740301245637905
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006658395331037924 -strength_y 0.0002158359955175747
Drift -name "D8" -length 0.316683940998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD7" -synrad $quad_synrad -length 0.683316059002 -strength [expr -0.00230057418892*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006941085727021983 -strength_y -0.0001906345722496509
Quadrupole -name "QD7" -synrad $quad_synrad -length 1.0 -strength [expr -0.00336677904553*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00696601084915151 -strength_y -0.0002588638153044761
Quadrupole -name "QD7" -synrad $quad_synrad -length 1.0 -strength [expr -0.00336677904553*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006935352265697557 -strength_y -0.0002646310560018422
Quadrupole -name "QD7" -synrad $quad_synrad -length 0.0903639409978 -strength [expr -0.000304235423023*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.482916059002 -angle 2.09302696368e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006962994664186669 -strength_y -0.0003440203710784892
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006926396478037854 -strength_y -0.00047047919807306467
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006867931698853082 -strength_y -0.0005658268242744399
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.461451940998 -angle 1.99999841991e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.538548059002 -angle 2.3341439733e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006844086836825278 -strength_y -0.000619801763797645
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006866434194698719 -strength_y -0.0005951242207398446
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007072773577165349 -strength_y -0.000507892604279359
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.405819940998 -angle 1.75888141029e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.594180059002 -angle 2.57526098292e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070585538676187715 -strength_y -0.0004113733489234021
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007148921797464107 -strength_y -0.0001233058881525453
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069014072685705434 -strength_y -5.304545843166186e-05
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.350187940998 -angle 1.51776440067e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.649812059002 -angle 2.81637799254e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006777734974931437 -strength_y -0.00017817396308279112
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006800002853967135 -strength_y -8.211490349342865e-05
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006990392948800761 -strength_y 2.014634374070192e-05
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.294555940998 -angle 1.27664739105e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.705444059003 -angle 3.05749500216e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007134969846992813 -strength_y 4.730965014051764e-05
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007077904918222224 -strength_y 0.00014715003092811207
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007081516998213514 -strength_y 2.98108292753659e-05
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.238923940997 -angle 1.03553038143e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.761076059003 -angle 3.29861201178e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070983849893370395 -strength_y -8.743924486294017e-05
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007236323108565235 -strength_y -0.00013593005718938534
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007226052890376064 -strength_y -0.0002649628262413804
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.183291940997 -angle 7.9441337181e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.816708059003 -angle 3.5397290214e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071388804644414066 -strength_y -0.00033951488680765315
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007101806114926489 -strength_y -0.00032072798504002934
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007028653948909881 -strength_y -0.00030031244438392214
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.127659940997 -angle 5.53296362191e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.872340059003 -angle 3.78084603102e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006978146959951719 -strength_y -0.000371523843367696
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070879822067685255 -strength_y -0.00030373493666250385
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007036534166342231 -strength_y -0.00024223375119600935
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.0720279409973 -angle 3.12179352572e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.927972059003 -angle 4.02196304063e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007111038372775792 -strength_y -0.00014992536554328314
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006924300971446103 -strength_y -0.00014429463173779312
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067831431421244625 -strength_y -0.00013969732667756023
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.0163959409973 -angle 7.10623429527e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.983604059003 -angle 4.26308005025e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006667486425931566 -strength_y -0.00018016739432238552
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 4.33414239321e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006625925215804967 -strength_y -0.00041498218067417494
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.960763940997 -angle 4.16408772654e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.0392360590029 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006748369334774989 -strength_y -0.00044090638775473573
Drift -name "LX0" -length 0.387483940997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD6C" -synrad $quad_synrad -length 0.612516059003 -strength [expr -0.00722285031727*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007148262336460318 -strength_y -0.0005486441482693088
Quadrupole -name "QD6C" -synrad $quad_synrad -length 1.0 -strength [expr -0.0117920995068*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007167185795482761 -strength_y -0.00054129886710712
Quadrupole -name "QD6C" -synrad $quad_synrad -length 1.0 -strength [expr -0.0117920995068*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007104620555754515 -strength_y -0.00052239992425514
Quadrupole -name "QD6C" -synrad $quad_synrad -length 0.161163940997 -strength [expr -0.00190046122914*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D7" -length 0.838836059003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007205908523047023 -strength_y -0.0007962858024200928
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007163804855015877 -strength_y -0.0007704495095843701
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007116993070048807 -strength_y -0.0007253416476471134
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007160264850793851 -strength_y -0.0007200587455660636
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071958539625142535 -strength_y -0.0007310625468673849
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007100588211897423 -strength_y -0.0006984715875555435
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006948346866983032 -strength_y -0.0006185676987043941
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068974637494066205 -strength_y -0.0005415586977937497
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006942251065192755 -strength_y -0.0004909575363708449
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006950225302862036 -strength_y -0.00043862444377681486
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006890599392739326 -strength_y -0.00036746544518815655
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006852437470004625 -strength_y -0.00031232369215014225
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006864704222602237 -strength_y -0.0003132805295286576
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006865091890091048 -strength_y -0.0003557352901862281
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006840911258897309 -strength_y -0.00038348903818242823
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00684624778962631 -strength_y -0.0003589481269238048
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006872780120997831 -strength_y -0.00029307391089367194
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006847710344729883 -strength_y -0.00022749239942087532
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006772882350236036 -strength_y -0.0002025127653603356
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006726303890066323 -strength_y -0.00023055181567400158
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067077307617511355 -strength_y -0.0002820040383750332
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006627188151903389 -strength_y -0.00030539883819758476
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006480952302107627 -strength_y -0.0002834436366894363
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0063955517417056676 -strength_y -0.00026841473631360885
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006442133539934739 -strength_y -0.0003395085823986554
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006537665948525448 -strength_y -0.0005136104684934268
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006594494274213282 -strength_y -0.0007100676874205961
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006642430138972243 -strength_y -0.0008184716205116069
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006723583609636688 -strength_y -0.0008019971258661541
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006784577827606861 -strength_y -0.000727661486487403
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067736892206256075 -strength_y -0.0006949483408429393
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006748715977166807 -strength_y -0.0007408432833286877
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006778773525370394 -strength_y -0.0008167756368133017
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068140160517918364 -strength_y -0.0008514506622161545
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006766209598542568 -strength_y -0.000827287382536768
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006663643010538138 -strength_y -0.0007938686678666583
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006612018051253745 -strength_y -0.0008107683489569087
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00663552186650733 -strength_y -0.0008788222268983131
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006664736922240088 -strength_y -0.0009282209617689504
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006664159429388373 -strength_y -0.0008822003392036489
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006668150160498058 -strength_y -0.0007404752930555057
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006688151234584987 -strength_y -0.0005905326390927095
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006675315284425578 -strength_y -0.00051854990791434
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006598644748949964 -strength_y -0.0005143228242685927
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006499934027801744 -strength_y -0.0004909574429552742
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006449352633112958 -strength_y -0.0004002204338407618
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006481178324620438 -strength_y -0.00029080162413895524
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006576753885137255 -strength_y -0.00023370353203594892
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006680231573129137 -strength_y -0.00023013444514629952
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006736368131415575 -strength_y -0.00023337407893623174
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006740357387803532 -strength_y -0.0002306435869316345
Drift -name "D7" -length 0.367563940997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.632436059003 -strength [expr -0.0107964387279*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007084236555361739 -strength_y -0.0005013818074991981
Quadrupole -name "QD6B" -synrad $quad_synrad -length 1.0 -strength [expr -0.017071194114*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00707375665489321 -strength_y -0.0004687420782497033
Quadrupole -name "QD6B" -synrad $quad_synrad -length 1.0 -strength [expr -0.017071194114*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007061670526668407 -strength_y -0.0003818133350008998
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.141243940997 -strength [expr -0.00241120273418*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.432036059003 -strength [expr -0.00737537142747*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006868388423451442 -strength_y -6.475838589916065e-05
Quadrupole -name "QD6B" -synrad $quad_synrad -length 1.0 -strength [expr -0.017071194114*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006763406650202804 -strength_y -0.0001496459924490025
Quadrupole -name "QD6B" -synrad $quad_synrad -length 1.0 -strength [expr -0.017071194114*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006797029333850147 -strength_y -0.0001784965388857027
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.341643940997 -strength [expr -0.00583227003462*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D6" -length 0.658356059003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007029563370901192 -strength_y 2.117962168948765e-05
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007137557219230513 -strength_y 2.4789165539377574e-05
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007122735539815669 -strength_y 6.067588280588452e-05
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007077566068493901 -strength_y 0.00012227328783956246
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070845248476882165 -strength_y 0.0001467771529222837
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070973033909946465 -strength_y 9.295885913313113e-05
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007069597863389191 -strength_y -2.6251404688196652e-06
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007061378598141008 -strength_y -7.027255519041896e-05
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007126651274924043 -strength_y -9.107636844128789e-05
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00720945570949308 -strength_y -0.00010678140456595428
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007239195776112576 -strength_y -0.00015368231423810956
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007232310128949942 -strength_y -0.00022084740842867338
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007222456669863049 -strength_y -0.00027930326422704937
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007187456495247258 -strength_y -0.00031980860648955995
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071228573761594185 -strength_y -0.00034352350895912423
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007089622182307802 -strength_y -0.0003416070140380598
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071065778951127185 -strength_y -0.0003133042161037039
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007093544819240566 -strength_y -0.0002900231106365255
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007009939783874814 -strength_y -0.0003070896281780278
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006949955177082915 -strength_y -0.00035183061464902387
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006996820511165966 -strength_y -0.0003714825573301345
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070772219619660084 -strength_y -0.0003407384373691722
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007079835195575789 -strength_y -0.0002912839335617258
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007036066538263643 -strength_y -0.0002589094177467521
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007049089314889751 -strength_y -0.00023274928724876102
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007105369416681816 -strength_y -0.00018695709207330904
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070983287833873724 -strength_y -0.00013829574080498992
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007004042729322618 -strength_y -0.0001276427640791252
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006900475032679743 -strength_y -0.00015101339956511152
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006830755640622858 -strength_y -0.00015818095942034437
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067674536253410815 -strength_y -0.00013305897508767472
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006701557770933095 -strength_y -0.00013167914665052354
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006658690656132676 -strength_y -0.0002096902773714442
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006636740184180338 -strength_y -0.0003399219763110404
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006624629948767024 -strength_y -0.00043799094889591304
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006664142755579977 -strength_y -0.0004580523596913438
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006793927890999612 -strength_y -0.00043269444842345425
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006955306827028902 -strength_y -0.00042324876121113073
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00705649851070361 -strength_y -0.00045704178769838004
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007101873059856587 -strength_y -0.0005129579942747624
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00714736887612708 -strength_y -0.0005483757216746654
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007167355105926683 -strength_y -0.0005414983473579142
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007104422131950852 -strength_y -0.0005223916714381042
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007022854312378315 -strength_y -0.0005526807469188213
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007049052683448592 -strength_y -0.0006527355128800219
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007160512378151923 -strength_y -0.0007607667239766673
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007204679416597686 -strength_y -0.0007963243215469663
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007147033586722368 -strength_y -0.0007582693209940427
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007119697152919486 -strength_y -0.0007198586815016266
Drift -name "D6" -length 0.721682340997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF6" -synrad $mult_synrad -type 3 -length 0.278317659003 -strength [expr 0.170413384231*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00683851191649418 -strength_y -0.0003832910075315276
Multipole -name "SF6" -synrad $mult_synrad -type 3 -length 1.0 -strength [expr 0.612298137466*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006737492329116501 -strength_y -0.00023389940699069836
Multipole -name "SF6" -synrad $mult_synrad -type 3 -length 0.121323940997 -strength [expr 0.0742864231026*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.451956059003 -strength [expr 0.00420147242541*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006969586642405314 -strength_y -1.740686886100264e-05
Quadrupole -name "QF5B" -synrad $quad_synrad -length 1.0 -strength [expr 0.00929619670258*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068505278785856 -strength_y -5.0164477783651e-05
Quadrupole -name "QF5B" -synrad $quad_synrad -length 1.0 -strength [expr 0.00929619670258*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006830271366301974 -strength_y -0.00019908915076528438
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.321723940997 -strength [expr 0.00299080903944*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.251556059003 -strength [expr 0.00233851460622*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00684118076418523 -strength_y -0.00023752896935105082
Quadrupole -name "QF5B" -synrad $quad_synrad -length 1.0 -strength [expr 0.00929619670258*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006866122370561014 -strength_y -0.00026028578841547745
Quadrupole -name "QF5B" -synrad $quad_synrad -length 1.0 -strength [expr 0.00929619670258*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006970518617076906 -strength_y -0.0002958186740091718
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.522123940997 -strength [expr 0.00485376685863*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D5OD" -length 0.477876059003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007044091635539205 -strength_y 0.00014431949705031078
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00695337729809846 -strength_y 0.00011290066608376088
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006859301282955123 -strength_y 6.156133654122517e-05
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067967406416486525 -strength_y 6.894746572547948e-05
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00676321052508822 -strength_y 0.00012279491379707793
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006717005950610499 -strength_y 0.00017753885799146415
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006658348317691257 -strength_y 0.00021586715960379173
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006648402637568053 -strength_y 0.00023640874322177863
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006701240270124408 -strength_y 0.00023893555536335555
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006732716305454988 -strength_y 0.00024518629923719394
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006683395352521212 -strength_y 0.0002851962117819185
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066170621898536145 -strength_y 0.00033984017031570777
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006613781931155672 -strength_y 0.0003453946554922242
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006638838325059175 -strength_y 0.00028234505513721075
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006622549520770135 -strength_y 0.0002104762458943926
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006605943145685332 -strength_y 0.00017998825189093393
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006679272871685248 -strength_y 0.00015579329922533515
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006815218487747033 -strength_y 7.894849806550695e-05
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068985917878821275 -strength_y -3.6245432648847164e-05
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006905272903169188 -strength_y -0.0001260551113037046
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00691771055856388 -strength_y -0.00017226420047213538
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00696449587892908 -strength_y -0.0002090107519965096
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006980749558684584 -strength_y -0.0002473957720107613
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006945377479401956 -strength_y -0.0002617943452666641
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006925869348764209 -strength_y -0.00025744307458027847
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006951250955609136 -strength_y -0.00028761578202948773
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006959632363423448 -strength_y -0.0003796019381652488
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006914650330679981 -strength_y -0.0004917045618823308
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006865286526265558 -strength_y -0.0005702522338480515
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068482342805847845 -strength_y -0.00060909405221033
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006838757741236819 -strength_y -0.0006251669054796416
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006840284486576398 -strength_y -0.000614911499885047
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006906728572901722 -strength_y -0.000573318782750814
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007023605984445693 -strength_y -0.0005262458599413185
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00708755832718563 -strength_y -0.0004976720059433571
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007068543109733377 -strength_y -0.0004593478948994291
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007066870766892989 -strength_y -0.0003594993046698663
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071283383531279354 -strength_y -0.00020382017948402759
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007135853928316393 -strength_y -7.080973146615514e-05
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006997759462871022 -strength_y -3.366236238367988e-05
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00682203258286008 -strength_y -8.715241807726463e-05
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006763299256234496 -strength_y -0.00015911347665151357
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006797646386398866 -strength_y -0.0001781855471128598
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006807073641107935 -strength_y -0.0001268875697669105
Drift -name "D5OD" -length 0.146080210997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD5L" -synrad $mult_synrad -type 3 -length 0.853919789003 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006628684030016663 -strength_y -0.00039417495619143035
Multipole -name "SD5L" -synrad $mult_synrad -type 3 -length 0.146080210997 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SD5" -synrad $mult_synrad -type 3 -length 0.853919789003 -strength [expr -1.23999183847*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007020167929427881 -strength_y -0.0006009563754128788
Multipole -name "SD5" -synrad $mult_synrad -type 3 -length 0.146080210997 -strength [expr -0.212125625534*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SD5R" -synrad $mult_synrad -type 3 -length 0.853919789003 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007200367680427109 -strength_y -0.000791677905344656
Multipole -name "SD5R" -synrad $mult_synrad -type 3 -length 0.146080210997 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D5ODT" -length 0.853919789003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007162555808309115 -strength_y -0.0007695799777483473
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007117168937940774 -strength_y -0.0007232871523263805
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00716936190607352 -strength_y -0.0007220106702333906
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007188546717539311 -strength_y -0.0007294346968914396
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007060471604740229 -strength_y -0.0006811775790917578
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006917422855211701 -strength_y -0.0005919971372104186
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006909625549897363 -strength_y -0.0005205714258746414
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006956147504847058 -strength_y -0.0004708746162521121
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006924932845777829 -strength_y -0.00040520634659194473
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068621185043598685 -strength_y -0.00033200291645861193
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006857306457466115 -strength_y -0.00030566118335511943
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006869359054622977 -strength_y -0.00034007280378464493
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006846931891958795 -strength_y -0.00038027527917815923
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00684120641966996 -strength_y -0.00036895727757241546
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068702957945242235 -strength_y -0.00030487780037421493
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006853609394054025 -strength_y -0.00023286442912143663
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006776051083490554 -strength_y -0.00020244192296640118
Drift -name "D5ODT" -length 0.146080210997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.853919789003 -strength [expr 0.00725947249816*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066284794791319915 -strength_y -0.0008651693184602248
Quadrupole -name "QF5A" -synrad $quad_synrad -length 1.0 -strength [expr 0.00850135175651*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006664925605109762 -strength_y -0.0008943869049618952
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.919760210997 -strength [expr 0.00781920508533*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.0802397890029 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006665819763546461 -strength_y 0.0009062721856564149
Drift -name "LX0" -length 0.346480210997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.653519789003 -strength [expr 0.00555580160616*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006681324901608253 -strength_y -0.0005234361686322497
Quadrupole -name "QF5A" -synrad $quad_synrad -length 1.0 -strength [expr 0.00850135175651*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006595000259481607 -strength_y -0.0005143635858012414
Quadrupole -name "QF5A" -synrad $quad_synrad -length 1.0 -strength [expr 0.00850135175651*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006482511780980329 -strength_y -0.0004763046470253347
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.120160210997 -strength [expr 0.00102152422082*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF5" -synrad $mult_synrad -type 3 -length 0.453119789003 -strength [expr 0.0679843456059*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066855792253277205 -strength_y -0.00023057580227446996
Multipole -name "SF5" -synrad $mult_synrad -type 3 -length 0.946521810997 -strength [expr 0.142012482094*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D4OD" -length 0.053478189003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006742006558493535 -strength_y -0.0002319751197057391
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006737383472898592 -strength_y -0.0002343624419578337
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006754089661735696 -strength_y -0.00025738467756157725
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068012607999766205 -strength_y -0.0002493485764393299
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068438425069911575 -strength_y -0.0001668288539850445
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006890429960430288 -strength_y -6.736342750535247e-05
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006950970969697474 -strength_y -2.215068044245684e-05
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006964284146174994 -strength_y -1.771118537608496e-05
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006895727973001991 -strength_y -2.8349734780981637e-05
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006825459706287492 -strength_y -8.081794730068745e-05
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006821689009811429 -strength_y -0.0001693676220427533
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006839323760889414 -strength_y -0.00022701467486179946
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068444983055720695 -strength_y -0.00024391653154480288
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006895278273373387 -strength_y -0.0002758326112301859
Drift -name "D4OD" -length 0.091574170997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT2" -length 0.908425829003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066585532629459135 -strength_y 0.00023850319963015682
Drift -name "OCTDRIFT2" -length 0.174907503997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCT1" -synrad $mult_synrad -type 4 -length 0.5 -strength [expr -1.0*8.882502817*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT3" -length 0.325092496003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006624120776523197 -strength_y 0.0003291851168730863
Drift -name "OCTDRIFT3" -length 0.0915741706968 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD4B" -synrad $quad_synrad -length 0.908425829303 -strength [expr -0.00509753626332*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006639288621942344 -strength_y 0.00026038536983466096
Quadrupole -name "QD4B" -synrad $quad_synrad -length 1.0 -strength [expr -0.00561139511768*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006609554750876069 -strength_y 0.000192962032037822
Quadrupole -name "QD4B" -synrad $quad_synrad -length 0.865254170697 -strength [expr -0.004855283029*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.134745829303 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006906039392083002 -strength_y -7.069051278477595e-05
Drift -name "LX0" -length 0.291974170697 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD4" -synrad $mult_synrad -type 3 -length 0.708025829303 -strength [expr 0.288677474916*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006906798712150165 -strength_y -0.00015562973537480978
Multipole -name "SD4" -synrad $mult_synrad -type 3 -length 0.691615770697 -strength [expr 0.281986738384*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.308384229303 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006982170649127524 -strength_y -0.00024366363220513902
Drift -name "LX0" -length 0.118335770697 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD4A" -synrad $quad_synrad -length 0.881664229303 -strength [expr -0.00496891777317*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006936419483583009 -strength_y -0.00026571295757225904
Quadrupole -name "QD4A" -synrad $quad_synrad -length 1.0 -strength [expr -0.0056358391415*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006941464857120376 -strength_y -0.0004386706536142521
Quadrupole -name "QD4A" -synrad $quad_synrad -length 0.892015770697 -strength [expr -0.00502725739533*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3" -length 0.107984229303 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006962900730568841 -strength_y 0.0003386997112570338
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00693238590848531 -strength_y -0.00045863748283534784
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006874323724043036 -strength_y -0.0005555757146789192
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006849644657029754 -strength_y -0.0006044012729674284
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006839907604544103 -strength_y -0.0006245091189744509
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068395302163657 -strength_y -0.0006157193040308186
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006910998297271736 -strength_y -0.0005712745400661943
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007035125899959493 -strength_y -0.0005221671856821763
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007088476603268578 -strength_y -0.0004931326870401061
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007061912192308665 -strength_y -0.0004413632786240594
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070827647013770945 -strength_y -0.00031282506379756687
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007146410081627126 -strength_y -0.00014382417692417935
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00708817000987263 -strength_y -3.992396073966885e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068966237941937404 -strength_y -5.456702352278988e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006769468704070126 -strength_y -0.00013366586439216306
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006784452627939394 -strength_y -0.0001801971580255219
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006809515683977275 -strength_y -0.0001453303362849313
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006800890787588874 -strength_y -6.0153692700384545e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00687990467615597 -strength_y 5.6434096307110185e-06
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007050106284640373 -strength_y 2.130750810420643e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007143279406386174 -strength_y 2.8483947522241923e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007108196668505242 -strength_y 7.656989671212634e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007074145440823454 -strength_y 0.0001379151899698932
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007093662198908176 -strength_y 0.0001343832432280146
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007087843667748093 -strength_y 4.90681881003819e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070570692175154475 -strength_y -4.516480672187976e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007093103179130274 -strength_y -8.642196169608093e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007185705699536057 -strength_y -9.895482257642907e-05
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0072369108902090055 -strength_y -0.0001379371037336441
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007234220411898085 -strength_y -0.00020728706020786342
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007224397795015475 -strength_y -0.0002725348184720762
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007191149079044115 -strength_y -0.00031756914028774097
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007122916906322477 -strength_y -0.000343510771747408
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00708967241335847 -strength_y -0.0003403932658351423
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007108705043425897 -strength_y -0.0003089541398090233
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00708120763530344 -strength_y -0.00028970233081133674
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006986171255835117 -strength_y -0.0003181875303594492
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006954020097539248 -strength_y -0.0003636183618348189
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007032888872430072 -strength_y -0.0003644942755391401
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007090199510252162 -strength_y -0.00031705978886582375
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007054771568291048 -strength_y -0.0002714863538230497
Drift -name "D3" -length 0.388755100697 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.611244899303 -angle -8.24311318798e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007114615736460345 -strength_y -0.00015942946308656716
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067861097684483165 -strength_y -0.00014103754302292724
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006624561337802984 -strength_y -0.0004333800056301002
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007064367273976982 -strength_y -0.000464078562012994
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007071623713816436 -strength_y -0.0005244767396506951
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007179009766181989 -strength_y -0.0007806698837927923
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00710129086642084 -strength_y -0.0006987580536485847
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069411650354730394 -strength_y -0.000424925850884122
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068549868823584645 -strength_y -0.0003723990549523941
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006801120287142575 -strength_y -0.0002053174932020389
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006503219856568279 -strength_y -0.0002884147812518138
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006596825564382333 -strength_y -0.0007196921243717153
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.976995100697 -angle -1.31755393105e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.0230048993033 -angle -3.10238971402e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006764820778128225 -strength_y -0.0006997083546066709
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006791229604097085 -strength_y -0.000831158689898331
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006742150252198672 -strength_y -0.0008172399960320161
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006612725580725803 -strength_y -0.0008243120446477031
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006666695297951334 -strength_y -0.0009276098612172753
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006672632491829555 -strength_y -0.000701272774442795
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00666254109263676 -strength_y -0.000514125596504445
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0064816057786757884 -strength_y -0.00047535531294742536
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00649933672627954 -strength_y -0.0002713887084588585
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006698635866673089 -strength_y -0.000231712997368332
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067386260558361406 -strength_y -0.0002317655887943029
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006780154238207102 -strength_y -0.00026132209912253253
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006859134125898145 -strength_y -0.00012607611807442142
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.565235100697 -angle -7.62263524517e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.434764899303 -angle -5.86314303678e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069590803456334735 -strength_y -1.9741191876780472e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00689780202508703 -strength_y -2.775981823548083e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006817890224440964 -strength_y -0.00014981588450946215
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006841255969741398 -strength_y -0.00023795641567303394
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006934164245739819 -strength_y -0.0002898645348703137
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007113774950452925 -strength_y -0.00015471248386553202
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007059560005355956 -strength_y 0.00013818450498284438
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006876713340228267 -strength_y 6.842344312041736e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006769621098927057 -strength_y 0.00011071599832864684
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006668278919909605 -strength_y 0.0002095751726433894
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066890671288899915 -strength_y 0.00023925433291021428
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006697822811417514 -strength_y 0.0002746007334216492
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006609487292474739 -strength_y 0.0003503845550024937
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.153475100697 -angle -2.0697311798e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.846524899303 -angle -1.14160471022e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006628785306354012 -strength_y 0.00022157986742857394
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006657352444700535 -strength_y 0.00016333770629022368
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006890108347220517 -strength_y -1.452774847236646e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00691183513649676 -strength_y -0.00016546047934612858
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00698260070238838 -strength_y -0.00024124639632144447
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006925432232921464 -strength_y -0.00025743310911022373
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006962363308521381 -strength_y -0.0003592522083256479
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006871924813885683 -strength_y -0.0005593681911355545
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068408346249114686 -strength_y -0.0006238023027051844
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006888957898593617 -strength_y -0.0005822864418510213
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00708355376691981 -strength_y -0.0005019147593860441
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007061543378659646 -strength_y -0.0003825048909041089
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.741715100697 -angle -1.00026053964e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.258284899303 -angle -3.48317288558e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007144701469162494 -strength_y -8.806515230020483e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006843846100428235 -strength_y -7.55592581748798e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006792475228740717 -strength_y -0.00018002998855788066
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006802047692679133 -strength_y -5.463632677789493e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007041955796273713 -strength_y 2.128146057601247e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007117899859472966 -strength_y 6.585673518173719e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007087000284810705 -strength_y 0.0001449253855274849
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007065974368585231 -strength_y -1.2612865515938876e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007137859854033727 -strength_y -9.219058182138145e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007239247038365272 -strength_y -0.0001621921825847552
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007219811362859637 -strength_y -0.0002862278524342171
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007113886623974938 -strength_y -0.0003452159038024442
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071091326126307645 -strength_y -0.0003078419305999617
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.329955100697 -angle -4.449701331e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.670044899303 -angle -9.03607695095e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006994666291537336 -strength_y -0.00031384134989839664
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007011246102093741 -strength_y -0.0003696811013937101
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007073180507097673 -strength_y -0.0002847821622800422
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007057220614366805 -strength_y -0.0002273202375999996
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007088747491815217 -strength_y -0.0001337149543052684
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006888832025680303 -strength_y -0.00015414284238347853
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006758215395746807 -strength_y -0.00012974960444181845
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006655151932857894 -strength_y -0.00022528263365027503
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006625394770583821 -strength_y -0.000444775435475217
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006813700029929183 -strength_y -0.0004297110064519573
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007063333706019886 -strength_y -0.00046308057175893083
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007152190671131906 -strength_y -0.0005496302467890982
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.918195100697 -angle -1.23825755476e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.0818048993031 -angle -1.10320273438e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007094395849044858 -strength_y -0.0005222774005082226
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007058456788520636 -strength_y -0.0006646099139148504
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00720227513352148 -strength_y -0.0007950457992940863
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007122251029925844 -strength_y -0.0007185648265337469
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071801599177459706 -strength_y -0.000727091321129064
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00691640633626325 -strength_y -0.0005909065559525701
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006953986214038009 -strength_y -0.00047608387910380584
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006871988515354159 -strength_y -0.00034552477370804706
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006868898818423342 -strength_y -0.00032464561502807456
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068375328924643085 -strength_y -0.0003818201504166031
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006872947893090974 -strength_y -0.00027029044456055543
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006752561443748785 -strength_y -0.00020610725214616974
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066929261468639005 -strength_y -0.00029427697581839576
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.506435100697 -angle -6.8296714822e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.493564899303 -angle -6.65610679975e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006441868617917866 -strength_y -0.00027415480270279625
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006472009211744921 -strength_y -0.0003837381900519046
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006606597870217684 -strength_y -0.0007560626317601874
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006748052337736148 -strength_y -0.0007808341008721181
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067630772383315526 -strength_y -0.0007012829951874897
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006793659576523247 -strength_y -0.0008336590758942067
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006736925034792399 -strength_y -0.0008152118760270872
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00661341395612533 -strength_y -0.0008274635966893691
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006666860325620573 -strength_y -0.0009266773591225985
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066736903751295875 -strength_y -0.0006931412879101946
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006659345134581468 -strength_y -0.0005136300006754311
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006478134665269996 -strength_y -0.0004714941415892736
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006503664302382653 -strength_y -0.0002676784860631274
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.094675100697 -angle -1.27676741682e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.905324899303 -angle -1.22090108651e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006702219860870521 -strength_y -0.00023202581331507183
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006738297638945106 -strength_y -0.00023217072055722516
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067823976727145125 -strength_y -0.00026074285050124013
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00686108026850566 -strength_y -0.0001213392185723249
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00696082335796856 -strength_y -1.9302009969440183e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006893849687359893 -strength_y -2.890544650526079e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006818519961491292 -strength_y -0.00015382024115736108
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00684137834849048 -strength_y -0.00023852496714058257
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006939309090831096 -strength_y -0.0002911536716846166
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007115412174929305 -strength_y -0.000146053764235023
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007055923012411746 -strength_y 0.00014018290648354308
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006872337916626314 -strength_y 6.650564790933892e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.682915100697 -angle -9.2096416334e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.317084899303 -angle -4.27613664855e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067680838857215935 -strength_y 0.00011368541876306827
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006665643607815555 -strength_y 0.0002112028744750341
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006692161113509871 -strength_y 0.0002391822349856677
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006694281946260282 -strength_y 0.00027719429487456246
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006610431202181923 -strength_y 0.0003493935008508586
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00662723665951608 -strength_y 0.00021853282710563354
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066627909207888514 -strength_y 0.00016156126350798858
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006892682695087621 -strength_y -2.0355921501013317e-05
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006913238035719231 -strength_y -0.00016731992264797547
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006982325616331259 -strength_y -0.00024299237301421813
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006925354717540602 -strength_y -0.0002573331321838691
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006961824672922624 -strength_y -0.0003647849102258315
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.3485778282e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006869934546099659 -strength_y -0.0005625605429724458
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.271155100697 -angle -3.65673756802e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF3B" -synrad $quad_synrad -length 0.302124899303 -strength [expr 0.000415186208241*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006791587095485348 -strength_y -0.00018019426763532143
Quadrupole -name "QF3B" -synrad $quad_synrad -length 1.0 -strength [expr 0.00137422042737*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006807251922777333 -strength_y -0.00012790640549690488
Quadrupole -name "QF3B" -synrad $quad_synrad -length 1.0 -strength [expr 0.00137422042737*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006810287986105008 -strength_y -3.574897061342161e-05
Quadrupole -name "QF3B" -synrad $quad_synrad -length 0.471555100697 -strength [expr 0.000648020652011*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.101724899303 -angle -1.37183943693e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007111957176395938 -strength_y 7.234492500531395e-05
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007074155036152237 -strength_y 0.00013837668303083274
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00709527215458498 -strength_y 0.0001296214788422606
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.458595100697 -angle -6.18451184545e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.541404899303 -angle -7.30126642835e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007066452803722738 -strength_y -1.1265376323805132e-05
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007139517027808528 -strength_y -9.235531274598344e-05
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007239118708867707 -strength_y -0.00016550066712570083
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.018915100697 -angle -2.55084854027e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.981084899303 -angle -1.32306934198e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007219684356814147 -strength_y -0.0002865135216487214
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007116001048650016 -strength_y -0.00034486103566762377
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.579235100697 -angle -7.81143613641e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.420764899303 -angle -5.6743421374e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007108013220661184 -strength_y -0.00031052685278546555
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007005194483550006 -strength_y -0.0003090586131698835
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006998321426725832 -strength_y -0.0003713600791242501
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.139555100697 -angle -1.88200914498e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.860444899303 -angle -1.16037691288e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007077934819640183 -strength_y -0.00028925343367928254
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070539479552823746 -strength_y -0.00022949333853866644
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.699875100697 -angle -9.43836042736e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.300124899303 -angle -4.04741784645e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007091076323743118 -strength_y -0.0001346623545344758
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068938525706253775 -strength_y -0.00015282443826203772
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006764422583680521 -strength_y -0.0001319114580038779
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.260195100697 -angle -3.50893343593e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.739804899303 -angle -9.97684483787e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006656834612744635 -strength_y -0.00021756529986210273
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066252720397314314 -strength_y -0.00044412181924632265
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.820515100697 -angle -1.10652847183e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.179484899303 -angle -2.42049355549e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006817130802606271 -strength_y -0.00042923340955343777
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007062887848172516 -strength_y -0.00046265717253313144
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007150625280142012 -strength_y -0.0005492764325160515
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.380835100697 -angle -5.13585772689e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.619164899303 -angle -8.34992054692e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007100651126864876 -strength_y -0.0005222773063696788
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007049577502949585 -strength_y -0.0006534293005745327
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.941155100697 -angle -1.26922090093e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.0588448993026 -angle -7.93569264539e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007205178838377886 -strength_y -0.000796562997896586
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007120151241935651 -strength_y -0.0007195639160574619
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007181635720157993 -strength_y -0.0007275229377039341
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.501475100697 -angle -6.76278201784e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF3A" -synrad $quad_synrad -length 0.0718048993026 -strength [expr 0.000405228184381*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006955055386402539 -strength_y -0.00044896934654389993
Quadrupole -name "QF3A" -synrad $quad_synrad -length 1.0 -strength [expr 0.0056434614952*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006902423620958625 -strength_y -0.0003804758613827136
Quadrupole -name "QF3A" -synrad $quad_synrad -length 1.0 -strength [expr 0.0056434614952*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006854757303004077 -strength_y -0.00031904054503590766
Quadrupole -name "QF3A" -synrad $quad_synrad -length 0.701875100697 -strength [expr 0.00396100510522*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.298124899303 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006838535504200349 -strength_y -0.00037539055443610355
Drift -name "LX0" -length 0.128595100697 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.871404899303 -angle -1.17515732587e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00686814055473432 -strength_y -0.00031204115705883674
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006854089301361604 -strength_y -0.00023335004642609347
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.688915100697 -angle -9.29055629748e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.311084899303 -angle -4.19522197633e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006753681270774708 -strength_y -0.00020573132395685295
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066957583067160905 -strength_y -0.00029262758603131603
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006450767621468132 -strength_y -0.00027628746421778027
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.249235100697 -angle -3.36112930606e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.750764899303 -angle -1.01246489678e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006467062555232282 -strength_y -0.000375974457423001
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006606643213572994 -strength_y -0.0007562114428798216
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.809555100697 -angle -1.09174805884e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.190444899303 -angle -2.56829768537e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006750635663940718 -strength_y -0.000778180493691169
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006762873967499763 -strength_y -0.000701481796555196
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006792555712074255 -strength_y -0.0008325341637968069
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.369875100697 -angle -4.98805359701e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.630124899302 -angle -8.4977246768e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006737292889326829 -strength_y -0.0008153527805114857
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006614204547311131 -strength_y -0.0008304983296494161
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.930195100698 -angle -1.25444048794e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.0698048993024 -angle -9.41373394418e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006666975314454295 -strength_y -0.000924332436654502
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006674981047534111 -strength_y -0.0006835764310094074
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006657310390476166 -strength_y -0.000513390591113871
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.490515100698 -angle -6.61497788796e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.509484899302 -angle -6.87080038585e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006478006563211153 -strength_y -0.0004713443460641065
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00650133840743816 -strength_y -0.0002696365591514113
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006698250849312638 -strength_y -0.00023167935371861713
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.0508351006976 -angle -6.85550896535e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.949164899302 -angle -1.28002273773e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006738375104880716 -strength_y -0.00023206616463008337
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006784113485931107 -strength_y -0.00026019678566990617
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.611155100698 -angle -8.24190217892e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.388844899302 -angle -5.24387609489e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006863311845192109 -strength_y -0.00011608398127101425
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006961716773046603 -strength_y -1.9087326716660862e-05
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006893980003929374 -strength_y -2.8866207710696344e-05
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.171475100698 -angle -2.31247518749e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.828524899302 -angle -1.11733030863e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006818987280883353 -strength_y -0.00015650470037972274
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.34857782738e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006841659833823341 -strength_y -0.0002394812714204875
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.731795100698 -angle -9.86882646987e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.268204899302 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007079509990988074 -strength_y -0.00024233987733655017
Drift -name "LX0" -length 0.158515100698 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD2" -synrad $quad_synrad -length 0.841484899302 -strength [expr -0.00560097585351*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007073556002404339 -strength_y 0.00012654783370855764
Quadrupole -name "QD2" -synrad $quad_synrad -length 1.0 -strength [expr -0.0066560622278*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00698975999633107 -strength_y 0.00013343614205928977
Quadrupole -name "QD2" -synrad $quad_synrad -length 0.932195100698 -strength [expr -0.00620474859869*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.0678048993022 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006747584372053371 -strength_y 0.000147000644618953
Drift -name "LX0" -length 0.358915100698 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.641084899302 -angle -2.59734324536e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006687493984417884 -strength_y 0.00019784071771220078
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006643117029047393 -strength_y 0.00023185271896703622
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.919235100698 -angle -3.72426348257e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.0807648993023 -angle -3.27217449504e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006698436789388492 -strength_y 0.00023901347038962026
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006688939430317946 -strength_y 0.00028111210909962156
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006611359449401518 -strength_y 0.0003483539645451863
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.479555100698 -angle -1.94290834635e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.520444899302 -angle -2.10857258572e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066267462866257485 -strength_y 0.000217614411880728
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006661394155864239 -strength_y 0.00016202320167221562
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006890612110760953 -strength_y -1.5629784680022675e-05
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.0398751006978 -angle -1.61553210141e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.960124899302 -angle -3.88992772193e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006913304521747579 -strength_y -0.00016740321643349466
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006981862059669671 -strength_y -0.00024473878465563044
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.600195100698 -angle -2.431679006e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.399804899302 -angle -1.61980192607e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006925493569889923 -strength_y -0.00025731610136483677
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006961266151816919 -strength_y -0.00036932769489048226
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006869466920511689 -strength_y -0.0005633176951161702
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.160515100698 -angle -6.50323869787e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.839484899302 -angle -3.40115706228e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006839700188722796 -strength_y -0.0006246460354857083
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006903553509684642 -strength_y -0.0005748647040187851
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.720835100698 -angle -2.92044966565e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.279164899302 -angle -1.13103126643e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007087513466515527 -strength_y -0.0004977513885037396
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007065763857643173 -strength_y -0.0003636204864861424
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007139374942221446 -strength_y -7.62822692681319e-05
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.281155100698 -angle -1.13909452943e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.718844899302 -angle -2.91238640264e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006833711424738518 -strength_y -8.067186146157575e-05
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067934181092160735 -strength_y -0.00017981107166707211
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.841475100698 -angle -3.40922032529e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.158524899302 -angle -6.4226060678e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006801800981318165 -strength_y -5.564571724464721e-05
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007045206721769327 -strength_y 2.129523715069023e-05
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007114874954748072 -strength_y 6.913588281435403e-05
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.401795100698 -angle -1.62786518908e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.598204899302 -angle -2.42361574299e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007088007130264699 -strength_y 0.00014391170759954662
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007066055077443923 -strength_y -1.238472968849412e-05
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.962115100698 -angle -3.89799098494e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.0378848993018 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071386144511435285 -strength_y -9.226544686910232e-05
Drift -name "LX0" -length 0.388835100698 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.611164899302 -angle -2.47612293587e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0072258954933182 -strength_y -0.00026577354070427033
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00719363489401378 -strength_y -0.000315954050383844
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.949155100698 -angle -3.84548379206e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0508448993019 -angle -2.05997140015e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007120423771269739 -strength_y -0.0003440283772769717
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007108033482252051 -strength_y -0.00031048344881096725
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069979187907523695 -strength_y -0.0003122982523405003
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.509475100698 -angle -2.06412865584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.490524899302 -angle -1.98735227623e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007008826600256472 -strength_y -0.00037007710711741223
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007075654376664331 -strength_y -0.00028701671377092145
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007052577337056715 -strength_y -0.00023040595798510277
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0697951006982 -angle -2.82773519631e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.930204899302 -angle -3.76870741244e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007091581221508558 -strength_y -0.00013487985209129506
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006887973953757609 -strength_y -0.00015436196264715366
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.630115100698 -angle -2.55289931549e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.369884899302 -angle -1.49858161658e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006756223536964443 -strength_y -0.00012911606250821735
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006655140773847163 -strength_y -0.00022533567337703364
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066251453060075524 -strength_y -0.0004433621635467562
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.190435100698 -angle -7.71544179276e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.809564899302 -angle -3.27993675279e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006804248505982438 -strength_y -0.0004310889677474578
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007058289739792067 -strength_y -0.00045853381130074334
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.750755100698 -angle -3.04166997513e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.249244899302 -angle -1.00981095694e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007147884682892798 -strength_y -0.0005485328233486871
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007099028361372774 -strength_y -0.0005222541591950995
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007058671179503494 -strength_y -0.0006648684499006583
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.311075100698 -angle -1.26031483892e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.688924899302 -angle -2.79116609315e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00720239315414098 -strength_y -0.0007951110807411625
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007121217545883162 -strength_y -0.0007189949117368782
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.871395100698 -angle -3.53044063478e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.128604899302 -angle -5.21040297291e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007183827488961675 -strength_y -0.0007281502436231869
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006916560545223564 -strength_y -0.0005910733942686414
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006955097839354589 -strength_y -0.00047371687834716687
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.431715100698 -angle -1.74908549857e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.568284899301 -angle -2.3023954335e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006869487334998748 -strength_y -0.0003422971096444672
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006869071557428831 -strength_y -0.00032556267495808866
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.992035100699 -angle -4.01921129443e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.00796489930144 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006837421659049113 -strength_y -0.0003810537050714723
Drift -name "LX0" -length 0.418755100699 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.581244899302 -angle -2.35490262638e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006774627227875701 -strength_y -0.00020246040267012368
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006724000543320156 -strength_y -0.00023670777122779827
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.979075100698 -angle -3.96670410155e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0209248993015 -angle -8.47768305254e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066946039175566595 -strength_y -0.0002933269901526579
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00644834204205425 -strength_y -0.0002757043495498591
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006463528018006439 -strength_y -0.00037054501885203756
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.539395100699 -angle -2.18534896533e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.460604899301 -angle -1.86613196674e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006603339158605033 -strength_y -0.0007448343101854877
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006745733236447084 -strength_y -0.0007831396231787315
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006762409319597272 -strength_y -0.0007019495565139847
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0997151006986 -angle -4.0399382912e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.900284899301 -angle -3.64748710295e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067935456166697125 -strength_y -0.0008335432276117127
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006740208090919118 -strength_y -0.0008164795726188819
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.660035100699 -angle -2.67411962498e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.339964899301 -angle -1.37736130709e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006612903535453922 -strength_y -0.0008251946683837638
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006666893407669878 -strength_y -0.0009263614681972698
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006675085554940623 -strength_y -0.0006828166230506667
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.220355100699 -angle -8.92764488765e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.779644899301 -angle -3.15871644331e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006655935631496443 -strength_y -0.0005132575170164025
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006476627337133796 -strength_y -0.00046969712443137047
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.780675100699 -angle -3.16289028462e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.219324899301 -angle -8.88590647447e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006503151347821859 -strength_y -0.00026810310804582685
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066997451883563045 -strength_y -0.00023181025882590512
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006738721382864624 -strength_y -0.00023166455274964556
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.340995100699 -angle -1.38153514841e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.659004899301 -angle -2.66994578366e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067804003695099 -strength_y -0.00026126595600949574
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006861311079313731 -strength_y -0.00012078674744256198
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.901315100699 -angle -3.65166094427e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0986848993011 -angle -3.99819987802e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006962356505106765 -strength_y -1.893790792087143e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006892383804251087 -strength_y -2.9353771137304797e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00681838034305497 -strength_y -0.00015297562667062798
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.461635100699 -angle -1.87030580806e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.538364899301 -angle -2.18117512401e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006841381739775406 -strength_y -0.0002385387679852716
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006944804300859761 -strength_y -0.0002923752329559183
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007119088003672529 -strength_y -0.00012041804978493706
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0219551006989 -angle -8.89506718433e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.551324899301 -angle -2.23368231689e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00690165234961883 -strength_y 8.123133084073638e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006809819040738348 -strength_y 6.036188064751653e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006765529705968499 -strength_y 0.00011853131690487057
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.00899510069889 -angle -3.64434789636e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.991004899301 -angle -4.01503745311e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006662979987931391 -strength_y 0.00021287209069023873
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006693644656161367 -strength_y 0.0002391440961618881
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.569315100699 -angle -2.30656927482e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.430684899301 -angle -1.74491165725e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066922681068656 -strength_y 0.0002786696751527471
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00661213183496681 -strength_y 0.00034744701667113384
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006623054993935209 -strength_y 0.00021127740493561217
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.129635100699 -angle -5.25214138609e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.870364899301 -angle -3.52626679346e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006675004166196967 -strength_y 0.00015734457765244006
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068961599523310714 -strength_y -2.9172455622583557e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.689955100699 -angle -2.79533993447e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.310044899301 -angle -1.2561409976e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006914694605089331 -strength_y -0.00016906521514814998
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006982171253130692 -strength_y -0.00024366126777149473
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006925357196571979 -strength_y -0.00025734478058116514
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.250275100699 -angle -1.01398479825e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.749724899301 -angle -3.03749613382e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006961584031879606 -strength_y -0.00036684588530084135
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006867419801607172 -strength_y -0.0005666726684523753
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.810595100699 -angle -3.28411059411e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.189404899301 -angle -7.67370337958e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006839073869824981 -strength_y -0.0006250103132686925
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006901036804021503 -strength_y -0.000576105763970658
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007086102432027303 -strength_y -0.0004996474169883338
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.370915100699 -angle -1.5027554579e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.629084899301 -angle -2.54872547417e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007064722183071356 -strength_y -0.0003677283269682024
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007136439258514149 -strength_y -7.164016398832318e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.931235100699 -angle -3.77288125376e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0687648993007 -angle -2.78599678313e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006817563239606718 -strength_y -8.982788971213122e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006797900589582548 -strength_y -0.00017804912378160525
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00680448977101394 -strength_y -4.6989305505791055e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.491555100699 -angle -1.99152611755e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.508444899301 -angle -2.05995481453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007049996612840756 -strength_y 2.130734339011807e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007117470006443944 -strength_y 6.632030156678579e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00708609136629812 -strength_y 0.00014570778786201276
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0518751006993 -angle -2.10170981333e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.521404899301 -angle -2.11246200741e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0071099098135459496 -strength_y -8.918255434528868e-05
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007206699349651931 -strength_y -0.00010553489967816264
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007239288436466835 -strength_y -0.0001601713821755033
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0389151006993 -angle -1.57663788453e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.961084899301 -angle -3.89381714362e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007221321963009985 -strength_y -0.0002825282644069547
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00711635752136148 -strength_y -0.00034479806441293533
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.599235100699 -angle -2.42778958431e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.400764899301 -angle -1.62369134776e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007108592355787236 -strength_y -0.0003092263344642371
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007001491187028824 -strength_y -0.0003106724029764654
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007001882018986715 -strength_y -0.0003710064058260858
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.159555100699 -angle -6.46434448098e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.840444899301 -angle -3.40504648397e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007076243005757728 -strength_y -0.0002875758602552077
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007056150469741868 -strength_y -0.00022803046924141224
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.719875100699 -angle -2.91656024396e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.280124899301 -angle -1.13492068811e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070882651953388534 -strength_y -0.00013352953131716162
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068906146457581785 -strength_y -0.00015368131459620002
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006761703565988079 -strength_y -0.00013093127090112479
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.280195100699 -angle -1.13520510774e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.7198048993 -angle -2.91627582433e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006657263903259871 -strength_y -0.00021568550911141098
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066246864887428885 -strength_y -0.00043903258030287027
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.8405151007 -angle -3.4053309036e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.1594848993 -angle -6.46150028469e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006792784246686524 -strength_y -0.00043287872435620944
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007058819901971158 -strength_y -0.0004589870142692346
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007151015389608414 -strength_y -0.0005493689031377024
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.4008351007 -angle -1.62397576739e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.5991648993 -angle -2.42750516468e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007096824946475485 -strength_y -0.0005222482898314464
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007053293166811691 -strength_y -0.0006582258909173922
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.9611551007 -angle -3.89410156325e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0388448993003 -angle -1.57379368824e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0072043999818312025 -strength_y -0.0007961849344029428
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007121277986333636 -strength_y -0.0007189668973335271
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007178725930068623 -strength_y -0.0007266652749157469
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.5214751007 -angle -2.11274642703e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.4785248993 -angle -1.93873450504e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069142716184256775 -strength_y -0.0005885339600425563
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069539849635442815 -strength_y -0.0004760859932346573
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006873472995724914 -strength_y -0.0003473954295386634
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0817951006997 -angle -3.31391290822e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.4914848993 -angle -1.99124169792e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006840226517820254 -strength_y -0.0003835992911529893
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006851745374901861 -strength_y -0.0003485761253096212
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00687266022204962 -strength_y -0.00026853593206325766
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.0688351006997 -angle -2.78884097942e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.9311648993 -angle -3.77259683413e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006752884769154914 -strength_y -0.00020599583909512183
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00669006308550501 -strength_y -0.0002957363923098755
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.6291551007 -angle -2.5490098938e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.3708448993 -angle -1.50247103827e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006435009952275802 -strength_y -0.0002725344250823289
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006475452569349565 -strength_y -0.0003892625617923385
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066068066386222955 -strength_y -0.0007567457327131051
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.1894751007 -angle -7.67654757587e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.8105248993 -angle -3.28382617448e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006746273993293206 -strength_y -0.0007826074877698591
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00676488508360027 -strength_y -0.0006996544489323372
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.7497951007 -angle -3.03778055345e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.2502048993 -angle -1.01370037863e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006790548995542632 -strength_y -0.0008304421726853731
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067383990608119605 -strength_y -0.000815778028371435
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066140071021411305 -strength_y -0.0008297795438441437
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.3101151007 -angle -1.25642541723e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.6898848993 -angle -2.79505551484e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006666922803254338 -strength_y -0.0009259921049077903
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006673697478635504 -strength_y -0.0006930867629054569
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.8704351007 -angle -3.52655121309e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.1295648993 -angle -5.2492971898e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006660645842988966 -strength_y -0.0005138120679849088
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00647610169263733 -strength_y -0.00046905205429949596
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006510953566266993 -strength_y -0.00026203914411237345
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.4307551007 -angle -1.74519607688e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.5692448993 -angle -2.30628485519e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006707842931619968 -strength_y -0.00023250019293165295
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006737978970556742 -strength_y -0.00023267748592497363
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.9910751007 -angle -4.01532187274e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.00892489929993 -angle -3.61590593343e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006783530564938951 -strength_y -0.0002603921405831354
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006860958818739488 -strength_y -0.00012163059936641064
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006959742309382796 -strength_y -1.9571198417919644e-05
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.5513951007 -angle -2.23396673652e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.4486048993 -angle -1.81751419555e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006896028905580528 -strength_y -2.8262628643333198e-05
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006818831803644418 -strength_y -0.00015563374725506907
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006841616013776133 -strength_y -0.00023935363482586037
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.1117151007 -angle -4.52611600311e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.4615648993 -angle -1.87002138843e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007104629784119525 -strength_y -0.00018982836616315745
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007113982179687378 -strength_y 2.5357306330725205e-05
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007046036521151243 -strength_y 0.0001438654381476757
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.0987551007001 -angle -4.00104407431e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.9012448993 -angle -3.65137652464e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006869607902619934 -strength_y 6.537031119696152e-05
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006767993754043797 -strength_y 0.00011385836428126925
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.6590751007 -angle -2.67023020329e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.3409248993 -angle -1.38125072878e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006665711589672436 -strength_y 0.0002111603654167611
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006695177904312532 -strength_y 0.00023910331889835104
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006687012423468296 -strength_y 0.0002825288388338109
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.2193951007 -angle -8.88875067077e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.7806048993 -angle -3.16260586499e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006612299915518134 -strength_y 0.0003472446193812742
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006625421753116661 -strength_y 0.00021523125445836229
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.7797151007 -angle -3.15900086293e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.2202848993 -angle -8.92480069136e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006665905736081616 -strength_y 0.00016051608693512187
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00689261341510026 -strength_y -2.0192226539137532e-05
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006912370054163944 -strength_y -0.00016619352290368297
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.3400351007 -angle -1.37764572672e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.6599648993 -angle -2.67383520535e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006982400782607309 -strength_y -0.00024261288486564418
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00692539825599467 -strength_y -0.0002573047990342103
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.9003551007 -angle -3.64777152258e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.0996448992996 -angle -4.03709409491e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006960616926066173 -strength_y -0.00037380043949231323
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006868005090188271 -strength_y -0.0005657059512812637
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006839982563472446 -strength_y -0.0006244577807768749
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.4606751007 -angle -1.86641638637e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.5393248993 -angle -2.1850645457e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006897178492657112 -strength_y -0.0005780378989891822
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007086910530719873 -strength_y -0.0004986700194397907
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007067241987525213 -strength_y -0.00035816450211131586
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.0209951007004 -angle -8.50612501547e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.9790048993 -angle -3.96641968192e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007136999529919297 -strength_y -7.246167464994169e-05
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006828145928345437 -strength_y -8.367421831633636e-05
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.5813151007 -angle -2.35518704601e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.418684899299 -angle -1.69629388606e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0067949662407206885 -strength_y -0.0001793454928521314
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006802448705829709 -strength_y -5.312343692150308e-05
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -4.05148093207e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007038060781120874 -strength_y 2.125832709895099e-05
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.141635100701 -angle -5.738319098e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "D2OD" -length 0.858364899299 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007144181969350931 -strength_y 3.202058285835343e-05
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007097879251450215 -strength_y 8.893575062470946e-05
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007075988868800783 -strength_y 0.00014526458333027662
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070976642048342815 -strength_y 0.0001162605903382725
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007075628890711076 -strength_y 1.356012839383733e-05
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007060050076386364 -strength_y -6.815995813008132e-05
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007133235751533851 -strength_y -9.173375275455141e-05
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007220176376901148 -strength_y -0.00011316421383625102
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0072384764444759254 -strength_y -0.00017434123302051925
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007228609716996542 -strength_y -0.0002487862010140908
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007207911946042795 -strength_y -0.0003041041105006623
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007143248911264414 -strength_y -0.0003382339926935878
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007091037416371478 -strength_y -0.00034440404615206386
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00710521069192766 -strength_y -0.0003156204992262257
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007090559524771421 -strength_y -0.00028975593383625756
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0069938366532008986 -strength_y -0.00031424467831984786
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006952996202555966 -strength_y -0.00036261619106081535
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070348660522108345 -strength_y -0.0003638672220831962
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007090230021293902 -strength_y -0.00031305905098451205
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0070484497758589075 -strength_y -0.0002675124996701298
Drift -name "D2OD" -length 0.969260460701 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT2" -length 0.0307395392995 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007047922660074959 -strength_y -0.00023354402008306826
Drift -name "OCTDRIFT2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007037719434208597 -strength_y -0.0006365547869001923
Drift -name "OCTDRIFT2" -length 0.0525937937005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCT2" -synrad $mult_synrad -type 4 -length 0.5 -strength [expr -1.0*0.2794726188*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT3" -length 0.4166666667 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF1" -synrad $mult_synrad -type 3 -length 0.0307395395996 -strength [expr -0.00826400521232*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.007125000987044641 -strength_y -0.0007177807205447643
Multipole -name "SF1" -synrad $mult_synrad -type 3 -length 0.8227004604 -strength [expr -0.221174454188*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "LX0" -length 0.1772995396 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.00717644305645697 -strength_y -0.0007259751208186662
Drift -name "LX0" -length 0.2494204604 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.7505795396 -strength [expr 0.0101285123946*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006988751620871879 -strength_y -0.0006445944962866834
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.0 -strength [expr 0.0134942559186*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068968365674699295 -strength_y -0.0005562716459447837
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.0 -strength [expr 0.0134942559186*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006934932885564662 -strength_y -0.0004976078175218309
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.0 -strength [expr 0.0134942559186*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006952611975830696 -strength_y -0.00044318310969731204
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.0 -strength [expr 0.0134942559186*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006890459930764468 -strength_y -0.0003673092949687613
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.8437196604 -strength [expr 0.011385369021*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1OD" -length 0.1562803396 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006864547964250344 -strength_y -0.0002468144861157812
Drift -name "D1OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006799079878807319 -strength_y -0.00020487133611009364
Drift -name "D1OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006736144923834686 -strength_y -0.00021591488708208739
Drift -name "D1OD" -length 0.3376396604 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "OCTDRIFT" -length 0.6623603396 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0066858441900805015 -strength_y -0.00029757531015990856
Drift -name "OCTDRIFT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006409725270342518 -strength_y -0.0002673689817335549
Drift -name "OCTDRIFT" -length 0.3376396604 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DEC0" -length 0.6623603396 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006582304409962493 -strength_y -0.0006587295044390057
Drift -name "DEC0" -length 0.1910796604 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD0" -synrad $mult_synrad -type 3 -length 0.3822003396 -strength [expr 0.37710461264*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006744566231257814 -strength_y -0.0007842725943771244
Multipole -name "SD0" -synrad $mult_synrad -type 3 -length 0.4712396604 -strength [expr 0.46495680716*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD0" -synrad $quad_synrad -length 0.1020403396 -strength [expr -0.00399850320432*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006750609901597025 -strength_y -0.0007247438196108116
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.0 -strength [expr -0.039185514474*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006767489146398976 -strength_y -0.0008005730082541205
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.0 -strength [expr -0.039185514474*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0068110959578403345 -strength_y -0.0008492287934855744
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.0 -strength [expr -0.039185514474*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006785914278269638 -strength_y -0.0008364278016176934
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.0 -strength [expr -0.039185514474*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006687068588505807 -strength_y -0.000798924424395653
Quadrupole -name "QD0" -synrad $quad_synrad -length 0.5918796604 -strength [expr -0.0231931089995*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D0" -length 0.4081203396 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006677412962206904 -strength_y -0.0006663402549344962
Drift -name "D0" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006689754090317663 -strength_y -0.0005456599838618303
Drift -name "D0" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006646883960556593 -strength_y -0.000512822457502813
Drift -name "D0" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006551041308263511 -strength_y -0.00051102294109312
Drift -name "D0" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006466975454471658 -strength_y -0.0004559716355472507
Drift -name "D0" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.006454017885998716 -strength_y -0.00034596690999153093
Drift -name "D0" -length 0.6086316604 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
