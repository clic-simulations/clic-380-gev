# Just to make sure that the path to the libraires is defined 
from sys import path
path.append('/Users/gohil/cernbox/simulations/clic_380_gev/twiss_parameters/bds' + \
            '/madx_calculations/Python_Classes4MAD')
path.append('/Users/gohil/cernbox/simulations/clic_380_gev/twiss_parameters/bds' + \
            '/madx_calculations/MapClass2')

import math
import sys
import random
from os import system
from mapclass25  import *
from metaclass25 import *

# BDS start condition
betx  = 41.18752699632758
bety  = 5.462815023282626
ex    = 950*1e-9
ey    = 30*1e-9
gamma = 371820.7409

sigmaFFS = [sqrt(ex*betx/gamma), sqrt(ex/betx/gamma), sqrt(ey*bety/gamma),
            sqrt(ey/bety/gamma), 0.01]

file1 = open('sigmas_new.dat', 'w')
file = 'fort.18'

map = Map(1,file)
print ";"
sigx1= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax1=",sigx1,";"
sigy1= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay1=",sigy1,";"
sigpx1= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx1=",sigpx1,";"
sigpy1= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy1=",sigpy1,";"
print >> file1,1,sigx1,sigy1,sigpx1,sigpy1

map=Map(2,file)
print ";"
sigx2= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax2=",sigx2,";"
sigy2= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay2=",sigy2,";"
sigpx2= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx2=",sigpx2,";"
sigpy2= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy2=",sigpy2,";"
print >> file1,2,sigx2,sigy2,sigpx2,sigpy2

map=Map(3,file)
print ";"
sigx3= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax3=",sigx3,";"
sigy3= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay3=",sigy3,";"
sigpx3= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx3=",sigpx3,";"
sigpy3= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy3=",sigpy3,";"
print >> file1,3,sigx3,sigy3,sigpx3,sigpy3

map=Map(4,file)
print ";"
sigx4= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax4=",sigx4,";"
sigy4= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay4=",sigy4,";"
sigpx4= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx4=",sigpx4,";"
sigpy4= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy4=",sigpy4,";"
print >> file1,4,sigx4,sigy4,sigpx4,sigpy4

map=Map(5,file)
print ";"
sigx5= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax5=",sigx5,";"
sigy5= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay5=",sigy5,";"
sigpx5= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx5=",sigpx5,";"
sigpy5= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy5=",sigpy5,";"
print >> file1,5,sigx5,sigy5,sigpx5,sigpy5

map=Map(6,file)
print ";"
sigx6= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax6=",sigx6,";"
sigy6= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay6=",sigy6,";"
sigpx6= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx6=",sigpx6,";"
sigpy6= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy6=",sigpy6,";"
print >> file1,6,sigx6,sigy6,sigpx6,sigpy6

map=Map(7,file)
print ";"
sigx7= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax7=",sigx7,";"
sigy7= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay7=",sigy7,";"
sigpx7= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx7=",sigpx7,";"
sigpy7= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy7=",sigpy7,";"
print >> file1,7,sigx7,sigy7,sigpx7,sigpy7

map=Map(8,file)
print ";"
sigx8= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax8=",sigx8,";"
sigy8= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay8=",sigy8,";"
sigpx8= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx8=",sigpx8,";"
sigpy8= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy8=",sigpy8,";"
print >> file1,8,sigx8,sigy8,sigpx8,sigpy8

map=Map(9,file)
print ";"
sigx9= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax9=",sigx9,";"
sigy9= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay9=",sigy9,";"
sigpx9= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx9=",sigpx9,";"
sigpy9= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy9=",sigpy9,";"
print >> file1,9,sigx9,sigy9,sigpx9,sigpy9

map=Map(10,file)
print ";"
sigx10= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax10=",sigx10,";"
sigy10= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay10=",sigy10,";"
sigpx10= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx10=",sigpx10,";"
sigpy10= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy10=",sigpy10,";"
print >> file1,10,sigx10,sigy10,sigpx10,sigpy10
sys.exit()

print "corryx=",map.correlation('y','x',sigmaFFS)-(map.offset('y',sigmaFFS)*map.offset('x',sigmaFFS)),";"
print "corrypx=",map.correlation('py','x',sigmaFFS)-(map.offset('py',sigmaFFS)*map.offset('x',sigmaFFS)),";"
print "corrpyx=",map.correlation('y','px',sigmaFFS)-(map.offset('y',sigmaFFS)*map.offset('px',sigmaFFS)),";"
print "corrpypx=",map.correlation('py','px',sigmaFFS)-(map.offset('py',sigmaFFS)*map.offset('px',sigmaFFS)),";"
print "corryd=",map.correlation('y','d',sigmaFFS),";"
