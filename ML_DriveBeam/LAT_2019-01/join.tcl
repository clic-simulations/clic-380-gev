set l0 [lindex $argv 0]
set f0 [lindex $argv 1]
# 1.8 1.56
set alpha 0.5
set beta 0.5
set scale [lindex $argv 2]
set gradient 0.072

exec make scale
exec ./scale $l0 $f0 $alpha $beta $scale $gradient

exec make match
exec ./match

set f [open match.ini]
gets $f l
set bx [lindex $l 1]
set ax [lindex $l 2]
gets $f l
set by [lindex $l 1]
set ay [lindex $l 2]
close $f
set f [open lattice.ini r]
set f2 [open sectors.out r]
gets $f line
set n $line
set sectors {}
for {set i 0} {$i<$n} {incr i} {
    gets $f line
    gets $f2 line2
    lappend sectors $line
    lappend sectors $line2
}
close $f
close $f2

set f [open lattice.def w]
puts $f "\# Lattice scaling was $l0 $f0 $alpha $beta $scale"
puts $f ""
puts $f "variable sectors \{"
foreach x $sectors {
    puts $f "\"$x\""
}
puts $f "\}"
puts $f ""
puts $f "variable set_match \{"
puts $f "    alpha_x $ax beta_x $bx alpha_y $ay beta_y $by"
puts $f "\}"
close $f
