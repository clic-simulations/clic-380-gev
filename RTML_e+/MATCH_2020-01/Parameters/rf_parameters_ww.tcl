set rfparamsbooster(gradientww)   1.834481502848e+07
set rfparamsbc1(gradientww)   2.230469974985e+07
set rfparamsbc2(gradientww)   7.450479565942e+07
set energy_wake_bc1  -3.529448703929e-05
set energy_wake_booster  -5.771336110289e-05
set energy_wake_bc2  -7.431954230433e-05
