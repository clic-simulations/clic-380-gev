set rfparamsbooster(gradientww)   1.834483191271e+07
set rfparamsbc1(gradientww)   2.230382815330e+07
set rfparamsbc2(gradientww)   7.454014391010e+07
set energy_wake_bc1  -3.525103153025e-05
set energy_wake_booster  -5.774418788889e-05
set energy_wake_bc2  -7.431122160451e-05
