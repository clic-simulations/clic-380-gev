#
# This script sets up the ML beamline wakefields.
# The short-range wakefields are created with "placet Wakefields/create_wake.tcl"
#

# Long-range wakefields
source $scriptdir/clic_basic_single.tcl

# Lattice
BeamlineNew
source ML_Lattices/machine-$machine.tcl
BeamlineSet -name ml

# Short-range wakefields
# To generate Wt_cav.dat and Wl_cav.dat run "placet Scripts/create_wake.tcl"
SplineCreate "cav_Wt" -file "$wakedir/Wt_ml_cav.dat"
SplineCreate "cav_Wl" -file "$wakedir/Wl_ml_cav.dat"
ShortRangeWake "cav_SR" -type 2  -wx "cav_Wt" -wy "cav_Wt" -wz "cav_Wl"

Octave {
    AI = placet_get_number_list("ml", "cavity");
    placet_element_set_attribute("ml", AI, "six_dim", true);
    placet_element_set_attribute("ml", AI, "short_range_wake", "cav_SR");
}

