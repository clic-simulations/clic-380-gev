proc my_survey {beamline} {
    global machine sigma bpmres
    Octave {

    sigma_pos_sr = 30;
    sigma_res_sr = 1;
    sigma_pos_bc2 = 30;
    sigma_res_bc2 = 1;

    roll = 100;

    randn("seed", $machine * 1e4);

    Start_BC1  = placet_get_name_number_list("rtml","Marker-BC1-start");
    Start_BC2  = placet_get_name_number_list("rtml","Marker-BC2-start");

    magnets = placet_get_number_list("$beamline", "quadrupole");
    magnets = [ magnets placet_get_number_list("$beamline", "sbend") ];
    magnets = [ magnets placet_get_number_list("$beamline", "multipole") ];

    magnets1 = magnets(magnets < Start_BC1);
    magnets2 = magnets(magnets >= Start_BC1 & magnets < Start_BC2);
    magnets3 = magnets(magnets >= Start_BC2);


    placet_element_set_attribute("$beamline", magnets, "x", [randn(size(magnets1))*sigma_pos_sr randn(size(magnets2))*$sigma randn(size(magnets3))*sigma_pos_bc2]);
    placet_element_set_attribute("$beamline", magnets, "y", [randn(size(magnets1))*sigma_pos_sr randn(size(magnets2))*$sigma randn(size(magnets3))*sigma_pos_bc2]);
    placet_element_set_attribute("$beamline", magnets, "xp", [randn(size(magnets1))*roll randn(size(magnets2))*roll randn(size(magnets3))*roll]);
    placet_element_set_attribute("$beamline", magnets, "yp", [randn(size(magnets1))*roll randn(size(magnets2))*roll randn(size(magnets3))*roll]);
    placet_element_set_attribute("$beamline", magnets, "roll", [randn(size(magnets1))*roll randn(size(magnets2))*roll randn(size(magnets3))*roll]);

    Start_CA  = placet_get_name_number_list("rtml","Marker-CA-start");
    End_CA  = placet_get_name_number_list("rtml","Marker-CA-end");
    Start_TAL  = placet_get_name_number_list("rtml","Marker-TAL1-start");
    End_TAL  = placet_get_name_number_list("rtml","Marker-TAL2-end");

    magnets = placet_get_number_list("$beamline", "quadrupole");
    magnets1 =  magnets((magnets >Start_CA & magnets<End_CA) | (magnets >Start_TAL & magnets<End_TAL));
    magnets2 =  magnets(magnets<=Start_CA | ( magnets>=End_CA & magnets<=Start_TAL) | magnets>=End_TAL);
    STRENGTH1 = placet_element_get_attribute("$beamline", magnets1, "strength");
    STRENGTH2 = placet_element_get_attribute("$beamline", magnets2, "strength");
    placet_element_set_attribute("$beamline", magnets1, "strength", STRENGTH1.*(1+0.0001*randn(size(magnets1))'));
    placet_element_set_attribute("$beamline", magnets2, "strength", STRENGTH2.*(1+0.001*randn(size(magnets2))'));

    magnets = placet_get_number_list("$beamline", "multipole");
    STRENGTH = placet_element_get_attribute("$beamline", magnets, "strength");
    placet_element_set_attribute("$beamline", magnets, "strength", STRENGTH.*(1+0.001*randn(size(magnets))'));

    magnets = placet_get_number_list("$beamline", "sbend");
    E0 = placet_element_get_attribute("$beamline", magnets, "e0");
    placet_element_set_attribute("$beamline", magnets, "e0", E0.*(1+0.001*randn(size(magnets))'));

    quadrupoles = placet_get_number_list("$beamline", "quadrupole");
    multipoles = placet_get_number_list("$beamline", "multipole");

    x_quad_all = randn(size(quadrupoles));
    y_quad_all = randn(size(quadrupoles));

    x_multi_all = randn(size(multipoles));
    y_multi_all = randn(size(multipoles));

    save -text movement_all.dat x_quad_all y_quad_all x_multi_all y_multi_all;

    BI = placet_get_number_list("$beamline","bpm");

    BI1 = BI(BI < Start_BC1);
    BI2 = BI(BI >= Start_BC1 & BI < Start_BC2);
    BI3 = BI(BI >= Start_BC2);

    placet_element_set_attribute("$beamline",BI, "x", [randn(size(BI1))*sigma_pos_sr randn(size(BI2))*$sigma randn(size(BI3))*sigma_pos_bc2]);
    placet_element_set_attribute("$beamline",BI, "y", [randn(size(BI1))*sigma_pos_sr randn(size(BI2))*$sigma randn(size(BI3))*sigma_pos_bc2]);
    placet_element_set_attribute("$beamline",BI, "xp", [randn(size(BI1))*roll randn(size(BI2))*roll randn(size(BI3))*roll]);
    placet_element_set_attribute("$beamline",BI, "yp", [randn(size(BI1))*roll randn(size(BI2))*roll randn(size(BI3))*roll]);
    placet_element_set_attribute("$beamline",BI, "roll", [randn(size(BI1))*roll randn(size(BI2))*roll randn(size(BI3))*roll]);


    placet_element_set_attribute("$beamline",BI, "resolution",sigma_res_sr);
    placet_element_set_attribute("$beamline",BI2, "resolution",$bpmres);
    placet_element_set_attribute("$beamline",BI3, "resolution",sigma_res_bc2);
    }
}
