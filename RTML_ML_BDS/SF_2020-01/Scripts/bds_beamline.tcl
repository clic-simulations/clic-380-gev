#
# This script setups the Beam Delivery System (BDS)
#
# Either the L*=4.3 m or L*=6 m BDS is setup. The variable bds_type is set as a
# command line argument.
#
if {$bds_type == "43"} {
    puts "Using L*=4.3 m BDS lattice"
    BeamlineNew
    source $latticedir/2043_bds_dipole_grid.tcl
    BeamlineSet -name bds
} elseif {$bds_type == "6"} {
    puts "Using L*=6 m BDS lattice"
    BeamlineNew
    source $latticedir/2006_bds_dipole_grid.tcl
    BeamlineSet -name bds
} else {
    puts "Error in main_e-.tcl: bds type not correctly specified; use 43 or 6."
    exit
}
