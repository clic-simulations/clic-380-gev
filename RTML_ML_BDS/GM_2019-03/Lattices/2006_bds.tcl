Girder
Bpm
Quadrupole -name "FQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.1593135711*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "FDD" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQD" -synrad $quad_synrad -length 0.5 -strength [expr -0.06708030646*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "FDD" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQF2" -synrad $quad_synrad -length 0.5 -strength [expr -0.1182922355*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "FDD" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQD2" -synrad $quad_synrad -length 0.5 -strength [expr 0.02788484518*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Bpm
Quadrupole -name "TQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.1149808129*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD" -synrad $quad_synrad -length 1 -strength [expr -0.1179949196*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF2A" -synrad $quad_synrad -length 0.5 -strength [expr 0.1033739537*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF2B" -synrad $quad_synrad -length 0.5 -strength [expr 0.1033739537*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD2A" -synrad $quad_synrad -length 0.5 -strength [expr -0.08499462268*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD2B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08499462268*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF3A" -synrad $quad_synrad -length 0.5 -strength [expr 0.05660408062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF3B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05660408062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD3A" -synrad $quad_synrad -length 0.5 -strength [expr -0.07925837696*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD3B" -synrad $quad_synrad -length 0.5 -strength [expr -0.07925837696*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF4A" -synrad $quad_synrad -length 0.5 -strength [expr 0.1039004288*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF4B" -synrad $quad_synrad -length 0.5 -strength [expr 0.1039004288*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD4A" -synrad $quad_synrad -length 0.5 -strength [expr -0.08989292421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD4B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08989292421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF5A" -synrad $quad_synrad -length 0.5 -strength [expr 0.05764637677*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF5B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05764637677*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD5" -synrad $quad_synrad -length 1 -strength [expr -0.1107619552*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF6A" -synrad $quad_synrad -length 0.5 -strength [expr 0.02612552684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF6B" -synrad $quad_synrad -length 0.5 -strength [expr 0.02612552684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD6A" -synrad $quad_synrad -length 0.5 -strength [expr -0.02002336697*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD6B" -synrad $quad_synrad -length 0.5 -strength [expr -0.02002336697*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF7A" -synrad $quad_synrad -length 0.5 -strength [expr 0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF7B" -synrad $quad_synrad -length 0.5 -strength [expr 0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD7A" -synrad $quad_synrad -length 0.5 -strength [expr -0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD7B" -synrad $quad_synrad -length 0.5 -strength [expr -0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF8A" -synrad $quad_synrad -length 0.5 -strength [expr 0.0200862619*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF8B" -synrad $quad_synrad -length 0.5 -strength [expr 0.0200862619*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD8" -synrad $quad_synrad -length 1 -strength [expr -0.1038009056*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL89" -length 3.794391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF9" -synrad $quad_synrad -length 1 -strength [expr -0.1524476254*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDLE" -length 2.020466817 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD9" -synrad $quad_synrad -length 1 -strength [expr 0.134790853*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "YGIRDER" -length 2.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*463.8761985*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC4" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-458.648884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DSXL" -length 2.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D1BC" -length 0.1 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BC" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BC" -length 11.633515 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D4BC" -length 0.1925765 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SEXIT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -20*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "BTFD0" -length 0.02536345 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 2.5 -strength [expr -0.1209993*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD1" -length 4.3563465 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 2.5 -strength [expr 0.03044046*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD2" -length 9.99755 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 2.5 -strength [expr 0.05048062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD3" -length 9.98829 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 2.5 -strength [expr -0.0666287*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD4" -length 0.02627595 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "LMD4" -length 9.75656061 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD11" -synrad $quad_synrad -length 2.8 -strength [expr 0.17223427*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD5" -length 5.942814728 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD12" -synrad $quad_synrad -length 2.8 -strength [expr -0.1308754807*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD6" -length 10.08 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD13" -synrad $quad_synrad -length 2.8 -strength [expr 0.1205441484*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD7" -length 10.08 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD14" -synrad $quad_synrad -length 2.8 -strength [expr -0.07891769534*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD8" -length 9.66644 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF8" -synrad $quad_synrad -length 2.77368 -strength [expr -0.01187738912*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D8" -length 12.8016 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD7" -synrad $quad_synrad -length 2.77368 -strength [expr -0.009338367703*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.944368 -angle 1.276131017e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*1.276131017e-05*1.276131017e-05/2.944368*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD6C" -synrad $quad_synrad -length 2.77368 -strength [expr -0.03270751056*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D7" -length 51.2064 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD6B" -synrad $quad_synrad -length 2.77368 -strength [expr -0.04735002969*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD6B" -synrad $quad_synrad -length 2.77368 -strength [expr -0.04735002969*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D6" -length 49.3800384 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF6" -synrad $mult_synrad -type 3 -length 1.3996416 -strength [expr 0.8569979448*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5B" -synrad $quad_synrad -length 2.77368 -strength [expr 0.02578467487*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5B" -synrad $quad_synrad -length 2.77368 -strength [expr 0.02578467487*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D5OD" -length 43.62395627 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD5L" -synrad $mult_synrad -type 3 -length 1 -strength [expr 0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SD5" -synrad $mult_synrad -type 3 -length 1 -strength [expr -1.452117464*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SD5R" -synrad $mult_synrad -type 3 -length 1 -strength [expr 0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D5ODT" -length 17 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5A" -synrad $quad_synrad -length 2.77368 -strength [expr 0.02358002934*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5A" -synrad $quad_synrad -length 2.77368 -strength [expr 0.02358002934*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF5" -synrad $mult_synrad -type 3 -length 1.3996416 -strength [expr 0.2099968277*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D4OD" -length 13.14505236 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT2" -length 1.083333333 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCT1" -synrad $mult_synrad -type 4 -length 0.5 -strength [expr -1.0*8.882502817*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT3" -length 0.4166666667 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD4B" -synrad $quad_synrad -length 2.77368 -strength [expr -0.01556421441*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD4" -synrad $mult_synrad -type 3 -length 1.3996416 -strength [expr 0.5706642133*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD4A" -synrad $quad_synrad -length 2.77368 -strength [expr -0.01563201431*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3" -length 40.49673933 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 12.58824 -angle -0.0001697622136 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001697622136*-0.0001697622136/12.58824*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF3B" -synrad $quad_synrad -length 2.77368 -strength [expr 0.003811647715*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF3A" -synrad $quad_synrad -length 2.77368 -strength [expr 0.01565315628*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 2.56032 -angle -3.452790783e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-3.452790783e-05*-3.452790783e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD2" -synrad $quad_synrad -length 2.77368 -strength [expr -0.01846178668*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 2.56032 -angle -1.037308766e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-1.037308766e-05*-1.037308766e-05/2.56032*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "D2OD" -length 20.82762536 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT2" -length 1.083333333 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCT2" -synrad $mult_synrad -type 4 -length 0.5 -strength [expr -1.0*0.2794726188*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT3" -length 0.4166666667 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
if {"$mitigation" == "preisolator-3" || "$mitigation" == "quad-stab-preisolator-3" || "$mitigation" == "recursive-preisolator-3" || "$mitigation" == "recursive-quad-stab-preisolator-3"} {
    Girder
    Multipole -name "SF1" -synrad $mult_synrad -type 3 -length 0.85344 -strength [expr -0.2294384594*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Bpm
    Quadrupole -name "QF1" -synrad $quad_synrad -length 5.5942992 -strength [expr 0.07549090509*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Dipole
    Drift -name "D1OD" -length 2.49392 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Drift -name "OCTDRIFT" -length 2 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Drift -name "DEC0" -length 0.85344 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Multipole -name "SD0" -synrad $mult_synrad -type 3 -length 0.85344 -strength [expr 0.8420614198*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Bpm
    Quadrupole -name "QD0" -synrad $quad_synrad -length 4.69392 -strength [expr -0.1839336701*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Dipole
    Girder
    Drift -name "D0" -length 6.016752 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
} else {
    Girder
    Multipole -name "SF1" -synrad $mult_synrad -type 3 -length 0.85344 -strength [expr -0.2294384594*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Girder
    Bpm
    Quadrupole -name "QF1" -synrad $quad_synrad -length 5.5942992 -strength [expr 0.07549090509*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Dipole
    Girder
    Drift -name "D1OD" -length 2.49392 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Drift -name "OCTDRIFT" -length 2 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Drift -name "DEC0" -length 0.85344 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Girder
    Multipole -name "SD0" -synrad $mult_synrad -type 3 -length 0.85344 -strength [expr 0.8420614198*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Girder
    Drift -name "LX0" -length 0.42672 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Girder
    Bpm
    Quadrupole -name "QD0" -synrad $quad_synrad -length 4.69392 -strength [expr -0.1839336701*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
    Dipole
    Girder
    Drift -name "D0" -length 6.016752 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
}
