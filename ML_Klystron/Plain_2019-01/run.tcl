#
# CLIC 380 GeV: Main e- Beam (ML - Drive Beam Design)
#
# This code was developed by C. Gohil from a simulation inherited from
# N. Blaskovic Kraljevic.
#
# To run: "placet run.tcl <beam_seed>", if no beam seed is given, the default is 0.
#
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set latticedir ./Lattices
set wakedir    ./Wakefields
set beamdir    ./Beams
set resultsdir ./Results

# Initialise a timer
Octave {initial = tic();}

# Set the number of cores to run in parallel
ParallelThreads -num 4

# Load parameters
source $scriptdir/set_parameters.tcl

# Setup the lattice
source $scriptdir/beamline_and_wake.tcl

# Setup the beam
source $scriptdir/clic_beam.tcl
make_beam_many beam0 $n_slice $n

# Tracking
source $scriptdir/octave_functions.tcl
Octave {
    [E, B] = placet_test_no_correction("ml", "beam0", "None", "%s %ex %ey %E %dE");
    disp_results(B);
    save -text $resultsdir/emitt.dat E;
    save_beam("$beamdir/particles.out", B);

    # Display how long the simulation took
    toc(initial);
}

exit
