#
# Diagnostics section
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_diagnostics_1 {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 10
set quad_synrad 0

set refenergy $beamparams(meanenergy)

set lquadm 0.3

set kqm1 0.676808031
set kqm2 -0.8234632887

set ldm 3.0

SetReferenceEnergy $refenergy

Girder

for {set cell 1} {$cell<=14} {incr cell} {
  Drift -length $ldm -six_dim $usesixdim
  Bpm -length 0.0
  Quadrupole -name "SRdiag" -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
  Dipole -name "DIQ-SRdiag-${cell}-2" -length 0.00000000000
  Drift -length $ldm -six_dim $usesixdim
  Bpm -length 0.0
  Quadrupole -name "SRdiag" -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
  Dipole -name "DIQ-SRdiag-${cell}-2" -length 0.00000000000
}

Drift -length $ldm -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SRdiag" -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SRdiag-1" -length 0.00000000000
Drift -length $ldm -six_dim $usesixdim
}
