#
# matching of Turn Around Loop to BC2 RF
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_tal_to_bc2_rf {bparray} {
    upvar $bparray beamparams

    set usesixdim 1
    set numthinlenses 10
    set quad_synrad 0

    set refenergy $beamparams(meanenergy)

    SetReferenceEnergy $refenergy
    
    # From match_0200.tcl
    set mk1 0.433248792530606
    set mk2 -0.315399063152534
    set ml1 2.57024477373983
    set ml2 4.96895249070090

    Quadrupole -name "matching-quad" -synrad $quad_synrad -length 0.3 -strength [expr $mk1*$refenergy] -e0 $refenergy
    Drift -name "matching-drift" -length $ml1
    Quadrupole -name "matching-quad" -synrad $quad_synrad -length 0.3 -strength [expr $mk2*$refenergy] -e0 $refenergy
    Drift -name "matching-drift" -length $ml2
}
