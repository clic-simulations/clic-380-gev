#
# This script sets up the RTML lattice from the individual lattice files for each
# section in $latticedir.
#

# Initialise long-range wakefields in BC1 and the Booster
source $wakedir/cavity_wake_setup.tcl
set wakelongrangedata(nmodes) 0
set wakelongrangedata(modes) {}
initialize_cavities rfparamsbc1 wakelong1 wakelongrangedata

#
# Lattice
#
BeamlineNew

Girder
Drift -name "MARKER_MATCH_DR_TO_RTML"
lattice_match_dr_to_rtml beamparams

Drift -name "MARKER_DIAGNOSTICS1"
lattice_diagnostics_1 beamparams

Drift -name "MARKER_MATCH_DIAG1_TO_SR"
lattice_dump_and_match_diag_to_sr beamparams

Drift -name "MARKER_SPIN_ROTATOR"
lattice_spin_rotator beamparams

Drift -name "MARKER_MATCH_SR_TO_BC1"
lattice_match_sr_to_bc1_rf beamparams

Drift -name "MARKER_BC1_RF"
lattice_bc1_rf rfparamsbc1 beamparams

Drift -name "MARKER_MATCH_BC1_RF_TO_CHICANE"
lattice_match_bc1_rf_to_chicane beamparams

Drift -name "MARKER_BC1_CHICANE"
lattice_bc1_chicane beamparams

Drift -name "MARKER_MATCH_BC1_TO_DIAG2"
lattice_match_bc1_to_diag beamparams

Drift -name "MARKER_DIAGNOSTICS2"
lattice_diagnostics_2 beamparams

Drift -name "MARKER_MATCH_DIAG2_TO_BOOSTER"
lattice_dump_and_match_diag_to_booster beamparams

Drift -name "MARKER_BOOSTER"
lattice_booster_linac rfparamsbooster beamparams

Drift -name "MARKER_MATCH_BOOSTER_TO_CA"
lattice_dump_and_match_booster_to_ca beamparams

Drift -name "MARKER_CA"
lattice_central_arc beamparams

Drift -name "MARKER_VT"
lattice_vertical_transfer beamparams

Drift -name "MARKER_MATCH_VT_TO_LTL"
lattice_match_vt_to_ltl beamparams

Drift -name "MARKER_LTL"
lattice_long_transfer_line beamparams

Drift -name "MARKER_MATCH_LTL_TO_TAL"
lattice_dump_and_match_ltl_to_tal beamparams

Drift -name "MARKER_MATCH_TAL"
lattice_turn_around_loop beamparams

Drift -name "MARKER_MATCH_TAL_TO_BC2_RF"
lattice_match_tal_to_bc2_rf beamparams

Drift -name "MARKER_BC2_RF"
# Long-range wakefields in BC2
WakeSet rtml_Wlong_bc2 {}
InjectorCavityDefine -lambda 0.025 -wakelong rtml_Wlong_bc2
lattice_bc2_rf rfparamsbc2 beamparams

Drift -name "MARKER_BC2_RF_TO_CHICANE"
lattice_match_bc2_rf_to_chicane_1 beamparams

Drift -name "MARKER_BC2_CHICANE1"
lattice_bc2_chicane_1 beamparams

Drift -name "MARKER_BC2_CHICANE1_TO_BC2_CHICANE2"
lattice_match_bc2_chicanes beamparams

Drift -name "MARKER_BC2_CHICANE2"
lattice_bc2_chicane_2 beamparams

Drift -name "MARKER_MATCH_BC2_TO_DIAG3"
lattice_match_bc2_to_diag beamparams

Drift -name "MARKER_DIAGNOSTICS3"
lattice_diagnostics_3 beamparams

Drift -name "MARKER_MATCH_RTML_TO_ML"
lattice_dump_and_match_rtml_to_main_linac beamparams

BeamlineSet -name rtml

#
# Short-range wakefields
#
if {$beamparams(usewakefields)==1} {
    # BC1
    SplineCreate "rtml_Wt_bc1" -file "$wakedir/Wt_bc1.dat"
    SplineCreate "rtml_Wl_bc1" -file "$wakedir/Wl_bc1.dat"
    ShortRangeWake "rtml_SR_bc1" -type 2 \
        -wx "rtml_Wt_bc1" -wy "rtml_Wt_bc1" -wz "rtml_Wl_bc1"
    Octave {
        CAV = placet_get_name_number_list("rtml", "060C");
        placet_element_set_attribute("rtml", CAV, "short_range_wake", "rtml_SR_bc1");
        placet_element_set_attribute("rtml", CAV, "six_dim", true);
    }

    # Booster
    SplineCreate "rtml_Wt_booster" -file "$wakedir/Wt_booster.dat"
    SplineCreate "rtml_Wl_booster" -file "$wakedir/Wl_booster.dat"
    ShortRangeWake "rtml_SR_booster" -type 2 \
        -wx "rtml_Wt_booster" -wy "rtml_Wt_booster"  -wz "rtml_Wl_booster"
    Octave {
        CAV = placet_get_name_number_list("rtml", "120C");
        placet_element_set_attribute("rtml", CAV, "short_range_wake","rtml_SR_booster");
        placet_element_set_attribute("rtml", CAV, "six_dim", true);
    }

    # BC2
    SplineCreate "rtml_Wt_bc2" -file "$wakedir/Wt_bc2.dat"
    SplineCreate "rtml_Wl_bc2" -file "$wakedir/Wl_bc2.dat"
    ShortRangeWake "rtml_SR_bc2" -type 2 \
         -wx "rtml_Wt_bc2" -wy "rtml_Wt_bc2" -wz "rtml_Wl_bc2"
    Octave {
        CAV = placet_get_name_number_list("rtml", "210C");
        placet_element_set_attribute("rtml", CAV, "short_range_wake", "rtml_SR_bc2");
        placet_element_set_attribute("rtml", CAV, "six_dim", true);
    }
}

#
# Coherent synchrotron radiation
# 
Octave {
    # Get the elements that are bends
    Sbends = placet_get_name_number_list("rtml", "080S");
    Sbends = [Sbends placet_get_name_number_list("rtml", "230S")];
    Sbends = [Sbends placet_get_name_number_list("rtml", "250S")];

    # Turn on coherent synchrotron radiation for the bends
    placet_element_set_attribute("rtml", Sbends, "thin_lens", 0);
    placet_element_set_attribute("rtml", Sbends, "csr", true);
    placet_element_set_attribute("rtml", Sbends, "csr_charge", $charge);
    placet_element_set_attribute("rtml", Sbends, "csr_nbins", 510);
    placet_element_set_attribute("rtml", Sbends, "csr_nsectors", 100);
    placet_element_set_attribute("rtml", Sbends, "csr_filterorder", 1);
    placet_element_set_attribute("rtml", Sbends, "csr_nhalffilter", 10);
    placet_element_set_attribute("rtml", Sbends, "csr_enforce_steady_state", true);
}
