1;
% file to generate distributions
% 

%%#########################################################################################
%%  some functions with mean sigmaa standard deviation 0. 
%%----------------------------------------------------------------------------------------
%  format long e


%   warning("off","Octave:possible-matlab-short-circuit-operator");
%   do_braindead_shortcircuit_evaluation (1)
 
function A=gaussList(sigmaa=1,nslice=11)
  sigmaa_cut=10/3;
  minz=-sigmaa_cut*sigmaa;
  maxz= sigmaa_cut*sigmaa;
%    printf('\n the sigmaa_cut variable is fixed for gaussian distribution \n sigma_cut= %g\n\n',sigmaa_cut)
  zl=linspace(minz,maxz,nslice);
  pl=1./sigmaa./sqrt(2.*pi)*exp(-(zl.*zl)./2./sigmaa./sigmaa);
  pl(1)=0;
  pl(end)=0; 
  sum=trapz(zl,pl);
  pl=pl./sum;
  A=[zl' pl'];
endfunction

function A=plateauList(sigmaa=1,nslice=11)

  sigmaa_cut=10/3/sqrt(2);
%    printf('\n the sigmaa_cut variable is fixed for plateau distribution \n sigma_cut= %g\n\n',sigmaa_cut)
  
  minz=-sigmaa_cut*sigmaa;
  maxz= sigmaa_cut*sigmaa;

  zl=linspace(minz,maxz,nslice);
  
  ll=10/3*sigmaa;
  rt=ll/8;
  pl=(1./ll)*(1./(1.+exp(2./rt.*(2.*abs(zl).-ll))));
  
  pl(1)=0;
  pl(end)=0;
  
  sum=trapz(zl,pl);
  pl=pl./sum;
  A=[zl' pl'];
endfunction


function A=parabolaList (sigmaa=1,nslice=11)
  sigmaa_cut=sqrt(5);
%    printf('\n the sigmaa_cut variable is fixed for parabola distribution \n sigma_cut= %g\n\n',sigmaa_cut)
  minz=-sigmaa_cut*sigmaa;
  maxz= sigmaa_cut*sigmaa;
  
  zl=linspace(minz,maxz,nslice);
  
  pl=3./4./maxz*(1.-zl.*zl./maxz./maxz);
  
  pl(1)=0;
  pl(end)=0;
  
  summ=trapz(zl,pl);
  pl=pl./summ;
  A=[zl' pl'];
endfunction


function A=uniformList (sigmaa=1,nslice=11)
 
  sigmaa_cut=2*sqrt(3);
  fwhm=sigmaa_cut*sigmaa;
%    printf('\n the sigmaa_cut variable is fixed for uniform distribution \n sigma_cut= %g\n\n',sigmaa_cut)
  
  minz=-0.5*fwhm;
  maxz= 0.5*fwhm;
  
  zl=linspace(minz,maxz,nslice);

  pl=zeros(0,nslice);
  for i=1:nslice
    if (zl(i)>=minz && zl(i)<=maxz)
      pl(i)=1/fwhm;
    else
      pl(i)=0.0;
    endif
  end

  pl(1)=0;
  pl(end)=0;
  
  summ=trapz(zl,pl);
  pl=pl./summ;
  A=[zl' pl'];
  
endfunction


function A=generate_zlist(type,sigmaa=1,nslice=11)
    switch (type)
      case {'gaussian'}
	vect=gaussList(sigmaa,nslice);
      case {'plateau'}
	vect=plateauList(sigmaa,nslice);
      case {'parabola'}
	vect=parabolaList(sigmaa,nslice);
      case {'uniform'}
	vect=uniformList(sigmaa,nslice);
      otherwise
	disp("no proper name is given gaussian distribution is being generated");
	vect=gaussList(sigmaa,nslice);
    endswitch
    A=vect;
endfunction


 




function x=randpdf(A,npart)

% RANDPDF
% Random numbers from a user defined distribution
% SYNTAX:
%   x = randpdf(A, npart)
% INPUT : A is the column of z,p
%   p     - probability density,
%   z     - values for probability density,
%   npart - dimension for the output matrix.
% OUTPUT:
%   x   - random numbers. Run function without output for some plots.

dim=[npart,1];
  
% vectorization and normalization of the input pdf
  z=A(:,1);
  p=A(:,2);
  integral=trapz(z,p);
  p=p./integral;

% interpolation of the input pdf for better integration
% in my opinion 5000 point is sufficient...
  zi=[linspace(min(z),max(z),5000)]';
  pi = interp1(z, p, zi, 'spline');

% remove negative elements due to the spline interpolation
  pi(pi < 0) = 0;

% normalize the function to have an area of 1.0 under it
  pi = pi / sum(pi);

% the integral of PDF is the cumulative distribution function
  cdfp = cumsum(pi);

% remove non-unique elements
  [cdfp, mask] = unique(cdfp);
  zi = zi(mask);
  
  
% create an array of random numbers
  uniformDistNum = rand(dim);

% inverse interpolation to achieve P(x) -> x projection of the random values
  userDistNum = interp1(cdfp, zi, uniformDistNum);

  x=reshape(userDistNum,dim);
  
%   ztt=linspace(1,npart);
%    filx = fopen('random.dat', 'w');
%    fprintf(filx, ' %.15g\n', x );
%    fclose(filx);
  
  
end


function beam=beam_random(npart,e0,de,ex,bx,ax,ey,by,ay,sz)
  gmm=e0/0.511e-3+1;
  ex=ex*1e-7/gmm;
  ey=ey*1e-7/gmm;
  sx=sqrt(ex*bx)*1e6;
  spx=sqrt(ex/bx)*1e6;
  sy=sqrt(ey*by)*1e6;
  spy=sqrt(ey/by)*1e6;
  se=0.01*abs(de);
  sz=sz;
  xxpc=-ax/bx;
  yypc=-ay/by;
  
  nc=6;
  %  creating all distributions
  allcol=randn(npart,nc);
  
%      %  eliminate any particle out of sigmaa_cut
%    for i=1:nc
%  	indxi=find(abs(allcol(:,i))>3.33);
%  	while (abs(allcol(indxi,i))>3.33)
%          allcol(indxi,i)=randn(1);
%      end
%    end;
  

  %  %  and eliminate any offset and correlation
  for i=1:nc
	allcol(:,i)=allcol(:,i)-mean(allcol(:,i),1);
  end;
  
  for i=1:nc-1
	for j=i+1:nc
	  a=allcol(:,i);
	  b=allcol(:,j);
	  c=ols(b,a);
	  b=b-c*a;
	  allcol(:,j)=b;
	end;
  end;


  for i=1:nc
	allcol(:,i)=allcol(:,i)/std(allcol(:,i),1);
  end;

  ee = e0*(1.0 + allcol(:,1)*se);
  xx = sx*allcol(:,2);
  yy = sy*allcol(:,3);    
  zz = sz*allcol(:,4);
  xp=spx*allcol(:,5)-ax*xx/sx*spx;
  yp=spy*allcol(:,6)-ay*yy/sy*spy;

  bb0=[ee,xx,yy,zz,xp,yp];
  
  [zs,id]=sort(bb0(:,4));
  
  bb0=bb0(id,:);

  beam=bb0;
endfunction



function beam=beam_zlist_no_offset(A,npart,e0,de,ex,bx,ax,ey,by,ay,sz)
    zlist=randpdf(A,npart);
    zlist=sort(zlist);
    npart=length(zlist);
    B=beam_random(npart,e0,de,ex,bx,ax,ey,by,ay,sz);
    B(:,4)=zlist;
    beam=B;
endfunction




function beam=beam_zlist_slice_no_offset(A,npart,e0,de,ex,bx,ax,ey,by,ay,sz)

    B=beam_zlist_no_offset(A,npart,e0,de,ex,bx,ax,ey,by,ay,sz);
        
    npart=length(B(:,1));
    B(:,2) = zeros(npart,1);
    B(:,3) = zeros(npart,1); 
    B(:,5) = zeros(npart,1); 
    B(:,6) = zeros(npart,1); 

   beam=B;
   
endfunction


function beam=add_offset(Bm,eoff=0,xoff=0,yoff=0,zoff=0,xpoff=0,ypoff=0)
     
    Bm(:,1)=Bm(:,1)+eoff;
    Bm(:,2)=Bm(:,2)+xoff;
    Bm(:,3)=Bm(:,3)+yoff;
    Bm(:,4)=Bm(:,4)+zoff;
    Bm(:,5)=Bm(:,5)+xpoff;
    Bm(:,6)=Bm(:,6)+ypoff;
    
   beam=Bm;
   
endfunction

