############################################################
## Placet2 lattice file generatared using "madx2placet2.py"
## Author: Raul Costa (raul.costa@cern.ch)"
## Input file: input/TurnAround_380GeV.txt
## File generated in 2019-02-09 17:48:02.237972
############################################################
## Missed conversion on:	"START_TO_END$START"	"MARKER"	0	0	1.906	0	0	0	0	0	0	0	0	0	""	
Drift	"START_D"	-len	1	
Sbend	"PEM_B1"	-len	1.005115441	-angle	-0.1748804447	-E1	0	-E2	-0.1748804447	-refen	1.906	
Drift	"PEM_D12"	-len	2.538722248	
Sbend	"PEM_B2"	-len	1.005115441	-angle	0.1748804447	-E1	0.1748804447	-E2	0	-refen	1.906	
Drift	"PEM_D23"	-len	1	
Sbend	"PEM_B3"	-len	1.005115441	-angle	0.1748804447	-E1	0	-E2	0.1748804447	-refen	1.906	
Drift	"PEM_D34"	-len	2.538722248	
Sbend	"PEM_B4"	-len	1.005115441	-angle	-0.1748804447	-E1	-0.1748804447	-E2	0	-refen	1.906	
Drift	"MPT_D1"	-len	0.8922428231	
Quadrupole	"MPT_Q1"	-len	0.3	-K1L	0.02651539578	-refen	1.906	
## Missed conversion on:	"MPT_Q1.BPM"	"BPM"	12.29014908	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"MPT_Q1.CORR"	"KICKER"	12.29014908	0	1.906	0	0	0	0	0	0	0	0	5.057333132444579e-05	"MBCO"	
Drift	"MPT_D2"	-len	0.7274886118	
Quadrupole	"MPT_Q2"	-len	0.3	-K1L	0.02230027948	-refen	1.906	
## Missed conversion on:	"MPT_Q2.BPM"	"BPM"	13.31763769	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"MPT_Q2.CORR"	"KICKER"	13.31763769	0	1.906	0	0	0	0	0	0	0	0	4.2533757826102484e-05	"MBCO"	
Drift	"MPT_D3"	-len	3.72849019	
Quadrupole	"MPT_Q3"	-len	0.3	-K1L	-0.4377810921	-refen	1.906	
## Missed conversion on:	"MPT_Q3.BPM"	"BPM"	17.34612788	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"MPT_Q3.CORR"	"KICKER"	17.34612788	0	1.906	0	0	0	0	0	0	0	0	-0.000834988412092675	"MBCO"	
Drift	"MPT_D4"	-len	0.1125578543	
Quadrupole	"TAL_QL1"	-len	0.3	-K1L	0.05998270813	-refen	1.906	
## Missed conversion on:	"TAL_QL1.BPM"	"BPM"	17.75868574	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QL1.CORR"	"KICKER"	17.75868574	0	1.906	0	0	0	0	0	0	0	0	0.00011440618865980276	"MBCO"	
Drift	"TAL_DL01"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	-refen	1.906	
Drift	"TAL_DL02"	-len	1.2	
Quadrupole	"TAL_QL2"	-len	0.3	-K1L	1.009913347	-refen	1.906	
## Missed conversion on:	"TAL_QL2.BPM"	"BPM"	20.25925718	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QL2.CORR"	"KICKER"	20.25925718	0	1.906	0	0	0	0	0	0	0	0	0.0019262274163501468	"MBCO"	
Drift	"TAL_DL03"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	-refen	1.906	
Drift	"TAL_DL04"	-len	0.2	
Quadrupole	"TAL_QL3"	-len	0.3	-K1L	-1.091704012	-refen	1.906	
## Missed conversion on:	"TAL_QL3.BPM"	"BPM"	21.75982863	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QL3.CORR"	"KICKER"	21.75982863	0	1.906	0	0	0	0	0	0	0	0	-0.0020822283463235087	"MBCO"	
Drift	"TAL_DL05"	-len	0.35	
Quadrupole	"TAL_QL4"	-len	0.3	-K1L	1.496043936	-refen	1.906	
## Missed conversion on:	"TAL_QL4.BPM"	"BPM"	22.40982863	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QL4.CORR"	"KICKER"	22.40982863	0	1.906	0	0	0	0	0	0	0	0	0.0028534337665185694	"MBCO"	
Drift	"TAL_DL06"	-len	0.1	
Sextupole	"TAL_SL"	-len	0.1	-K2L	-2.6	-refen	1.906	
Drift	"TAL_DL07"	-len	1	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	-refen	1.906	
Drift	"TAL_DL08"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	-refen	1.906	
Drift	"TAL_DL09"	-len	0.2	
Quadrupole	"TAL_QL5"	-len	0.3	-K1L	-0.2425113337	-refen	1.906	
## Missed conversion on:	"TAL_QL5.BPM"	"BPM"	25.91097151	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QL5.CORR"	"KICKER"	25.91097151	0	1.906	0	0	0	0	0	0	0	0	-0.00046254659485016125	"MBCO"	
Drift	"TAL_DL10"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	-refen	1.906	
Drift	"TAL_DL11"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	-refen	1.906	
Drift	"TAL_DL12"	-len	1	
Sextupole	"TAL_SL"	-len	0.1	-K2L	-2.6	-refen	1.906	
Drift	"TAL_DL13"	-len	0.1	
Quadrupole	"TAL_QL6"	-len	0.3	-K1L	1.496043936	-refen	1.906	
## Missed conversion on:	"TAL_QL6.BPM"	"BPM"	29.4121144	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QL6.CORR"	"KICKER"	29.4121144	0	1.906	0	0	0	0	0	0	0	0	0.0028534337665185694	"MBCO"	
Drift	"TAL_DL14"	-len	0.35	
Quadrupole	"TAL_QL7"	-len	0.3	-K1L	-1.091704012	-refen	1.906	
## Missed conversion on:	"TAL_QL7.BPM"	"BPM"	30.0621144	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QL7.CORR"	"KICKER"	30.0621144	0	1.906	0	0	0	0	0	0	0	0	-0.0020822283463235087	"MBCO"	
Drift	"TAL_DL15"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	-refen	1.906	
Drift	"TAL_DL16"	-len	0.2	
Quadrupole	"TAL_QL8"	-len	0.3	-K1L	1.009913347	-refen	1.906	
## Missed conversion on:	"TAL_QL8.BPM"	"BPM"	31.56268584	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QL8.CORR"	"KICKER"	31.56268584	0	1.906	0	0	0	0	0	0	0	0	0.0019262274163501468	"MBCO"	
Drift	"TAL_DL17"	-len	1.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	-refen	1.906	
Drift	"TAL_DL18"	-len	0.2	
Quadrupole	"TAL_QL9"	-len	0.3	-K1L	-0.2190154096	-refen	1.906	
## Missed conversion on:	"TAL_QL9.BPM"	"BPM"	34.06325729	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QL9.CORR"	"KICKER"	34.06325729	0	1.906	0	0	0	0	0	0	0	0	-0.00041773236072963516	"MBCO"	
Drift	"TAL_DM1"	-len	2	
Quadrupole	"TAL_QM1"	-len	0.3	-K1L	-0.3550502363	-refen	1.906	
## Missed conversion on:	"TAL_QM1.BPM"	"BPM"	36.36325729	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QM1.CORR"	"KICKER"	36.36325729	0	1.906	0	0	0	0	0	0	0	0	-0.0006771942378761909	"MBCO"	
Drift	"TAL_DM2"	-len	2.65	
Quadrupole	"TAL_QM2"	-len	0.3	-K1L	0.3991282895	-refen	1.906	
## Missed conversion on:	"TAL_QM2.BPM"	"BPM"	39.31325729	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QM2.CORR"	"KICKER"	39.31325729	0	1.906	0	0	0	0	0	0	0	0	0.0007612651681053964	"MBCO"	
Drift	"TAL_DM3"	-len	2.65	
Quadrupole	"TAL_QM3"	-len	0.3	-K1L	-0.3574829148	-refen	1.906	
## Missed conversion on:	"TAL_QM3.BPM"	"BPM"	42.26325729	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QM3.CORR"	"KICKER"	42.26325729	0	1.906	0	0	0	0	0	0	0	0	-0.0006818341330075755	"MBCO"	
Drift	"TAL_DM4"	-len	2	
Quadrupole	"TAL_QR1A"	-len	0.3	-K1L	-0.2067952833	-refen	1.906	
## Missed conversion on:	"TAL_QR1A.BPM"	"BPM"	44.56325729	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR1A.CORR"	"KICKER"	44.56325729	0	1.906	0	0	0	0	0	0	0	0	-0.0003944246755898709	"MBCO"	
Drift	"TAL_DR01"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR02"	-len	1.2	
Quadrupole	"TAL_QR2"	-len	0.3	-K1L	0.976659716	-refen	1.906	
## Missed conversion on:	"TAL_QR2.BPM"	"BPM"	47.06427358	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR2.CORR"	"KICKER"	47.06427358	0	1.906	0	0	0	0	0	0	0	0	0.0018628021176196501	"MBCO"	
Drift	"TAL_DR03"	-len	0.26	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR04"	-len	0.2	
Quadrupole	"TAL_QR3"	-len	0.3	-K1L	-1.046061342	-refen	1.906	
## Missed conversion on:	"TAL_QR3.BPM"	"BPM"	48.62528987	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR3.CORR"	"KICKER"	48.62528987	0	1.906	0	0	0	0	0	0	0	0	-0.0019951731919673576	"MBCO"	
Drift	"TAL_DR05"	-len	0.33	
Quadrupole	"TAL_QR4"	-len	0.3	-K1L	1.499922127	-refen	1.906	
## Missed conversion on:	"TAL_QR4.BPM"	"BPM"	49.25528987	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR4.CORR"	"KICKER"	49.25528987	0	1.906	0	0	0	0	0	0	0	0	0.002860830715823411	"MBCO"	
Drift	"TAL_DR06"	-len	0.1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	-refen	1.906	
Drift	"TAL_DR07"	-len	1	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR08"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR09"	-len	0.2	
Quadrupole	"TAL_QR5"	-len	0.3	-K1L	-0.2263675601	-refen	1.906	
## Missed conversion on:	"TAL_QR5.BPM"	"BPM"	52.75732246	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR5.CORR"	"KICKER"	52.75732246	0	1.906	0	0	0	0	0	0	0	0	-0.00043175526071833335	"MBCO"	
Drift	"TAL_DR10"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR11"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR12"	-len	1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	-refen	1.906	
Drift	"TAL_DR13"	-len	0.1	
Quadrupole	"TAL_QR6"	-len	0.3	-K1L	1.499922127	-refen	1.906	
## Missed conversion on:	"TAL_QR6.BPM"	"BPM"	56.25935505	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR6.CORR"	"KICKER"	56.25935505	0	1.906	0	0	0	0	0	0	0	0	0.002860830715823411	"MBCO"	
Drift	"TAL_DR14"	-len	0.33	
Quadrupole	"TAL_QR7"	-len	0.3	-K1L	-1.046061342	-refen	1.906	
## Missed conversion on:	"TAL_QR7.BPM"	"BPM"	56.88935505	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR7.CORR"	"KICKER"	56.88935505	0	1.906	0	0	0	0	0	0	0	0	-0.0019951731919673576	"MBCO"	
Drift	"TAL_DR15"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR16"	-len	0.26	
Quadrupole	"TAL_QR8"	-len	0.3	-K1L	0.976659716	-refen	1.906	
## Missed conversion on:	"TAL_QR8.BPM"	"BPM"	58.45037134	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR8.CORR"	"KICKER"	58.45037134	0	1.906	0	0	0	0	0	0	0	0	0.0018628021176196501	"MBCO"	
Drift	"TAL_DR17"	-len	1.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR18"	-len	0.2	
Quadrupole	"TAL_QR9"	-len	0.3	-K1L	-0.2238900167	-refen	1.906	
## Missed conversion on:	"TAL_QR9.BPM"	"BPM"	60.95138764	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR9.CORR"	"KICKER"	60.95138764	0	1.906	0	0	0	0	0	0	0	0	-0.0004270297938884773	"MBCO"	
Quadrupole	"TAL_QR1"	-len	0.3	-K1L	-0.2238900167	-refen	1.906	
## Missed conversion on:	"TAL_QR1.BPM"	"BPM"	61.25138764	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR1.CORR"	"KICKER"	61.25138764	0	1.906	0	0	0	0	0	0	0	0	-0.0004270297938884773	"MBCO"	
Drift	"TAL_DR01"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR02"	-len	1.2	
Quadrupole	"TAL_QR2"	-len	0.3	-K1L	0.976659716	-refen	1.906	
## Missed conversion on:	"TAL_QR2.BPM"	"BPM"	63.75240393	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR2.CORR"	"KICKER"	63.75240393	0	1.906	0	0	0	0	0	0	0	0	0.0018628021176196501	"MBCO"	
Drift	"TAL_DR03"	-len	0.26	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR04"	-len	0.2	
Quadrupole	"TAL_QR3"	-len	0.3	-K1L	-1.046061342	-refen	1.906	
## Missed conversion on:	"TAL_QR3.BPM"	"BPM"	65.31342023	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR3.CORR"	"KICKER"	65.31342023	0	1.906	0	0	0	0	0	0	0	0	-0.0019951731919673576	"MBCO"	
Drift	"TAL_DR05"	-len	0.33	
Quadrupole	"TAL_QR4"	-len	0.3	-K1L	1.499922127	-refen	1.906	
## Missed conversion on:	"TAL_QR4.BPM"	"BPM"	65.94342023	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR4.CORR"	"KICKER"	65.94342023	0	1.906	0	0	0	0	0	0	0	0	0.002860830715823411	"MBCO"	
Drift	"TAL_DR06"	-len	0.1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	-refen	1.906	
Drift	"TAL_DR07"	-len	1	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR08"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR09"	-len	0.2	
Quadrupole	"TAL_QR5"	-len	0.3	-K1L	-0.2263675601	-refen	1.906	
## Missed conversion on:	"TAL_QR5.BPM"	"BPM"	69.44545282	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR5.CORR"	"KICKER"	69.44545282	0	1.906	0	0	0	0	0	0	0	0	-0.00043175526071833335	"MBCO"	
Drift	"TAL_DR10"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR11"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR12"	-len	1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	-refen	1.906	
Drift	"TAL_DR13"	-len	0.1	
Quadrupole	"TAL_QR6"	-len	0.3	-K1L	1.499922127	-refen	1.906	
## Missed conversion on:	"TAL_QR6.BPM"	"BPM"	72.9474854	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR6.CORR"	"KICKER"	72.9474854	0	1.906	0	0	0	0	0	0	0	0	0.002860830715823411	"MBCO"	
Drift	"TAL_DR14"	-len	0.33	
Quadrupole	"TAL_QR7"	-len	0.3	-K1L	-1.046061342	-refen	1.906	
## Missed conversion on:	"TAL_QR7.BPM"	"BPM"	73.5774854	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR7.CORR"	"KICKER"	73.5774854	0	1.906	0	0	0	0	0	0	0	0	-0.0019951731919673576	"MBCO"	
Drift	"TAL_DR15"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR16"	-len	0.26	
Quadrupole	"TAL_QR8"	-len	0.3	-K1L	0.976659716	-refen	1.906	
## Missed conversion on:	"TAL_QR8.BPM"	"BPM"	75.1385017	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR8.CORR"	"KICKER"	75.1385017	0	1.906	0	0	0	0	0	0	0	0	0.0018628021176196501	"MBCO"	
Drift	"TAL_DR17"	-len	1.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR18"	-len	0.2	
Quadrupole	"TAL_QR9"	-len	0.3	-K1L	-0.2238900167	-refen	1.906	
## Missed conversion on:	"TAL_QR9.BPM"	"BPM"	77.63951799	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR9.CORR"	"KICKER"	77.63951799	0	1.906	0	0	0	0	0	0	0	0	-0.0004270297938884773	"MBCO"	
Quadrupole	"TAL_QR1"	-len	0.3	-K1L	-0.2238900167	-refen	1.906	
## Missed conversion on:	"TAL_QR1.BPM"	"BPM"	77.93951799	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR1.CORR"	"KICKER"	77.93951799	0	1.906	0	0	0	0	0	0	0	0	-0.0004270297938884773	"MBCO"	
Drift	"TAL_DR01"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR02"	-len	1.2	
Quadrupole	"TAL_QR2"	-len	0.3	-K1L	0.976659716	-refen	1.906	
## Missed conversion on:	"TAL_QR2.BPM"	"BPM"	80.44053429	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR2.CORR"	"KICKER"	80.44053429	0	1.906	0	0	0	0	0	0	0	0	0.0018628021176196501	"MBCO"	
Drift	"TAL_DR03"	-len	0.26	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR04"	-len	0.2	
Quadrupole	"TAL_QR3"	-len	0.3	-K1L	-1.046061342	-refen	1.906	
## Missed conversion on:	"TAL_QR3.BPM"	"BPM"	82.00155058	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR3.CORR"	"KICKER"	82.00155058	0	1.906	0	0	0	0	0	0	0	0	-0.0019951731919673576	"MBCO"	
Drift	"TAL_DR05"	-len	0.33	
Quadrupole	"TAL_QR4"	-len	0.3	-K1L	1.499922127	-refen	1.906	
## Missed conversion on:	"TAL_QR4.BPM"	"BPM"	82.63155058	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR4.CORR"	"KICKER"	82.63155058	0	1.906	0	0	0	0	0	0	0	0	0.002860830715823411	"MBCO"	
Drift	"TAL_DR06"	-len	0.1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	-refen	1.906	
Drift	"TAL_DR07"	-len	1	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR08"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR09"	-len	0.2	
Quadrupole	"TAL_QR5"	-len	0.3	-K1L	-0.2263675601	-refen	1.906	
## Missed conversion on:	"TAL_QR5.BPM"	"BPM"	86.13358317	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR5.CORR"	"KICKER"	86.13358317	0	1.906	0	0	0	0	0	0	0	0	-0.00043175526071833335	"MBCO"	
Drift	"TAL_DR10"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR11"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR12"	-len	1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	-refen	1.906	
Drift	"TAL_DR13"	-len	0.1	
Quadrupole	"TAL_QR6"	-len	0.3	-K1L	1.499922127	-refen	1.906	
## Missed conversion on:	"TAL_QR6.BPM"	"BPM"	89.63561576	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR6.CORR"	"KICKER"	89.63561576	0	1.906	0	0	0	0	0	0	0	0	0.002860830715823411	"MBCO"	
Drift	"TAL_DR14"	-len	0.33	
Quadrupole	"TAL_QR7"	-len	0.3	-K1L	-1.046061342	-refen	1.906	
## Missed conversion on:	"TAL_QR7.BPM"	"BPM"	90.26561576	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR7.CORR"	"KICKER"	90.26561576	0	1.906	0	0	0	0	0	0	0	0	-0.0019951731919673576	"MBCO"	
Drift	"TAL_DR15"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR16"	-len	0.26	
Quadrupole	"TAL_QR8"	-len	0.3	-K1L	0.976659716	-refen	1.906	
## Missed conversion on:	"TAL_QR8.BPM"	"BPM"	91.82663205	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR8.CORR"	"KICKER"	91.82663205	0	1.906	0	0	0	0	0	0	0	0	0.0018628021176196501	"MBCO"	
Drift	"TAL_DR17"	-len	1.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	-refen	1.906	
Drift	"TAL_DR18"	-len	0.2	
Quadrupole	"TAL_QR9C"	-len	0.3	-K1L	-0.2406355661	-refen	1.906	
## Missed conversion on:	"TAL_QR9C.BPM"	"BPM"	94.32764835	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"TAL_QR9C.CORR"	"KICKER"	94.32764835	0	1.906	0	0	0	0	0	0	0	0	-0.00045896890673607276	"MBCO"	
Drift	"MTB_D1"	-len	7.946950693	
Quadrupole	"MTB_Q1"	-len	0.3	-K1L	0.009953216451	-refen	1.906	
## Missed conversion on:	"MTB_Q1.BPM"	"BPM"	102.574599	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"MTB_Q1.CORR"	"KICKER"	102.574599	0	1.906	0	0	0	0	0	0	0	0	1.8983963788314515e-05	"MBCO"	
Drift	"MTB_D2"	-len	7.8206454	
Quadrupole	"MTB_Q2"	-len	0.3	-K1L	-0.3670876175	-refen	1.906	
## Missed conversion on:	"MTB_Q2.BPM"	"BPM"	110.6952444	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"MTB_Q2.CORR"	"KICKER"	110.6952444	0	1.906	0	0	0	0	0	0	0	0	-0.0007001533697238642	"MBCO"	
Drift	"MTB_D3"	-len	0.1885032606	
Quadrupole	"MTB_Q3"	-len	0.3	-K1L	0.3969302332	-refen	1.906	
## Missed conversion on:	"MTB_Q3.BPM"	"BPM"	111.1837477	0	1.906	0	0	0	0	0	0	0	0	0	""	
## Missed conversion on:	"MTB_Q3.CORR"	"KICKER"	111.1837477	0	1.906	0	0	0	0	0	0	0	0	0.0007570727724703467	"MBCO"	
Drift	"MTB_D4"	-len	0.1220094165	
Sbend	"BCPC_B1"	-len	0.7511653817	-angle	-0.09650349724	-E1	0	-E2	-0.09650349724	-refen	1.906	
Drift	"BCPC_D12"	-len	8.0373968	
Sbend	"BCPC_B2"	-len	0.7511653817	-angle	0.09650349724	-E1	0.09650349724	-E2	0	-refen	1.906	
Drift	"BCPC_D23"	-len	1	
Sbend	"BCPC_B3"	-len	0.7511653817	-angle	0.09650349724	-E1	0	-E2	0.09650349724	-refen	1.906	
Drift	"BCPC_D34"	-len	8.0373968	
Sbend	"BCPC_B4"	-len	0.7511653817	-angle	-0.09650349724	-E1	-0.09650349724	-E2	0	-refen	1.906	
Drift	"END_D"	-len	1	
## Missed conversion on:	"START_TO_END$END"	"MARKER"	132.3852122	0	1.906	0	0	0	0	0	0	0	0	0	""	
