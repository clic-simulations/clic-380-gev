Octave {
    # Read in some useful functions
    scriptdir = "$scriptdir/";
    wakedir   = "$wakedir/";
    source([scriptdir, "octave_beam_creation.m"]);
    source([scriptdir, "octave_beam_statistics.m"]);
    source([scriptdir, "octave_results.m"]);
    source([wakedir, "octave_cavity_wake_setup.m"]);
}
