#
# Compact Linear Collider (CLIC) - 380 GeV - RTML to BDS
#
# This code was developed by C. Gohil from a code inherited from A. Latina, Y. Han,
# N. Blaskovic Kraljevic and F. Plassard.
#
# This runs a full simulation of the 380 GeV design of CLIC including the effects
# of ground motion. Electron and positron beams are tracked through the RTML, ML and
# BDS with PLACET and a luminosity calculation is done with GUINEA-PIG.
#
# To execute use:
# tclsh run.tcl <beam_seed> <emitt_dr> <bds_type> <model> <gm_seed> <gm_iter> \
#               <mitigation>
# - beam_seed and gm_seed are any integer.
# - emitt_dr is either 'perfect', 'nominal' or 'dynamic'.
# - bds_type is either 43 for the L*=4.3 m BDS or 6 for the L*=6 m BDS.
# - gm_iter is the time step iteration of ground motion, t=gm_iter*delta_t.
# - see Ground_Motion/ground_motion.tcl for different <mitigation> options.
#
set resultsdir ./Results
set beamdir    ./Beams

# Get simulation parameters from command line arguments
source Scripts/check_arguments.tcl
set beam_seed  [lindex $argv 0]
set emitt_dr   [lindex $argv 1]
set bds_type   [lindex $argv 2]
set model      [lindex $argv 3]
set gm_seed    [lindex $argv 4]
set gm_iter    [lindex $argv 5]
set mitigation [lindex $argv 6]
set ta_corr    [lindex $argv 7]

# Track the electron beam
exec placet -s main.tcl e- $beam_seed $emitt_dr $bds_type $model $gm_seed \
                           $gm_iter $mitigation $ta_corr >@stdout

# Track the positron beam
#
# This simulation mirrors the electron and positron beamlines,
# i.e. the e- beamline is used for both the e- and e+ beams.
exec placet -s main.tcl e+ -$beam_seed $emitt_dr $bds_type $model $gm_seed \
                            $gm_iter $mitigation $ta_corr >@stdout

# Calculate luminosity
exec placet -s guinea_pig.tcl >@stdout

exit
