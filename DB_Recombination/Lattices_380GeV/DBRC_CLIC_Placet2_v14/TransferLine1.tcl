##------> Author: Raul Costa [raul.costa@cern.ch] <------------------##
## Placet2 lattice generated from Eduardo Marin's design for Placet1
## Later adapted to be defined by K1L/K2L instead of S1/S2
## Sextupole strength optimised for low emittance/T566
## Extraction quadrupoles optimised for CR1 injection matching 
## Low energy version for 380GeV stage
if {$Dipole_synrad} { 
set refen 1.905733391 
} else { 
set refen 1.906 
} 
Marker "TL1.INJECTION" 
Drift "DRRF" -len 1.8 
Rfmultipole "RFMulti.DLEXTSEPTUM" -len 0.2 -freq 0.49975 -strength [expr 0.0075*$refen] -phase $pi -refen $refen 
Drift "TL1.D11" -len 0.8 
Quadrupole "QUAD.TL1.Q12" -len 0.75 -K1L -0.724266702 -refen $refen 
Drift "TL1.D13" -len 0.8 
Quadrupole "QUAD.TL1.Q14" -len 0.75 -K1L 0.9174544470000001 -refen $refen 
Drift "TL1.D15" -len 1.6 
Quadrupole "QUAD.TL1.Q16" -len 0.75 -K1L -0.8542453973 -refen $refen 
Drift "TL1.D17" -len 0.8 
Quadrupole "QUAD.TL1.Q18" -len 0.75 -K1L 0.6709421753 -refen $refen 
Drift "TL1.D19" -len 0.8 
Sbend "BEND.TL1.BEND20" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL1.D222" -len 0.7773893387 
Sextupole "SEXT.TL1.SX225" -len 0.3 -K2L 10.248766189122312 -refen $refen 
Drift "TL1.D228" -len 0.3136946693 
Quadrupole "QUAD.TL1.Q23" -len 0.75 -K1L 0.45997660940000007 -refen $refen 
Drift "TL1.D242" -len 0.1562568231 
Sextupole "SEXT.TL1.SX245" -len 0.3 -K2L -3.9885195757980085 -refen $refen 
Drift "TL1.D248" -len 0.1562568231 
Quadrupole "QUAD.TL1.Q25" -len 0.75 -K1L -0.5296565843 -refen $refen 
Drift "TL1.D262" -len 0.0500000253 
Drift "TL1.D268" -len 0.0500000253 
Sbend "BEND.TL1.BEND27" -len 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL1.D282" -len 0.1536564076 
Sextupole "SEXT.TL1.SX285" -len 0.3 -K2L 1.4029077345452474 -refen $refen 
Drift "TL1.D288" -len 0.1536564076 
Quadrupole "QUAD.TL1.Q30" -len 0.75 -K1L 0.4993038684 -refen $refen 
Drift "TL1.D322" -len 0.1536564076 
Sextupole "SEXT.TL1.SX325" -len 0.3 -K2L 1.4029077345452474 -refen $refen 
Drift "TL1.D328" -len 0.1536564076 
Sbend "BEND.TL1.BEND33" -len 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL1.D342" -len 0.0500000253 
Drift "TL1.D348" -len 0.0500000253 
Quadrupole "QUAD.TL1.Q35" -len 0.75 -K1L -0.5296565843 -refen $refen 
Drift "TL1.D362" -len 0.1562568231 
Sextupole "SEXT.TL1.SX365" -len 0.3 -K2L -3.9885195757980085 -refen $refen 
Drift "TL1.D368" -len 0.1562568231 
Quadrupole "QUAD.TL1.Q37" -len 0.75 -K1L 0.45997660940000007 -refen $refen 
Drift "TL1.D382" -len 0.3136946693 
Sextupole "SEXT.TL1.SX385" -len 0.3 -K2L 10.248766189122312 -refen $refen 
Drift "TL1.D388" -len 0.7773893387 
Sbend "BEND.TL1.BEND40" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL1.D402" -len 0.4980886966 
Sextupole "SEXT.TL1.SXC405" -len 0.3 -K2L -19.847620911994753 -refen $refen 
Drift "TL1.D408" -len 0.06602956553 
Quadrupole "QUAD.TL1.Q41" -len 0.75 -K1L 0.17865521019999997 -refen $refen 
Drift "TL1.D412" -len 0.9874829865 
Sextupole "SEXT.TL1.SXC415" -len 0.3 -K2L 41.63329867998703 -refen $refen 
Drift "TL1.D418" -len 0.2291609955 
Quadrupole "QUAD.TL1.Q42" -len 0.75 -K1L -0.5380648101999999 -refen $refen 
Drift "TL1.D422" -len 1.245610625 
Sextupole "SEXT.TL1.SXC425" -len 0.3 -K2L -1.3803360907242637 -refen $refen 
Drift "TL1.D428" -len 0.3152035415 
Quadrupole "QUAD.TL1.Q43" -len 0.75 -K1L 0.5385499766 -refen $refen 
Drift "TL1.D432" -len 1.58013019 
Sextupole "SEXT.TL1.SXC435" -len 0.3 -K2L 1.7148542858149327 -refen $refen 
Drift "TL1.D438" -len 0.4267100632 
Quadrupole "QUAD.TL1.Q44" -len 0.75 -K1L -0.6426636216999999 -refen $refen 
Drift "TL1.D442" -len 1.673352339 
Sextupole "SEXT.TL1.SXC445" -len 0.3 -K2L -38.32051678792359 -refen $refen 
Drift "TL1.D448" -len 0.457784113 
Quadrupole "QUAD.TL1.Q45" -len 0.75 -K1L 0.666494741 -refen $refen 
Drift "TL1.D452" -len 0.457784113 
Sextupole "SEXT.TL1.SXC455" -len 0.3 -K2L -31.98102270541222 -refen $refen 
Drift "TL1.D458" -len 1.673352339 
Quadrupole "QUAD.TL1.Q46" -len 0.75 -K1L -0.6390286404 -refen $refen 
Drift "TL1.D462" -len 0.4267100632 
Sextupole "SEXT.TL1.SXC465" -len 0.3 -K2L -1.512343341476847 -refen $refen 
Drift "TL1.D468" -len 1.58013019 
Quadrupole "QUAD.TL1.Q47" -len 0.75 -K1L 0.5420133188 -refen $refen 
Drift "TL1.D472" -len 0.3152035415 
Sextupole "SEXT.TL1.SXC475" -len 0.3 -K2L 3.4890670488483018 -refen $refen 
Drift "TL1.D478" -len 1.245610625 
Quadrupole "QUAD.TL1.Q48" -len 0.75 -K1L -0.5286812296 -refen $refen 
Drift "TL1.D482" -len 0.2291609955 
Sextupole "SEXT.TL1.SXC485" -len 0.3 -K2L -28.323814351677758 -refen $refen 
Drift "TL1.D488" -len 0.9874829865 
Quadrupole "QUAD.TL1.Q49" -len 0.75 -K1L 0.1559722145 -refen $refen 
Drift "TL1.D492" -len 0.06602956553 
Sextupole "SEXT.TL1.SXC495" -len 0.3 -K2L -31.043465418218226 -refen $refen 
Drift "TL1.D498" -len 0.4980886966 
Sbend "BEND.TL1.BEND50" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL1.D522" -len 0.7773893387 
Sextupole "SEXT.TL1.SX525" -len 0.3 -K2L 10.190496907073024 -refen $refen 
Drift "TL1.D528" -len 0.3136946693 
Quadrupole "QUAD.TL1.Q53" -len 0.75 -K1L 0.44648187078861856 -refen $refen 
Drift "TL1.D542" -len 0.1562568231 
Sextupole "SEXT.TL1.SX545" -len 0.3 -K2L -0.5580793795719026 -refen $refen 
Drift "TL1.D548" -len 0.1562568231 
Quadrupole "QUAD.TL1.Q55" -len 0.75 -K1L -0.5081481114760656 -refen $refen 
Drift "TL1.D562" -len 0.0500000253 
Drift "TL1.D568" -len 0.0500000253 
Sbend "BEND.TL1.BEND57" -len 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL1.D582" -len 0.1536564076 
Sextupole "SEXT.TL1.SX585" -len 0.3 -K2L -3.453564555656445 -refen $refen 
Drift "TL1.D588" -len 0.1536564076 
Quadrupole "QUAD.TL1.Q60A" -len 0.75 -K1L 0.499768678887644 -refen $refen 
Drift "TL1.D622" -len 0.05000058615 
Sextupole "SEXT.TL1.SX625" -len 0.3 -K2L 3.932014431159659 -refen $refen 
Drift "TL1.D628" -len 0.05000058615 
Sbend "BEND.TL1.BEND63" -len 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL1.D642" -len 0.2366852486 
Drift "TL1.D648" -len 0.2366852486 
Quadrupole "QUAD.TL1.Q65" -len 0.75 -K1L -0.5666858875065315 -refen $refen 
Drift "TL1.D662" -len 0.1747711281 
Sextupole "SEXT.TL1.SX665" -len 0.3 -K2L -7.942694752374344 -refen $refen 
Drift "TL1.D668" -len 0.1747711281 
Quadrupole "QUAD.TL1.Q67" -len 0.75 -K1L 0.4808637551030651 -refen $refen 
Drift "TL1.D682" -len 0.258673648 
Sextupole "SEXT.TL1.SX685" -len 0.3 -K2L 16.003474406320038 -refen $refen 
Drift "TL1.D688" -len 0.667347296 
Sbend "BEND.TL1.BEND70" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
## Offset needed for CR1 Injection 
Kalign CR1offset -cx 0.01708861599238307 -cxp 0.0001113719435327479 -cz -5.761172761e-05 
Marker "CR1_offset" -len 0. -nodes 1 -kick CR1offset 
Marker "TL1.EXTRACTION" 
