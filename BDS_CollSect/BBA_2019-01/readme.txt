These files were developed by C. Gohil from a simulation inherited from J. Ogren.

Performs BBA on the BDS collimation section (not final focus system).

The collimation system is the same for L*=4.3 and 6 m final focus systems.

To run:
placet main/singlebeam_BBA.tcl machine <machine-number>

E.g. "placet main/singlebeam_BBA.tcl machine 31" to perform BBA on machine number 31.
