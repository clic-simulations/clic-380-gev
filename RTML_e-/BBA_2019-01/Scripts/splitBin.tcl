proc splitBin {beamline  ncarray Start End} { 
  upvar $ncarray ncparams
  Octave {
                  elements_all = placet_get_number_list("$beamline","quadrupole");
		  Start = placet_get_name_number_list("$beamline","$Start");
		  End = placet_get_name_number_list("$beamline","$End");
                
                  quads = elements_all((elements_all >= Start) & (elements_all <= End)); 
                  quad_list = [Start quads End];
 
                  indexBin = 1;
                  last_elem = Start;

                  n_quad = $ncparams(1,1);
                  n_step = n_quad * (1 - $ncparams(1,2));

                  while "ture"
                       if indexBin == 1
                            first_elem = Start;
                       else
                            first_elem = quad_list(1,(indexBin -1) * n_step);
                       endif 


                       if find(quad_list == first_elem) + n_quad > length(quad_list)
                           last_elem = End;
                       else
                           last_elem = quad_list(1,find(quad_list == first_elem) + n_quad); 
                       endif

		       minBpm=find(Response.Bpms >= first_elem)(1);
		       maxBpm=find(Response.Bpms <= last_elem)(end);
		       minCorrector = find(Response.Corrector < Response.Bpms(minBpm))(end);
		       maxCorrector = find(Response.Corrector < Response.Bpms(maxBpm))(end);


		       Bins(indexBin).Bpms = minBpm:maxBpm;
		       Bins(indexBin).Corrector = minCorrector:maxCorrector;
		       Bins(indexBin).first = first_elem;
		       Bins(indexBin).last = last_elem;

                       if last_elem == End
                           break;
                       endif

                       indexBin += 1;
                       
		   endwhile
  }
}
