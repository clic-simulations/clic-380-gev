#ifndef PI
#define PI 3.141592653589793
#endif

#ifndef TWOPI
#define TWOPI 6.283185307179586
#endif

#ifndef max
#define max(a,b) (((a)<(b))?(b):(a))
#endif

#ifndef min
#define min(a,b) (((a)>(b))?(b):(a))
#endif

#define RNDM_EPS 6e-8

#define rndm_survey() rndm5a()

#ifndef rndm
#define rndm() rndm5()
#endif 

void rndmst0(int );
float rndm0();
void rndmst1(int );
float rndm1();
void rndmst2(int );
float rndm2();
void rndmst3(int );
float rndm3();
void rndmst5(int ,int ,int , int );
float rndm5();
void rndmst5a(int ,int ,int , int );
float rndm5a();
void rndmst6(int );
float rndm6();
void rndmst7(int );
float rndm7();
void rndmst8(int );
float rndm8();
float expdev();
float gasdev_0();
float gasdev();
float gasdev2();
void rndmst();
void rndm_save(char *);
void rndm_load(char *);
