##------> Author: Raul Costa [raul.costa@cern.ch]  (Dec 2023 version) <------------------##
## Placet2 lattice generated from Eduardo Marin's design for Placet1
## Later adapted to be defined by K1L/K2L instead of S1/S2
## Sextupoles opltimized for no radiation in 2024
## Low energy version for 380GeV stage
#~ if {$Dipole_synrad} {
#~ set refen 1.905993977 
#~ } else { 
#~ set refen 1.906
#~ } 

## Periodic solution (from a monochromatic beam with sextupoles off):
## bx= 3.877300112288511 m 
## by= 4.175358321926064 m 
## ax=ay=0

Quadrupole "QUAD.QI1" -len [expr 0.4/2.] -K1L [expr -1.493024/2.] -refen $refen  
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L -2.60468 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SN1" -len 0.1 -K2L 0.34356 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SN2" -len 0.1 -K2L -0.14220 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SN3" -len 0.1 -K2L -0.34541 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len [expr 0.4/2.] -K1L [expr -0.3333352/2.] -refen $refen 
Quadrupole "QUAD.QS2C" -len [expr 0.4/2.] -K1L [expr -0.3333352/2.] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -K2L 1.02713 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -K2L -43.72410 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -K2L -35.79306 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L -2.60468 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len [expr 0.4/2.] -K1L [expr -1.493024/2.] -refen $refen  
