set tcl_precision 15
# ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set beamdir    ./Beams
set wakedir    ./Wakefields

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/plainTracking.tcl

create_particles_file $beamdir/particles.in beamparams

source $scriptdir/beamline_and_wake.tcl

make_particle_beam beam1 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
# make_particle_beam beam3 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam4 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

# load present settings
source $scriptdir/load_mag.tcl

# track to CA-start
Octave {

    Start = placet_get_name_number_list("rtml","Marker-SR-start")
    End = placet_get_name_number_list("rtml","Marker-CA-start")
    End2 = placet_get_name_number_list("rtml","Marker-LTL-start")

    [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1, Start, End);
    printf("\nEmittance entering CA\n");
    printf("Emittance: %10.2f %10.2f nm\n", E(end,2)*100,E(end,6)*100);
    # set to beam4
    placet_set_beam("beam4", B);

    # apply optmized offset
    Multipole_all = placet_get_name_number_list("rtml","Multi-CA*")
    # choosing the first five sextuple
    N_Multipole = 5
    length(Multipole_all)
    Multipoles = Multipole_all(1:N_Multipole)
    # add_offset(Multipoles, X_(1:5), Multipoles, X_(6:10));

    # most effective
    function [x_list, y_list]=select_case(select_num=5,beamline_name="rtml",select_name="Multi-CA*")
        Multipole_all = placet_get_name_number_list(beamline_name,select_name);
        if (select_num > length(Multipole_all))
            error ("select_num is larger than available number!");
        endif

        R=[];
        Start = placet_get_name_number_list("rtml","Marker-CA-start")
        End = placet_get_name_number_list("rtml","Marker-LTL-start")
        for i=1:length(Multipole_all)
            placet_element_vary_attribute(beamline_name, [Multipole_all(i)], "x", [1]);
            [E,B] = placet_test_no_correction("rtml", "beam4", "None", 1, Start, End);
            merit=E(end,2)/7.0 + E(end,6)/0.05;
            placet_element_vary_attribute(beamline_name, [Multipole_all(i)], "x", [-1]);
            R=[R;Multipole_all(i) merit];
        endfor
        R_=sortrows(R,[-2]);
        x_list=sort(R_(1:select_num));

        R=[];
        for i=1:length(Multipole_all)
            placet_element_vary_attribute(beamline_name, [Multipole_all(i)], "y", [1]);
            [E,B] = placet_test_no_correction("rtml", "beam4", "None", 1, Start, End);
            merit=E(end,2)/7.0 + E(end,6)/0.05;
            placet_element_vary_attribute(beamline_name, [Multipole_all(i)], "y", [-1]);
            R=[R;Multipole_all(i) merit];
        endfor
        R_=sortrows(R,[-2]);
        y_list=sort(R_(1:select_num));
    endfunction
    
    [x_list, y_list]=select_case(5)

}


