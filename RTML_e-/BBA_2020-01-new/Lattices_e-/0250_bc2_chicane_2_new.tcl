#
# BC2 chicane 2 lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
# where necessary units were converted to placet units in main.tcl

proc lattice_bc2_chicane_2 {bparray} {
    upvar $bparray beamparams

    set usecsr        0
    set usesynrad     $beamparams(useisr)
    set usesixdim     1
    set numthinlenses 100

    set pi   3.141592653589793
    set c    299792458
    set q0   1.6021765e-19 
    set eps0 [expr 1/(4e-7*$pi*$c*$c)]

    set theta    [expr 0.324572/180*$pi]
    set lbend    1.5
    set rho      [expr $lbend/sin($theta)]
    set lbendarc [expr $theta*$rho]

    set d12 [expr 11.5/cos($theta)]
    set d23 1.0
    set d34 [expr $d12]

    set r_e     2.81794e-15
    set N       [expr 0.85e-9/$q0]
    set sigma_z 72

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    # Reference energy of the final two Sbends are set from matching to the ML
    # old
    set e3 8.995584198845005
    set e4 8.995185901520106
    # a0
    # set e3    8.99741228401768
    # set e4    8.99724746972698
    # a1
    # set e3    8.99938315615809
    # set e4    8.99922879479431
    # a2
    # set e3    8.99919111072617
    # set e4    8.99902810017058
    # a3
    # set e3    9.00108116483043
    # set e4    9.00085862075828
    # a4
    # set e3    9.0006
    # set e4    9.0004
    # a5
    # set e3    9.00005175679281
    # set e4    8.99985945917788
    

    Girder
    Sbend -name "250S" -length $lbendarc -angle $theta -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 0.0 -E2 $theta
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
    set de_csr [expr 0.35*$N*$r_e*$rho**(1.0/3)*$theta/$g0/$sigma_z**(4.0/3)*$refenergy]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad - $de_csr]
    SetReferenceEnergy $refenergy
    Drift -length $d12 -six_dim $usesixdim
    Sbend -name "250S" -length $lbendarc -angle -$theta -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 -$theta -E2 0.0
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
    set de_csr [expr 0.35*$N*$r_e*$rho**(1.0/3)*$theta/$g0/$sigma_z**(4.0/3)*$refenergy]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad - $de_csr]
    SetReferenceEnergy $refenergy
    Drift -length $d23 -six_dim $usesixdim
    Sbend -name "B250match" -length $lbendarc -angle -$theta -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e3 -E1 0.0 -E2 -$theta
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
    set de_csr [expr 0.35*$N*$r_e*$rho**(1.0/3)*$theta/$g0/$sigma_z**(4.0/3)*$refenergy]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad - $de_csr]
    SetReferenceEnergy $refenergy
    Drift -length $d34 -six_dim $usesixdim
    Sbend -name "B250match" -length $lbendarc -angle $theta -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e4 -E1 $theta -E2 0.0
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
    set de_csr [expr 0.35*$N*$r_e*$rho**(1.0/3)*$theta/$g0/$sigma_z**(4.0/3)*$refenergy]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad - $de_csr]
    SetReferenceEnergy $refenergy
    set beamparams(meanenergy) $refenergy
}
