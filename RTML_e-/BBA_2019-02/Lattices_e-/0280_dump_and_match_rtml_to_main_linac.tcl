#
# Dump and matching of RTML to Main Linac
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_rtml_to_main_linac {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 100
    set quad_synrad   0

    set lquadm 0.3

    set kqm1  1.5484145133563805
    set kqm2 -1.5466787196360552

    set ldm1 0.02000000000162589
    set ldm2 0.7832157181245879
    set ldm3 5.622316493739453

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy
    
    Girder
    Drift -length $ldm1
    Bpm
    Quadrupole -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy]
    Dipole -name "D280-1"
    Drift -length $ldm2
    Bpm
    Quadrupole -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy]
    Dipole -name "D280-2"
    Drift -length $ldm3

    # Remove any longitudinal offset to ensure the correct RF phase is seen
    # in the ML cavities
    #TclCall -script {Octave {B=placet_get_beam(); B(:,4)-=5.865967144941343; placet_set_beam(B);}}

    # Half quadrupole from the ML
    #Quadrupole -name "" -s 0 -x 0 -y 0 -xp 0 -yp 0 -roll 0 -tilt 0 -tilt_deg 0 -length 0.215 -synrad 0 -six_dim 0 -thin_lens 0 -e0 -1 -aperture_x 1 -aperture_y 1 -aperture_losses 0 -aperture_shape "none" -tclcall_entrance "" -tclcall_exit "" -short_range_wake "" -strength 3.0913947 -Kn 0 -type 0 -hcorrector "x" -hcorrector_step_size 0 -vcorrector "y" -vcorrector_step_size 0

    Drift -name "Marker-BC2-end" -length 0.0
}
