set rfparamsbooster(gradientww)   1.834479085162e+07
set rfparamsbc1(gradientww)   2.230368366668e+07
set rfparamsbc2(gradientww)   7.454696654033e+07
set energy_wake_bc1  -3.527208418513e-05
set energy_wake_booster  -5.768797530716e-05
set energy_wake_bc2  -7.428678993385e-05
