# This script can be run with: placet run.tcl
set script_dir "."

# Set initial beam energy
set e_initial 9.0
set e0 $e_initial

# scale is a variabled needed in script clic_basic_single.tcl
set scale 1.0
source clic_basic_single.tcl

# Set up the ML beamline
# This file lattice.tcl is generated in folder make_lattice_file/placet_output
BeamlineNew
source lattice.tcl
BeamlineSet -name ml

source clic_beam.tcl

# Parameters of the beam at the start of the ML
set match(emitt_x) 7.8451068698407962
set match(emitt_y) 0.0572210226904264
set match(e_spread) 1.6
set match(sigma_z) 70
set match(phase) 0.0
set match(charge) 5.2e9
set match(n_total) 500
set match(alpha_x) 1.73163269979984e-17
set match(beta_x) 8.07819977748438
set match(alpha_y) -3.42260633016671e-17
set match(beta_y) 0.851581049394505
set charge $match(charge)

set n_total 500
set n_slice 31

# Make the beam
make_beam_slice beam0 $n_slice 1

FirstOrder 0

Octave {
    [emitt, beam] = placet_test_no_correction("ml", "beam0", "None", "%ex %ey %sex %sey %x %y %xp %yp %Env %sx %sy %sxp %syp %E %dE %s %n %name");
    ml = placet_get_emittance(beam)
}

exit
