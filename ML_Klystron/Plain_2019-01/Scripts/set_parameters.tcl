#
# This script sets all the parameters needed in this simulation
#

# Number of particles
set n_slice 50
set n       2000
set n_total [expr $n_slice*$n]

# Beam parameters
source $paramsdir/initial_beam_parameters_e-.tcl

# RF cavity parameters
source $paramsdir/rf_parameters.tcl
