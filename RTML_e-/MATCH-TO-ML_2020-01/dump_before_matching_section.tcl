#
# CLIC 380 GeV: Main Beam RTML
#
# This code with developed by C. Gohil from the simulation inherited from
# Y. Han.
#
# The order of the lines up until the Octave code must not be changed.
# The seeds of the random number generated of this simulation have been set
# so that the result does not vary from run to run.
#

proc FF1 {} {}
proc FF2 {} {}

# Directories containing files needed by this simulation
set latticedir ./Lattices_e-
set scriptdir ./Scripts
set paramsdir ./Parameters

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $scriptdir/setParam.tcl
source $scriptdir/beamsetup.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/lattice_functions.tcl
source $scriptdir/octave_functions.tcl

# Generate wakefields
source create_wake.tcl
if {$beamparams(usewakefields)==1} {
   source rf_parameteres_ww.tcl
}
create_particles_file particles.in beamparams
create_zero_wakes_file zero_wake.dat beamparams rfparamsbc1 particles.in

# Set up the RTML lattice
BeamlineNew
source $scriptdir/beamline_and_wake_dump_before_matching.tcl
BeamlineSet -name rtml

# Generate the beam at the start of the RTML
make_particle_beam beam1 beamparams particles.in zero_wake.dat

# Turn on coherent synchrotron radiation
source $scriptdir/setCSR.m

FirstOrder 1
Octave {

    # Track the beam through the RTML
    [emitt, beam] = placet_test_no_correction("rtml", "beam1", "None", "%ex %ey %sex %sey %x %y %xp %yp %Env %sx %sy %sxp %syp %E %dE %s %n %name");
    save -text emitt_before_matching.dat emitt;
    save_beam("beam_before_matching.in", beam);

    # Display the emittance at the end of the RMTL
    rtml = placet_get_emittance(beam)

    # Display the twiss parameters at the end of the RTML
    twiss = placet_get_twiss_matrix(beam);
    beta_x = twiss(1,1)
    beta_y = twiss(3,3)
    alpha_x = twiss(1,2)
    alpha_y = twiss(3,4)
    disp("");

    # Display the offset and angle of the beam at the end of the RTML
    x = mean(beam(:,2))
    y = mean(beam(:,3))
    xp = mean(beam(:,5))
    yp = mean(beam(:,6))


}

exit
