#
# This script sets up the Ring to Main Linac (RTML) beamline and wakefields in it.
# The short-range wakefields are generated with "placet Wakefields/create_wake.tcl"
#

# Long-range wakefields in BC1 RF and the Booster
source $wakedir/cavity_wake_setup.tcl
set wakelongrangedata(nmodes) 0
set wakelongrangedata(modes) {}
initialize_cavities rfparamsbc1 wakelong1 wakelongrangedata

# Lattice
BeamlineNew
source $latticedir/0000_e-_rtml.tcl
BeamlineSet -name rtml

# Short-range wakefields
SplineCreate "rtml_Wt_bc1" -file "$wakedir/Wt_bc1.dat"
SplineCreate "rtml_Wl_bc1" -file "$wakedir/Wl_bc1.dat"
ShortRangeWake "rtml_SR_bc1" -type 2 -wx "rtml_Wt_bc1" \
                                     -wy "rtml_Wt_bc1" \
                                     -wz "rtml_Wl_bc1"

SplineCreate "rtml_Wt_booster" -file "$wakedir/Wt_booster.dat"
SplineCreate "rtml_Wl_booster" -file "$wakedir/Wl_booster.dat"
ShortRangeWake "rtml_SR_booster" -type 2 -wx "rtml_Wt_booster" \
                                         -wy "rtml_Wt_booster" \
                                         -wz "rtml_Wl_booster"

SplineCreate "rtml_Wt_bc2" -file "$wakedir/Wt_bc2.dat"
SplineCreate "rtml_Wl_bc2" -file "$wakedir/Wl_bc2.dat"
ShortRangeWake "rtml_SR_bc2" -type 2 -wx "rtml_Wt_bc2" \
                                     -wy "rtml_Wt_bc2" \
                                     -wz "rtml_Wl_bc2"
