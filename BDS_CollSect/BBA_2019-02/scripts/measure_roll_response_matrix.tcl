# Measure roll response matrix
Octave {
   disp("Measuring response matrix");
   disp("Turning multipoles OFF");
   MStemp = placet_element_get_attribute("test", MI, "strength");
   placet_element_set_attribute("test", MI, "strength", 0.0);
   r = 1;
   d = 200;
   RR = [];
   inds = [QI, BI];
   for i = 1:length(inds)
      disp(['Measuring ', num2str(i),' out of ', num2str(length(inds))]);
      placet_element_set_attribute("test", inds(i), "roll", r);
      yvec = [];
      for j = 1:length(QI)
         # displace quad j in x:
         placet_element_set_attribute("test", QI(j), "x", d);
         placet_test_no_correction("test", "beam0", "None");
         Bpos1 = placet_get_bpm_readings("test");

         placet_element_set_attribute("test", QI(j), "x", -d);
         placet_test_no_correction("test", "beam0", "None");
         Bpos2 = placet_get_bpm_readings("test");
         placet_element_set_attribute("test", QI(j), "x", 0.0);
         
         dx = (Bpos1(:,1) - Bpos2(:,1))/r;
         dy = (Bpos1(:,2) - Bpos2(:,2))/r;
         yvec = [yvec; dx; dy];

         # displace quad j in y:
         placet_element_set_attribute("test", QI(j), "y", d);
         placet_test_no_correction("test", "beam0", "None");
         Bpos1 = placet_get_bpm_readings("test");

         placet_element_set_attribute("test", QI(j), "y", -d);
         placet_test_no_correction("test", "beam0", "None");
         Bpos2 = placet_get_bpm_readings("test");
         placet_element_set_attribute("test", QI(j), "y", 0.0);
         
         dx = (Bpos1(:,1) - Bpos2(:,1))/r;
         dy = (Bpos1(:,2) - Bpos2(:,2))/r;
         yvec = [yvec; dx; dy];         
      end
      placet_element_set_attribute("test", inds(i), "roll", 0);
      size(yvec);
      RR = [RR, yvec];
   end

   size(RR)
   placet_element_set_attribute("test", MI, "strength", MStemp);

   RollRespMat = RR;
   save ../matrices/RollResponseMatrix.dat RollRespMat
}


