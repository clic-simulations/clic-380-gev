#
# matching of Turn Around Loop to BC2 RF
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_tal_to_bc2_rf {bparray} {
    upvar $bparray beamparams

    set usesixdim 1
    set numthinlenses 10
    set quad_synrad 0

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    set lquadm 0.3
    
    # From match_0200.tcl
    set kqm1  3.99953460081851
    set kqm2 -6.17900297004170
    set kqm3  3.49372565725678
    set kqm4 -5.25939276084426

    set ldm1 0.0338580162633493
    set ldm2 0.5691300110354337
    set ldm3 0.7613014822074761
    set ldm4 0.0437335150999885

    Girder
    Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -e0 $refenergy
    Drift -name "matching-drift" -length $ldm1
    Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -e0 $refenergy
    Drift -name "matching-drift" -length $ldm2
    Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -e0 $refenergy
    Drift -name "matching-drift" -length $ldm3
    Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -e0 $refenergy
    Drift -name "matching-drift" -length $ldm4
}
