TclCall -script {
    global flag
     Octave {
         if ($flag == 0 )
         k1 = 0;
         k2 = 0;
         k3 = 0;
         k4 = 0;
         else
         B0 = placet_get_beam();
         x = B0(:,2);
         y = B0(:,3);
         xp = B0(:,5);
         yp = B0(:,6);
        
         l = 0.3;


         B_cov = cov([ x xp y yp]);
         B_avg = mean([ x xp y yp]);
         save -text 'B_cov.dat' B_cov B_avg

	 system('cp func_min.m tmp-fmin.m');

	 source 'tmp-fmin.m'
        
	 #[CAk, cor_min] = fminsearch("correction", [0;0;0;0])
	[CAk,fval, info] = fsolve('correctionSlove', [ 0, 0, 0 ,0 ],optimset("TolFun",0.00000000001,"GradObj", "on"));
	[CAk,fval, info] = fsolve('correctionSlove',CAk);
	[CAk,fval, info] = fsolve('correctionSlove',CAk);
	[CAk,fval, info] = fsolve('correctionSlove',CAk);
	[CAk,fval, info] = fsolve('correctionSlove',CAk);
	[CAk,fval, info] = fsolve('correctionSlove',CAk);


         save CAk1.dat CAk

         HALFPI = [0,1,0,0;-1,0,0,0;0,0,0,1;0,0,-1,0];
         PI = [-1,0,0,0;0,-1,0,0;0,0,-1,0;0,0,0,-1];
         PIHALFPI = [-1,0,0,0;0,-1,0,0;0,0,0,1;0,0,-1,0];
         ZeroHALFPI = [1,0,0,0;0,1,0,0;0,0,0,1;0,0,-1,0];

         k1 = CAk(1);
         k2 = CAk(2);
         k3 = CAk(3);
         k4 = CAk(4);

 
         M1 = [1,0,0,0;0,1,-k1*l,0;0,0,1,0;-k1*l,0,0,1];
         M2 = [1,0,0,0;0,1,-k2*l,0;0,0,1,0;-k2*l,0,0,1];
         M3 = [1,0,0,0;0,1,-k3*l,0;0,0,1,0;-k3*l,0,0,1];
         M4 = [1,0,0,0;0,1,-k4*l,0;0,0,1,0;-k4*l,0,0,1];



         vec_xy = [x xp y yp];
         vec_xy = vec_xy * (M1');
         vec_xy = vec_xy*(HALFPI')*(M2')*(PIHALFPI');
         vec_xy = vec_xy * (M3');
         vec_xy = vec_xy * (HALFPI')*(M4')*(ZeroHALFPI');
        
         B0(:,2) = vec_xy(:,1);
         B0(:,3) = vec_xy(:,3);
         B0(:,5) = vec_xy(:,2);
         B0(:,6) = vec_xy(:,4);
       
	 x = B0(:,2);
         y = B0(:,3);
         xp = B0(:,5);
         yp = B0(:,6);


         B_cov = cov([ x xp y yp]);
         B_avg = mean([ x xp y yp]);
         save -text 'B_cov1.dat' B_cov B_avg;
         save -text 'CAk1.dat' CAk;

         placet_set_beam(B0);

         endif
     }
}
