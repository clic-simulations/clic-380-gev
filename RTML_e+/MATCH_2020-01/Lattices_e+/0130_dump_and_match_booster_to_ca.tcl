#
# Dump and matching of booster to central arc
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_booster_to_ca {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 10
set quad_synrad 0

set refenergy $beamparams(meanenergy)

set lquadm 0.3

set kqm1  1.117777840083191
set kqm2 -1.123126109195231
set kqm3  1.424361326306554
set kqm4 -0.966309472466489

set ldm1 2.935426973830079
set ldm2 0.343745016978141
set ldm3 3.471163929612517
set ldm4 3.928535783159713
set ldm5 3.274630793467815

SetReferenceEnergy $refenergy

Girder
Drift -name "matching-drift" -length $ldm1 -six_dim $usesixdim
Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "matching-drift" -length $ldm2 -six_dim $usesixdim
Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "matching-drift" -length $ldm3 -six_dim $usesixdim
Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "matching-drift" -length $ldm4 -six_dim $usesixdim
Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "matching-drift" -length $ldm5 -six_dim $usesixdim
}
