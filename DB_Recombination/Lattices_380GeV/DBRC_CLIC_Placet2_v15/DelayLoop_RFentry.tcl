##------> Author: Raul Costa [raul.costa@cern.ch]  (Fev 2024 version) <------------------##
if {$Dipole_synrad} {
set refen 1.905993977 
} else { 
set refen 1.906
} 
Drift "DGDL.D5" -len 1.070190712276695 
Rfmultipole "RFMulti.DLINJSEPTUM" -len 0.06 -freq [expr 3.*0.49975] -strength [expr atan(5e-3)*$refen] -phase $pi -refen $refen 
Drift "DRRF" -len 2.929809287723305 
Quadrupole "DL.SEPTQ" -len [expr 0.5/2.] -K1L [expr -0.5000007127/2.] -refen $refen 
Quadrupole "DL.SEPTQ" -len [expr 0.5/2.] -K1L [expr -0.5000007127/2.] -refen $refen 
