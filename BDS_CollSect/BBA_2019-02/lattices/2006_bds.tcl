Girder
Bpm
Quadrupole -name "FQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.1593135711*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "FDD" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQD" -synrad $quad_synrad -length 0.5 -strength [expr -0.06708030646*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "FDD" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQF2" -synrad $quad_synrad -length 0.5 -strength [expr -0.1182922355*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "FDD" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQD2" -synrad $quad_synrad -length 0.5 -strength [expr 0.02788484518*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Bpm
Quadrupole -name "TQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.1149808129*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD" -synrad $quad_synrad -length 1 -strength [expr -0.1179949196*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF2A" -synrad $quad_synrad -length 0.5 -strength [expr 0.1033739537*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF2B" -synrad $quad_synrad -length 0.5 -strength [expr 0.1033739537*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD2A" -synrad $quad_synrad -length 0.5 -strength [expr -0.08499462268*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD2B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08499462268*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF3A" -synrad $quad_synrad -length 0.5 -strength [expr 0.05660408062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF3B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05660408062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD3A" -synrad $quad_synrad -length 0.5 -strength [expr -0.07925837696*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD3B" -synrad $quad_synrad -length 0.5 -strength [expr -0.07925837696*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF4A" -synrad $quad_synrad -length 0.5 -strength [expr 0.1039004288*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF4B" -synrad $quad_synrad -length 0.5 -strength [expr 0.1039004288*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD4A" -synrad $quad_synrad -length 0.5 -strength [expr -0.08989292421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD4B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08989292421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF5A" -synrad $quad_synrad -length 0.5 -strength [expr 0.05764637677*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF5B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05764637677*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD5" -synrad $quad_synrad -length 1 -strength [expr -0.1107619552*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF6A" -synrad $quad_synrad -length 0.5 -strength [expr 0.02612552684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF6B" -synrad $quad_synrad -length 0.5 -strength [expr 0.02612552684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD6A" -synrad $quad_synrad -length 0.5 -strength [expr -0.02002336697*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD6B" -synrad $quad_synrad -length 0.5 -strength [expr -0.02002336697*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF7A" -synrad $quad_synrad -length 0.5 -strength [expr 0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF7B" -synrad $quad_synrad -length 0.5 -strength [expr 0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD7A" -synrad $quad_synrad -length 0.5 -strength [expr -0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD7B" -synrad $quad_synrad -length 0.5 -strength [expr -0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF8A" -synrad $quad_synrad -length 0.5 -strength [expr 0.0200862619*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF8B" -synrad $quad_synrad -length 0.5 -strength [expr 0.0200862619*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD8" -synrad $quad_synrad -length 1 -strength [expr -0.1038009056*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL89" -length 3.794391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF9" -synrad $quad_synrad -length 1 -strength [expr -0.1524476254*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDLE" -length 2.020466817 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD9" -synrad $quad_synrad -length 1 -strength [expr 0.134790853*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "YGIRDER" -length 2.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*463.8761985*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC4" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-458.648884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DSXL" -length 2.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D1BC" -length 0.1 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BC" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BC" -length 11.633515 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D4BC" -length 0.1925765 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SEXIT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -20*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "BTFD0" -length 0.02536345 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 2.5 -strength [expr -0.1209993*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD1" -length 4.3563465 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 2.5 -strength [expr 0.03044046*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD2" -length 9.99755 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 2.5 -strength [expr 0.05048062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD3" -length 9.98829 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 2.5 -strength [expr -0.0666287*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD4" -length 0.02627595 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
