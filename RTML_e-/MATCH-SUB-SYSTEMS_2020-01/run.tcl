# CLIC Main Beam RTML
Octave {initial=tic();}

ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set wakedir    ./Wakefields
set beamdir    ./Beams

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
if {$beamparams(usewakefields)==1} {
   source $paramsdir/rf_parameters_ww.tcl
}
create_particles_file $beamdir/particles.in beamparams
source $scriptdir/beamline_and_wake.tcl
make_particle_beam beam0 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

Octave {
    disp("0010");
    [E,B] = placet_test_no_correction("0010", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0020");
    [E,B] = placet_test_no_correction("0020", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0030");
    [E,B] = placet_test_no_correction("0030", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0040");
    [E,B] = placet_test_no_correction("0040", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0050");
    [E,B] = placet_test_no_correction("0050", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0060");
    [E,B] = placet_test_no_correction("0060", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0070");
    [E,B] = placet_test_no_correction("0070", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0080");
    [E,B] = placet_test_no_correction("0080", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0090");
    [E,B] = placet_test_no_correction("0090", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0100");
    [E,B] = placet_test_no_correction("0100", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0110");
    [E,B] = placet_test_no_correction("0110", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0120");
    [E,B] = placet_test_no_correction("0120", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0130");
    [E,B] = placet_test_no_correction("0130", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0140");
    [E,B] = placet_test_no_correction("0140", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0150");
    [E,B] = placet_test_no_correction("0150", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0160");
    [E,B] = placet_test_no_correction("0160", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0170");
    [E,B] = placet_test_no_correction("0170", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0180");
    [E,B] = placet_test_no_correction("0180", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0190");
    [E,B] = placet_test_no_correction("0190", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0200");
    [E,B] = placet_test_no_correction("0200", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0210");
    [E,B] = placet_test_no_correction("0210", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0220");
    [E,B] = placet_test_no_correction("0220", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0230");
    [E,B] = placet_test_no_correction("0230", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0240");
    [E,B] = placet_test_no_correction("0240", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0250");
    [E,B] = placet_test_no_correction("0250", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0260");
    [E,B] = placet_test_no_correction("0260", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0270");
    [E,B] = placet_test_no_correction("0270", "beam0", "None");
    disp_results(B);
    save_beam("$beamdir/particles.270.out", B);
    placet_set_beam("beam0", B);

    disp("0280");
    [E,B] = placet_test_no_correction("0280", "beam0", "None");
    disp_results(B);

    toc(initial);
}
