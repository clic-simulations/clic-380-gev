This folder contains PLACET simulations of CLIC at 380 GeV.
Only main beam simulations are included here.

Within each sub-folder there is an isolated folder with the name: TYPE_YYYY-VV,
where YYYY is the year, VV is the version and TYPE is one of the following:
- Plain - performs a plain tracking simulation.
- BBA   - performs a beam-based alignment simulation.
- GM    - performs a tracking simulation with ground motion.
- SF    - performs a tracking simulations with stray magnetic fields.
- LAT   - generates a lattice file (does no tracking).
- MATCH - performs matching.

Each simulation contains a run.tcl file which can be used to execute the simulation. Normally this file takes some command line arguments.

N.B. luminosity calculations require the C-version of GUINEA-PIG, on LXPLUS the executable is guinea-old.
