#
# matching of Turn Around Loop to BC2 RF
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_tal_to_bc2_rf {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 10
    set quad_synrad   0

    set lquadm 0.3
    
    set kqm1  1.4441626417686868
    set kqm2 -1.0513302105084468

    set ldm1 2.57024477373983
    set ldm2 4.96895249070090

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
    Drift -name "Marker-BC2-start" -length 0.0
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D200-1"
    Drift -length $ldm1
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D200-2"
    Drift -length $ldm2
    Drift -name "Marker-TAL2-end" -length 0.0
}
