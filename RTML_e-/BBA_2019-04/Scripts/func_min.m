function demitt = correction1(X)
    global counter;
    global MaxIter;
    global machine;
    save(['X_ca-' num2str(machine) '.dat'], 'X');
    ret = system(['placet -s Main/sext_ca.tcl machine ' num2str(machine)]);
    if(ret == 0)
        R = load(['Emitt0-CA-rtml-30-' num2str(machine) '.dat']).E;
        emitx = R(end,2)*(1+0.01*randn());
        emity = R(end,6)*(1+0.01*randn());
        demitt = emitx/7.0 + emity/0.05;
    else
        demitt = 100000000;
    end
end

function demitt = correction2(X)
    global counter;
    global MaxIter;
    global machine;
    save(['X_tal-' num2str(machine) '.dat'], 'X');
    ret = system(['placet -s Main/sext_tal1.tcl machine ' num2str(machine)]);
    if(ret == 0)
        R = load(['Emitt0-TAL1-rtml-30-' num2str(machine) '.dat']).E;
        emitx = R(end,2)*(1+0.01*randn());
        emity = R(end,6)*(1+0.01*randn());
        demitt = emitx/7.0 + emity/0.05;
    else
        demitt = 100000000;
    end
end

function demitt = correction3(Xh)
    global counter;
    global MaxIter;
    global machine;
    X = load(['X_tal-' num2str(machine) '.dat']).X;
    X(1:5) = 5*Xh;
    save(['X_tal-' num2str(machine) '.dat'], 'X');
    ret = system(['placet -s Main/sext_tal2.tcl machine ' num2str(machine)]);
    if(ret == 0)
        R = load(['Emitt0-ALL-rtml-30-' num2str(machine) '.dat']).E;
        emitx = R(end,2)*(1+0.01*randn());
        demitt = emitx/7.0;
    else
        demitt = 100000000;
    end
end

function demitt = correction4(Xh)
    global counter;
    global MaxIter;
    global machine;
    X = load(['X_tal-' num2str(machine) '.dat']).X;
    X(6:10) = Xh;
    save(['X_tal-' num2str(machine) '.dat'], 'X');
    ret = system(['placet -s Main/sext_tal2.tcl machine ' num2str(machine)]);
    if(ret == 0)
        R = load(['Emitt0-ALL-rtml-30-' num2str(machine) '.dat']).E;
        emity = R(end,6)*(1+0.01*randn());
        demitt = emity/0.05;
    else
        demitt = 100000000;
    end
end
