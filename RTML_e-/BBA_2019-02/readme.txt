#
# These files were modified by C. Gohil from files developed by Y. Han.
#
# Static errors are applied to the RTML and Beam-Based Alignment (BBA) techniques
# are applied.
#

# STEP 1 : generate wakefield file
# These are the files Wt_*.dat and Wl_*.dat.
# They are already present in ./Wakefields, so this step can be skipped.
# However, this would need to be re-run if RF parameters change.
placet ./Wakefields/create_wake.tcl

# STEP 2 : generate response matrix
# These are the files R_parm_*.dat.
# These are already present in ./ResponseMatrix, so this step can be skipped.
# This would need to be re-run if lattices change.
# Generating the response matrices takes approximately 2-3 hour.
placet main_e-_responseMatrix.tcl

# STEP 3: Apply the BBA for different random seed (machine)
# <machine> is an integer passed as a command line argument to run.tcl
placet run.tcl <machine>
