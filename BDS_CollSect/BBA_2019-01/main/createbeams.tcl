puts "Creating beams"

set e0 $e_initial
set e2 [expr $e0 * (1.0 + $deltae)]
set e1 [expr $e0 * (1.0 - $deltae)]

set n_slice 1
set n 1
set n_total [expr $n_slice*$n]

make_beam_many beam0 $n_slice $n
make_beam_many beam1 $n_slice $n
make_beam_many beam2 $n_slice $n

set n_slice 20
set n 500
set n_total [expr $n_slice*$n]

make_beam_many beam0t $n_slice $n

Octave {
    disp("Loading beam-$machine.dat");
    BeamIn = load("$script_dir/beams/beam-$machine.dat");
    placet_set_beam("beam0t", BeamIn);

    Tcl_SetVar("ml_x",  mean(BeamIn(:,2)));
    Tcl_SetVar("ml_y",  mean(BeamIn(:,3)));
    Tcl_SetVar("ml_xp", mean(BeamIn(:,5)));
    Tcl_SetVar("ml_yp", mean(BeamIn(:,6)));
}

exec echo "$e0 $ml_x $ml_y 0 $ml_xp $ml_yp" > particles.in
BeamRead -file particles.in -beam beam0

exec echo "$e1 $ml_x $ml_y 0 $ml_xp $ml_yp" > particles.in
BeamRead -file particles.in -beam beam1

exec echo "$e2 $ml_x $ml_y 0 $ml_xp $ml_yp" > particles.in
BeamRead -file particles.in -beam beam2
