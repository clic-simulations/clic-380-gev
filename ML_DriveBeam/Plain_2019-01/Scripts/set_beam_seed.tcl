#
# This script sets a seed for the random generator used to generate the initial
# beam distribution.
#
# The seed is taken as a command line argument. If no argument is given the beam seed
# is set to zero.
#
if {$argc == 1} {
    set beam_seed [lindex $argv 0]
} else {
    set beam_seed 0
}
