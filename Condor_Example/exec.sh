#!/bin/bash

# Setup PLACET/GUINEA-PIG
source /cvmfs/clicbp.cern.ch/x86_64-slc6-gcc62-opt/setup.sh

# Path to PLACET simulations files
export PATH_TO_SIMULATION_FILES=/afs/cern.ch/user/c/cgohil/condor_example/Simulation_Files

# Copy the simulation files to the worker node
cp -r $PATH_TO_SIMULATION_FILES/* .

# Run the simulation
placet -s run.tcl ${1} ${2}
