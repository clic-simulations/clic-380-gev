#
# Long Transfer Line Lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_long_transfer_line {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 10
set quad_synrad 0

set refenergy $beamparams(meanenergy)
set e0 $refenergy

set lquadm 0.36

set kqmx 0.009713020617
set kqm1 $kqmx
set kqm2 [expr $kqmx * -1.0]
set kqm3 $kqmx
set kqm4 [expr $kqmx * -1.0]

set ldm1 [expr 219.0 - 0.36]
set ldm2 $ldm1
set ldm3 $ldm1
set ldm4 $ldm1

SetReferenceEnergy $refenergy

Girder
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr 0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-1" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr -0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-2" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr 0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-3" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr -0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-4" -length 0.0
Drift -length 218.64 -six_dim 1
Girder
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr 0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-1-1" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr -0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-1-2" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr 0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-1-3" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr -0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-1-4" -length 0.0
Drift -length 218.64 -six_dim 1
Girder
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr 0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-2-1" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr -0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-2-2" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr 0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-2-3" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr -0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-2-4" -length 0.0
Drift -length 218.64 -six_dim 1
Girder
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr 0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-3-4" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr -0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-3-2" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr 0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-3-3" -length 0.0
Drift -length 218.64 -six_dim 1
Bpm
Quadrupole -synrad 0 -length 0.36 -strength [expr -0.00349668742212*$e0] -six_dim 1 -thin_lens 10
Dipole -name "D170-3-4" -length 0.0
Drift -length 218.64 -six_dim 1

}
