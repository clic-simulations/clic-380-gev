## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
 ## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
 ## Which means there will be a slight magnet missfocus for the other bunches. 
 set Pi [expr acos(-1)] 
 if {$Dipole_synrad} { 
 set refen 1.90487239 
 } else { 
 set refen 1.906 
 } 
 ####START#### 
 Marker "CR2_P1.INJECTION" 
 #### Beamline under construction #################################### 
 #### Injection needs to be done into after the sextupole section #### 
 #Drift "CR2.SEPTUM" -len 1 
 #Drift "CR2.DRFB6" -len 2.367622162 
 #Sextupole "SEXT.CR2.SRFB6.IN2" -len 0.4 -S2 [expr -8.181902960699396/2.38*$refen] -refen $refen 
 #Drift "CR2.DRFB4A" -len 0.709135479 
 #Drift "CR2.DS1" -len 0.5 
 #Sextupole "SEXT.CR2.SRFB5.IN2" -len 0.5 -S2 [expr 0.6930613244632331/2.38*$refen] -refen $refen 
 #Drift "CR2.DS1" -len 0.5 
 #Sextupole "SEXT.CR2.SRFB4.IN2" -len 0.5 -S2 [expr 3.395028044914335/2.38*$refen] -refen $refen 
 ##################################################################### 
 Drift "CR2.D1M" -len 1 
 Drift -name "CR2.D1CM" -len 0.01 
 Quadrupole "QUAD.CR2.QRFB3.IN2" -len 0.5 -K1L 0.12217763660487391 -refen $refen 
 Drift "CR2.DRFB3" -len 3.771233433 
 Quadrupole "QUAD.CR2.QRFB2.IN2" -len 0.5 -K1L 0.19663214464538725 -refen $refen 
 Drift "CR2.DRFB2" -len 1.193240969 
 Quadrupole "QUAD.CR2.QRFB1.IN2" -len 0.5 -K1L -0.16496467233449622 -refen $refen 
 Drift "CR2.DRFB1" -len 0.217192186 
 #### RF CORRECTOR #### 
 Drift "DRRF" -len 1 
 #Kalign al2 -cz 0 
 #Drift "CR2.al2" -len 0 -node 1 -kick al2 
 Rfmultipole "RFMulti.CR2.RFKICK2" -len 0 -strength [expr -0.004*$refen] -freq [expr 0.9995*3.] -refen $refen 
 Drift "DRRF" -len 1 
 ###################### 
 Drift "CR2.DL3IN" -len 0.4000389758 
 Quadrupole "QUAD.CR2.QL3IN.IN2" -len 0.5 -K1L 0.07808894574928223 -refen $refen 
 Bpm "BPM.CR2_1.4" -len 0 
 Drift "CR2.DL2IN" -len 1.103409345 
 Quadrupole "QUAD.CR2.QL2IN.IN2" -len 0.5 -K1L -0.6360699407020488 -refen $refen 
 Bpm "BPM.CR2_1.5" -len 0 
 Drift "CR2.DL1IN" -len 0.4000469975 
 Quadrupole "QUAD.CR2.QL1IN.IN2" -len 0.5 -K1L 1.1952772365386342 -refen $refen 
 Bpm "BPM.CR2_1.6" -len 0 
 Drift "CR2.DL0IN" -len 0.5 
 Quadrupole "QUAD.CR2.QL0IN.IN2" -len 0.5 -K1L -0.38500969083552583 -refen $refen 
 Drift "CR2.DL0" -len 1.246504682 
 #### Missing length #### 
 Drift "CR2.MISSLENGTH" -len 0.202692445399988 
 ######################## 
 Marker "CR2MDACELLSTART" 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 ####################### 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SN1.1" -len 0.1 -K2L 2.9099364205214404 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Marker "CR2.DBA.HBM" 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.1" -len 0.1 -K2L 0.024782760562737288 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SN3.1" -len 0.1 -K2L -1.662617056158258 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SNN3.1" -len 0.1 -K2L -0.6779866291498523 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.1" -len 0.1 -K2L 0.024782767506930113 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SNN1.1" -len 0.1 -K2L -5.597601140472675 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183728 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183728 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.039925864400000004 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SNN1.1" -len 0.1 -K2L -5.597644355631491 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.1" -len 0.1 -K2L 0.024782958837324478 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SNN3.1" -len 0.1 -K2L -0.6779920533826441 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SN3.1" -len 0.1 -K2L -1.6626303579278927 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.1" -len 0.1 -K2L 0.024782965781406194 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SN1.1" -len 0.1 -K2L 2.909960516839415 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 ######################################## 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864928 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Marker "CR2MDACELLEND" 
 Drift "CR2.DTR1A" -len 1.1 
 Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
 Drift "CR2.DTR1B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
 Bpm "BPM.CR2_1.7" -len 0 
 Drift "CR2.DTR2A" -len 1.1 
 Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.84671680555554 -refen $refen 
 Drift "CR2.DTR2B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
 Bpm "BPM.CR2_1.8" -len 0 
 Drift "CR2.DTR3A" -len 1.5 
 Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458445 -refen $refen 
 Drift "CR2.DTR3B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
 Bpm "BPM.CR2_1.9" -len 0 
 Drift "CR2.DTR4A" -len 2.1328 
 Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
 Drift "CR2.DTR4B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR4" -len 0.25 -K1L 0.18700585960992677 -refen $refen 
 Bpm "BPM.CR2_1.10" -len 0 
 Quadrupole "QUAD.CR2.QTR4" -len 0.25 -K1L 0.18700585960992677 -refen $refen 
 Bpm "BPM.CR2_1.11" -len 0 
 Drift "CR2.DTR4B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
 Drift "CR2.DTR4A" -len 2.1328 
 Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
 Bpm "BPM.CR2_1.12" -len 0 
 Drift "CR2.DTR3B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458445 -refen $refen 
 Drift "CR2.DTR3A" -len 1.5 
 Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
 Bpm "BPM.CR2_1.13" -len 0 
 Drift "CR2.DTR2B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.84671680555554 -refen $refen 
 Drift "CR2.DTR2A" -len 1.1 
 Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
 Bpm "BPM.CR2_1.14" -len 0 
 Drift "CR2.DTR1B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
 Drift "CR2.DTR1A" -len 1.1 
 Marker "CR2MDACELLSTART" 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864928 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 ####################### 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SN1.2" -len 0.1 -K2L 2.8946720250731914 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Marker "CR2.DBA.HBM" 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.2" -len 0.1 -K2L 1.0533004831254797 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SN3.2" -len 0.1 -K2L -2.6276115462021217 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SNN3.2" -len 0.1 -K2L -0.40885622141464434 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967743999999 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.2" -len 0.1 -K2L 1.053300778248799 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SNN1.2" -len 0.1 -K2L -1.5505738437255494 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.039925864400000004 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SNN1.2" -len 0.1 -K2L -1.5505858140458277 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.2" -len 0.1 -K2L 1.0533089096562234 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SNN3.2" -len 0.1 -K2L -0.408859492312291 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SN3.2" -len 0.1 -K2L -2.627632567402174 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.2" -len 0.1 -K2L 1.053309204774821 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.1143160000000003 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SN1.2" -len 0.1 -K2L 2.894695993840666 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 ######################################## 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Marker "CR2MDACELLEND" 
 Drift "CR2.DS1C_A" -len 2.179474654 
 Sextupole "SEXT.CR2.SXS1C" -len 0.1 -K2L 2.1606775839716965 -refen $refen 
 Drift "CR2.DS1C_B" -len 0.3 
 Quadrupole "QUAD.CR2.QS1C" -len 0.5 -K1L -0.647356904204382 -refen $refen 
 Bpm "BPM.CR2_1.15" -len 0 
 Drift "CR2.DS2C" -len 0.2003907272 
 Quadrupole "QUAD.CR2.QS2C" -len 0.5 -K1L 0.7905004870236609 -refen $refen 
 Bpm "BPM.CR2_1.16" -len 0 
 Drift "CR2.DS3C_A" -len 0.3 
 Sextupole "SEXT.CR2.SXS3C" -len 0.1 -K2L -0.42652765870388576 -refen $refen 
 Drift "CR2.DS3C_B" -len 2.777418619 
 Quadrupole "QUAD.CR2.QS3C" -len 0.5 -K1L -0.24686896328832697 -refen $refen 
 Drift "CR2.DS3C_C" -len 0.5 
 Quadrupole "QUAD.CR2.QS4C" -len 0.5 -K1L 0.24225644224167997 -refen $refen 
 Bpm "BPM.CR2_1.17" -len 0 
 Drift "CR2.DS4C" -len 0.2407160001 
 Drift "CR2.BC1" -len 0.4 
 Drift "CR2.BC2" -len 0.4 
 Drift "CR2.BC3" -len 0.4 
 Drift "CR2.BC1" -len 0.4 
 Drift "CR2.DS4C" -len 0.2407160001 
 Quadrupole "QUAD.CR2.QS4C" -len 0.5 -K1L 0.24225644224167997 -refen $refen 
 Drift "CR2.DS3C_C" -len 0.5 
 Quadrupole "QUAD.CR2.QS3C" -len 0.5 -K1L -0.24686896328832697 -refen $refen 
 Bpm "BPM.CR2_1.18" -len 0 
 Drift "CR2.DS3C_B" -len 2.777418619 
 Sextupole "SEXT.CR2.SXS3C" -len 0.1 -K2L -0.42652765870388576 -refen $refen 
 Drift "CR2.DS3C_A" -len 0.3 
 Quadrupole "QUAD.CR2.QS2C" -len 0.5 -K1L 0.7905004870236609 -refen $refen 
 Bpm "BPM.CR2_1.19" -len 0 
 Drift "CR2.DS2C" -len 0.2003907272 
 Quadrupole "QUAD.CR2.QS1C" -len 0.5 -K1L -0.647356904204382 -refen $refen 
 Bpm "BPM.CR2_1.20" -len 0 
 Drift "CR2.DS1C_B" -len 0.3 
 Sextupole "SEXT.CR2.SXS1C" -len 0.1 -K2L 2.1606775839716965 -refen $refen 
 Drift "CR2.DS1C_A" -len 2.179474654 
 Marker "CR2MDACELLSTART" 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 ####################### 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SN1.3" -len 0.1 -K2L 3.4541656350989833 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Marker "CR2.DBA.HBM" 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.3" -len 0.1 -K2L 0.6232886190538374 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SN3.3" -len 0.1 -K2L -2.667854622409345 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SNN3.3" -len 0.1 -K2L -0.3217211501703927 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.3" -len 0.1 -K2L 0.6232887936841351 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SNN1.3" -len 0.1 -K2L -1.1292387031098672 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.03992586439999999 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.08046148038247242 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.28064251000565993 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.08046148038247242 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SNN1.3" -len 0.1 -K2L -1.1292474203342584 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.3" -len 0.1 -K2L 0.6232936051985298 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SNN3.3" -len 0.1 -K2L -0.3217237238537268 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SN3.3" -len 0.1 -K2L -2.667875964534275 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.3" -len 0.1 -K2L 0.6232937798260335 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SN1.3" -len 0.1 -K2L 3.4541942352714012 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 ######################################## 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Marker "CR2MDACELLEND" 
 Drift "CR2.DTR1A" -len 1.1 
 Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
 Drift "CR2.DTR1B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
 Bpm "BPM.CR2_1.7" -len 0 
 Drift "CR2.DTR2A" -len 1.1 
 Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.846716805555538 -refen $refen 
 Drift "CR2.DTR2B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
 Bpm "BPM.CR2_1.8" -len 0 
 Drift "CR2.DTR3A" -len 1.5 
 Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458447 -refen $refen 
 Drift "CR2.DTR3B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
 Bpm "BPM.CR2_1.9" -len 0 
 Drift "CR2.DTR4A" -len 2.1328 
 Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
 Drift "CR2.DTR4B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR4" -len 0.25 -K1L 0.18700585960992677 -refen $refen 
 Bpm "BPM.CR2_1.10" -len 0 
 Quadrupole "QUAD.CR2.QTR4" -len 0.25 -K1L 0.18700585960992677 -refen $refen 
 Bpm "BPM.CR2_1.11" -len 0 
 Drift "CR2.DTR4B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
 Drift "CR2.DTR4A" -len 2.1328 
 Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
 Bpm "BPM.CR2_1.12" -len 0 
 Drift "CR2.DTR3B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458447 -refen $refen 
 Drift "CR2.DTR3A" -len 1.5 
 Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
 Bpm "BPM.CR2_1.13" -len 0 
 Drift "CR2.DTR2B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.846716805555538 -refen $refen 
 Drift "CR2.DTR2A" -len 1.1 
 Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
 Bpm "BPM.CR2_1.14" -len 0 
 Drift "CR2.DTR1B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
 Drift "CR2.DTR1A" -len 1.1 
 Marker "CR2MDACELLSTART" 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 ####################### 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SN1.4" -len 0.1 -K2L 4.993544501614853 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Marker "CR2.DBA.HBM" 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.4" -len 0.1 -K2L 0.6933812943848937 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SN3.4" -len 0.1 -K2L -2.467767181454387 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SNN3.4" -len 0.1 -K2L -0.6647339857763788 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.4" -len 0.1 -K2L 0.6933814886441285 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SNN1.4" -len 0.1 -K2L -9.259473276453614 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.03992586439999999 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SNN1.4" -len 0.1 -K2L -9.25954475206984 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.4" -len 0.1 -K2L 0.6933868409862867 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SNN3.4" -len 0.1 -K2L -0.6647393032156171 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SN3.4" -len 0.1 -K2L -2.4677869219856707 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.4" -len 0.1 -K2L 0.6933870352424135 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SN1.4" -len 0.1 -K2L 4.993585845717636 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 ######################################## 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864919 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Marker "CR2MDACELLEND" 
 #### Missing length #### 
 Drift "CR2.MISSLENGTH" -len 0.202692445399988 
 ######################## 
 Drift "CR2.DL0IN" -len 2.246504682 
 Quadrupole "QUAD.CR2.QL1IN.EXT" -len 0.5 -K1L -0.6970772319488282 -refen $refen 
 Bpm "BPM.CR2_1.29" -len 0 
 Drift "CR2.DL1IN" -len 0.4000469975 
 Quadrupole "QUAD.CR2.QL2IN.EXT" -len 0.5 -K1L 0.8526680541738719 -refen $refen 
 Bpm "BPM.CR2_1.30" -len 0 
 Drift "CR2.DL2IN" -len 1.103409345 
 Quadrupole "QUAD.CR2.QL3IN.EXT" -len 0.5 -K1L -0.24998982537871087 -refen $refen 
 Bpm "BPM.CR2_1.31" -len 0 
 Drift "CR2.DL3IN" -len 0.4000389758 
 #### STEP EXTRACTOR - kicks on 4*3*244 bunches at a time (-strength [expr -0.004e6*$refen]) ### 
 Stepdipole "CR2.EXTKICKER" -len 2. -strength [expr -0.004*$refen] -freq [expr 0.49975/(244*3*4)] -phase [expr 3./4.*$Pi] -duty [expr 1./4.] -refen $refen 
 Drift "CR2.DRFB1" -len 0.217192186 
 Quadrupole "QUAD.CR2.QRFB1.EXT" -len 0.5 -K1L -1.079720421877582 -refen $refen 
 Bpm "BPM.CR2_1.32" -len 0 
 Drift "CR2.DRFB2" -len 1.193240969 
 Quadrupole "QUAD.CR2.QRFB2.EXT" -len 0.5 -K1L 0.5591986720709291 -refen $refen 
 Bpm "BPM.CR2_1.33" -len 0 
 Drift "CR2.DRFB3" -len 3.771233433 
 Quadrupole "QUAD.CR2.QRFB3.EXT" -len 0.5 -K1L -0.22580823301932673 -refen $refen 
 Bpm "BPM.CR2_1.34" -len 0 
 Drift -name "CR2.D1CM" -len 0.01 
 Drift "CR2.D1M" -len 1 
 Drift "CR2.D1M" -len 1 
 Drift "CR2.D1M" -len 1 
 Drift "CR2.DRFB4A" -len 0.709135479 
 Drift "CR2.DSRFB6" -len 0.4 
 Drift "CR2.DRFB6" -len 2.367622162 
 Drift "CR2.SEPTUM" -len 1 
 Marker "CR2_P1.EXTRACTION" 
 
