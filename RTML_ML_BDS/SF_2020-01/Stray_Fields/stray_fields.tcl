# 
# This code was developed by C. Gohil.
#
# Contains an implementation to simulate stray magnetic fields.
#
# Involves hard coding a dipole grid into the lattice files and using these to 
# represent kicks from the stray field.
#
# This implementation requires the functions from generator.py. To make these
# available execute:
#
#   export PYTHONPATH=/path/to/these/files/Stray_Fields:$PYTHONPATH
#

#
# Stray field settings
#
array set stray_field_settings {
    model        "lhc_2d_amplitudes.dat"
    model_type   "2d_psd"
    delta_T      0.02
    spacing      1.0
}

if {"$mitigation" == "None"} {
    puts "No mitigation"
    set stray_field_settings(use_filter) 0
    set stray_field_settings(filter)     ""
    set stray_field_settings(filter_ml)  ""
    set stray_field_settings(mu_metal)   "none"
} elseif {"$mitigation" == "beam-pipe"} {
    puts "Simulating beam pipe"
    set stray_field_settings(use_filter) 1
    set stray_field_settings(filter)     "copper_steel_beam_pipe.dat"
    set stray_field_settings(filter_ml)  "ml_cavities.dat"
    set stray_field_settings(mu_metal)   "none"
} elseif {"$mitigation" == "recursive"} {
    puts "Simulating beam-based feedback"
    set stray_field_settings(use_filter) 1
    set stray_field_settings(filter)     "recursive_feedback.dat"
    set stray_field_settings(filter_ml)  "recursive_feedback.dat"
    set stray_field_settings(mu_metal)   "none"
} elseif {"$mitigation" == "recursive-beam-pipe"} {
    puts "Simulating beam-based feedback and beam pipe"
    set stray_field_settings(use_filter) 1
    set stray_field_settings(filter)     "copper_steel_beam_pipe_recursive_feedback.dat"
    set stray_field_settings(filter_ml)  "ml_cavities_recursive_feedback.dat"
    set stray_field_settings(mu_metal)   "none"
} elseif {"$mitigation" == "recursive-mu-metal"} {
    puts "Simulating beam-based feedback and mu-metal"
    set stray_field_settings(use_filter) 1
    set stray_field_settings(filter)     "recursive_feedback.dat"
    set stray_field_settings(filter_ml)  "recursive_feedback.dat"
    set stray_field_settings(mu_metal)   "rtml+bds"
} elseif {"$mitigation" == "mu-metal"} {
    puts "Simulating mu-metal"
    set stray_field_settings(use_filter) 0
    set stray_field_settings(filter)     "copper_steel_beam_pipe.dat"
    set stray_field_settings(filter_ml)  "ml_cavities.dat"
    set stray_field_settings(mu_metal)   "rtml+bds"
} elseif {"$mitigation" == "beam-pipe-mu-metal"} {
    puts "Simulating beam pipe and mu-metal"
    set stray_field_settings(use_filter) 1
    set stray_field_settings(filter)     "copper_steel_beam_pipe.dat"
    set stray_field_settings(filter_ml)  "ml_cavities.dat"
    set stray_field_settings(mu_metal)   "rtml+bds"
} elseif {"$mitigation" == "recursive-beam-pipe-mu-metal"} {
    puts "Simulating beam-based feedback, beam pipe and mu-metal"
    set stray_field_settings(use_filter) 1
    set stray_field_settings(filter)     "copper_steel_beam_pipe_recursive_feedback.dat"
    set stray_field_settings(filter_ml)  "ml_cavities_recursive_feedback.dat"
    set stray_field_settings(mu_metal)   "rtml+bds"
} elseif {"$mitigation" == "tal-correction"} {
    puts "Simulating beam-based feedback, beam pipe and mu-metal (in BDS only)"
    set stray_field_settings(use_filter) 1
    set stray_field_settings(filter)     "copper_steel_beam_pipe_recursive_feedback.dat"
    set stray_field_settings(filter_ml)  "ml_cavities_recursive_feedback.dat"
    set stray_field_settings(mu_metal)   "bds"
} elseif {"$mitigation" == "ip-feedback"} {
    puts "Simulating beam-based feedback, beam pipe and mu-metal (in RTML only)"
    set stray_field_settings(use_filter) 1
    set stray_field_settings(filter)     "copper_steel_beam_pipe_recursive_feedback.dat"
    set stray_field_settings(filter_ml)  "ml_cavities_recursive_feedback.dat"
    set stray_field_settings(mu_metal)   "rtml"
}

#
# Stray field implementation
#
# Arguments:
# - sf_seed is any integer - seed for the stray field generator
# - sf_iter - time-index for the stray field generator, t=sf_iter*delta_T
# - particle is "e-" or "e+"
#
proc set_stray_field {beamline sf_seed sf_iter particle} {
global stray_field_settings sfdir
Python {
from python_functions import generator
generator("$sfdir/models/$stray_field_settings(model)",
          "$stray_field_settings(model_type)",
          "$beamline",
           $stray_field_settings(use_filter),
          "$sfdir/transfer_functions/$stray_field_settings(filter)",
          "$sfdir/transfer_functions/$stray_field_settings(filter_ml)",
           $stray_field_settings(delta_T),
           $stray_field_settings(spacing),
           $sf_seed, $sf_iter, "$particle",
          "$sfdir/footprints",
           None,
          "$stray_field_settings(mu_metal)")
}
}
