#
# Dump and match long transfer line to turn around loop
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

# Feed forward system
proc FF2 {} {}

proc lattice_dump_and_match_ltl_to_tal {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 10
    set quad_synrad   0
	set usesynrad     $beamparams(useisr)
	set mult_synrad   0
    
    set lquadm 0.3
    set lquadx 0.36
    
    set kqm1  0.009713020617
    set kqm2 -0.01666831502
    set kqm3  0.02506010184
    set kqm4  0.0817375657
    set kqm5 -0.1208106861
    set kqm6  0.5742405616
    
    set ldm1  5.0
    set ldm2 10.0
    set ldm3 10.0
    set ldm4 13.0
    set ldm5 65.0

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
	Drift -name "DRIFT_1" -length 1
	Sbend -name "BEND1" -synrad $usesynrad -length 0.8436627613 -angle 0.002812209204 -E1 0 -E2 0 -six_dim 1 -e0 $refenergy -comment D2T1
	set e0 [expr $refenergy-14.1e-6*0.002812209204*0.002812209204/0.8436627613*$refenergy*$refenergy*$refenergy*$refenergy*$usesynrad]
	SetReferenceEnergy $refenergy
	Drift -name "DRIFT_2" -length 0.1
	Quadrupole -name "QM6" -synrad $quad_synrad -length 0.35 -strength [expr 0.01033514896*$refenergy*2] -e0 $refenergy
	Drift -name "DRIFT_3387" -length 18.21174984
	Quadrupole -name "QM7" -synrad $quad_synrad -length 0.35 -strength [expr -0.01666403078*$refenergy*2] -e0 $refenergy
	Drift -name "DRIFT_2" -length 0.1
	Sbend -name "BEND2" -synrad $usesynrad -length 0.8436627613 -angle -0.002812209204 -E1 0 -E2 0 -six_dim 1 -e0 $refenergy -comment D2T1
	set e0 [expr $refenergy-14.1e-6*-0.002812209204*-0.002812209204/0.8436627613*$refenergy*$refenergy*$refenergy*$refenergy*$usesynrad]
	SetReferenceEnergy $refenergy
	Drift -name "DRIFT_3388" -length 24.43066582
	Quadrupole -name "QM1" -synrad $quad_synrad -length 0.35 -strength [expr 0.02183037577*$refenergy*2] -e0 $refenergy
	Drift -name "DRIFT_3389" -length 1.200000001
	Quadrupole -name "QM2" -synrad $quad_synrad -length 0.35 -strength [expr -0.02717745522*$refenergy*2] -e0 $refenergy
	Drift -name "DRIFT_3389" -length 1.200000001
	Quadrupole -name "QM1" -synrad $quad_synrad -length 0.35 -strength [expr 0.02183037577*$refenergy*2] -e0 $refenergy
	Drift -name "DRIFT_3388" -length 24.43066582
	Sbend -name "BEND2" -synrad $usesynrad -length 0.8436627613 -angle -0.002812209204 -E1 0 -E2 0 -six_dim 1 -e0 $refenergy -comment D2T1
	set e0 [expr $refenergy-14.1e-6*-0.002812209204*-0.002812209204/0.8436627613*$refenergy*$refenergy*$refenergy*$refenergy*$usesynrad]
	SetReferenceEnergy $refenergy
	Drift -name "DRIFT_2" -length 0.1
	Quadrupole -name "QM7" -synrad $quad_synrad -length 0.35 -strength [expr -0.01666403078*$refenergy*2] -e0 $refenergy
	Drift -name "DRIFT_3387" -length 18.21174984
	Quadrupole -name "QM6" -synrad $quad_synrad -length 0.35 -strength [expr 0.01033514896*$refenergy*2] -e0 $refenergy
	Drift -name "DRIFT_2" -length 0.1
	Sbend -name "BEND1" -synrad $usesynrad -length 0.8436627613 -angle 0.002812209204 -E1 0 -E2 0 -six_dim 1 -e0 $refenergy -comment D2T1
	set e0 [expr $refenergy-14.1e-6*0.002812209204*0.002812209204/0.8436627613*$refenergy*$refenergy*$refenergy*$refenergy*$usesynrad]
	SetReferenceEnergy $refenergy
	Drift -name "DRIFT_2" -length 0.1

    set kqm1  0.1361195030358495
    set kqm2 -0.0792179668488854
    set kqm3  0.0951947901571650
    set kqm4 -0.0596419081177367
    set kqm5  0.0325699276640651
    set kqm6 -0.0885876182553608
    set kqm7  0.0780189563741866
    set kqm8  0.1557430985906845

    Bpm
    Quadrupole -synrad $quad_synrad -length 0.35 -strength [expr $kqm1*$refenergy] -e0 $refenergy
    Dipole -name "D180-15"
	Drift -name "DRIFT_0" -length 12.65
    Bpm
	Quadrupole -synrad $quad_synrad -length 0.35 -strength [expr $kqm2*$refenergy] -e0 $refenergy
    Dipole -name "D180-17"
	Drift -name "DRIFT_0" -length 12.65
    Bpm
	Quadrupole -synrad $quad_synrad -length 0.35 -strength [expr $kqm3*$refenergy] -e0 $refenergy
    Dipole -name "D180-19"
	Drift -name "DRIFT_0" -length 12.65
    Bpm
	Quadrupole -synrad $quad_synrad -length 0.35 -strength [expr $kqm4*$refenergy] -e0 $refenergy
    Dipole -name "D180-21"
	Drift -name "DRIFT_0" -length 12.65
    Bpm
	Quadrupole -synrad $quad_synrad -length 0.35 -strength [expr $kqm5*$refenergy] -e0 $refenergy
    Dipole -name "D180-23"
	Drift -name "DRIFT_3390" -length 21.83150356
    Bpm
	Quadrupole -synrad $quad_synrad -length 0.35 -strength [expr $kqm6*$refenergy] -e0 $refenergy
    Dipole -name "D180-25"
	Drift -name "DRIFT_3391" -length 6.401700061
    Bpm
	Quadrupole -synrad $quad_synrad -length 0.35 -strength [expr $kqm7*$refenergy] -e0 $refenergy
    Dipole -name "D180-27"
	Drift -name "DRIFT_3392" -length 8.849291577
    Bpm
	Quadrupole -synrad $quad_synrad -length 0.35 -strength [expr $kqm8*$refenergy] -e0 $refenergy
    Dipole -name "D180-29"

    # Install feed forward
    TclCall -name "FF2" -script "FF2"
    Drift -name "Marker-LTL-end"
}
