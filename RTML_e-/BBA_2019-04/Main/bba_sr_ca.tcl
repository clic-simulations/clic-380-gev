set tcl_precision 15
ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set beamdir    ./Beams
set wakedir    ./Wakefields

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/getResponseMatrix.tcl
source $scriptdir/disperseEnergy.tcl
source $scriptdir/survey.tcl
source $scriptdir/splitBin.tcl
source $scriptdir/plainTracking.tcl
source $scriptdir/OTO.tcl
source $scriptdir/DFS.tcl
source $scriptdir/DFS-bc1.tcl
source $scriptdir/DFS-boo.tcl
source $scriptdir/DFS-ca.tcl
source $scriptdir/DFS-bc2.tcl
source $scriptdir/DFS-tal.tcl

create_particles_file $beamdir/particles.in beamparams
create_zero_wakes_file $wakedir/zero_wake.dat beamparams rfparamsbc1 $beamdir/particles.in

source $scriptdir/beamline_and_wake.tcl

make_particle_beam beam1 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam2 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam3 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam4 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam6 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beamDFS beamparams $beamdir/particles.in $wakedir/zero_wake.dat

source $scriptdir/load_mag.tcl

puts "\nApplying BBA to Spin Rotator"
puts "*****************************************" 
getResponseMatrix SR rtml Marker-SR-start Marker-SR-end beam1 $SRde
splitBin rtml SRncarray Marker-SR-start Marker-SR-end
otoCorrection SR rtml Marker-SR-start Marker-SR-end beam1 $SRbeta0 $SROTOnloop
dfsCorrection SR rtml Marker-SR-start Marker-SR-end beam1 $SRde $SRbeta1 $SRwgt $SRDFSnloop

Octave {
    Start = placet_get_name_number_list("rtml","Marker-SR-start");
    End = placet_get_name_number_list("rtml","Marker-BC1-start");
    [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1, Start, End);
    placet_set_beam("beam2", B);
    Bins = Bins(1);
}

puts "\nApplying BBA to Bunch Compressor 1"
puts "*****************************************" 
getResponseMatrix BC1 rtml Marker-BC1-start Marker-BC1-end beam2 $BC1de
splitBin rtml BC1ncarray Marker-BC1-start Marker-BC1-end
otoCorrection BC1 rtml Marker-BC1-start Marker-BC1-end beam2 $BC1beta0 $BC1OTOnloop
dfsCorrection-bc1 BC1 rtml Marker-BC1-start Marker-BC1-end beam2 $BC1de $BC1beta1 $BC1wgt $BC1DFSnloop

Octave {
    Start = placet_get_name_number_list("rtml","Marker-BC1-start");
    End = placet_get_name_number_list("rtml","Marker-BOO-start");
    [E,B] = placet_test_no_correction("rtml", "beam2", "None", 1, Start, End);
    placet_set_beam("beam3", B);
    Bins = Bins(1);
}

puts "\nApplying BBA to Booster Linac"
puts "*****************************************" 
getResponseMatrix BOO rtml Marker-BOO-start Marker-BOO-end beam3 $BOOde
splitBin rtml BOOncarray Marker-BOO-start Marker-BOO-end
otoCorrection BOO rtml Marker-BOO-start Marker-BOO-end beam3 $BOObeta0 $BOOOTOnloop
dfsCorrection-boo BOO rtml Marker-BOO-start Marker-BOO-end beam3 $BOOde $BOObeta1 $BOOwgt $BOODFSnloop

Octave {
    Start = placet_get_name_number_list("rtml","Marker-BOO-start");
    End = placet_get_name_number_list("rtml","Marker-CA-start");
    [E,B] = placet_test_no_correction("rtml", "beam3", "None", 1, Start, End);
    placet_set_beam("beam4", B);
    Bins = Bins(1);
}

puts "\nApplying BBA to Central Arc"
puts "*****************************************" 
Octave {
    Start = placet_get_name_number_list("rtml","Marker-BOO-start");
    End = placet_get_name_number_list("rtml","Marker-CA-start");
    decrease_grad("rtml",$BOOde,Start,End);
    decrease_strength("rtml",$BOOde,Start,End);
    [E,B] = placet_test_no_correction("rtml", "beam3", "None", 1, Start, End);
    reset_grad("rtml",$BOOde,Start,End);
    reset_strength("rtml",$BOOde,Start,End);
    placet_set_beam("beamDFS", B);
}

getResponseMatrix CA rtml Marker-CA-start Marker-VT-end beam4 $CAde
splitBin rtml CAncarray1 Marker-CA-start Marker-VT-end
otoCorrection CA rtml Marker-CA-start Marker-VT-end beam4 $CAbeta0 $CAOTOnloop
dfsCorrection-ca CA rtml Marker-CA-start Marker-VT-end beam4 $CAde $CAbeta1 $CAwgt $CADFSnloop

Octave {
    Start = placet_get_name_number_list("rtml","Marker-CA-start");
    End = placet_get_name_number_list("rtml","Marker-LTL-start");
    [E,B] = placet_test_no_correction("rtml", "beam4", "None", 1, Start, End);
    placet_set_beam("beam6", B);
    Bins = Bins(1);
}

source $scriptdir/save.tcl
