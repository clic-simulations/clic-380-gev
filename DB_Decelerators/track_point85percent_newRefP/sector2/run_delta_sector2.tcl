set c 2.99792458e8
set f1 [expr 24*499.75e6]
set nbins 200
set RoverQ 1147.335
set lm_beta 0.4529
set charge 5.2428677e+10
set t_bunch [expr 1/$f1]


#~ set bx 0.64222
set bx 0.61341
set by 4.0
set ax 0.
set ay 0.
#~ set p0 1.99218842222222
set p0 1.91
set delta 0.85e-2
set refp [expr $p0*(1-1.*$delta)]


set inj_bunch "../deltapoint85percent_1e5_decHalfQuad.dat"

Machine m

m new_injector inj
m new_beamline bl1
#~ Kwatch winj
#~ Drift -node 1 -kick winj
source "../../Lattices_PLACET3/withpickups/decelerator_380GeV_sector2_withpickups.tcl"
Kwatch wdmp
m new_dump dmp -kick wdmp

for {set i 0} {$i <11} {incr i} {
  BunchMulti -source_file $inj_bunch -p0 $p0 -charge $charge -time_before_next $t_bunch \
              -collect element s bx by ax ay enx eny egx egy p0 delta minP maxP sx sy sz cz nparticles
}

#~ for {set i 0} {$i <1} {incr i} {
  #~ BunchMulti -nparticles 100000 -p0 $p0 -charge $charge -time_before_next $t_bunch \
              #~ -bx $bx -by $by -ax $ax -ay $ay -enx 150e-6 -eny 150e-6 \
              #~ -sz 1e-3 -delta $delta \
              #~ -collect element s bx by ax ay enx eny egx egy p0 delta minP maxP sx sy sz cz nparticles
#~ }


m run
m -delete
exit
