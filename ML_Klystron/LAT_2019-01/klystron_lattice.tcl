set e0 9.0
namespace eval Linac {

    variable cav_len 0.46
    variable logit 0
    variable fout

    global script_dir

#    variable dE -1.76992816722e-3
    variable dE -1.79998467688e-3
    variable dE -1.91151725038e-3
    variable dE -1.40791438501e-3

    set quadrupole_list {}

    variable phase_count 0
    variable cavity_count 0

    variable decelerator_count 0
    variable decelerator_end {}

    proc log_start {name} {
	variable fout
	variable logit
	set logit 1
	set fout [open $name w]
        flush stdout
    }

    proc log_stop {} {
	variable fout
	variable logit
	set logit 0
	close $fout
    }

    proc put_cavity {l grad} {
	global e0
	variable dE
	variable phases
	variable cavity_count
	variable phase_count
	if {$cavity_count>=$phases($phase_count.n)} {
	    incr phase_count
	}
	set phase $phases($phase_count.ph)
	Cavity -length $l -gradient $grad -phase $phase -type 0
	set e0 [expr $e0+$l*($grad*cos($phase*acos(-1.0)/180.0)+$dE)]
	incr cavity_count
#	incr cavity_count
    }
    
    proc module0 {grad phase} {
	variable cav_len
	variable fout
	variable logit
	if {$logit} {
	    puts $fout "0 0"
	}
	global e0 cavlength1 cavlength2 dE
	Girder
	Drift -length 0.03
	put_cavity $cav_len $grad
	Drift -length 0.04
	put_cavity $cav_len $grad
	Drift -length 0.04
	put_cavity $cav_len $grad
	Drift -length 0.04
	put_cavity $cav_len $grad
	Drift -length 0.02
    }
    
    proc module1 {grad phase strength} {
	variable cav_len
	variable fout
	variable logit
	if {$logit} {
	    puts $fout "1"
	}
	global e0 cavlength1 cavlength2 dE quadrupole_list
	Girder
	Bpm -store_bunches 2 -length 0.08
	Drift -length 0.04
	Quadrupole -length 0.4 -strength [expr 0.4*$strength*$e0]
	Drift -length 0.08
    }
    
    proc module1a {grad phase strength} {
	variable cav_len
	variable fout
	variable logit
	if {$logit} {
	    puts $fout "1"
	}
	global e0 cavlength1 cavlength2 dE quadrupole_list
	Girder
	Quadrupole -length [expr 0.5*0.4] \
	    -strength [expr 0.5*0.4*$strength*$e0]
	Drift -length 0.08
    }
    
    proc module2 {grad phase strength} {
	variable cav_len
	variable fout
	variable logit
	if {$logit} {
	    puts $fout "2"
	}
	global e0 cavlength1 cavlength2 dE quadrupole_list
	Girder
	Bpm -store_bunches 2 -length 0.08
	Drift -length 0.04
	Quadrupole -length 0.65 -strength [expr 0.65*$strength*$e0]
	Drift -length 0.08
    }
    
    proc module3 {grad phase strength} {
	variable cav_len
	variable fout
	variable logit
	if {$logit} {
	    puts $fout "3n"
	}
	global e0 cavlength1 cavlength2 dE quadrupole_list
	Girder
	Bpm -store_bunches 2 -length 0.08
	Drift -length 0.04
	Quadrupole -length 0.8 -strength [expr 0.8*$strength*$e0]
	Drift -length 0.08
    }
    
    proc module4 {grad phase strength} {
	variable cav_len
	variable fout
	variable logit
	if {$logit} {
	    puts $fout "4"
	}
	global e0 cavlength1 cavlength2 dE quadrupole_list
#	variable e0
	Girder
	Bpm -store_bunches 2 -length 0.08
	Drift -length 0.04
	Quadrupole -length 1.0 -strength [expr 1.0*$strength*$e0]
	Drift -length 0.08
    }

    proc module4a {} {
	variable fout
	variable logit
#	global e0
	Girder
	Bpm -store_bunches 2 -length 0.08
    }
    
    namespace export put_linac

    variable module_count 0
    variable phase_counter 0

#    source $script_dir/lattice.def
    
    variable qlcmd {
	module1a
	module1
	module2
	module3
	module4
	module4a
    }
    
    proc put_quadrupole_obsolete {which nc s} {
	variable module_count
	variable phase_counter
	variable phases
	set ph $phases($phase_counter.ph)
	set grad $phases(gradient)
	$which $grad $ph $s
	incr module_count
	for {set i 0} {$i<$nc} {incr i} {
	    module0 $grad $ph
	    incr module_count
	}
	if {$module_count>$phases($phase_counter.n)} {
	    incr phase_counter
	}
    }
    
    proc put_quadrupole {which nc s} {
	variable module_count
	variable phases
	set grad $phases(gradient)
	$which $grad 0 $s
	incr module_count
	for {set i 0} {$i<$nc} {incr i} {
	    module0 $grad 0
	    incr module_count
	}
    }
    
    variable phases

    set decelerator_end {
	100000000 }



    variable qmatch ""
 
    proc put_sector {which which0 n nc st} {
	variable qmatch
	put_quadrupole $which0 $nc [lindex $st 2]
	lappend qmatch 1
	put_quadrupole $which $nc -[lindex $st 3]
	lappend qmatch 1
	put_quadrupole $which $nc [lindex $st 4]
	lappend qmatch 1
	put_quadrupole $which $nc -[lindex $st 5]
	lappend qmatch 1
	for {set i 4} {$i<$n-5} {incr i 2} {
	    put_quadrupole $which $nc [lindex $st 0]
	    lappend qmatch 0
	    put_quadrupole $which $nc -[lindex $st 1]
	    lappend qmatch 0
	}
	put_quadrupole $which $nc [lindex $st 0]
	lappend qmatch 1
	put_quadrupole $which $nc -[lindex $st 6]
	lappend qmatch 1
	put_quadrupole $which $nc [lindex $st 7]
	lappend qmatch 1
	put_quadrupole $which $nc -[lindex $st 8]
	lappend qmatch 1
    }
    
    proc put_linac {} {
	variable sectors 
	variable module_count
	variable qlcmd
	variable set_match
	variable qmatch
	global match
	set qmatch ""
	foreach {x q0} $sectors {
	    set n [lindex $x 0]
	    set ql [lindex $x 1]
	    set nc [lindex $x 2]
	    if {$module_count==0} {
		put_sector [lindex $qlcmd $ql] [lindex $qlcmd 0] $n $nc $q0
	    } {
		put_sector [lindex $qlcmd $ql] [lindex $qlcmd $ql] $n $nc $q0
	    }
	}
	module4a
	set k1 [lindex [lindex $sectors 1] 0]
	set k2 [lindex [lindex $sectors 1] 1]
#	array set match [MatchFodo -l1 0.38 -l2 0.38 -K1 [expr 0.38*$k1] -K2 [expr -0.38*$k2] -L 2.01]
	array set match $set_match
	set qmatch [concat "0 0 0 0" [lrange $qmatch 4 end-4] "0 0 0 0"]
    }

    proc choose_linac {name ph} {
	variable phase_count
	variable cavity_count
	variable module_count
	variable decelerator_count
	variable decelerator_end
	variable phases
	set phase_count 0
	set cavity_count 0
	set module_count 0
	set decelerator_count 0
	source $name
	if {$ph>=0} {
	    puts "phase $ph"
	    puts "$phase_set($ph)"
	    array set phases $phase_set($ph)
	} {
# Added (Neven, 130618):
	puts "phase not defined"
	exit
# Commented (Neven, 130618):
#	    array set phases {
#		0.n 100000 0.ph 0.0
#		gradient 0.1
#	    }
	}
    }

    proc quadrupole_match_list {} {
	variable qmatch
	return $qmatch
    }
}

proc find_max_energy {beam} {
    EnergySpreadPlot -beam $beam -file espread
    set m 0.0
    set f [open espread r]
    gets $f line
    while {![eof $f]} {
	if {$m<[lindex $line 2]} {set m [lindex $line 2]}
	gets $f line
    }
    close $f
    return $m
}

proc find_final_energy {beam phase n_cav i1 i2} {
    if {$i2>$n_cav} {set i2 $n_cav}
    puts "$i1 $i2 $n_cav"
    set ph {}
    for {set i 0} {$i<$i1} {incr i} {
	lappend ph -30.0
    }
    for {} {$i<$i2} {incr i} {
	lappend ph $phase
    }
    for {} {$i<$n_cav} {incr i} {
	lappend ph 30.0
	}
    CavitySetPhaseList $ph
    EnergySpreadPlot -beam $beam -file espread
    return [lindex [exec tail -n 1 espread] 2]
}

proc optimise_phase_new {beam first phase} {
    set n_cav [llength [CavityGetPhaseList]]
    set fph $phase
    
    set ph {}
    for {set i 0} {$i<$n_cav} {incr i} {
	lappend ph $phase
    }
    CavitySetPhaseList $ph
    EnergySpreadPlot -beam $beam -file espread
    set e_target [lindex [exec tail -n 1 espread] 2]

    while {$e_target>0.01} {
	set ph {}
	set fph [expr $fph+0.1]
	puts "phase step $fph $e_target"
	for {set i 0} {$i<$first} {incr i} {
	    lappend ph $phase
	}
	for {} {$i<$n_cav} {incr i} {
	    lappend ph $fph
	}
	CavitySetPhaseList $ph
	EnergySpreadPlot -beam $beam -file espread
	set e_target [lindex [exec tail -n 1 espread] 2]
    }
    return "$fph"
}

proc optimise_phase_new2 {beam first second phase maxphase} {
    set n_cav [llength [CavityGetPhaseList]]
    set fph1 $phase
    set fph2 $phase
    
    set ph {}
    for {set i 0} {$i<$n_cav} {incr i} {
	lappend ph $phase
    }
    CavitySetPhaseList $ph
    EnergySpreadPlot -beam $beam -file espread
    set e_target [lindex [exec tail -n 1 espread] 2]

    while {($e_target>0.01)&&($fph2<$maxphase)} {
	set ph {}
	set fph2 [expr $fph2+0.1]
	puts "phase step $fph2 $e_target"
	for {set i 0} {$i<$second} {incr i} {
	    lappend ph $phase
	}
	for {} {$i<$n_cav} {incr i} {
	    lappend ph $fph2
	}
	CavitySetPhaseList $ph
	EnergySpreadPlot -beam $beam -file espread
	set e_target [lindex [exec tail -n 1 espread] 2]
    }

    while {($e_target>0.01)&&($fph1<$maxphase)} {
	set ph {}
	set fph1 [expr $fph1+0.1]
	puts "phase step $fph1 $fph2 $e_target"
	for {set i 0} {$i<$first} {incr i} {
	    lappend ph $phase
	}
	for {} {$i<$second} {incr i} {
	    lappend ph $fph1
	}
	for {} {$i<$n_cav} {incr i} {
	    lappend ph $fph2
	}
	CavitySetPhaseList $ph
	EnergySpreadPlot -beam $beam -file espread
	set e_target [lindex [exec tail -n 1 espread] 2]
    }

    return "$fph1 $fph2"
}

proc optimise_phase {beam phase final_phase} {

    set n_cav [llength [CavityGetPhaseList]]

    set i1 $n_cav
    set i2 $n_cav

    set e_target 0.01
    set e_final 0.015

    while {$e_final>$e_target} {
	incr i1 -4
	set ph {}
	for {set i 0} {$i<$i1} {incr i} {
	    lappend ph $phase
	}
	for {} {$i<$i2} {incr i} {
	    lappend ph $final_phase
	}
	CavitySetPhaseList $ph
	EnergySpreadPlot -beam $beam -file espread
	set e_final [lindex [exec tail -n 1 espread] 2]
	puts "lower bound at $phase $i1 $e_final"
    }
    return "$i1 $i2"
}

proc optimise_gradient {beam e_target} {
    set n_cav [llength [CavityGetGradientList]]
    set grad [lindex [CavityGetGradientList] 0]
    EnergySpreadPlot -beam $beam -file espread
    set e_final [lindex [exec tail -n 1 espread] 1]
    puts "$grad $e_final"
    while {$e_final>$e_target} {
	set g {}
	set grad [expr $grad*0.999]
#	set grad [expr $grad*(1.0-0.5*($e_final/$e_target-1.0)]
	for {set i 0} {$i<$n_cav} {incr i} {
	    lappend g $grad
	}
	CavitySetGradientList $g
	EnergySpreadPlot -beam $beam -file espread
	puts "$grad $e_final"
	set e_final [lindex [exec tail -n 1 espread] 1]
    }
    return $grad
}

proc optimise_gradient2 {beam e0 e_target} {
    set n_cav [llength [CavityGetGradientList]]
    set grad [lindex [CavityGetGradientList] 0]
    for {set j 0} {$j<10} {incr j} {
	EnergySpreadPlot -beam $beam -file espread
	set e_final [lindex [exec tail -n 1 espread] 1]
	set g {}
	set grad [expr $grad*($e_target-$e0)/($e_final-$e0)]
	for {set i 0} {$i<$n_cav} {incr i} {
	    lappend g $grad
	}
	puts "step 3 $e_final $grad"
	CavitySetGradientList $g
    }
    return $grad
}
