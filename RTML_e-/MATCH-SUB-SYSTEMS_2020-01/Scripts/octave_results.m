function disp_results(beam)
    emitt = placet_get_emittance(beam)
    twiss = placet_get_twiss_matrix(beam);
    beta_x = twiss(1,1)
    beta_y = twiss(3,3)
    alpha_x = -twiss(1,2)
    alpha_y = -twiss(3,4)
    disp("");
    x = mean(beam(:,2))
    y = mean(beam(:,3))
    xp = mean(beam(:,5))
    yp = mean(beam(:,6))
    disp("");
    z = mean(beam(:,4))
    sz = std(beam(:,4))
    disp("");
    E = mean(beam(:,1))
    dE = std(beam(:,1))
    disp("");
endfunction
