set tcl_precision 15
ParallelThreads -num 4

proc FF1 {} {}
proc FF2 {} {}

set latticedir ./Lattices_e-
set scriptdir ./Scripts
set paramsdir ./Parameters

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/plainTracking.tcl

if {$beamparams(usewakefields)==1} {
   source $paramsdir/rf_parameters_ww.tcl
}

create_particles_file particles.in beamparams

set wakelongrangedata(nmodes) 0                                                          
set wakelongrangedata(modes) {}                                                          
initialize_cavities rfparamsbc1 wakelong1 wakelongrangedata 

BeamlineNew
source $latticedir/0000_e-_rtml.tcl
BeamlineSet -name rtml

if {$beamparams(usewakefields)==1} {                                                     
    SplineCreate "rtml_Wt_bc1" -file "Wakefields/Wt_bc1.dat"                             
    SplineCreate "rtml_Wl_bc1" -file "Wakefields/Wl_bc1.dat"                             
    ShortRangeWake "rtml_SR_bc1" -type 2 \
        -wx "rtml_Wt_bc1" -wy "rtml_Wt_bc1" -wz "rtml_Wl_bc1"                            
    Octave {                                                                             
        CAV = placet_get_name_number_list("rtml", "060C");                               
        placet_element_set_attribute("rtml", CAV, "short_range_wake", "rtml_SR_bc1");    
        placet_element_set_attribute("rtml", CAV, "six_dim", true);                      
    }                                                                                    
    SplineCreate "rtml_Wt_booster" -file "Wakefields/Wt_booster.dat"                     
    SplineCreate "rtml_Wl_booster" -file "Wakefields/Wl_booster.dat"                     
    ShortRangeWake "rtml_SR_booster" -type 2 \
        -wx "rtml_Wt_booster" -wy "rtml_Wt_booster"  -wz "rtml_Wl_booster"               
    Octave {                                                                             
        CAV = placet_get_name_number_list("rtml", "120C");                               
        placet_element_set_attribute("rtml", CAV, "short_range_wake","rtml_SR_booster"); 
        placet_element_set_attribute("rtml", CAV, "six_dim", true);                      
    }                                                                                    
    SplineCreate "rtml_Wt_bc2" -file "Wakefields/Wt_bc2.dat"                             
    SplineCreate "rtml_Wl_bc2" -file "Wakefields/Wl_bc2.dat"                             
    ShortRangeWake "rtml_SR_bc2" -type 2 \
         -wx "rtml_Wt_bc2" -wy "rtml_Wt_bc2" -wz "rtml_Wl_bc2"                           
    Octave {                                                                             
        CAV = placet_get_name_number_list("rtml", "210C");                               
        placet_element_set_attribute("rtml", CAV, "short_range_wake", "rtml_SR_bc2");    
        placet_element_set_attribute("rtml", CAV, "six_dim", true);                      
    }                                                                                    
}

Octave {
    load mag-$machine.dat;
    magnets = placet_get_number_list("rtml", "quadrupole");
    magnets = [ magnets placet_get_number_list("rtml", "sbend") ];
    magnets = [ magnets placet_get_number_list("rtml", "multipole") ];
    placet_element_set_attribute("rtml", magnets, "x", mag.x);
    placet_element_set_attribute("rtml", magnets, "y", mag.y);
    placet_element_set_attribute("rtml", magnets, "xp", mag.xp);
    placet_element_set_attribute("rtml", magnets, "yp", mag.yp);
    placet_element_set_attribute("rtml", magnets, "roll", mag.roll);

    magnets = placet_get_number_list("rtml", "quadrupole");
    magnets = [ magnets placet_get_number_list("rtml", "multipole") ];
    placet_element_set_attribute("rtml", magnets, "strength", mag.strength);

    magnets = placet_get_number_list("rtml", "sbend");
    placet_element_set_attribute("rtml", magnets, "e0", mag.e0);

    BI = placet_get_number_list("rtml","bpm");
    placet_element_set_attribute("rtml", BI, "x", Bpm.x);
    placet_element_set_attribute("rtml", BI, "y", Bpm.y);
    placet_element_set_attribute("rtml", BI, "xp", Bpm.xp);
    placet_element_set_attribute("rtml", BI, "yp", Bpm.yp);
    placet_element_set_attribute("rtml", BI, "roll", Bpm.roll);
    placet_element_set_attribute("rtml",BI, "resolution",Bpm.res);
}

Octave {
load dipole-$machine.dat
Corrector_all = placet_get_number_list("rtml","dipole");
placet_element_set_attribute("rtml", Corrector_all, 'strength_x',dipole_x);
placet_element_set_attribute("rtml", Corrector_all, 'strength_y',dipole_y);
}

Octave {
    load("X_ca.dat");
    offset_X = round(10*X(1:5));
    offset_Y = round(10*X(6:10));
    Multipole_all = placet_get_name_number_list("rtml","Multi-CA*");
    N_Multipole = length(X)/2;
    Multipoles = Multipole_all(1:N_Multipole);
    placet_element_vary_attribute("rtml",Multipoles,"x", offset_X);
    placet_element_vary_attribute("rtml",Multipoles,"y", offset_Y);

    load("X_tal.dat");
    offset_X = round(10*X(1:5));
    offset_Y = round(10*X(6:10));
    Multipole_all = placet_get_name_number_list("rtml","Multipole-left*");
    N_Multipole = length(X)/2;
    Multipoles = Multipole_all(1:N_Multipole);
    placet_element_vary_attribute("rtml",Multipoles,"x", offset_X);
    placet_element_vary_attribute("rtml",Multipoles,"y", offset_Y);
}
                                                                                         
BeamlineList -file lattice-$machine.tcl
