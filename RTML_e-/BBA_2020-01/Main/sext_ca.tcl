set tcl_precision 15
ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set beamdir    ./Beams
set wakedir    ./Wakefields

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/plainTracking.tcl

create_particles_file $beamdir/particles.in beamparams

source $scriptdir/beamline_and_wake.tcl

make_particle_beam beam4 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

source $scriptdir/load_mag.tcl

Octave {
    X = load("X_ca-$machine.dat").X;
    offset_X = round(10*X(1:5));                     
    offset_Y = round(10*X(6:10));
    Multipole_all = placet_get_name_number_list("rtml","Multi-CA*");
    N_Multipole = length(X)/2;
    Multipoles = Multipole_all(1:N_Multipole);
    placet_element_vary_attribute("rtml", Multipoles, "x", offset_X); 
    placet_element_vary_attribute("rtml", Multipoles, "y", offset_Y); 
}

Octave {
    load "$beamdir/beam-$machine-4.dat";
    placet_set_beam("beam4", B);
}

plainTracking CA rtml Marker-CA-start Marker-LTL-start beam4
