#
# Dump and matching of booster to central arc
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_booster_to_ca {bparray} {
    upvar $bparray beamparams

    set usesixdim 1
    set numthinlenses 10
    set quad_synrad 0

    set refenergy $beamparams(meanenergy)

    SetReferenceEnergy $refenergy
    
    # From match_0130.tcl
    set mk1 -0.179341173032838
    set mk2 -0.689373215486956
    set mk3 1.583484303732447
    set mk4 -1.084702084957000
    set ml1 3.9850195514336519
    set ml2 0.2716507910438755
    set ml3 0.0299723029640980

	Quadrupole -name "matching-quad" -synrad $quad_synrad -length 0.35 -strength [expr $mk1*$refenergy] -e0 $refenergy
	Drift -name "matching-drift" -length $ml1
	Quadrupole -name "matching-quad" -synrad $quad_synrad -length 0.35 -strength [expr $mk2*$refenergy] -e0 $refenergy
	Drift -name "matching-drift" -length $ml2
	Quadrupole -name "matching-quad" -synrad $quad_synrad -length 0.35 -strength [expr $mk3*$refenergy] -e0 $refenergy
	Drift -name "matching-drift" -length $ml3
	Quadrupole -name "matching-quad" -synrad $quad_synrad -length 0.35 -strength [expr $mk4*$refenergy] -e0 $refenergy

}
