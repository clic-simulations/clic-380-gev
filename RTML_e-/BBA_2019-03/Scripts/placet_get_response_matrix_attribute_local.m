## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_response_matrix_attribute_local (@var{beamline}, @var{beam}, @var{bpms}, @var{direction}, @var{correctors}, @var{attribute}, @var{survey},@var{Start},@var{End})
## Return the response matrix for 'beamline' and 'beam0', using selected 'bpms' and 'correctors'.
## @end deftypefn

function [R,b0]=placet_get_response_matrix_attribute_local(beamline, beam, B, direction, C, attribute, survey,Start,End)
  if nargin==9 && ischar(beamline) && ischar(beam) && isvector(B) && ischar(direction) && isvector(C) && ischar(attribute) && ischar(survey)
    Res=placet_element_get_attribute(beamline, B, "resolution");
    placet_element_set_attribute(beamline, B, "resolution", 0.0);
    placet_test_no_correction(beamline, beam, survey,1,Start,End);
    b0=placet_get_bpm_readings(beamline, B);
    R=zeros(rows(b0), columns(C));
    for i=1:columns(C)
      placet_element_vary_attribute(beamline, C(i), attribute, 1);
      placet_test_no_correction(beamline, beam, "None",1,Start,End);
      placet_element_vary_attribute(beamline, C(i), attribute, -1);
      b=placet_get_bpm_readings(beamline, B) - b0;
      b(B < C(i),:) = 0.0;
      if direction=="x"
        R(:,i)=b(:,1);
      else
        R(:,i)=b(:,2);
      endif
    endfor
    if direction=="x"
      b0=b0(:,1);
    else
      b0=b0(:,2);
    endif
    placet_element_set_attribute(beamline, B, "resolution", Res);
  else  
    help placet_get_response_matrix_attribute_local
  endif
endfunction
