# Mar 31, 2020
# xingguang.liu@cern.ch

set tcl_precision 15
# ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set beamdir    ./Beams
set wakedir    ./Wakefields

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/plainTracking.tcl

create_particles_file $beamdir/particles.in beamparams

source $scriptdir/beamline_and_wake.tcl

make_particle_beam beam1 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam4 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

# load present settings
source $scriptdir/load_mag.tcl

Octave {
    source(["$scriptdir/","sextupole_tuning_functions.m"]);  

    # define a merit function
    function merit = test_fun(X)
        global x_select
        global y_select
        num=length(X);
        x_offset =  X(1:num/2);
        y_offset =  X(num/2+1:num);
        marker_start = "Marker-TAL2-start";
        marker_end = "Marker-BC2-start";

        [ex, ey]=test_sextupole_tuning(x_select,x_offset, y_select,y_offset,marker_start, marker_end, "rtml", "beam4");
        # reset the offset for new test
        add_offset(x_select,-x_offset, y_select,-y_offset,"rtml");

        merit=ex/7.0 + ey/0.05;
    endfunction

    # select setupoles for tuning
    global x_select
    global y_select 
    select_num = 5;
    select_name = "Multipole-right*";
    printf("select number of sextuple for tuning: %u \n", select_num);
    [x_select, y_select]=select_simple(select_num,select_name,"rtml",1)


    # choosing key points
    marker_beam_line_start= "Marker-SR-start";
    marker_start = "Marker-TAL2-start";
    marker_end = "Marker-BC2-start";

    # track to marker_tuning_start
    Start = placet_get_name_number_list("rtml",marker_beam_line_start);
    End = placet_get_name_number_list("rtml",marker_start);
    [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1, Start, End);
    printf("\nEmittance entering TAL2\n");
    printf("Emittance: %10.2f %10.2f nm\n", E(end,2)*100,E(end,6)*100);
    # assign to beam4
    placet_set_beam("beam4", B);
    
    # do the sextuple tuning
    printf("tuning...\n");
    # fminsearch option and starting points
    options = optimset("TolX", 0.1, "MaxIter", 100);
    # first half for x, second harlf for y
    X=zeros(1,select_num*2);
    [X_, merit] = fminsearch("test_fun", X, options);
    # test the result
    printf("After tuning:\n");
    x_offset =  X_(1:select_num);
    y_offset =  X_(select_num+1:select_num*2);
    [ex, ey]=test_sextupole_tuning(x_select,x_offset, y_select,y_offset,marker_start, marker_end, "rtml", "beam4",0.0);
    printf("Emittance: %10.2f %10.2f nm\n",ex*100,ey*100);
}

# save the new settings
source $scriptdir/save.tcl
