#
# CLIC 380 GeV BDS
#
ParallelThreads -num 6

# Directories containing files needed by this similation
set script_dir  ./scripts
set lattice_dir ./lattices

set scale 1.0
source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl

#
# Parameters
#
set e_initial 190.45
set e0 $e_initial
set match(emitt_x) 9.5
set match(emitt_y) 0.3
set match(charge) 5.2e9
set match(sigma_z) 70.0
set match(phase) 0.0
set match(e_spread) 0.36
set match(alpha_x) -2.87689298392958
set match(alpha_y) 0.4832163278647598
set match(beta_x) 41.05101124309615
set match(beta_y) 5.564870435510085
set charge $match(charge)

#
# Lattice
#
set synrad 1
set quad_synrad 1
set mult_synrad 1
set sbend_synrad 1
set sbend_sixdim 1

# Parameters to optimise in tracking
set kFQF 0.0937157543664202
set kFQD -0.0916145482938311
set kFQF2 -0.0426591601099834
set kFQD2 0.0624933697356673
set kTQF 0.1585467339302492
set kTQD --0.0391770021025199

# Set up the BDS lattice with L*=4.3 m
BeamlineNew
source $lattice_dir/bds43_no_ffs.tcl
BeamlineSet -name bds43

# Set up the BDS lattice with L*=6 m
BeamlineNew
source $lattice_dir/bds6_no_ffs.tcl
BeamlineSet -name bds6

#
# Beam
#
set n_slice 50
set n 2000
set n_total [expr $n_slice*$n]
make_beam_many beam1 $n_slice $n
#BeamRead -file beam_ml.dat -beam beam1

# Twiss parameters at the start of the final focus to match to
array set twiss1 {
  beta_x 31.636
  beta_y 8.7385
  alpha_x 0.015548
  alpha_y 0.002369
}

#
# Matching
#
FirstOrder 1
Octave {	
    pkg load optim;
    format long;

    # Elements to use to match
    global Correctors = placet_get_name_number_list("bds6", "matching-quad");

    global twiss1 = zeros(4,4);
    twiss1(1:2,1:2) = [ $twiss1(beta_x), -($twiss1(alpha_x)); -($twiss1(alpha_x)), (1+($twiss1(alpha_x))*($twiss1(alpha_x)))/($twiss1(beta_x)) ];
    twiss1(3:4,3:4) = [ $twiss1(beta_y), -($twiss1(alpha_y)); -($twiss1(alpha_y)), (1+($twiss1(alpha_y))*($twiss1(alpha_y)))/($twiss1(beta_y)) ];

    # Sum of squares function to minimise
    function merit = Matching(beamline)
        global Correctors;
        global twiss1;

        target = [9.5 0.3];
        beam_target = [0.1 0.002];

        # Display the current parameters
        Quads = placet_element_get_attribute("bds6", Correctors, "strength") ./ $e0

        # Calculate the Twiss parameters at the end of the matching section
        [emitt, beam] = placet_test_no_correction("bds6", "beam1", "None");
        twiss = placet_get_twiss_matrix(beam);
        beta_x = twiss(1,1)/twiss1(1,1)
        beta_y = twiss(3,3)/twiss1(3,3)
        alpha_x = -twiss(1,2)/twiss1(1,2)
        alpha_y = -twiss(3,4)/twiss1(3,4)

        #sigma = placet_get_sigma_matrix(beam);
        #sx = sqrt(sigma(1,1))
        #sy = sqrt(sigma(3,3))

        #emitt = placet_get_emittance(beam)

        x = mean(beam(:,2))
        y = mean(beam(:,3))
        #xp = mean(beam(:,5))
        #yp = mean(beam(:,6))

        # Calculate the sum of squares
        #merit = sqrt(x**2) + sqrt(y**2);
        merit = sum((1.0 - [beta_x, beta_y, alpha_x, alpha_y]).**2) + sqrt(x**2) + sqrt(y**2);
        #merit = sum((1.0 - [beta_x, beta_y]).**2);
        #merit = sum(((emitt-target) ./ target) .** 2);
        #merit = ((emitt(1,2) - target(1,2))/target(1,2))**2;
        #merit = sum((1.0 - [beta_x, beta_y, alpha_x, alpha_y]).**2) + ((emitt(1,2) - target(1,2))/target(1,2))**2;
        #merit = sum((1.0 - [beta_x, beta_y]).**2) + ((emitt(1,2) - target(1,2))/target(1,2))**2 + xp**2 + yp**2;
        #merit = sum((1.0 - [beta_x, beta_y]).**2) + sum(((emitt-target) ./ target) .** 2);
        #merit = sum((1.0 - [beta_x, beta_y]).**2) + sum((([sx sy]-beam_target) ./ beam_target) .** 2);
        #merit = sum((([sx sy]-beam_target) ./ beam_target) .** 2) + sqrt(xp**2 + yp**2);
        #merit = sqrt(xp**2) + sqrt(yp**2);
        #merit = sum((([sx sy]-beam_target) ./ beam_target) .** 2) + sum(((emitt-target) ./ target) .** 2);

    endfunction

    # Parameters to vary to find optimum
    Leverages = ["strength"; "strength"; "strength"; "strength"; "strength"; "strength";];
    Constraints = [0  0; 0  0; 0  0; 0  0; 0  0; 0  0];

    # Perform the optimisation
    [optimum,merit] = placet_optimize_constraint("bds6", "Matching", Correctors, Leverages, Constraints);

    # Display the optimum
    Quads = placet_element_get_attribute("bds6", Correctors, "strength") ./ $e0

    beam = placet_get_beam("beam1");
    disp("before bds");
    x = mean(beam(:,2))
    y = mean(beam(:,3))
    xp = mean(beam(:,5))
    yp = mean(beam(:,6))

    # Track the beam through the BDS
    [emitt,beam]=placet_test_no_correction("bds6", "beam1", "None", "%ex %ey %sex %sey %x %y %xp %yp %Env %sx %sy %sxp %syp %E %dE %s %n %name");
    save -text emitt_bds.dat emitt;

    # Display the emittance at the IP
    bds = placet_get_emittance(beam)

    twiss = placet_get_twiss_matrix(beam);
    beta_x = twiss(1,1)/twiss1(1,1)
    beta_y = twiss(3,3)/twiss1(3,3)
    alpha_x = -twiss(1,2)/twiss1(1,2)
    alpha_y = -twiss(3,4)/twiss1(3,4)
    disp("");

    x = mean(beam(:,2))
    y = mean(beam(:,3))
    xp = mean(beam(:,5))
    yp = mean(beam(:,6))
    
}

exit
