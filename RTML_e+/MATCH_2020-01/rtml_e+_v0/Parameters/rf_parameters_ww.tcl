set rfparamsbooster(gradientww)   1.489875372601e+07
set rfparamsbc1(gradientww)   1.327251035645e+07
set rfparamsbc2(gradientww)   7.182976244093e+07
set energy_wake_bc1  -3.530533823800e-05
set energy_wake_booster  -5.772097215936e-05
set energy_wake_bc2  -7.428893568219e-05
