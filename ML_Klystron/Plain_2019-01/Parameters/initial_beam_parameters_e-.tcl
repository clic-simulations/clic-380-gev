set e_initial 9.0
set e0 $e_initial

set match(emitt_x)  9.2
set match(emitt_y)  0.1
set match(e_spread) 1
set match(sigma_z)  70
set match(phase)    0.0
set match(charge)   5.2e9
set match(beta_x)   8.054208256047598
set match(beta_y)   1.201443036029169
set match(alpha_x)  2.455451375064132e-02
set match(alpha_y)  6.250882009649877e-03
set charge          $match(charge)
