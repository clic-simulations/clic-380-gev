# To run: placet gen_lattice.tcl <phase-id>

set scriptdir  ./Scripts
set paramsdir  ./Parameters

source klystron_lattice.tcl
Linac::choose_linac phase.def [lindex $argv 0]
Linac::put_linac
BeamlineSet -name ml

BeamlineList -file 1001_kly_ml.tcl

exit
