#
# This code was developed by C. Gohil
#
# Creates a lattice file for the ML.
# [lindex $argv 0] is the phase ID in phase.def
#
# The matching of the ML is done by generating the file phases.def. To generate
# phases.def, first generate a file called lattice.def by using the script run_scan.tcl.
# The file lattice.def may need manual editing to get the desired number of 
# sector/quadrupoles/cavities. Then run auto_rebase.tcl which will use lattice.def to
# generate phase.def. auto_rebase.tcl find the phase combinations of the cavities which
# minimises the energy spread.
#
# phase.def is used to generate a lattice with lattice4a.tcl.
#
if {$argc != 1} {
    puts "Please specify a phase ID."
    exit
}


set script_dir "."

set e_initial 9.0
set e0 $e_initial

set scale 1.0
source clic_basic_single.tcl
source lattice4a.tcl
Linac::choose_linac phase.def [lindex $argv 0]
Linac::put_linac

BeamlineList -file 1000_db_ml.tcl

#puts "******************************************************************"
#puts "* Remember to fix the first half quadrupole in the lattice file! *"
#puts "******************************************************************"

exit
