#
# matching of BC2 RF to BC2 chicane 1
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_bc2_rf_to_chicane_1 {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 100
    set quad_synrad   0

    set lquadm 0.3

    set kqm1  0.6260896377748464
    set kqm2 -1.1455472889573088
    set kqm3  0.4128132094569047
    set kqm4 -0.2872950092257217
    set kqm5  1.9483056072681648
    set kqm6 -2.02952949380762

    set ldm1 3.85344476669227
    set ldm2 0.957864209663127
    set ldm3 9.35911621813729
    set ldm4 20.4633501637627
    set ldm5 30.9193396703442
    set ldm6 0.00249383490181243
    set ldm7 2.14223407574243

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
    Drift -length $ldm1
    Bpm
    Quadrupole -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy]
    Dipole -name "D220-1"
    Drift -length $ldm2
    Bpm
    Quadrupole -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy]
    Dipole -name "D220-2"
    Drift -length $ldm3
    Bpm
    Quadrupole -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy]
    Dipole -name "D220-3"
    Drift -length $ldm4
    Bpm
    Quadrupole -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy]
    Dipole -name "D220-4"
    Drift -length $ldm5
    Bpm
    Quadrupole -length $lquadm -strength [expr $kqm5*$lquadm*$refenergy]
    Dipole -name "D220-5"
    Drift -length $ldm6
    Bpm
    Quadrupole -length $lquadm -strength [expr $kqm6*$lquadm*$refenergy]
    Dipole -name "D220-6"
    Drift -length $ldm7
}
