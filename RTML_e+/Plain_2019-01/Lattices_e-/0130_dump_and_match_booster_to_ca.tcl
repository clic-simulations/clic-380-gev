#
# Dump and matching of booster to central arc
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_booster_to_ca {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 10
    set quad_synrad   0

    set lquadm 0.35
    
    set kqm1 -0.5124033515223944
    set kqm2 -1.96963775853416
    set kqm3  4.524240867806992
    set kqm4 -3.099148814162857

    set ldm1 3.9850195514336519
    set ldm2 0.2716507910438755
    set ldm3 0.0299723029640980

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
    Bpm
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D130-1"
	Drift -length $ldm1
    Bpm
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D130-2"
	Drift -length $ldm2
    Bpm
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D130-3"
	Drift -length $ldm3
    Bpm
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D130-4"
}
