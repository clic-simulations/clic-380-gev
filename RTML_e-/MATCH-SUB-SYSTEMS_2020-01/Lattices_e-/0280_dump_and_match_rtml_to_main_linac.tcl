#
# Dump and matching of RTML to Main Linac
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_rtml_to_main_linac {bparray} {
    upvar $bparray beamparams

    set usesixdim 1
    set numthinlenses 10
    set quad_synrad 0

    set refenergy $beamparams(meanenergy)

    set lquadm 0.3

    set kqm1 1
    set kqm2 -1

    set ldm1 1
    set ldm2 1
    set ldm3 1

    SetReferenceEnergy $refenergy

    Girder
    Drift -name "matching-drift" -length $ldm1
    Quadrupole -name "matching-quad" -length $lquadm -strength [expr $kqm1*$refenergy]
    Drift -name "matching-drift" -length $ldm2
    Quadrupole -name "matching-quad" -length $lquadm -strength [expr $kqm2*$refenergy]
    Drift -name "matching-drift" -length $ldm3

Quadrupole -name "" -s 0 -x 0 -y 0 -xp 0 -yp 0 -roll 0 -tilt 0 -tilt_deg 0 -length 0.215 -synrad 0 -six_dim 0 -thin_lens 0 -e0 -1 -aperture_x 1 -aperture_y 1 -aperture_losses 0 -aperture_shape "none" -tclcall_entrance "" -tclcall_exit "" -short_range_wake "" -strength 3.0913947 -Kn 0 -type 0 -hcorrector "x" -hcorrector_step_size 0 -vcorrector "y" -vcorrector_step_size 0
}
