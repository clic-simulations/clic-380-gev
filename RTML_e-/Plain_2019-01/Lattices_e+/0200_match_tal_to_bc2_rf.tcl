#
# matching of Turn Around Loop to BC2 RF
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_tal_to_bc2_rf {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 10
    set quad_synrad   0

    set lquadm 0.3
    
    set kqm1  3.99953460081851
    set kqm2 -6.17900297004170
    set kqm3  3.49372565725678
    set kqm4 -5.25939276084426

    set ldm1 0.0338580162633493
    set ldm2 0.5691300110354337
    set ldm3 0.7613014822074761
    set ldm4 0.0437335150999885

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D200-1"
    Drift -length $ldm1
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D200-2"
    Drift -length $ldm2
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D200-3"
    Drift -length $ldm3
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -e0 $refenergy
    Dipole -name "D200-4"
    Drift -length $ldm4
}
