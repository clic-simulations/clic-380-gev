Octave {
    format long;
    % read in some useful functions
    scriptdir="$scriptdir/";
    source([scriptdir,"octave_beam_creation.m"]);
    source([scriptdir,"octave_beam_statistics.m"]);
    source([scriptdir,"octave_beam_statistics_output.m"]);
    source([scriptdir,"octave_beamline_errors.m"]);
    source([scriptdir,"octave_cavitywakesetup.m"]);
    source([scriptdir,"octave_correction_algorithms.m"]);
}

Octave {
    function disp_results(B)
        emitt   =  placet_get_emittance(B)
        twiss   =  placet_get_twiss_matrix(B);
        E       =  mean(B(:,1))
        dE      =  std(B(:,1))
        beta_x  =  twiss(1,1)
        beta_y  =  twiss(3,3)
        alpha_x = -twiss(1,2)
        alpha_y = -twiss(3,4)
        x       =  mean(B(:,2))
        y       =  mean(B(:,3))
        xp      =  mean(B(:,5))
        yp      =  mean(B(:,6))
        z       =  mean(B(:,4))
        sz      =  std(B(:,4))
        disp("");
    endfunction
}
