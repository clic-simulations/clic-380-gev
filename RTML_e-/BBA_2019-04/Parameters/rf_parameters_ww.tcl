set rfparamsbooster(gradientww)   1.834482721335e+07
set rfparamsbc1(gradientww)   2.230187588859e+07
set rfparamsbc2(gradientww)   7.455177496691e+07
set energy_wake_bc1  -3.529494125783e-05
set energy_wake_booster  -5.772324800032e-05
set energy_wake_bc2  -7.434911495855e-05
