#
# define beam parameters at entrances of all sections
# if tracking starts in front of a section, the corresponding
# beam parameters as defined here should be used
# these values are close to what you would get at a certain position by tracking from the start
# but the values are not corrected for wake field effects etc.
# i.e. they are not necessarily what a real simulation will give
#
# beamXXX=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#          uncespr,echirp,energy]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV]
#
# Attention: before usage the parameters must be converted to Placet units
#

# at entrance of section 0010 (Matching DR to RTML)
set beam0010(betax)    20.0
set beam0010(alphax)    0.0
set beam0010(emitnx)  700e-9
set beam0010(betay)     5.0
set beam0010(alphay)    0.0
set beam0010(emitny)    5.0e-9
set beam0010(sigmaz) 1800.0e-6
set beam0010(meanz)     0.0e-6
set beam0010(charge)    0.85e-9
set beam0010(uncespr)   1.1655e-3
set beam0010(echirp)    0.0
set beam0010(energy)    2.86e9

# at the entrance of section 0280 as simulated by dump_before_matching.tcl
set beam0280(betax)     37.9881075074213
set beam0280(alphax)    1.35994605153317
set beam0280(emitnx)    780.23941901692719e-9
set beam0280(betay)     21.4497032490020
set beam0280(alphay)   -0.733276682664595
set beam0280(emitny)    5.64440125058216e-9
set beam0280(sigmaz)    70.0e-6
set beam0280(meanz)     0.0e-6
set beam0280(charge)    5.2e-9
set beam0280(uncespr)   1.52e-2
set beam0280(echirp)    0.0
set beam0280(energy)    9.0e9

# at end of section 0280 (Dump and match RTML to Main Linac)
set beam0290(betax)     3.969038431111105e+01
set beam0290(alphax)   -1.498310794232508e+00
set beam0290(emitnx)  850.0e-9
set beam0290(betay)     1.789978887234355e+01
set beam0290(alphay)    6.807489722771091e-01
set beam0290(emitny)   10.0e-9
set beam0290(sigmaz)   70.0e-6
set beam0290(meanz)     0.0e-6
set beam0290(charge)    0.65e-9
set beam0290(uncespr)   1.52e-2
set beam0290(echirp)    0.0
set beam0290(energy)    9.0e9


