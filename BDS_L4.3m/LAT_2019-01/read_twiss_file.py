#!/usr/bin/python
from madx import read_twiss_file

data = read_twiss_file('twiss.bds')

print data['S'][-1], data['BETX'][-1], data['BETY'][-1], data['ALFX'][-1], data['ALFY'][-1], data['DX'][-1]
