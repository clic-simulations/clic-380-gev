#
# This script sets up all simulation parameters.
#

# Number of macro-particles to simulate
set n_slice 50
set n       2000
set n_total [expr $n_slice*$n]

# Beam parameters
source $paramsdir/initial_beam_parameters_e-.tcl

# Flags for synchrotron radiation
set synrad       1
set quad_synrad  1
set mult_synrad  1
set sbend_synrad 1
set sbend_sixdim 1

# This setting is needed when tracking a particle (opposed to a sliced) beam 
FirstOrder 1 
