#
# This script was developed by C. Gohil from files developed by Y. Han.
#
# This is the main script which performs Beam-Based Alignment on the CLIC 380 GeV RTML.
#
# Can be executed with: placet run.tcl <machine>, where <machine> is the id number for
# a machine.
#

# Get the machine number from command line arguments
set machine [lindex $argv 0]

Octave {
    global machine = $machine;

    source Scripts/sextupole_tuning_functions.m
    options = optimset("TolX", 1e-1, "MaxIter", 100);
    X = [0; 0; 0; 0; 0; 0; 0; 0; 0; 0];

    disp("Performing BBA on machine $machine\n");

    disp("Setting imperfections");
    disp("*****************************************");
    system("placet -s Main/set_imperfections.tcl machine $machine");

    disp("121 and DFS: SR to CA");
    disp("*****************************************");
    system("placet -s Main/bba_sr_ca.tcl machine $machine");

    disp("Sextupole tuning: CA");
    disp("*****************************************");
    system("placet -s Main/plain_sr_ca.tcl machine $machine");
    [cor, cor_min] = fminsearch("correction1", X, options);
    system("placet -s Main/save_ca_sextupole_offset.tcl machine $machine");

    disp("121 and DFS: CA to TAL1");
    disp("*****************************************");
    system("placet -s Main/bba_ca_tal1.tcl machine $machine");

    disp("Sextupole tuning: TAL1");
    disp("*****************************************");
    system("placet -s Main/plain_sr_tal1.tcl machine $machine");
    [cor, cor_min] = fminsearch("correction2", X, options);
    system("placet -s Main/save_tal1_sextupole_offset.tcl machine $machine");

    disp("121 and DFS: TAL2");
    disp("*****************************************");
    system("placet -s Main/bba_tal2.tcl machine $machine");

    disp("Sextupole tuning: TAL2");
    disp("*****************************************");
    system("placet -s Main/plain_sr_tal2.tcl machine $machine");
    [cor, cor_min] = fminsearch("correction3", X, options);
    system("placet -s Main/save_tal2_sextupole_offset.tcl machine $machine");

    disp("121 and DFS: BC2");
    disp("*****************************************");
    system("placet -s Main/bba_bc2.tcl machine $machine");

    disp("Setupole retuning: TAL1");
    disp("*****************************************");
    X = load("X_tal1-$machine.dat").X;
    system("placet -s Main/plain_sr_tal1.tcl machine $machine");
    [cor, cor_min] = fminsearch("correction4", X(1:5),  options);
    [cor, cor_min] = fminsearch("correction5", X(6:10), options);
    system("placet -s Main/save_tal1_sextupole_offset.tcl machine $machine");

    disp("Setupole retuning: TAL2");
    disp("*****************************************");
    X = load("X_tal2-$machine.dat").X;
    system("placet -s Main/plain_sr_tal2.tcl machine $machine");
    [cor, cor_min] = fminsearch("correction6", X(1:5),  options);
    [cor, cor_min] = fminsearch("correction7", X(6:10), options);
    system("placet -s Main/save_tal2_sextupole_offset.tcl machine $machine");

    system("placet -s Main/save_lattice.tcl machine $machine");
}
