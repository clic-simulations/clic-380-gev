#
# These files were developed by C. Gohil from files inherited from Y. Han.
#
# Static errors are applied to the RTML and Beam-Based Alignment (BBA) techniques
# are applied.
#

#-----------------------------------#
# STEP 1 : generate wakefield files #
#-----------------------------------#
These are the files Wt_*.dat and Wl_*.dat.
They are already present in ./Wakefields, so this step can be skipped.
However, this would need to be re-run if RF parameters change.

Execute: placet Wakefields/create_wake.tcl

#-------------------------------------#
# STEP 2 : generate response matrices #
#-------------------------------------#
These are the files R_parm_*.dat.
These are already present in ./ResponseMatrix, so this step can be skipped.
This would need to be re-run if lattices change.
Generating the response matrices takes a few hours.

Execute: placet Main/response_matrix.tcl

#-----------------------------------------------------------#
# STEP 3: Apply the BBA for different random seed (machine) #
#-----------------------------------------------------------#
Execute: placet run.tcl <machine>
<machine> is an integer passed as a command line argument to run.tcl

E.g. "placet run.tcl 22" would perform BBA on machine number 22.
