##------> Author: Raul Costa [raul.costa@cern.ch] <------------------##
## Placet2 lattice generated from Eduardo Marin's design for Placet1
## Later adapted to be defined by K1L/K2L instead of S1/S2
## TL3 optimised to match CR2 to TTA
## Sextupole strength optimised for low emittance
## Low energy version for 380GeV stage
if {$Dipole_synrad} { 
  set refen 1.904809675 
} else { 
  set refen 1.906 
} 
####START#### 
Marker "DBRC_END.INJECTION" 
## The beam exits CR2 with an offset in the combiner ring's referencial 
## Kalign sets the referencial so that the beam is centered
## Fixed transformation used for multibunch studies 
#~ Kalign TL3offset -cx 0 -cxp 0 -cz 0 
Kalign TL3offset -fixed 1 -cx 0.02726400778367424 -cxp 0.001541740824946881 
Marker "TL3_offset" -nodes 1 -kick TL3offset 
Sbend "BEND.TL3.BEND10" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL3.D122" -len 1.9174 
Sextupole "SEXT.TL3.SX125" -len 0.3 -K2L -6.024609525630969 -refen $refen 
Drift "TL3.D128" -len 0.8837 
Quadrupole "QUAD.TL3.Q13" -len 0.75 -K1L 0.496438933008182 -refen $refen 
Drift "TL3.D142" -len 0.042885 
Sextupole "SEXT.TL3.SX145" -len 0.3 -K2L 5.075349497988554 -refen $refen 
Drift "TL3.D148" -len 0.042885 
Quadrupole "QUAD.TL3.Q15" -len 0.75 -K1L -0.3129080437523358 -refen $refen 
Drift "TL3.D168" -len 0.195355 
Sbend "BEND.TL3.BEND17" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL3.D182" -len 0.0001175 
Sextupole "SEXT.TL3.SX185" -len 0.3 -K2L -1.53078078311799 -refen $refen 
Drift "TL3.D188" -len 0.0001175 
Quadrupole "QUAD.TL3.Q20" -len 0.75 -K1L 0.0343538882868973 -refen $refen 
Drift "TL3.D222" -len 0.1905674726 
Sextupole "SEXT.TL3.SX225" -len 0.3 -K2L 0.3778440120463122 -refen $refen 
Drift "TL3.D228" -len 0.1905674726 
Sbend "BEND.TL3.BEND23" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL3.D248" -len 0.1000000005 
Quadrupole "QUAD.TL3.Q25" -len 0.75 -K1L -0.4032644898347889 -refen $refen 
Drift "TL3.D262" -len 0.2203857562 
Sextupole "SEXT.TL3.SX265" -len 0.3 -K2L -1.392838681835412 -refen $refen 
Drift "TL3.D268" -len 0.2203857562 
Quadrupole "QUAD.TL3.Q27" -len 0.75 -K1L 0.5082121037733964 -refen $refen 
Drift "TL3.D282" -len 0.7273957003 
Sextupole "SEXT.TL3.SX285" -len 0.3 -K2L 2.38506228567498 -refen $refen 
Drift "TL3.D288" -len 1.604791401 
Sbend "BEND.TL3.BEND30" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL3.D71" -len 0.5 
Quadrupole "QUAD.TL3.Q72" -len 0.75 -K1L -0.2901647138528359 -refen $refen 
Drift "TL3.D73" -len 0.5 
Quadrupole "QUAD.TL3.Q74" -len 0.75 -K1L 0.385071593447961 -refen $refen 
Drift "TL3.D75" -len 2.19951 
Quadrupole "QUAD.TL3.Q76" -len 0.75 -K1L -0.5261422322666798 -refen $refen 
Drift "TL3.D77" -len 0.5 
Quadrupole "QUAD.TL3.Q78" -len 0.75 -K1L 0.4796626328499751 -refen $refen 
Drift "TL3.D79" -len 2.150751 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 4 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3B" -len 3.177193097  
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4B" -len 0.3 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.933198237443271 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Drift "TTA.D3B" -len 3.177193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 11.35019003677644 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -9.504442766394783 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D2M" -len 2 
#### Compression Chicane #### 
Drift "DLDG.D4" -len 1.59261465 
Quadrupole "QUAD.DLDG.Q4" -len 0.2 -K1L -0.29769867017504065 -refen $refen 
Drift "DLDG.D3" -len 1.01854763 
Quadrupole "QUAD.DLDG.Q3" -len 0.2 -K1L 0.42529904044745614 -refen $refen 
Drift "DLDG.D2" -len 5.26626489 
Quadrupole "QUAD.DLDG.Q2" -len 0.2 -K1L -0.3683553248971516 -refen $refen 
Drift "DLDG.D1" -len 0.99977365 
Quadrupole "QUAD.DLDG.Q1" -len 0.2 -K1L 0.29549594172325233 -refen $refen 
Drift "DLDG.D0" -len 0.83396146 
Sbend "BEND.BRB1A" -len 0.45 -angle -0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Sbend "BEND.BRB1B" -len 0.45 -angle -0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DG.L1" -len 1.8 
Quadrupole "QUAD.QDDOG" -len 0.1 -K1L 0.1539537815 -refen $refen 
Drift "DG.L1" -len 1.8 
Sbend "BEND.BRB2A" -len 0.45 -angle 0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Sbend "BEND.BRB2B" -len 0.45 -angle 0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DG.L2" -len 0.3 
Quadrupole "QUAD.QFDOG" -len 0.1 -K1L -0.162804451 -refen $refen 
Drift "DG.L3" -len 0.1 
Quadrupole "QUAD.QDDOG2" -len 0.1 -K1L 0.03547706207 -refen $refen 
Drift "DG.L4" -len 0.1 
Drift "DG.L4" -len 0.1 
Quadrupole "QUAD.QDDOG2" -len 0.1 -K1L 0.03547706207 -refen $refen 
Drift "DG.L3" -len 0.1 
Quadrupole "QUAD.QFDOG" -len 0.1 -K1L -0.162804451 -refen $refen 
Drift "DG.L2" -len 0.3 
Sbend "BEND.BRB2B" -len 0.45 -angle 0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Sbend "BEND.BRB2A" -len 0.45 -angle 0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DG.L1" -len 1.8 
Quadrupole "QUAD.QDDOG" -len 0.1 -K1L 0.1539537815 -refen $refen 
Drift "DG.L1" -len 1.8 
Sbend "BEND.BRB1B" -len 0.45 -angle -0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Sbend "BEND.BRB1A" -len 0.45 -angle -0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "DBRC_END.EXTRACTION" 
