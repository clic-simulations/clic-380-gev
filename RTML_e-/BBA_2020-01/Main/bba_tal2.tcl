set tcl_precision 15
ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set beamdir    ./Beams
set wakedir    ./Wakefields

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/getResponseMatrix.tcl
source $scriptdir/disperseEnergy.tcl
source $scriptdir/survey.tcl
source $scriptdir/splitBin.tcl
source $scriptdir/plainTracking.tcl
source $scriptdir/OTO.tcl
source $scriptdir/DFS.tcl
source $scriptdir/DFS-bc2.tcl

create_particles_file $beamdir/particles.in beamparams
create_zero_wakes_file $wakedir/zero_wake.dat beamparams rfparamsbc1 $beamdir/particles.in

source $scriptdir/beamline_and_wake.tcl

make_particle_beam beam1 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam8 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

source $scriptdir/load_mag.tcl

Octave {
    Start = placet_get_name_number_list("rtml","Marker-SR-start");
    End = placet_get_name_number_list("rtml","Marker-TAL2-start");
    [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1, Start, End);
    placet_set_beam("beam8", B);
}

puts "\nApplying BBA to Turn Around Loop 2"
puts "*****************************************" 
getResponseMatrix TAL2 rtml Marker-TAL2-start Marker-TAL2-end beam8 $TAL2de
splitBin rtml TAL2ncarray Marker-TAL2-start Marker-TAL2-end
otoCorrection TAL2 rtml Marker-TAL2-start Marker-TAL2-end beam8 $TAL2beta0 $TAL2OTOnloop
dfsCorrection TAL2 rtml Marker-TAL2-start Marker-TAL2-end beam8 $TAL2de $TAL2beta1 $TAL2wgt $TAL2DFSnloop

source $scriptdir/save.tcl
