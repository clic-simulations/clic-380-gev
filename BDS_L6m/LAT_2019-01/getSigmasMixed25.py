# Just to make sure that the path to the libraires is defined 
from sys import path
path.append('/Users/gohil/cernbox/simulations/clic_380_gev/twiss_parameters/bds/madx_calculations/Python_Classes4MAD')
path.append('/Users/gohil/cernbox/simulations/clic_380_gev/twiss_parameters/bds/madx_calculations/MapClass2')

import math
#import copy
import sys
from os import system
#from simplex import Simplex
from mapclass import Map2
from mapclass25  import *
import random
import metaclass2
# BDS start condition
#betx=66.14532014
#bety=17.92472388
#============================


#FFIN conditions with L*=6m lsca =1.4 ffs entry
#betx=64.9998842
#bety=17.99971386

#FFIN conditions with L*=6m lsca =1.4 bds entry

betx=33.07266007
bety=8.962361942 

#============================
gamma=371820.7409
ex=950*1e-9
ey=30*1e-9



sigmaFFS=[sqrt(ex*betx/gamma), sqrt(ex/betx/gamma), sqrt(ey*bety/gamma), sqrt(ey/bety/gamma), 0.01]

file1=open('sigmas_new.dat','w')

file='fort.18'
map=Map(1,file)
#tw=twiss("twiss.scaled")
#IPi=tw.indx["IP"]
print ";"
sigx1= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax1=",sigx1,";"
sigy1= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay1=",sigy1,";"
sigpx1= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx1=",sigpx1,";"
sigpy1= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy1=",sigpy1,";"
print >> file1,1,sigx1,sigy1,sigpx1,sigpy1

map=Map(2,file)
#tw=twiss("twiss")
#IPi=tw.indx["IP"]
print ";"
sigx2= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax2=",sigx2,";"
sigy2= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay2=",sigy2,";"
sigpx2= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx2=",sigpx2,";"
sigpy2= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy2=",sigpy2,";"
print >> file1,2,sigx2,sigy2,sigpx2,sigpy2

map=Map(3,file)
#tw=twiss("twiss")
#IPi=tw.indx["IP"]
print ";"
sigx3= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax3=",sigx3,";"
sigy3= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay3=",sigy3,";"
sigpx3= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx3=",sigpx3,";"
sigpy3= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy3=",sigpy3,";"
print >> file1,3,sigx3,sigy3,sigpx3,sigpy3

map=Map(4,file)
print ";"
sigx4= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax4=",sigx4,";"
sigy4= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay4=",sigy4,";"
sigpx4= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx4=",sigpx4,";"
sigpy4= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy4=",sigpy4,";"
print >> file1,4,sigx4,sigy4,sigpx4,sigpy4

map=Map(5,file)
print ";"
sigx5= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax5=",sigx5,";"
sigy5= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay5=",sigy5,";"
sigpx5= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx5=",sigpx5,";"
sigpy5= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy5=",sigpy5,";"
print >> file1,5,sigx5,sigy5,sigpx5,sigpy5
#sys.exit()
map=Map(6,file)
print ";"
sigx6= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax6=",sigx6,";"
sigy6= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay6=",sigy6,";"
sigpx6= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx6=",sigpx6,";"
sigpy6= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy6=",sigpy6,";"
print >> file1,6,sigx6,sigy6,sigpx6,sigpy6
'''
map=Map(7,file)
print ";"
sigx7= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax7=",sigx7,";"
sigy7= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay7=",sigy7,";"
sigpx7= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx7=",sigpx7,";"
sigpy7= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy7=",sigpy7,";"
print >> file1,7,sigx7,sigy7,sigpx7,sigpy7

map=Map(8,file)
print ";"
sigx8= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax8=",sigx8,";"
sigy8= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay8=",sigy8,";"
sigpx8= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx8=",sigpx8,";"
sigpy8= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy8=",sigpy8,";"
print >> file1,8,sigx8,sigy8,sigpx8,sigpy8

map=Map(9,file)
print ";"
sigx9= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax9=",sigx9,";"
sigy9= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay9=",sigy9,";"
sigpx9= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx9=",sigpx9,";"
sigpy9= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy9=",sigpy9,";"
print >> file1,9,sigx9,sigy9,sigpx9,sigpy9

map=Map(10,file)
print ";"
sigx10= sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2)
print "sigmax10=",sigx10,";"
sigy10= sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2)
print "sigmay10=",sigy10,";"
sigpx10= sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2)
print "sigmapx10=",sigpx10,";"
sigpy10= sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2)
print "sigmapy10=",sigpy10,";"
print >> file1,10,sigx10,sigy10,sigpx10,sigpy10
#sys.exit()
'''
#print "beta_x=",tw.BETX[IPi],";"
#print "beta_y=",tw.BETY[IPi],";"
#print "alfa_y=",tw.ALFY[IPi],";"
#print "D_y=",tw.DY[IPi],";"
#print "corryx=",map.correlation('y','x',sigmaFFS)-(map.offset('y',sigmaFFS)*map.offset('x',sigmaFFS)),";"
#print "corrypx=",map.correlation('py','x',sigmaFFS)-(map.offset('py',sigmaFFS)*map.offset('x',sigmaFFS)),";"
#print "corrpyx=",map.correlation('y','px',sigmaFFS)-(map.offset('y',sigmaFFS)*map.offset('px',sigmaFFS)),";"
#print "corrpypx=",map.correlation('py','px',sigmaFFS)-(map.offset('py',sigmaFFS)*map.offset('px',sigmaFFS)),";"
#print "corryd=",map.correlation('y','d',sigmaFFS),";"



tw = metaclass2.twiss2("twiss.bds")


#print tw.elems[0].BETX
#print tw.elems[0]
#print tw.getBeta
oide = sqrt(tw.oide(ey,gamma))
print oide 

betay_ip = tw.markers[1].BETY
betax_ip = tw.markers[1].BETX

print bety
print betay_ip

oide1 = sqrt(tw.oide(ey,gamma,betay_ip,1000))
print oide1
bend_rad = sqrt(tw.sigmaBends(E=190))
print bend_rad

eq = Map2(3,file)

T336 = eq.y[0,0,1,0,1]
T346 = eq.y[0,0,0,1,1]

print T336, T346

T116 = eq.x[1,0,0,0,1]
T126 = eq.x[0,1,0,0,1]

print T116, T126

chroma_y = sqrt((T336**2*(bety/betay_ip)) + (T346**2*(1/(bety*betay_ip))))
chroma_x = sqrt((T116**2*(betx/betax_ip)) + (T126**2*(1/(betx*betax_ip))))

print "chroma_y = ", chroma_y
print "chroma_x = ", chroma_x




