#include <stdlib.h>
#define NRANSI

double amotry(double **p, double y[], double psum[], int ndim,
	      double (*funk)(double []), int ihi, double fac)
{
  int j;
  double fac1,fac2,ytry,*ptry;
  
  ptry=malloc(sizeof(double)*(ndim+1));
  fac1=(1.0-fac)/ndim;
  fac2=fac1-fac;
  for (j=1;j<=ndim;j++) ptry[j]=psum[j]*fac1-p[ihi][j]*fac2;
  ytry=(*funk)(ptry);
  if (ytry < y[ihi]) {
    y[ihi]=ytry;
    for (j=1;j<=ndim;j++) {
      psum[j] += ptry[j]-p[ihi][j];
      p[ihi][j]=ptry[j];
    }
  }
  free(ptry);
  return ytry;
}
#undef NRANSI
/* (C) Copr. 1986-92 Numerical Recipes Software ^. */

#include <math.h>
#define NRANSI
#define NMAX 3000
#define GET_PSUM \
  for (j=1;j<=ndim;j++) {\
  for (sum=0.0,i=1;i<=mpts;i++) sum += p[i][j];\
  psum[j]=sum;}
#define SWAP(a,b) {swap=(a);(a)=(b);(b)=swap;}

double amoeba2(double **p, double y[], int ndim, double ftol,
	    double (*funk)(double []), int *nfunk)
{
  double amotry(double **p, double y[], double psum[], int ndim,
		double (*funk)(double []), int ihi, double fac);
  int i,ihi,ilo,inhi,j,mpts=ndim+1;
  double rtol,sum,swap,ysave,ytry,*psum,tiny=1e-300;
  
  psum=malloc(sizeof(double)*(ndim+1));
  *nfunk=0;
  GET_PSUM
    for (;;) {
      ilo=1;
      ihi = y[1]>y[2] ? (inhi=2,1) : (inhi=1,2);
      for (i=1;i<=mpts;i++) {
	if (y[i] <= y[ilo]) ilo=i;
	if (y[i] > y[ihi]) {
	  inhi=ihi;
	  ihi=i;
	} else if (y[i] > y[inhi] && i != ihi) inhi=i;
      }
      rtol=fabs(y[ilo]);
      if (rtol < ftol) {
	SWAP(y[1],y[ilo])
	  for (i=1;i<=ndim;i++) SWAP(p[1][i],p[ilo][i])
				  break;
      }
      if (*nfunk >= NMAX){
	//	printf("amoeba gives up after %d iterations\n",*nfunk);
	//	printf("residual function: %g\n",rtol);
	return;/* my_error("NMAX exceeded");*/
      }
      *nfunk += 2;
      ytry=amotry(p,y,psum,ndim,funk,ihi,-1.0);
      if (ytry <= y[ilo])
	ytry=amotry(p,y,psum,ndim,funk,ihi,2.0);
      else if (ytry >= y[inhi]) {
	ysave=y[ihi];
	ytry=amotry(p,y,psum,ndim,funk,ihi,0.5);
	if (ytry >= ysave) {
	  for (i=1;i<=mpts;i++) {
	    if (i != ilo) {
	      for (j=1;j<=ndim;j++)
		p[i][j]=psum[j]=0.5*(p[i][j]+p[ilo][j]);
	      y[i]=(*funk)(psum);
	    }
	  }
	  *nfunk += ndim;
	  GET_PSUM
	    }
      } else --(*nfunk);
    }
  free(psum);
  return rtol;
}
#undef SWAP
#undef GET_PSUM
#undef NMAX
#undef NRANSI
/* (C) Copr. 1986-92 Numerical Recipes Software ^. */
