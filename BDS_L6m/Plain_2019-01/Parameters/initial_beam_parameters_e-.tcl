#
# This script contains the initial beam parameters used in the simulation.
#
set e_initial 190.0
set e0 $e_initial

set match(emitt_x)   9.5
set match(emitt_y)   0.30
set match(charge)    5.2e9
set match(sigma_z)   70.0
set match(phase)     0.0
set match(e_spread) -1.0; # negative: uniform distribution; positive: gaussian
set match(beta_x)    30.49309113139894
set match(beta_y)    5.351885767617622
set match(alpha_x)  -2.502710433712449
set match(alpha_y)   0.5400454080769824
set charge           $match(charge)
