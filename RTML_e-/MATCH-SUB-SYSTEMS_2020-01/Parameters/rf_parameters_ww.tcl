set rfparamsbooster(gradientww)   1.834480985026e+07
set rfparamsbc1(gradientww)   2.230459946276e+07
set rfparamsbc2(gradientww)   7.456045964276e+07
set energy_wake_bc1  -3.526686662242e-05
set energy_wake_booster  -5.768958070178e-05
set energy_wake_bc2  -7.438328609058e-05
