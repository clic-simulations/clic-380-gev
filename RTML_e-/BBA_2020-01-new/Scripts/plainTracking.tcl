proc plainTracking {name beamline Start End beam} {
    global sigma
    global machine
    Octave {
        disp("TRACKING...");
        Start = placet_get_name_number_list("$beamline","$Start");
        End   = placet_get_name_number_list("$beamline","$End");
        [E,B] = placet_test_no_correction("$beamline", "$beam", "None", 1, Start, End);
        printf("Emittance: %10.2f %10.2f nm\n", E(end,2)*100,E(end,6)*100);
        save(['Emitt0-' "$name-$beamline-" num2str($sigma) '-' num2str($machine) '.dat'], 'E')
    }
}
