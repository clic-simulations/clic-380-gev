#
# This script was developed by C. Gohil from files developed by Y. Han.
#
# This is the main script which performs Beam-Based Alignment on the CLIC 380 GeV RTML.
# Can be executed with: placet run.tcl <machine>, where <machine> is the id number for
# a machine.
#

# Get the machine number from command line arguments
set machine [lindex $argv 0]

Octave {
    source Scripts/func_min.m
    machine=$machine;
    save Machine.dat machine

    global counter = 0;
    global MaxIter = 200;
    options = optimset("TolX", 1e-3, "MaxIter", MaxIter);

    X = [0; 0; 0; 0; 0; 0; 0; 0; 0; 0];
    save X_ca.dat X;
    save X_tal.dat X;

    # Apply one-to-one steering and dispersion-free steering to the entire RTML
    system(["placet -s main_e-_bba.tcl machine " num2str(machine)])

    # Central arc tuning
    system(["placet -s main_e-_boo.tcl machine " num2str(machine)])
    try
        [CAk, cor_min] = fminsearch("correction1", X, options)
    catch err
        if (!strcmp(err.message, "merit function minimised"))
            exit
        endif
    end_try_catch

    # Turn-around loop tuning
    counter = 0;
    X=[0; 0; 0; 0; 0; 0; 0; 0; 0; 0];
    save X_tal.dat X;
    system(["placet -s main_e-_ltl.tcl machine " num2str(machine)])
    try
        [CAk, cor_min] = fminsearch("correction2", X, options)
    catch err
        if (!strcmp(err.message, "merit function minimised"))
            exit
        endif
    end_try_catch

    system(["placet -s main_e-_tal.tcl machine " num2str(machine)])

    # Retunes the turn-around loop with tighter target emittances
    counter = 0;
    load X_tal.dat;
    system(["placet -s main_e-_ltl.tcl machine " num2str(machine)])
    try
        [CAk, cor_min] = fminsearch("correction3", X, options)
    catch err
        if (!strcmp(err.message, "merit function minimised"))
            exit
        endif
    end_try_catch

    system(["placet -s main_e-_tal.tcl machine " num2str(machine)])
    
    # Save the lattice as a hardcoded list of elements
    system(["placet -s save_lattice.tcl machine " num2str(machine)])
}
