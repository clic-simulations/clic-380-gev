WakeSet rtml_Wlong_booster {}
SplineCreate "rtml_Wt_booster" -file "./Wakefields/Wt_booster.dat"
SplineCreate "rtml_Wl_booster" -file "./Wakefields/Wl_booster.dat"
ShortRangeWake "rtml_SR_booster" -type 2 \
-wx "rtml_Wt_booster" \
-wy "rtml_Wt_booster" \
-wz "rtml_Wl_booster"

TclCall -script {
       Octave {
               CAV = placet_get_name_number_list('rtml','120C');
               placet_element_set_attribute("rtml", CAV, "short_range_wake", "rtml_SR_booster");
               placet_element_set_attribute("rtml", CAV, "six_dim", true);

               lambda_booster = placet_element_get_attribute("rtml", CAV(1),"lambda");
               Tcl_SetVar("lambda_booster",lambda_booster);
       }
       InjectorCavityDefine -lambda $lambda_booster -wakelong rtml_Wlong_booster
}
