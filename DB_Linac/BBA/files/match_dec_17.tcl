#idendtity matrix
proc identyM { }  {
    for {set i 0} {$i<4} {incr i} {
        for {set j 0} {$j<4} {incr j} {
            if {$i==$j} { set miden($i,$j) 1.0 } else { set miden($i,$j) 0.0 }
        }
    }
    return [array get miden]
}

#procedure for multiplication two matrix
proc matrix_mult {aa bb}  {
    array set m1l $aa
    array set m2l $bb
    for {set i 0} {$i<4} {incr i} {
        for {set j 0} {$j<4} {incr j} {
            set iprod 0
            for {set k 0} {$k<4} {incr k} {
                set iprod [expr $iprod + $m1l($i,$k)*$m2l($k,$j)]
            }
            set m($i,$j) [expr $iprod]
        }
    }
    return [array get  m]
}

#transpose of a matrix
proc transpose {aa}  {
    array set mm $aa
    for {set i 0} {$i<4} {incr i} {
        for {set j 0} {$j<4} {incr j} { 
            set m($j,$i) $mm($i,$j)
        }
    }
    return [array get  m]
}

# quadrupole matrix
proc  qdfM {l k}  {
    set ll $l
    set kk $k
    array set mq [identyM ]
    if {$kk>0 } {
        set k [expr sqrt($kk)]
        set  mq(0,0) [expr cos($k*$ll)]
        set  mq(0,1) [expr sin($k*$ll)/$k ]
        set  mq(1,0) [expr -$k*sin($k*$ll) ]
        set  mq(1,1) [expr cos($k*$ll) ]
        set  mq(2,2) [expr cosh($k*$ll) ]
        set  mq(2,3) [expr sinh($k*$ll)/$k ]
        set  mq(3,2) [expr sinh($k*$ll)*$k ]
        set  mq(3,3) [expr cosh($k*$ll) ]
    } else {
        set kk -$kk
        set k [expr sqrt($kk)]
        set  mq(0,0) [expr cosh($k*$ll)]
        set  mq(0,1) [expr sinh($k*$ll)/$k ]
        set  mq(1,0) [expr $k*sinh($k*$ll) ]
        set  mq(1,1) [expr cosh($k*$ll) ]
        set  mq(2,2) [expr cos($k*$ll) ]
        set  mq(2,3) [expr sin($k*$ll)/$k ]
        set  mq(3,2) [expr -$k*sin($k*$ll) ]
        set  mq(3,3) [expr cos($k*$ll) ]
    }
    return [array get  mq]
}

# drift matrix
proc drM {l}  {
    array set md [identyM ]
    set  md(0,1) $l
    set  md(2,3) $l
    return [array get  md]
}

proc show_matrix { matrx } {
    array set mtrx $matrx
    set n [expr sqrt([array size mtrx])]
    
    puts " "
    for {set i 0} {$i<$n} {incr i} {
        set line {}
        for {set j 0} {$j<$n} {incr j} {
#             set mij
            lappend line  [format %.8f $mtrx($i,$j)]
        }
        puts $line
    }
    puts " "

}

# check matrix
proc checkmatrix {matrix}  {
    upvar $matrix md
#     array set md $mdd
    set  a1 [expr abs(($md(0,0) + $md(1,1))/2.) ]
    set  a2 [expr abs(($md(2,2) + $md(3,3))/2.) ]
    if { $a1>1 || $a2 >1} {
	 puts "**** warning **** \ntrace of the matrix is invalid check the lattice parameters"
	 exit
    }
}

set fodo "
q1     l      q2       l     q1
FF-----------DDDD------------FF"
set trip "
q1   l1   q2         l          q2   l1   q1
FF-------DDDD------------------DDDD-------FF"

set doub "
  l1   q1         l          q2   l1  
------DDDD------------------FFFF------"

proc read_pars { argsv } {
    
    global fodo trip doub
    
#    upvar $args params 
    array set params [regsub -all "=" $argsv " "]
   
   set pars ""
    
   if {[info exist params(type)]<1} {
       puts "\ndefine the lattice type first  type=<fodo|triplet|doublet> \n"
       exit
   } else {
       set type  $params(type)
       lappend pars $type
   }
      
   if {[info exist params(position)]<1} {
       puts "the matching position is not defined (position=<symmetry|center|quad_in>)  \nposition=symmetry point is going to be used"
       set params(position) symmetry
    } elseif {$params(position)=="symmetry" || $params(position)=="center" || $params(position)=="quad_in" } {
       set position  $params(position)
   } else {
       puts "the matching position is not known (position=<symmetry|center|quad_in>)  \nposition=symmetry point is going to be used"
       set params(position) symmetry 
   }
 
   lappend pars $params(position)
   
  if {$type=="fodo"} {
        puts "fodo type of lattice going to be matched"
       
        if {[info exist params(lq1)]<1} {
            puts "$fodo \ndefine the first quad length lq1=<lq1>" ; exit
        } else { set lq1  $params(lq1) }
        
        if {[info exist params(kq1)]<1} {
            puts "$fodo \ndefine the first quad strength kq1=<kq1>" ; exit
        } else { set kq1  $params(kq1) } 
        
        if {[info exist params(lq2)]<1} {
            puts "$fodo \ndefine the second quad length lq2=<lq2>" ; exit
        } else {set lq2  $params(lq2) } 
        
        if {[info exist params(kq2)]<1} {
            puts "$fodo \ndefine the second quad strength kq2=<kq2>" ; exit
        } else { set kq2  $params(kq2) } 

        if {[info exist params(l)]<1} {
            puts "$fodo \ndefine the space between quads l=<l>" ; exit
        } else { set ls  $params(l) } 
        
        lappend pars $lq1 $kq1 $lq2 $kq2 $ls
       
   } elseif  {$type=="triplet"} {
       puts "triplet type of lattice going to be matched"

        if {[info exist params(lq1)]<1} {
            puts "$trip\ndefine first quad length lq1=<lq1>" ; exit
        } else { set lq1  $params(lq1) }
        
        if {[info exist params(kq1)]<1} {
            puts "$trip \ndefine first quad strength kq1=<kq1>" ; exit
        } else { set kq1  $params(kq1) } 
        
        if {[info exist params(lq2)]<1} {
            puts "$trip \ndefine second quad length lq2=<lq2>" ; exit
        } else { set lq2  $params(lq2) } 
        
        if {[info exist params(kq2)]<1} {
            puts "$trip \ndefine second quad strength kq2=<kq2>" ; exit
        } else { set kq2  $params(kq2) } 

        if {[info exist params(l1)]<1} { 
            puts "$trip \ndefine spacing between quads l1=<l1>" ; exit
        } else { set ls  $params(l1) } 
        
        if {[info exist params(l)]<1} { 
            puts "$trip \ndefine spacing between triplets l=<l>" ; exit
        } else { set lb  $params(l) }        
        
        lappend pars $lq1 $kq1 $lq2 $kq2 $ls $lb
       
   } elseif  {$type=="doublet"} {
       puts "doublet type of lattice going to be matched"
       
        if {[info exist params(lq1)]<1} {
            puts "$doub \ndefine first quad length lq1=<lq1>" ; exit
        } else { set lq1  $params(lq1) }
        
        if {[info exist params(kq1)]<1} {
            puts "$doub \ndefine first quad strength kq1=<kq1>" ; exit
        } else { set kq1  $params(kq1) } 
        
        if {[info exist params(lq2)]<1} {
            puts "$doub \ndefine second quad length lq2=<lq2>" ; exit
        } else { set lq2  $params(lq2) } 
        
        if {[info exist params(kq2)]<1} {
            puts "$doub \ndefine second quad strength kq2=<kq2>" ; exit
        } else { set kq2  $params(kq2) } 

        if {[info exist params(l1)]<1} {
            puts "$doub \ndefine spacing between quads l1=<l1>" ; exit
        } else { set ls  $params(l1)  } 
        
        if {[info exist params(l)]<1} { 
            puts "$doub \ndefine spacing between doublets l=<l>" ; exit
        } else { set lb  $params(l) }        
        
        lappend pars $lq1 $kq1 $lq2 $kq2 $ls $lb
       
   } else {
       puts "the lattice type is not known"
       exit
   }   
 return $pars
}

proc match_symmetry { parameters }  {
    
    if { [lindex $parameters 0] == "fodo" } {
        
        set lq1 [expr [lindex $parameters 2]*0.5]
        set kq1 [lindex $parameters 3]
        set lq2 [lindex $parameters 4]
        set kq2 [lindex $parameters 5]
        set ls  [lindex $parameters 6]
        
        set mf [qdfM $lq1 $kq1 ]
        set md [qdfM $lq2 $kq2 ]
        set ld [drM $ls]
        
        lappend allmat $mf $ld $md $ld $mf
        
    } elseif { [lindex $parameters 0] == "triplet" } {
        
        set lq1 [expr [lindex $parameters 2]*0.5]
        set kq1 [lindex $parameters 3]
        set lq2 [lindex $parameters 4]
        set kq2 [lindex $parameters 5]
        set ls  [lindex $parameters 6]        
        set lb  [lindex $parameters 7]
        
        set mf [qdfM $lq1 $kq1 ]
        set md [qdfM $lq2 $kq2 ]
        set mds [drM $ls]
        set mdb [drM $lb]
        
        lappend allmat $mf $mds $md $mdb $md $mds $mf
        
    } elseif { [lindex $parameters 0] == "doublet" } {

        set lq1 [lindex $parameters 2]
        set kq1 [lindex $parameters 3]
        set lq2 [lindex $parameters 4]
        set kq2 [lindex $parameters 5]
        set ls  [expr 0.5*[lindex $parameters 6]]    
        set lb  [lindex $parameters 7]
        
        
        set mf [qdfM $lq1 $kq1 ]
        set md [qdfM $lq2 $kq2 ]
        set mds [drM $ls]
        set mdb [drM $lb]
        lappend allmat  $mds $mf $mdb $md $mds       
       
    } else {
        puts "cant be matched"
    }
    
    set multiply [identyM ]
    foreach  m $allmat {
        set multiply [matrix_mult $m $multiply]
    }
    array set mtrs $multiply
    	   
    checkmatrix mtrs 
    
    set phix [expr acos(($mtrs(0,0) + $mtrs(1,1))/2)]
    set phiy [expr acos(($mtrs(2,2) + $mtrs(3,3))/2)]
    set beta_x0 [expr $mtrs(0,1)/sin($phix) ]
    set beta_y0 [expr $mtrs(2,3)/sin($phiy) ]
    set alpha_x0 [expr ($mtrs(0,0) - $mtrs(1,1))/sin($phix)/2]
    set alpha_y0 [expr ($mtrs(2,2) - $mtrs(3,3))/sin($phiy)/2]
    set gamma_x0 [expr (1+$alpha_x0*$alpha_x0)/$beta_x0]
    set gamma_y0 [expr (1+$alpha_y0*$alpha_y0)/$beta_y0]
   
   array set match { }
   
#    set match(beta_x)   $beta_x0 
#    set match(beta_y)   $beta_y0  
#    set match(alpha_x)  $alpha_y0 
#    set match(alpha_y)  $alpha_x0 
#    set match(gamma_x)  $gamma_x0 
#    set match(gamma_y)  $gamma_y0

#    return [array get  match]
return "$alpha_x0 $beta_x0 $gamma_x0 $alpha_y0 $beta_y0 $gamma_y0 $phix $phiy"

}

proc transport_twiss { twiss matris } {
    
    array set mtrs $matris 
    
    set alpha_x0 [lindex $twiss 0]
    set beta_x0  [lindex $twiss 1]
    set gamma_x0 [lindex $twiss 2]
    set alpha_y0 [lindex $twiss 3]
    set beta_y0  [lindex $twiss 4]
    set gamma_y0 [lindex $twiss 5]
    
    set  beta_x [expr ($mtrs(0,0))**2*$beta_x0-2.0*($mtrs(0,0)*$mtrs(0,1))*$alpha_x0+($mtrs(0,1))**2*$gamma_x0]
    set  beta_y [expr ($mtrs(2,2))**2*$beta_y0-2.0*($mtrs(2,2)*$mtrs(2,3))*$alpha_y0+($mtrs(2,3))**2*$gamma_y0]
    
#     set  alpha_x [expr (1+2*$mtrs(0,1)*$mtrs(1,0))*$alpha_x0-$mtrs(1,0)*$mtrs(0,0)*$beta_x0-$mtrs(0,1)*$mtrs(1,1)*$gamma_x0]
#     set  alpha_y [expr (1+2*$mtrs(2,3)*$mtrs(3,2))*$alpha_y0-$mtrs(3,2)*$mtrs(2,2)*$beta_y0-$mtrs(2,3)*$mtrs(3,3)*$gamma_y0]
    
    set  alpha_x [expr -$mtrs(1,0)*$mtrs(0,0)*$beta_x0 + ($mtrs(0,1)*$mtrs(1,0)+$mtrs(1,1)*$mtrs(0,0))*$alpha_x0 - $mtrs(0,1)*$mtrs(1,1)*$gamma_x0]
    set  alpha_y [expr -$mtrs(3,2)*$mtrs(2,2)*$beta_y0 + ($mtrs(2,3)*$mtrs(3,2)+$mtrs(3,3)*$mtrs(2,2))*$alpha_y0 - $mtrs(2,3)*$mtrs(3,3)*$gamma_y0]
    
    set gamma_x [expr (1+$alpha_x*$alpha_x)/$beta_x]
    set gamma_y [expr (1+$alpha_y*$alpha_y)/$beta_y]

#     set gamma_x [expr $mtrs(1,0)**2*$beta_x0 - 2.0*$mtrs(1,1)*$mtrs(1,0)*$alpha_x0 + $mtrs(1,1)**2*$gamma_x0]
#     set gamma_y [expr $mtrs(3,2)**2*$beta_y0 - 2.0*$mtrs(3,3)*$mtrs(3,2)*$alpha_y0 + $mtrs(3,3)**2*$gamma_y0]
    
    return "$alpha_x $beta_x $gamma_x $alpha_y $beta_y $gamma_y"
    
}



proc prepare_matrix_4_position { parameters }  {
    
    set allmat ""
    lappend allmat [identyM]
    
    if { [lindex $parameters 0] == "fodo" } {
        
        set lq1 [expr [lindex $parameters 2]*0.5]
        set kq1 [lindex $parameters 3]
        set lq2 [lindex $parameters 4]
        set kq2 [lindex $parameters 5]
        set ls  [lindex $parameters 6]
        set ls2 [expr 0.5*$ls]
        
        set mf  [qdfM $lq1 $kq1 ]
        set md  [qdfM $lq2 $kq2 ]
        set ld  [drM  $ls]
        set ld2 [drM  $ls2]
        
        if { [lindex $parameters 1] == "center" } {
            lappend allmat $mf $ld $md $ld2       
        } elseif  { [lindex $parameters 1] == "quad_in" } {
            lappend allmat $mf $ld $md $ld 
        } else {
            lappend allmat $mf $ld $md $ld $mf
        }
        
    } elseif { [lindex $parameters 0] == "triplet" } {
        
        set lq1 [expr [lindex $parameters 2]*0.5]
        set kq1 [lindex $parameters 3]
        set lq2 [lindex $parameters 4]
        set kq2 [lindex $parameters 5]
        set ls  [lindex $parameters 6]        
        set lb  [lindex $parameters 7]
        
        set mf [qdfM $lq1 $kq1 ]
        set md [qdfM $lq2 $kq2 ]
        set mds [drM $ls]
        set mdb [drM $lb]
        set mdb2 [drM [expr 0.5*$lb]]
        
        if { [lindex $parameters 1] == "center" } {
            lappend allmat $mf $mds $md $mdb2   
        } elseif  { [lindex $parameters 1] == "quad_in" } {
            lappend allmat $mdb $md $mds $mf
            puts "note that the beta is goint to be calculated for the first quadrupole of triplet"
        } else {
            lappend allmat $mf $mds $md $mdb $md $mds $mf
        }

    } elseif { [lindex $parameters 0] == "doublet" } {
        
        set lq1 [lindex $parameters 2]
        set kq1 [lindex $parameters 3]
        set lq2 [lindex $parameters 4]
        set kq2 [lindex $parameters 5]
        set ls  [expr 0.5*[lindex $parameters 6]]    
        set lb  [lindex $parameters 7]
        
        set mf [qdfM $lq1 $kq1 ]
        set md [qdfM $lq2 $kq2 ]
        set mds [drM $ls]
        set mdb [drM $lb]
        set mdb2 [drM [expr 0.5*$lb]]

        if { [lindex $parameters 1] == "center" } {
            lappend allmat  $mds $md $mdb2           
        } elseif  { [lindex $parameters 1] == "quad_in" } {
            puts "note that the beta is goint to be calculated for the first quadrupole of doublet"
            lappend allmat  $mds $md $mdb     
        } else {
            lappend allmat  $mds $md $mdb $mf $mds   
        }
        
    } else {
        puts "can not order lattice"
    }
    
    set multiply [identyM ]
            
    foreach  m $allmat {
        set multiply [matrix_mult $m $multiply]
    }
            
    array set mtrs $multiply
    return [array get mtrs]
}



proc slice_matrix { parameters step } {
    
        set allmat ""
        set stepss ""
        set s 0 
        lappend stepss $s
        lappend allmat [identyM]
    
    if { [lindex $parameters 0] == "fodo" } {
        
        set lq1 [expr [lindex $parameters 2]*0.5]
        set kq1 [lindex $parameters 3]
        set lq2 [lindex $parameters 4]
        set kq2 [lindex $parameters 5]
        set ls  [lindex $parameters 6]
        
        set n_lq1 [expr int($lq1/$step)]
        set r_lq1 [expr $lq1-$step*$n_lq1]
    
        set n_lq2 [expr int($lq2/$step)]
        set r_lq2 [expr $lq2-$step*$n_lq2]
        
        set n_ls  [expr int($ls/$step)]
        set r_ls [expr  $ls-$step*$n_ls]
        

        for {set i 0 } { $i < $n_lq1} { incr i } { 
            lappend allmat [qdfM $step $kq1 ]  
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_lq1>0} { 
            lappend allmat [qdfM $r_lq1 $kq1 ] ;
            set s [expr $s+$r_lq1] ; 
            lappend stepss $s 
        }
        


        for {set i 0 } { $i < $n_ls} { incr i } { 
            lappend allmat [drM $step] ; 
            set s [expr $s+$step] ; 
            lappend stepss $s  
        }
        if {$r_ls>0} { 
            lappend allmat [drM $r_ls ] ;
            set s [expr $s+$r_ls] ; 
            lappend stepss $s 
        }
        
        for {set i 0 } { $i < $n_lq2} { incr i } { 
            lappend allmat [qdfM $step $kq2 ] ; 
            set s [expr $s+$step] ; 
            lappend stepss $s  
        }
        if {$r_lq2>0} { 
            lappend allmat [qdfM $r_lq2 $kq2 ] ;
            set s [expr $s+$r_lq2] ; 
            lappend stepss $s  
        }
        
        for {set i 0 } { $i < $n_ls} { incr i } { 
            lappend allmat [drM $step] ; 
            set s [expr $s+$step] ; 
            lappend stepss $s  
        }
        if {$r_ls>0} { 
            lappend allmat [drM $r_ls ] ;
            set s [expr $s+$r_ls] ; 
            lappend stepss $s  
        }      

        for {set i 0 } { $i < $n_lq1} { incr i } { 
            lappend allmat [qdfM $step $kq1 ] ; 
            set s [expr $s+$step] ; 
            lappend stepss $s  
        }
        if {$r_lq1>0} { 
            lappend allmat [qdfM $r_lq1 $kq1 ]  ;
            set s [expr $s+$r_lq1] ; 
            lappend stepss $s
        }
        
#         lappend allmat $mf1 $ld1 $md1 $ld1 $mf1      
        
    } elseif { [lindex $parameters 0] == "triplet" } {
        
        set lq1 [expr [lindex $parameters 2]*0.5]
        set kq1 [lindex $parameters 3]
        set lq2 [lindex $parameters 4]
        set kq2 [lindex $parameters 5]
        set ls  [lindex $parameters 6]        
        set lb  [lindex $parameters 7]
        
        set n_lq1 [expr int($lq1/$step)]
        set r_lq1 [expr $lq1-$step*$n_lq1]
        
        set n_lq2 [expr int($lq2/$step)]
        set r_lq2 [expr $lq2-$step*$n_lq2]
        
        set n_ls  [expr int($ls/$step)]
        set r_ls [expr  $ls-$step*$n_ls]

        set n_lb  [expr int($lb/$step)]
        set r_lb [expr  $lb-$step*$n_ls]
        
        set allmat "" 
        for {set i 0 } { $i < $n_lq1} { incr i } { 
            lappend allmat [qdfM $step $kq1 ] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_lq1>0} { 
            lappend allmat [qdfM $r_lq1 $kq1 ] 
            set s [expr $s+$r_lq1] 
            lappend stepss $s  
        }
        
        for {set i 0 } { $i < $n_ls} { incr i } { 
            lappend allmat [drM $step] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_ls>0} { 
            lappend allmat [drM $r_ls ] 
            set s [expr $s+$r_ls] 
            lappend stepss $s  
        }        

        for {set i 0 } { $i < $n_lq2} { incr i } { 
            lappend allmat [qdfM $step $kq2 ] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_lq2>0} { 
            lappend allmat [qdfM $r_lq2 $kq2 ] 
            set s [expr $s+$r_lq2] 
            lappend stepss $s 
        }
        
        for {set i 0 } { $i < $n_lb} { incr i } {  
            lappend allmat [drM $step] 
            set s [expr $s+$step] 
            lappend stepss $s              
        }
        if {$r_lb>0} { 
            lappend allmat [drM $r_lb ] 
            set s [expr $s+$r_lb] 
            lappend stepss $s 
        }

        for {set i 0 } { $i < $n_lq2} { incr i } { 
            lappend allmat [qdfM $step $kq2 ] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_lq2>0} { 
            lappend allmat [qdfM $r_lq2 $kq2 ] 
            set s [expr $s+$r_lq2] 
            lappend stepss $s 
        }

        for {set i 0 } { $i < $n_ls} { incr i } { 
            lappend allmat [drM $step] 
            set s [expr $s+$step] 
            lappend stepss $s              
        }
        if {$r_ls>0} { 
            lappend allmat [drM $r_ls ]
            set s [expr $s+$r_ls] 
            lappend stepss $s 
        }  


        for {set i 0 } { $i < $n_lq1} { incr i } { 
            lappend allmat [qdfM $step $kq1 ] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_lq1>0} { 
            lappend allmat [qdfM $r_lq1 $kq1 ] 
            set s [expr $s+$r_lq1] 
            lappend stepss $s              
        }
        
    } elseif { [lindex $parameters 0] == "doublet" } {

        set lq1 [lindex $parameters 2]
        set kq1 [lindex $parameters 3]
        set lq2 [lindex $parameters 4]
        set kq2 [lindex $parameters 5]
        set ls  [expr 0.5*[lindex $parameters 6]]    
        set lb  [lindex $parameters 7]
        
        set n_lq1 [expr int($lq1/$step)]
        set r_lq1 [expr $lq1-$step*$n_lq1]
        
        set n_lq2 [expr int($lq2/$step)]
        set r_lq2 [expr $lq2-$step*$n_lq2]
        
        set n_ls  [expr int($ls/$step)]
        set r_ls [expr  $ls-$step*$n_ls]
        
        set n_lb  [expr int($lb/$step)]
        set r_lb [expr  $lb-$step*$n_ls]
        
        set allmat ""
        for {set i 0 } { $i < $n_ls} { incr i } { 
            lappend allmat [drM $step] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_ls>0} { 
            lappend allmat [drM $r_ls ] 
            set s [expr $s+$r_ls] 
            lappend stepss $s 
        }
        
        for {set i 0 } { $i < $n_lq1} { incr i } { 
            lappend allmat [qdfM $step $kq1 ] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_lq1>0} { 
            lappend allmat [qdfM $r_lq1 $kq1 ]
            set s [expr $s+$r_lq1] 
            lappend stepss $s              
        }
        
        for {set i 0 } { $i < $n_lb} { incr i } { 
            lappend allmat [drM $step] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_lb>0} { 
            lappend allmat [drM $r_lb ] 
            set s [expr $s+$r_lb] 
            lappend stepss $s              
        }
        
        for {set i 0 } { $i < $n_lq2} { incr i } { 
            lappend allmat [qdfM $step $kq2 ] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_lq2>0} { 
            lappend allmat [qdfM $r_lq2 $kq2 ] 
            set s [expr $s+$r_lq2] 
            lappend stepss $s              
        }
        
        for {set i 0 } { $i < $n_ls} { incr i } { 
            lappend allmat [drM $step] 
            set s [expr $s+$step] 
            lappend stepss $s  
        }
        if {$r_ls>0} { 
            lappend allmat [drM $r_ls ] 
            set s [expr $s+$r_ls] 
            lappend stepss $s             
        }        
     
    } else {
        puts "cant slice"
        exit
    }
    
#     foreach m $allmat {
#         
#         puts "$m"
#         
#     }  

    return "{$stepss} {$allmat} "
    
}

 proc transport_twiss_slice { twiss matris plot } {
     
     set mtrs [lindex $matris 1]
     set steps [lindex $matris 0]
     set twi  $twiss
     
     set sbetas ""
     set s ""
     set bx ""
     set by ""
     if {$plot!="false"} { set aa  [open "cell_twiss.dat" w]}
     foreach mtri $mtrs st $steps {
         set tmp [transport_twiss $twi  $mtri]
         set twi $tmp
         lappend s $st
         lappend bx $st "[lindex $tmp 1]"
         lappend by $st [lindex $tmp 4]
         set print  [format "%.5f %.8f %.8f" $st [lindex $tmp 1] [lindex $tmp 4] ]
         if {$plot!="false"} { puts $aa $print}         
         lappend sbetas $print
     }
    if {$plot!="false"} { close $aa}
     
#      exec echo " -e 'plot \"-\"\n1 1\ne' $sbetas " | gnuplot -p 
#      exec echo "plot '-' u 1:2 w l $bx " | gnuplot -p 
#      plot data xydata -colour black -lines 1 -points 0 -coords $bx
     return $sbetas
     

         
 }

proc beta_integrate { listt } {
    
    set leng [llength $listt]
    set intx 0.0
    set inty 0.0
    
    for {set i 0} {$i < [expr $leng-1]} {incr i} {
        set j [expr $i+1]
        set linei [lindex $listt $i]
        set linej [lindex $listt $j]

        set s1 [lindex $linei 0]
        set s2 [lindex $linej 0]
        set ds [expr 0.5*($s2-$s1)]
        set bx1 [lindex $linei 1]
        set bx2 [lindex $linej 1]
        
        set by1 [lindex $linei 2]
        set by2 [lindex $linej 2]    
        
        set intx [expr $intx+($bx1+$bx2)*$ds]
        set inty [expr $inty+($by1+$by2)*$ds]
    }
    set integrals [format "int (0->L) beta_x,y = %.8f , %.8f " $intx $inty]
    puts $integrals
    return "$intx $inty"
    
}


proc match { args } {

    
    
    set tmp $args
    set lattice_parameter [read_pars $tmp]

    set twiss0 [match_symmetry $lattice_parameter]
    
    set matrix0 [prepare_matrix_4_position $lattice_parameter]
    
#     puts $twiss0
    
    if {[info exist params2(position)]!="symmetry"} {
        set twiss [transport_twiss $twiss0 $matrix0]
    } else {
        set twiss $twiss0
    }
    
#     puts $twiss

    
    array set params2 [regsub -all "=" $args " "]
    
        
    if {[info exist params2(integrate)]<1} {
        set integrate false
    } else {
        set integrate  $params2(integrate)
    }
    
    if {[info exist params2(step)]<1} {
        set step 0.1
    } else {
        set step  $params2(step)
    }
    
    if {[info exist params2(plot)]<1} {
        set plot false
    } else {
        set plot  $params2(plot)
    }
    
    if {$integrate!="false"} {
        set slicemat [slice_matrix $lattice_parameter $step]
        set betas [transport_twiss_slice $twiss $slicemat $plot]
        beta_integrate $betas
    }
   
   
    array set match { }
    set match(alpha_x)  [lindex $twiss 0] 
    set match(beta_x)   [lindex $twiss 1] 
    set match(phi_x)    [format "%.2f" [expr 180.0*[lindex $twiss0 6]/acos(-1.0)]]
#     set match(gamma_x)  [lindex $twiss 2] 
    set match(alpha_y)  [lindex $twiss 3] 
    set match(beta_y)   [lindex $twiss 4] 
#     set match(gamma_y)  [lindex $twiss 5] 
    set match(phi_y)   [format "%.2f" [expr 180.0*[lindex $twiss0 7]/acos(-1.0)]]

    return [array get  match]
}

 
# set q1 2.5
# set l1 0.2
# 
# 
# array set BeamDefine [match type=fodo position=symmetry lq1=$l1 kq1=$q1 lq2=$l1 kq2=-$q1 l=3 integrate=true plot=true]
# puts [array get  BeamDefine]
# 
# 
# exit

