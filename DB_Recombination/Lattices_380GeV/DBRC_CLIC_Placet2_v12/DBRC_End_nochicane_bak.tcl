## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
## Which means there will be a slight magnet missfocus for the other bunches. 
if {$Dipole_synrad} { 
set refen 1.904809675 
} else { 
set refen 1.906 
} 
####START#### 
Marker "DBRC_END.INJECTION" 
### The beam exits CR2 with an offset in the combiner ring's referencial, 
### This kicker puts it in TL3's referential 
#~ Kalign TL3offset -cx 0. -cxp 0. 
Kalign TL3offset -fixed 1 -cx 0.02726400778367424 -cxp 0.001541740824946881 
Marker "TL3_offset" -nodes 1 -kick TL3offset 
Bpm "BPM.END.0" -len 0 
Sbend "BEND.TL3.BEND10" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL3.D122" -len 1.9174 
Sextupole "SEXT.TL3.SX125" -len 0.3 -K2L 0.6101201634115153 -refen $refen 
Drift "TL3.D128" -len 0.8837 
Quadrupole "QUAD.TL3.Q13" -len 0.75 -K1L 0.37753890645489374 -refen $refen 
Bpm "BPM.END.4" -len 0 
Drift "TL3.D142" -len 0.042885 
Sextupole "SEXT.TL3.SX145" -len 0.3 -K2L -1.2476114913873912 -refen $refen 
Drift "TL3.D148" -len 0.042885 
Quadrupole "QUAD.TL3.Q15" -len 0.75 -K1L -0.42676363805217143 -refen $refen 
Bpm "BPM.END.5" -len 0 
Drift "TL3.D162" -len 0.0976775 
Drift "TL3.D168" -len 0.0976775 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 0.5000086579 
Sbend "BEND.TL3.BEND17" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL3.D182" -len 0.0001175 
Sextupole "SEXT.TL3.SX185" -len 0.3 -K2L 2.2951254993058656 -refen $refen 
Drift "TL3.D188" -len 0.0001175 
Quadrupole "QUAD.TL3.Q20A" -len 0.375 -K1L 0.19846813593395213 -refen $refen 
Bpm "BPM.END.6" -len 0 
Marker "TL3.MIDCELLONE" 
Quadrupole "QUAD.TL3.Q20B" -len 0.375 -K1L 0.19846813593395213 -refen $refen 
Bpm "BPM.END.7" -len 0 
Drift "TL3.D222" -len 0.1905674726 
Sextupole "SEXT.TL3.SX225" -len 0.3 -K2L 0.2135962577096832 -refen $refen 
Drift "TL3.D228" -len 0.1905674726 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 0.5000086579 
Sbend "BEND.TL3.BEND23" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TL3.D242" -len 0.05000000025 
Drift "TL3.D248" -len 0.05000000025 
Quadrupole "QUAD.TL3.Q25" -len 0.75 -K1L -0.4657287654819109 -refen $refen 
Bpm "BPM.END.8" -len 0 
Drift "TL3.D262" -len 0.2203857562 
Sextupole "SEXT.TL3.SX265" -len 0.3 -K2L -4.113511690933955 -refen $refen 
Drift "TL3.D268" -len 0.2203857562 
Quadrupole "QUAD.TL3.Q27" -len 0.75 -K1L 0.4107748907367076 -refen $refen 
Bpm "BPM.END.9" -len 0 
Drift "TL3.D282" -len 0.7273957003 
Sextupole "SEXT.TL3.SX285" -len 0.3 -K2L 6.024769884992168 -refen $refen 
Drift "TL3.D288" -len 1.604791401 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL3.BEND30" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TL3.EXTRACTION" 
Drift "TL3.D71" -len 0.5 
Quadrupole "QUAD.TL3.Q72" -len 0.75 -K1L -0.39386932381490647 -refen $refen 
Bpm "BPM.END.10" -len 0 
Drift "TL3.D73" -len 0.5 
Quadrupole "QUAD.TL3.Q74" -len 0.75 -K1L 0.5563139137271079 -refen $refen 
Bpm "BPM.END.11" -len 0 
Drift "TL3.D75" -len 2.19951 
Quadrupole "QUAD.TL3.Q76" -len 0.75 -K1L -0.19129446378314474 -refen $refen 
Bpm "BPM.END.12" -len 0 
Drift "TL3.D77" -len 0.5 
Quadrupole "QUAD.TL3.Q78" -len 0.75 -K1L -0.17825237105701902 -refen $refen 
Bpm "BPM.END.13" -len 0 
Drift "TL3.D79" -len 0.150751 
Bpm "BPM.END.TTA" -len 0 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843603823226587 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.14" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.15" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228603049360155 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.16" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889933149734986 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889933713880491 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.17" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22860411405261 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214290999998 -refen $refen 
Bpm "BPM.END.18" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.19" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843604728133404 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.84360653794641 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.20" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.21" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22860624343678 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.22" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88993484217111 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889935406316224 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.23" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228607308128497 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.24" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.25" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436074428526 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843609252664355 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.26" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.27" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228609437511198 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.28" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889936534606063 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889937098750789 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.29" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228610502202178 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.30" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.31" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.84361015756992 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843611967380423 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.32" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.33" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228612631583404 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.34" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889938227039846 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88993879118418 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.35" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22861369627365 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.36" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.37" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436128722853615 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843614682094611 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.38" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.39" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228615825653401 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.40" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889939919472457 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.8899404836164 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.41" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228616890342908 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.42" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.43" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843615586998921 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843617396806919 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.44" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214290999998 -refen $refen 
Bpm "BPM.END.45" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228619019721185 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.46" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889941611903897 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889942176047449 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.47" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228620084409954 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.48" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.49" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843618301710603 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843620111517346 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.50" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.51" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228622213786757 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.52" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889943304334164 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889943868477326 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.53" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22862327847479 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.54" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.55" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843621016420406 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436228262258965 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.56" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.57" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228625407850119 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.58" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88994499676326 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889945560906032 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.59" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228626472537416 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.60" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.61" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436237311283294 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436255409325675 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.62" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.63" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22862860191127 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.64" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889946689191184 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889947253333566 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.65" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22862966659783 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214290999998 -refen $refen 
Bpm "BPM.END.66" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.67" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843626445834372 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843628255637357 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.68" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.69" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228631795970209 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.70" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889948381617937 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889948945759927 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.71" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228632860656031 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.72" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.73" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843629160538535 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843630970340268 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.74" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.75" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228634990026938 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.76" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889950074043518 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889950638185117 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.77" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228636054712021 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214290999998 -refen $refen 
Bpm "BPM.END.78" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.79" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436318752408205 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843633685041299 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.80" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.81" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228638184081452 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.82" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889951766467926 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889952330609136 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.83" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.2286392487658 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214290999998 -refen $refen 
Bpm "BPM.END.84" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.85" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436345899412245 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843636399740451 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.86" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.87" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228641378133757 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.88" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.8899534588911635 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889954023031982 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.89" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228642442817367 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.90" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.91" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843637304639749 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843639114437723 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.92" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214290999998 -refen $refen 
Bpm "BPM.END.93" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228644572183851 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.94" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889955151313229 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889955715453658 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.95" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228645636866725 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.96" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.97" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843640019336397 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843641829133119 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.98" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.99" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228647766231738 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.100" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889956843734126 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889957407874164 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.101" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228648830913876 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.102" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.103" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843642734031167 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436445438266365 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.104" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.105" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228650960277413 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.106" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889958536153849 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889959100293497 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.107" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228652024958814 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.108" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.109" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843645448724058 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843647258518274 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.110" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.111" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228654154320878 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.112" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889960228572402 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889960792711659 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.113" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228655219001542 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.114" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.115" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.84364816341507 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436499732080325 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.116" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.117" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22865734836213 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.118" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.8899619209897836 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88996248512865 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.119" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228658413042057 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.120" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.121" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843650878104201 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843652687895911 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.122" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.123" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22866054240117 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.124" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.8899636134059925 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889964177544469 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.125" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22866160708036 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.126" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.127" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843653592791454 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843655402581911 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.128" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.129" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228663736438001 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.130" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88996530582103 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889965869959116 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.131" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228664801116453 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.132" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.133" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843656307476826 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.84365811726603 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.134" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.135" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228666930472619 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.136" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889966998234896 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88996756237259 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.137" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228667995150335 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.138" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.139" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843659022160319 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.84366083194827 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.140" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214290999998 -refen $refen 
Bpm "BPM.END.141" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228670124505028 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.142" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889968690647589 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889969254784894 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.143" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228671189182004 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.144" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356000001 -refen $refen 
Bpm "BPM.END.145" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843661736841932 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843663546628631 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.146" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.147" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228673318535222 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.148" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.8899703830591115 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.8899709471960255 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.149" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228674383211462 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.150" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356000001 -refen $refen 
Bpm "BPM.END.151" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843664451521668 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436662613071135 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.152" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.153" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22867651256321 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.154" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889972075469464 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889972639605988 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.155" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228677577238715 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.156" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.157" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843667166199525 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.84366897598372 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.158" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.159" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228679706588988 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.160" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.8899737678786455 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889974332014778 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.161" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228680771263754 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.162" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.163" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843669880875504 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843671690658446 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.164" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.165" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228682900612553 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.166" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889975460286655 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889976024422397 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.167" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228683965286583 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.168" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.169" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843672595549603 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843674405331291 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.170" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.171" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228686094633906 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.172" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889977152693492 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889977716828844 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.173" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228687159307201 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214290999998 -refen $refen 
Bpm "BPM.END.174" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.175" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436753102218235 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843677120002259 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.176" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.177" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22868928865305 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.178" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889978845099158 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88997940923412 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.179" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228690353325607 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.180" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.181" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843678024892164 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843679834671346 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.182" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.183" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228692482669983 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.184" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889980537503652 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889981101638223 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.185" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.2286935473418 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.186" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.187" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843680739560624 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843682549338553 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.188" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.189" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228695676684701 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.190" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889982229906974 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889982794041154 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.191" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228696741355783 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.192" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.193" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843683454227204 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843685264003881 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.194" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.195" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22869887069721 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.196" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889983922309124 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.8899844864429145 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.197" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228699935367555 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.198" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.199" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843686168891906 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843687978667332 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.200" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.201" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22870206470751 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.202" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889985614710105 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889986178843505 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.203" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22870312937712 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.204" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.205" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843688883554733 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843690693328906 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.206" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.207" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.2287052587156 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.208" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889987307109914 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889987871242923 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.209" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228706323384472 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.210" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.211" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843691598215678 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843693407988598 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.212" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.213" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228708452721479 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.214" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889988999508552 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88998956364117 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744930000004 -refen $refen 
Bpm "BPM.END.215" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228709517389614 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.216" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356000001 -refen $refen 
Bpm "BPM.END.217" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843694312874745 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843696122646411 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.218" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.219" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228711646725147 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.220" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889990691906018 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889991256038246 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.221" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228712711392545 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.222" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.223" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843697027531933 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8436988373023455 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.224" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.225" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228714840726601 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.226" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88999238430231 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.8899929484341484 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.227" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228715905393264 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.228" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.229" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.84369974218724 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.8437015519564 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003356 -refen $refen 
Bpm "BPM.END.230" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291000001 -refen $refen 
Bpm "BPM.END.231" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228718034725848 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.232" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889994076697434 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88999464082888 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.233" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.22871909939177 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.234" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.235" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843702456840667 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843704266608575 -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.236" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.237" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228721228722879 -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.3042074493 -refen $refen 
Bpm "BPM.END.238" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.889995769091383 -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -K2L -4.88999633322244 -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -K1L 0.30420744929999993 -refen $refen 
Bpm "BPM.END.239" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -K2L 9.228722293388065 -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -K1L -0.6004214291 -refen $refen 
Bpm "BPM.END.240" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -K1L 0.6863003355999999 -refen $refen 
Bpm "BPM.END.241" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -K2L -7.843705171492214 -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Marker "TTA.EXTRACTION" 
#~ Bpm "BPM.END.242" -len 0 
#~ Marker "CHICANESTART" 
#~ #### -0.7076427116472274 1.010954352151602 -0.875596658885479 0.7024067301303147 
#~ Drift "DLDG.D4" -len 1.59261465 
#~ Quadrupole "QUAD.DLDG.Q4" -len 0.2 -K1L -0.29769867017504065 -refen $refen 
#~ Drift "DLDG.D3" -len 1.01854763 
#~ Quadrupole "QUAD.DLDG.Q3" -len 0.2 -K1L 0.42529904044745614 -refen $refen 
#~ Drift "DLDG.D2" -len 5.26626489 
#~ Quadrupole "QUAD.DLDG.Q2" -len 0.2 -K1L -0.3683553248971516 -refen $refen 
#~ Drift "DLDG.D1" -len 0.99977365 
#~ Quadrupole "QUAD.DLDG.Q1" -len 0.2 -K1L 0.29549594172325233 -refen $refen 
#~ Drift "DLDG.D0" -len 0.83396146 
#~ Bpm "BPM.END.243" -len 0 
#~ Sbend "BEND.BRB1A" -len 0.45 -angle -0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
#~ set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
#~ Sbend "BEND.BRB1B" -len 0.45 -angle -0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
#~ set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
#~ Drift "DG.L1" -len 1.8 
#~ Quadrupole "QUAD.QDDOG" -len 0.1 -K1L 0.15395378150000003 -refen $refen 
#~ Drift "DG.L1" -len 1.8 
#~ Sbend "BEND.BRB2A" -len 0.45 -angle 0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
#~ set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
#~ Sbend "BEND.BRB2B" -len 0.45 -angle 0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
#~ set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
#~ Drift "DG.L2" -len 0.3 
#~ Quadrupole "QUAD.QFDOG" -len 0.1 -K1L -0.162804451 -refen $refen 
#~ Drift "DG.L3" -len 0.1 
#~ Quadrupole "QUAD.QDDOG2" -len 0.1 -K1L 0.03547706207 -refen $refen 
#~ Drift "DG.L4" -len 0.1 
#~ Drift "DG.L4" -len 0.1 
#~ Quadrupole "QUAD.QDDOG2" -len 0.1 -K1L 0.03547706207 -refen $refen 
#~ Drift "DG.L3" -len 0.1 
#~ Quadrupole "QUAD.QFDOG" -len 0.1 -K1L -0.162804451 -refen $refen 
#~ Drift "DG.L2" -len 0.3 
#~ Sbend "BEND.BRB2B" -len 0.45 -angle 0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
#~ set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
#~ Sbend "BEND.BRB2A" -len 0.45 -angle 0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
#~ set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
#~ Drift "DG.L1" -len 1.8 
#~ Quadrupole "QUAD.QDDOG" -len 0.1 -K1L 0.15395378150000003 -refen $refen 
#~ Drift "DG.L1" -len 1.8 
#~ Sbend "BEND.BRB1B" -len 0.45 -angle -0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
#~ set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
#~ Sbend "BEND.BRB1A" -len 0.45 -angle -0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
#~ set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$refen*$refen*$refen*$refen*$Dipole_synrad] 
#~ Bpm "BPM.END.244" -len 0 
Marker "DBRC_END.EXTRACTION" 

