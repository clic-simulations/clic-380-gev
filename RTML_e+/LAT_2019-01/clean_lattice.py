old_lattice = open('0001_e+_rtml.tcl', 'r')
elements = old_lattice.readlines()
old_lattice.close()

for i in range(len(elements)):
    element = elements[i].split()
    if len(element) > 0:
        if element[0] == 'Multipole':
            element[4] = element[4].split(',')[0][1:]
    elements[i] = ' '.join(element) + '\n'

new_lattice = open('0001_e+_rtml.tcl', 'w')
for element in elements:
    new_lattice.write(element)
new_lattice.close()
