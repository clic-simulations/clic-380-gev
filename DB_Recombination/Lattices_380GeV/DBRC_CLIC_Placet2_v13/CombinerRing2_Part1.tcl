##------> Author: Raul Costa [raul.costa@cern.ch] <------------------##
## Placet2 lattice generated from Eduardo Marin's design for Placet1
## Sectors 1 and 5 extended to match ring length
## Injector deflectors are now RF
## Extraction Stripline added
## Later adapted to be defined by K1L/K2L instead of S1/S2
## Sextupole strength optimised for low emittance/T566
## Straight sector quadrupoles optimised to match arcs
## Center of arc cells optimised for periodicity
## Low energy version for 380GeV stage
set Pi [expr acos(-1)] 
if {$Dipole_synrad} { 
set refen 1.90487239 
} else { 
set refen 1.906 
} 
Marker "CR2_P1.INJECTION" 
Drift "CR2.D1M" -len 1.01 
Quadrupole "QUAD.CR2.QRFB3.IN2" -len 0.5 -K1L 0.12217763660487391 -refen $refen 
Drift "CR2.DRFB3" -len 3.771233433 
Quadrupole "QUAD.CR2.QRFB2.IN2" -len 0.5 -K1L 0.19663214464538725 -refen $refen 
Drift "CR2.DRFB2" -len 1.193240969 
Quadrupole "QUAD.CR2.QRFB1.IN2" -len 0.5 -K1L -0.16496467233449622 -refen $refen 
Drift "CR2.DRFB1" -len 0.217192186 
#### RF DEFLECTOR #### 
Rfmultipole "RFMulti.CR2.RFKICK2" -len 2 -strength [expr -0.004*$refen] -freq [expr 0.9995*3.] -refen $refen 
###################### 
Drift "CR2.DL3IN" -len 0.4000389758 
Quadrupole "QUAD.CR2.QL3IN.IN2" -len 0.5 -K1L 0.07808894574928223 -refen $refen 
Drift "CR2.DL2IN" -len 1.103409345 
Quadrupole "QUAD.CR2.QL2IN.IN2" -len 0.5 -K1L -0.6360699407020488 -refen $refen 
Drift "CR2.DL1IN" -len 0.4000469975 
Quadrupole "QUAD.CR2.QL1IN.IN2" -len 0.5 -K1L 1.1952772365386342 -refen $refen 
Drift "CR2.DL0IN" -len 0.5 
Quadrupole "QUAD.CR2.QL0IN.IN2" -len 0.5 -K1L -0.38500969083552583 -refen $refen 
Drift "CR2.DL0" -len 1.4491971274 
### Arc 1 Start ### 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.1" -len 0.1 -K2L 8.078678196050845 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.1" -len 0.1 -K2L 0.007562204221765205 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.1" -len 0.1 -K2L -1.485849212797018 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.1" -len 0.1 -K2L -1.260372516435647 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.1" -len 0.1 -K2L 0.007562204221765205 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.1" -len 0.1 -K2L -7.646745221779208 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183728 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183728 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.039925864400000004 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.1" -len 0.1 -K2L -7.646745221779208 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.1" -len 0.1 -K2L 0.007562204221765205 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.1" -len 0.1 -K2L -1.260372516435647 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.1" -len 0.1 -K2L -1.485849212797018 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.1" -len 0.1 -K2L 0.007562204221765205 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.1" -len 0.1 -K2L 8.078678196050845 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864928 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
#### Arc 1 End ####
Drift "CR2.DTR1A" -len 1.1 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.84671680555554 -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458445 -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Quadrupole "QUAD.CR2.QTR4" -len 0.5 -K1L 0.374011719219854 -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458445 -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.84671680555554 -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
Drift "CR2.DTR1A" -len 1.1 
### Arc 2 Start ### 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864928 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.2" -len 0.1 -K2L 7.387684999458668 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.2" -len 0.1 -K2L 0.6342199554138814 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.2" -len 0.1 -K2L -2.592665940344615 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.2" -len 0.1 -K2L -0.7762922573586234 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967743999999 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.2" -len 0.1 -K2L 0.6342199554138814 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.2" -len 0.1 -K2L -8.937779491669776 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.039925864400000004 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.2" -len 0.1 -K2L -8.937779491669776 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.2" -len 0.1 -K2L 0.6342199554138814 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.2" -len 0.1 -K2L -0.7762922573586234 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.2" -len 0.1 -K2L -2.592665940344615 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.2" -len 0.1 -K2L 0.6342199554138814 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.2" -len 0.1 -K2L 7.387684999458668 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
### Arc 2 End ### 
Drift "CR2.DS1C_A" -len 2.179474654 
Sextupole "SEXT.CR2.SXS1C" -len 0.1 -K2L 2.1606775839716965 -refen $refen 
Drift "CR2.DS1C_B" -len 0.3 
Quadrupole "QUAD.CR2.QS1C" -len 0.5 -K1L -0.647356904204382 -refen $refen 
Drift "CR2.DS2C" -len 0.2003907272 
Quadrupole "QUAD.CR2.QS2C" -len 0.5 -K1L 0.7905004870236609 -refen $refen 
Drift "CR2.DS3C_A" -len 0.3 
Sextupole "SEXT.CR2.SXS3C" -len 0.1 -K2L -0.42652765870388576 -refen $refen 
Drift "CR2.DS3C_B" -len 2.777418619 
Quadrupole "QUAD.CR2.QS3C" -len 0.5 -K1L -0.24686896328832697 -refen $refen 
Drift "CR2.DS3C_C" -len 0.5 
Quadrupole "QUAD.CR2.QS4C" -len 0.5 -K1L 0.24225644224167997 -refen $refen 
Drift "CR2.DS4C" -len 2.081432 
Quadrupole "QUAD.CR2.QS4C" -len 0.5 -K1L 0.24225644224167997 -refen $refen 
Drift "CR2.DS3C_C" -len 0.5 
Quadrupole "QUAD.CR2.QS3C" -len 0.5 -K1L -0.24686896328832697 -refen $refen 
Drift "CR2.DS3C_B" -len 2.777418619 
Sextupole "SEXT.CR2.SXS3C" -len 0.1 -K2L -0.42652765870388576 -refen $refen 
Drift "CR2.DS3C_A" -len 0.3 
Quadrupole "QUAD.CR2.QS2C" -len 0.5 -K1L 0.7905004870236609 -refen $refen 
Drift "CR2.DS2C" -len 0.2003907272 
Quadrupole "QUAD.CR2.QS1C" -len 0.5 -K1L -0.647356904204382 -refen $refen 
Drift "CR2.DS1C_B" -len 0.3 
Sextupole "SEXT.CR2.SXS1C" -len 0.1 -K2L 2.1606775839716965 -refen $refen 
Drift "CR2.DS1C_A" -len 2.179474654 
### Arc 3 Start ### 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.3" -len 0.1 -K2L 2.789394228828729 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.3" -len 0.1 -K2L 0.7336798378818274 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.3" -len 0.1 -K2L -2.530941523617107 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.3" -len 0.1 -K2L -0.4846971136107785 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.3" -len 0.1 -K2L 0.7336798378818274 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.3" -len 0.1 -K2L -1.160752349633134 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.08046148038247242 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.28064251000565993 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.08046148038247242 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.3" -len 0.1 -K2L -1.160752349633134 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.3" -len 0.1 -K2L 0.7336798378818274 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.3" -len 0.1 -K2L -0.4846971136107785 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.3" -len 0.1 -K2L -2.530941523617107 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.3" -len 0.1 -K2L 0.7336798378818274 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.3" -len 0.1 -K2L 2.789394228828729 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
### Arc 3 End ### 
Drift "CR2.DTR1A" -len 1.1 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.846716805555538 -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458447 -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Quadrupole "QUAD.CR2.QTR4" -len 0.5 -K1L 0.374011719219854 -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458447 -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.846716805555538 -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
Drift "CR2.DTR1A" -len 1.1 
### Arc 4 Start ### 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.4" -len 0.1 -K2L 6.724754158207781 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.4" -len 0.1 -K2L 0.6985947903817897 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.4" -len 0.1 -K2L -2.392402908254149 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.4" -len 0.1 -K2L -0.9890482592497188 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.4" -len 0.1 -K2L 0.6985947903817897 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.4" -len 0.1 -K2L -8.096111352597577 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.4" -len 0.1 -K2L -8.096111352597577 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.4" -len 0.1 -K2L 0.6985947903817897 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.4" -len 0.1 -K2L -0.9890482592497188 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.4" -len 0.1 -K2L -2.392402908254149 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.4" -len 0.1 -K2L 0.6985947903817897 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.4" -len 0.1 -K2L 6.724754158207781 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864919 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
### Arc 4 End ### 
Drift "CR2.DL0IN" -len 2.4491971274 
Quadrupole "QUAD.CR2.QL1IN.EXT" -len 0.5 -K1L -0.6970772319488282 -refen $refen 
Drift "CR2.DL1IN" -len 0.4000469975 
Quadrupole "QUAD.CR2.QL2IN.EXT" -len 0.5 -K1L 0.8526680541738719 -refen $refen 
Drift "CR2.DL2IN" -len 1.103409345 
Quadrupole "QUAD.CR2.QL3IN.EXT" -len 0.5 -K1L -0.24998982537871087 -refen $refen 
Drift "CR2.DL3IN" -len 0.4000389758 
#### STEP EXTRACTOR - kicks on 4*3*244 bunches at a time #### 
Stepdipole "CR2.EXTKICKER" -len 2. -strength [expr -0.004*$refen] -freq [expr 0.49975/(244*3*4)] -phase [expr 3./4.*$Pi] -duty [expr 1./4.] -refen $refen 
####-----------------------------------------------------####
Drift "CR2.DRFB1" -len 0.217192186 
Quadrupole "QUAD.CR2.QRFB1.EXT" -len 0.5 -K1L -1.079720421877582 -refen $refen 
Drift "CR2.DRFB2" -len 1.193240969 
Quadrupole "QUAD.CR2.QRFB2.EXT" -len 0.5 -K1L 0.5591986720709291 -refen $refen 
Drift "CR2.DRFB3" -len 3.771233433 
Quadrupole "QUAD.CR2.QRFB3.EXT" -len 0.5 -K1L -0.22580823301932673 -refen $refen 
Drift "CR2.SEPTUM" -len 7.486757641 
Marker "CR2_P1.EXTRACTION" 
