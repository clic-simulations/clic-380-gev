#
# This code was developed by C. Gohil from code inherited from E. Marin.
#
# Implements short-term ground motion in the 380 GeV design of CLIC.
#

#
# Ground motion settings
#
array set groundmotion {}

# x:
# - 0: don't apply in x-direction
# - 1: apply in x-direction
# y:
# - 0: don't apply in y-direction
# - 1: apply in y-direction
# delta_t:
# - time step in seconds
# t_start:
# -start time after perfect beamline in seconds
# filtertype:
#  - 0: no filter,
#  - 1: use filters specified in x and y for the whole beamline
# preisolator:
#  - 0: no preisolator,
#  - 1: with F. Ramos et al. preisolator including tilt motion
set groundmotion(x)       1
set groundmotion(y)       1
set groundmotion(delta_t) 0.02
set groundmotion(t_start) 0
set groundmotion(model)   $model

if {"$mitigation" == "no-filter"} {
    puts "No mitigation for ground motion"
    set groundmotion(filtertype)   0
    set groundmotion(filtertype_x) ""
    set groundmotion(filtertype_y) ""
    set groundmotion(preisolator)  0
}
if {"$mitigation" == "preisolator"} {
    puts "Using preisolator"
    set groundmotion(filtertype)   0
    set groundmotion(filtertype_x) ""
    set groundmotion(filtertype_y) ""
    set groundmotion(preisolator)  1
}
if {"$mitigation" == "quad-stab"} {
    puts "Using quadrupole stabilisation"
    set groundmotion(filtertype)   1
    set groundmotion(filtertype_x) "quad_stab.dat"
    set groundmotion(filtertype_y) "quad_stab.dat"
    set groundmotion(preisolator)  0
}
if {"$mitigation" == "quad-stab-preisolator"} {
    puts "Using quadrupole stabilisation and preisolator"
    set groundmotion(filtertype)   1
    set groundmotion(filtertype_x) "quad_stab.dat"
    set groundmotion(filtertype_y) "quad_stab.dat"
    set groundmotion(preisolator)  1
}
if {"$mitigation" == "recursive"} {
    puts "Using beam-based feedback system"
    set groundmotion(filtertype)   1
    set groundmotion(filtertype_x) "recursive.dat"
    set groundmotion(filtertype_y) "recursive.dat"
    set groundmotion(preisolator)  0
}
if {"$mitigation" == "recursive-preisolator"} {
    puts "Using beam-based feedback system and preisolator"
    set groundmotion(filtertype)   1
    set groundmotion(filtertype_x) "recursive.dat"
    set groundmotion(filtertype_y) "recursive.dat"
    set groundmotion(preisolator)  1
}
if {"$mitigation" == "recursive-quad-stab"} {
    puts "Using beam-based feedback system and quadrupole stabilisation"
    set groundmotion(filtertype)   1
    set groundmotion(filtertype_x) "rec_quad_stab.dat"
    set groundmotion(filtertype_y) "rec_quad_stab.dat"
    set groundmotion(preisolator)  0
}
if {"$mitigation" == "recursive-quad-stab-preisolator"} {
    puts "Using beam-based feedback system, quadrupole stabiliastion and preisolator"
    set groundmotion(filtertype)   1
    set groundmotion(filtertype_x) "rec_quad_stab.dat"
    set groundmotion(filtertype_y) "rec_quad_stab.dat"
    set groundmotion(preisolator)  1
}

# Move ground with respect to IP
set groundmotion(ip0) 1

# Move cavities with stabilisation
set groundmotion(cav_stab) 0

#
# Initialiser for ground motion generator
#
proc ground_motion_init {beamlinename model sign s_start} {
    global gmdir groundmotion mitigation bds_type

    BeamlineUse -name $beamlinename
    puts "Initialising ground motion generator in $beamlinename"

    GroundMotionInit -file $gmdir/models/harm.$model.300 -x $groundmotion(x) \
        -y $groundmotion(y) -t_step $groundmotion(delta_t) -s_sign $sign -s_abs 0 \
        -last_start 1 -s_start $s_start -t_start $groundmotion(t_start)

    if {$groundmotion(filtertype)} {
        AddGMFilter -file $gmdir/models/harm.$model.300 \
            -filter_x $gmdir/transfer_functions/$groundmotion(filtertype_x) \
            -filter_y $gmdir/transfer_functions/$groundmotion(filtertype_y)
    }

    # special stabilisation for final focus, overwrites other filter:
    if {"$beamlinename" == "bds"} {
        if {($groundmotion(preisolator))} {
            # filter from stabilisation groups, (passive, from F. Ramos et al.)
            if {$bds_type == 43} {
                set pos_QD0 5.97408
                set pos_QF1 14.734844
            } elseif {$bds_type == 6} {
                set pos_QD0 8.363712
                set pos_QF1 20.5620616
            }
            if {"$mitigation" == "recursive-preisolator" \
                || "$mitigation" == "recursive-quad-stab-preisolator"} {
                # Preisolator and beam-based feedback
                AddPreIsolator -file_mnt1QD0 $gmdir/transfer_functions/preisolator_recursive_mount1_QD0.dat -file_mnt2QD0 $gmdir/transfer_functions/preisolator_recursive_mount2_QD0.dat -file_mnt1QF1 $gmdir/transfer_functions/preisolator_recursive_mount1_QF1.dat -file_mnt2QF1 $gmdir/transfer_functions/preisolator_recursive_mount2_QF1.dat -file_mnt1QD0_x $gmdir/transfer_functions/preisolator_recursive_mount1_QD0_x.dat -file_mnt2QD0_x $gmdir/transfer_functions/preisolator_recursive_mount2_QD0_x.dat -file_mnt1QF1_x $gmdir/transfer_functions/preisolator_recursive_mount1_QF1_x.dat -file_mnt2QF1_x $gmdir/transfer_functions/preisolator_recursive_mount2_QF1_x.dat -pos_mnt1 8.306 -pos_mnt2 10.946 -pos_QD0 $pos_QD0 -pos_QF1 $pos_QF1
            } else {
                # Preisolator only
                AddPreIsolator -file_mnt1QD0 $gmdir/transfer_functions/preisolator_mount1_QD0.dat -file_mnt2QD0 $gmdir/transfer_functions/preisolator_mount2_QD0.dat -file_mnt1QF1 $gmdir/transfer_functions/preisolator_mount1_QF1.dat -file_mnt2QF1 $gmdir/transfer_functions/preisolator_mount2_QF1.dat -file_mnt1QD0_x $gmdir/transfer_functions/preisolator_mount1_QD0_x.dat -file_mnt2QD0_x $gmdir/transfer_functions/preisolator_mount2_QD0_x.dat -file_mnt1QF1_x $gmdir/transfer_functions/preisolator_mount1_QF1_x.dat -file_mnt2QF1_x $gmdir/transfer_functions/preisolator_mount2_QF1_x.dat -pos_mnt1 8.306 -pos_mnt2 10.946 -pos_QD0 $pos_QD0 -pos_QF1 $pos_QF1
            }
        }
    }
}

#
# Main ground motion implentation
#
proc ground_motion {beamlinename} {
    # Applies ground motion to a beamline by evolving the offset of each girder
    # by one time step.
    global groundmotion
    BeamlineUse -name $beamlinename
    GroundMotion -cav_stab $groundmotion(cav_stab) -ip0 $groundmotion(ip0)
}

proc set_rtml_ground_motion {gm_seed gm_iter particle} {
    # Setups ground motion in the RTML
    global groundmotion sec_len
    RandomReset -seed $gm_seed -stream Groundmotion
    if {$particle == "e-"} {
        ground_motion_init rtml $groundmotion(model) 1 \
                           [expr -$sec_len(bds)-$sec_len(ml)]
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion rtml
        }
    } elseif {$particle == "e+"} {
        ground_motion_init rtml $groundmotion(model) -1 \
                           [expr $sec_len(bds)+$sec_len(ml)]
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion rtml
        }
    }
}

proc set_ml_ground_motion {gm_seed gm_iter particle} {
    # Setups ground motion in the ML
    global groundmotion sec_len
    RandomReset -seed $gm_seed -stream Groundmotion
    if {$particle == "e-"} {
        ground_motion_init ml $groundmotion(model) 1 -$sec_len(bds)
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion ml
        }
    } elseif {$particle == "e+"} {
        ground_motion_init ml $groundmotion(model) -1 $sec_len(bds)
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion ml
        }  
    }
}

proc set_bds_ground_motion {gm_seed gm_iter particle} {
    # Setups ground motion in the BDS
    global groundmotion sec_len
    RandomReset -seed $gm_seed -stream Groundmotion
    if {$particle == "e-"} {
        ground_motion_init bds $groundmotion(model) 1 0
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion bds
        }
    } elseif {$particle == "e+"} {
        ground_motion_init bds $groundmotion(model) -1 0
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion bds
        }
    }
}

proc write_quad_offsets {particle} {
    # Saves the offset of every quadrupole in e- and e+ beamlines
    global gmdir sec_len
    if {$particle == "e-"} {
        set offsets_file [open $gmdir/quad_offsets.dat w]
        BeamlineUse -name rtml
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr -$sec_len(rtml_e-)-$sec_len(ml)-$sec_len(bds) + \
              [ElementGetAttribute $i -s]] [ElementGetOffset $i]"
        }
        BeamlineUse -name ml
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr -$sec_len(ml)-$sec_len(bds)+[ElementGetAttribute $i -s]] \
             [ElementGetOffset $i]"
        }
        BeamlineUse -name bds
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr -$sec_len(bds)+[ElementGetAttribute $i -s]] \
             [ElementGetOffset $i]"
        }
        close $offsets_file
    } elseif {$particle == "e+"} {
        set offsets_file [open $gmdir/quad_offsets.dat a]
        BeamlineUse -name rtml
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr $sec_len(rtml_e+)+$sec_len(ml)+$sec_len(bds) - \
              [ElementGetAttribute $i -s]] [ElementGetOffset $i]"
        }
        BeamlineUse -name ml
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr $sec_len(ml)+$sec_len(bds)-[ElementGetAttribute $i -s]] \
             [ElementGetOffset $i]"
        }
        BeamlineUse -name bds
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr $sec_len(bds)-[ElementGetAttribute $i -s]] \
             [ElementGetOffset $i]"
        }
        close $offsets_file
    }
}
