function disp_results(B)
    E = placet_get_emittance(B);
    ex = E(1)
    ey = E(2)
    twiss = placet_get_twiss_matrix(B);
    bx = twiss(1,1)
    by = twiss(3,3)
    ax = -twiss(1,2)
    ay = -twiss(3,4)
    x = mean(B(:,2))
    y = mean(B(:,3))
    xp = mean(B(:,5))
    yp = mean(B(:,6))
    E = mean(B(:,1))
    dE = std(B(:,1))
    z = mean(B(:,4))
    sz = std(B(:,4))
    disp("");
endfunction
