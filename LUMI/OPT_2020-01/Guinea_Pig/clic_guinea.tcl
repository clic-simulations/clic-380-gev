#
# Define some parameters for GUINEA-PIG
#
set e_final 190.0

array set gp_param "
    energy $e_final
    particles [expr 5.2e9*1e-10]
    sigmaz 70.0
    cut_x 10.0
    cut_y 1.0
    n_x 64
    n_y 320
    do_coherent 0
    do_trident 0
    do_muons 0
    do_pairs 0
    track_pairs 0
    do_compt 0
    do_hadrons 0
    do_photons 0
    do_isr 0
    do_espread 0
    n_t 1
    charge_sign -1.0
    ecm_min [expr 2.0*$e_final*0.99]"

#
# Set the minimum energy for lumi_high to 99% of the nominal centre-of-mass
# energy
#

set gp_param(ecm_min) [expr 2.0*$gp_param(energy)*0.99]

proc write_guinea_files {offsetx offsety roffsety angley waisty} {
    global n_slice gp_param n_total
    set f [open acc.dat w]

    set offsety1 [expr $offsety + 0.5*$roffsety]
    set offsety2 [expr $offsety - 0.5*$roffsety]
    set angley [expr 1e-6*$angley]

    puts $f "\$ACCELERATOR:: clic_380"
    puts $f "\{energy=$gp_param(energy);particles=$gp_param(particles);"
    puts $f "beta_x=8.0;beta_y=0.100;emitt_x=0.95;emitt_y=0.003;"
    puts $f "sigma_z=$gp_param(sigmaz);espread=0.035;dist_z=0;f_rep=50.0;n_b=352;"
    puts $f "offset_y.1=$offsety1;offset_y.2=$offsety2;"
    puts $f "offset_x.1=$offsetx;offset_x.2=$offsetx;"
    puts $f "waist_y=$waisty;angle_y=$angley;\}"

    puts $f "\$PARAMETERS:: default_simple"
    puts $f "\{n_x=$gp_param(n_x);n_y=$gp_param(n_y);n_z=$n_slice;"
    puts $f "n_t=$gp_param(n_t);n_m=$n_total;cut_x=$gp_param(cut_x);"
    puts $f "cut_y=$gp_param(cut_y);cut_z=3.0*sigma_z.1;"
    puts $f "force_symmetric=0;electron_ratio=1.0;do_photons=1;do_isr=0;do_lumi=0;beam_size=1;"
    puts $f "ecm_min=$gp_param(ecm_min);photon_ratio=1.0;do_muons=0;"
    puts $f "do_coherent=1;grids=0;rndm_load=0;do_espread=0.0;"
    puts $f "do_pairs=0;track_pairs=0;store_beam=0;do_compt=0;photon_ratio=0.2;load_beam=3;"
    puts $f "do_hadrons=0;store_hadrons=0;do_jets=0;store_jets=0;store_photons=0;"
    puts $f "hist_ee_bins=1010;hist_ee_max=2.01*energy.1;charge_sign=$gp_param(charge_sign);"
    puts $f "do_prod=0; prod_e=0.0; prod_scal=0.0; do_cross=0;do_eloss=1;ext_field=0;\}"
    close $f
}

proc run_guinea {roffset angle waist} {
    global gp_param
    set res [exec grid]
    set yoff [expr -0.5*([lindex $res 2]+[lindex $res 3])]
    set xoff [expr -0.5*([lindex $res 0]+[lindex $res 1])]
    set tx $gp_param(cut_x)
    set ty $gp_param(cut_y)
    if {[lindex $res 1]-[lindex $res 0]>2.0*$tx} {
        set gp_param(cut_x) [expr 0.5*([lindex $res 1]-[lindex $res 0])]
       }
    if {[lindex $res 3]-[lindex $res 2]>2.0*$ty} {
        set gp_param(cut_y) [expr 0.5*([lindex $res 3]-[lindex $res 2])]
       }
    set offset [open "offset.dat" w]
    puts $offset " $xoff  $yoff"
    close $offset

    write_guinea_files $xoff $yoff $roffset $angle $waist
    exec guinea-old clic_380 default_simple result.out
}

proc get_lumi {} {
    set l [exec grep lumi_fine result.out]
    set i1 [expr [string last "=" $l]+1]
    set i2 [expr [string last ";" $l]-1]
    return [string range $l $i1 $i2]
}
