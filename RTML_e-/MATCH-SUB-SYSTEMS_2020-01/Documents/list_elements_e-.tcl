# CLIC Main Beam RTML

##### If No wakefield file, run the create_wake.tcl script
#source create_wake.tcl
######

set tcl_precision 15
ParallelThreads -num 4

proc FF1 {} {}
proc FF2 {} {}

set latticedir ./Lattices_e-
set scriptdir ./Scripts
set paramsdir ./Parameters

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl

if {$beamparams(usewakefields)==1} {
   source rf_parameters_ww.tcl
}

create_particles_file particles.in beamparams
create_zero_wakes_file zero_wake.dat beamparams rfparamsbc1 particles.in

BeamlineNew
source $scriptdir/beamline_and_wake.tcl
BeamlineSet -name rtml

Octave {
    BENDI = length(placet_get_number_list("rtml", "sbend"))
    QI = length(placet_get_number_list("rtml", "quadrupole"))
    CI = length(placet_get_number_list("rtml", "dipole"))
    BI = length(placet_get_number_list("rtml", "bpm"))
    AI = length(placet_get_number_list("rtml", "cavity"))
    SI = length(placet_get_number_list("rtml", "sextupole"))
    OI = length(placet_get_number_list("rtml", "solenoid"))    
}

Octave {

    global COLS_NAME % preceeded by "NAME" and "KEYWORD" and followed by "Type"
    COLS_NAME = { "S" ; "L" ; "E0" ; "K1L" ; "K1SL" ; "K2L" ; "K2SL" ; "KS" ; "ANGLE" ; "E1" ; "E2" ; "Strength" };
    ALL = placet_element_get_attributes("rtml");
    TABLE_NAME = [];
    TABLE_KEY  = [];
    TABLE_DATA = [];
    TABLE_TYPE = [];
    function T = append_to_row(T, name, value)
        global COLS_NAME
        T(find(strcmp(COLS_NAME, name))) = value;
    end

    %% element by element
    sigma_quad = 30; % um, rms misalignment of quadrupoles
    last_quad_K1L = 0.0; % 1/m
    
    index = 1;    
    for i=1:size(ALL,1);
        THIS = ALL{i};
        T = zeros(1, size(COLS_NAME,1));
        T = append_to_row(T, "S", THIS.s);
        T = append_to_row(T, "L", THIS.length);
        T = append_to_row(T, "E0", THIS.e0);
        switch THIS.type_name
         case "quadrupole"
            strength = abs(THIS.strength / 0.299792458);
            T = append_to_row(T, "K1L", THIS.strength / THIS.e0);
            T = append_to_row(T, "Strength", strength); % GV/c/m = (1 / 0.299792458) T
            last_quad_K1L = THIS.strength / THIS.e0; % 1/m
            %if THIS.length == 0.3
              if     strength <= 0.57 ; THIS.comment = "Q5" ;
              elseif strength <= 1.2  ; THIS.comment = "Q4T1" ;
              elseif strength <= 2.43 ; THIS.comment = "Q4T2" ;
              elseif strength <= 3.6  ; THIS.comment = "Q4T3" ;
              elseif strength <= 5.5  ; THIS.comment = "Q3T1" ;
              elseif strength <= 7.8  ; THIS.comment = "Q3T2" ;
              elseif strength <= 11.1 ; THIS.comment = "Q3T3" ;
              elseif strength <= 15   ; THIS.comment = "Q2" ;
              elseif strength <= 21   ; THIS.comment = "Q1" ;
            endif
           %endif
         case "sbend"
            T = append_to_row(T, "ANGLE", THIS.angle);
            T = append_to_row(T, "E1", THIS.E1);
            T = append_to_row(T, "E2", THIS.E2);
            T = append_to_row(T, "Strength", THIS.e0 * THIS.angle / 0.299792458); % GV/c = (1 / 0.299792458) T*m
         case "multipole"
            THIS.type_name = "sextupole";
            strength = abs(THIS.strength / 0.299792458);
            T = append_to_row(T, "K2L", THIS.strength / THIS.e0);
            T = append_to_row(T, "Strength", strength); % GV/c/m^2 = (1 / 0.299792458) T/m
            if     strength <= 300 ; THIS.comment = "SX2" ;
            elseif strength <= 600 ; THIS.comment = "SX1" ;
            endif
          case "bpm"
          case "dipole"
            THIS.type_name = "kicker";
            max_strength = 3*sigma_quad*abs(last_quad_K1L)*THIS.e0*1e3; % V, 3 * sigma_quad * last_quad_K1L * THIS.e0
            T = append_to_row(T, "Strength", max_strength / 299792458); % T*m
          case "cavity"
            T = append_to_row(T, "Strength", 1e3 * THIS.gradient * THIS.length); % MV
            T = append_to_row(T, "ANGLE", THIS.phase / 180.0 * pi); % rad
          case "solenoid"
            T = append_to_row(T, "KS", 0.29979246 * THIS.bz); % solenoid strength from T/(GeV/c) to 1/m
            T = append_to_row(T, "Strength", 2.9); % T
          case "drift"
            if strfind(THIS.name, "MARKER")
              THIS.type_name = "marker";
            endif
        end
        
        %% for all types
        switch THIS.type_name
          case { "quadrupole" , "sbend", "sextupole", "solenoid", "bpm" , "kicker" , "cavity", "marker" }
            TABLE_DATA(index,:) = T;
            TABLE_NAME{index} = THIS.name;
            TABLE_KEY {index} = toupper(THIS.type_name);
            TABLE_TYPE{index} = THIS.comment;
            index++;
        end
    end
    
    %% save on disk
    fid = fopen("elements_table_e-.txt", 'w');
    if fid != -1
        fprintf(fid, '* NAME\tKEYWORD\t%s\n', strjoin (COLS_NAME, '\t'));
        fprintf(fid, '* <STR> <STR> [m] [m] [GeV] [m^-1] [m^-1] [m^-2] [m^-2] [m^-1] [rad] [rad] [rad] <STR>\n');
        for i=1:size(TABLE_DATA,1)
            fprintf(fid, [ '\"%s\"\t\"%s\"\t%.3f\t' repmat('%g\t', 1, size(TABLE_DATA,2)-1) '%s\n' ], TABLE_NAME{i}, TABLE_KEY{i}, TABLE_DATA(i,:), TABLE_TYPE{i});
        end
        fclose(fid);
    end
}

BeamlineSaveFootprint -beamline rtml -file rtml_e-_footprint.dat
