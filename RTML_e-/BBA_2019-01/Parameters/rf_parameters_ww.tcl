set rfparamsbooster(gradientww)   1.489872660091e+07
set rfparamsbc1(gradientww)   1.346452208100e+07
set rfparamsbc2(gradientww)   7.189388555346e+07
set energy_wake_bc1  -3.529357760428e-05
set energy_wake_booster  -5.768514043160e-05
set energy_wake_bc2  -7.427476871013e-05
