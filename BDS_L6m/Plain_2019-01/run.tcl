#
# CLIC 380 GeV: Main e- Beam (BDS - L*=6 m)
#
# This code was developed by C. Gohil from a simulation inherited from F. Plassard.
#
# To run "placet run.tcl <beam_seed>", if no beam seed if given, the default is 0.
#
set scriptdir  ./Scripts
set latticedir ./Lattices
set paramsdir  ./Parameters
set wakedir    ./Wakefields
set beamdir    ./Beams
set resultsdir ./Results
set gpdir      ./Guinea_Pig

# Initialise a timer
Octave {initial = tic();}

# Set the number of cores to run in parallel
ParallelThreads -num 4

# Load parameters
source $scriptdir/set_parameters.tcl

# Set up the lattice
source $scriptdir/beamline_and_wake.tcl

# Setup the beam
source $scriptdir/clic_beam.tcl
make_beam_many beam0 $n_slice $n

# Tracking
source $scriptdir/octave_functions.tcl
Octave {	
    [E, B] = placet_test_no_correction("bds", "beam0", "None");
    disp_results(B);
    save -text $resultsdir/emitt.dat E;
    save_beam("$beamdir/particles.out", B);
}

puts "Running GUINEA-PIG"
# Setup electron and positron bunches
exec cp $beamdir/particles.out $gpdir/electron.ini
exec cp $beamdir/particles.out $gpdir/positron.ini

# Calcualte luminosity
cd $gpdir
source clic_guinea.tcl
run_guinea 0.0 0.0

# Display how long the simulation took
Octave {toc(initial);}

exit
