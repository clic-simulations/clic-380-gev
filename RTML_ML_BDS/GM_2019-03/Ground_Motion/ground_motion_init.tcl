proc ground_motion_init {beamlinename type model sign s_start} {
    global gmdir delta_T_short ground_motion_x ground_motion_y groundmotion use_main_linac use_bds bds_type mitigation
    
    if {$type==2} {
        puts -nonewline "Init ground motion generator in "
        puts $beamlinename
        BeamlineUse -name $beamlinename

        GroundMotionInit -file $gmdir/models/harm.$model.300 -x $ground_motion_x -y $ground_motion_y -t_step $delta_T_short -s_sign $sign -s_abs 0 -last_start 1 -s_start $s_start -t_start $groundmotion(t_start)
        # stabilisation for the machine
        if {$groundmotion(filtertype)} {
            global use_quad_stab_failure
            if {$use_quad_stab_failure} {
                global quad_stab_failure
                # global nr_girders
                set quad_list [QuadrupoleNumberList]
                if {[string compare $quad_stab_failure(distribution) "random"]==0 } {
                    AddGMFilter -file $gmdir/models/harm.$model.300 -filter_x $gmdir/transfer_functions/$groundmotion(filtertype_x) -filter_y $gmdir/transfer_functions/$groundmotion(filtertype_y)
                    # put no filter one by one.
                    foreach i $quad_list {
                        set broken [expr rand() + $quad_stab_failure(prob)]
                        # if broken (>1 then no filter is added)
                        if {$broken > 1} {
                            AddGMFilter -file $gmdir/models/harm.$model.300 -filter_x $gmdir/transfer_functions/no_filter.dat -filter_y $gmdir/transfer_functions/no_filter.dat -start_element $i -end_element $i
                        }
                    }
                } elseif {[string compare $quad_stab_failure(distribution) "list"]==0 } {
                    AddGMFilter -file $gmdir/models/harm.$model.300 -filter_x $gmdir/transfer_functions/$groundmotion(filtertype_x) -filter_y $gmdir/transfer_functions/$groundmotion(filtertype_y)
                    # put no filter one by one.
                    foreach i $quad_stab_failure(list) {
                        set element_nr [lindex $quad_list $i] 
                        AddGMFilter -file $gmdir/models/harm.$model.300 -filter_x $gmdir/transfer_functions/no_filter.dat -filter_y $gmdir/transfer_functions/no_filter.dat -start_element $element_nr -end_element $element_nr
                    }
                } else {
                    # failure in block mode, decide start and end point of filter
                    set start_element 0
                    set end_element 10000000
                    # calculate nr of broken quads, todo: protect against 0!
                    set nr_quad_stab_failures [expr round($quad_stab_failure(prob)*[llength $quad_list])]
                    puts "nr of quadrupole stabilisation failures: $nr_quad_stab_failures"
                    if {[string compare $quad_stab_failure(distribution) "block_begin"]==0 } {
                        set start_element [lindex $quad_list [expr $nr_quad_stab_failures + 1]]
                    } elseif {[string compare $quad_stab_failure(distribution) "block_end"]==0 } {
                        set end_element [lindex $quad_list [expr [llength $quad_list] - $nr_quad_stab_failures]]
                    } elseif {[string compare $quad_stab_failure(distribution) "block_end_ml"]==0 } {
                        set quad_begin_bds 1
                        if {$use_main_linac} {
                            set nr_quads_ml 2010
                            set quad_begin_bds [lindex $quad_list [expr $nr_quads_ml+1]]
                            # add BDS filter
                            AddGMFilter -file $gmdir/models/harm.$model.300 -filter_x $gmdir/transfer_functions/$groundmotion(filtertype_x) -filter_y $gmdir/transfer_functions/$groundmotion(filtertype_y) -start_element $quad_begin_bds
                    }
                    set end_element [lindex $quad_list [expr $nr_quads_ml - $nr_quad_stab_failures]]
                    } elseif {[string compare $quad_stab_failure(distribution) "block_begin_bds"]==0 } {
                        # get element nr of end of ML
                        set quad_end_ml 0
                        set nr_quads_ml 0
                        if {$use_main_linac} {
                            set nr_quads_ml 2010
                            set quad_end_ml [lindex $quad_list $nr_quads_ml]
                            # add ML filter
                            AddGMFilter -file $gmdir/models/harm.$model.300 -filter_x $gmdir/transfer_functions/$groundmotion(filtertype_x) -filter_y $gmdir/transfer_functions/$groundmotion(filtertype_y) -end_element $quad_end_ml
                        }
                        set start_element [lindex $quad_list [expr $nr_quad_stab_failures + $nr_quads_ml]]
                    } else {
                        puts "Quad stabilisation failure mode $quad_stab_failure(distribution) is not known!"
                        exit
                    }
                    AddGMFilter -file $gmdir/models/harm.$model.300 -filter_x $gmdir/transfer_functions/$groundmotion(filtertype_x) -filter_y $gmdir/transfer_functions/$groundmotion(filtertype_y) -start_element $start_element -end_element $end_element
                }
            } else {
                # add all filters at once
                AddGMFilter -file $gmdir/models/harm.$model.300 -filter_x $gmdir/transfer_functions/$groundmotion(filtertype_x) -filter_y $gmdir/transfer_functions/$groundmotion(filtertype_y)
            }
        }

        # special stabilisation for final focus, overwrites other filter:
        if {($groundmotion(preisolator)==1) && $use_bds!=0} {
            # pre-isolator (passive, from F. Ramos et al.) , add x direction
            AddGMFilter -file $gmdir/models/harm.$model.300 -filter $gmdir/transfer_functions/filter_preisolatorFF.dat
        } elseif {($groundmotion(preisolator)==2) && $use_bds!=0} {
            # mechanical feedback (active, from B. Caron et al.)
            AddGMFilter -file $gmdir/models/harm.$model.300 -filter $gmdir/transfer_functions/filter_mechanical_fb_FF.dat
        } elseif {($groundmotion(preisolator)==3) && $use_bds!=0} {
            # filter from stabilisation groups
            # pre-isolator (passive, from F. Ramos et al.)
            if {$bds_type == 43} {
                set pos_QD0 5.97408
                set pos_QF1 14.734844
            } elseif {$bds_type == 6} {
                set pos_QD0 8.363712
                set pos_QF1 20.5620616
            }
            if {"$mitigation" == "recursive-preisolator-3" || "$mitigation" == "recursive-quad-stab-preisolator-3"} {
                # Preisolator and beam-based feedback
                AddPreIsolator -file_mnt1QD0 $gmdir/transfer_functions/preisolator_recursive_mount1_QD0.dat -file_mnt2QD0 $gmdir/transfer_functions/preisolator_recursive_mount2_QD0.dat -file_mnt1QF1 $gmdir/transfer_functions/preisolator_recursive_mount1_QF1.dat -file_mnt2QF1 $gmdir/transfer_functions/preisolator_recursive_mount2_QF1.dat -file_mnt1QD0_x $gmdir/transfer_functions/preisolator_recursive_mount1_QD0_x.dat -file_mnt2QD0_x $gmdir/transfer_functions/preisolator_recursive_mount2_QD0_x.dat -file_mnt1QF1_x $gmdir/transfer_functions/preisolator_recursive_mount1_QF1_x.dat -file_mnt2QF1_x $gmdir/transfer_functions/preisolator_recursive_mount2_QF1_x.dat -pos_mnt1 8.306 -pos_mnt2 10.946 -pos_QD0 $pos_QD0 -pos_QF1 $pos_QF1
            } else {
                # Preisolator only
                AddPreIsolator -file_mnt1QD0 $gmdir/transfer_functions/preisolator_mount1_QD0.dat -file_mnt2QD0 $gmdir/transfer_functions/preisolator_mount2_QD0.dat -file_mnt1QF1 $gmdir/transfer_functions/preisolator_mount1_QF1.dat -file_mnt2QF1 $gmdir/transfer_functions/preisolator_mount2_QF1.dat -file_mnt1QD0_x $gmdir/transfer_functions/preisolator_mount1_QD0_x.dat -file_mnt2QD0_x $gmdir/transfer_functions/preisolator_mount2_QD0_x.dat -file_mnt1QF1_x $gmdir/transfer_functions/preisolator_mount1_QF1_x.dat -file_mnt2QF1_x $gmdir/transfer_functions/preisolator_mount2_QF1_x.dat -pos_mnt1 8.306 -pos_mnt2 10.946 -pos_QD0 $pos_QD0 -pos_QF1 $pos_QF1
            }
        }
    } elseif {$type==1} {
        # ATL motion
        puts -nonewline "Init ATL motion in "
        puts $beamlinename
        ground_motion_long $beamlinename $groundmotion(t_start) $ground_motion_x $ground_motion_y
    } elseif {$type==0} {
        puts -nonewline "No ground motion applied "
        puts $beamlinename
    } else {
        puts -nonewline "Unknown ground motion type "
        puts $beamlinename
        exit
    }
}
