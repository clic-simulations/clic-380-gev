#
#  Mar 30, 2020, xingguang.liu@cern.ch
# modification: 
# 1) set a flag for sextuple tuning
# 2) add timer for each section

# following on old comment

# This script was developed by C. Gohil from files developed by Y. Han.
#
# This is the main script which performs Beam-Based Alignment on the CLIC 380 GeV RTML.
#
# Can be executed with: placet run.tcl <machine>, where <machine> is the id number for
# a machine.
#

# Get the machine number from command line arguments
set machine [lindex $argv 0]

Octave {
    global machine = $machine;

    # update the wake files in case of the RF changes:
    # system("placet Wakefields/create_wake.tcl");

    sextuple_tuning_flag=1;
    timer=tic();

    disp("Performing BBA on machine $machine\n");

    disp("Setting imperfections");
    disp("*****************************************");
    system("placet -s Main/set_imperfections.tcl machine $machine");

    disp("121 and DFS: SR to CA");
    disp("*****************************************");
    system("placet -s Main/bba_sr_ca.tcl machine $machine");

    disp("Time spent from SR to CA 121 and DFS:");
    toc(timer);
    disp("total time:");toc(timer);
    timerNew=tic();

    if (sextuple_tuning_flag == 1)
        # move x and y 
        disp("Sextupole tuning: CA");
        disp("*****************************************");
        system("placet -s Scripts/sextupole_tuning_ca.tcl machine $machine");
        
        # move x 
        disp("Sextupole tuning x: CA");
        disp("*****************************************");
        system("placet -s Scripts/sextupole_tuning_ca_LTL_x.tcl machine $machine");
        
        # move y 
        disp("Sextupole tuning y: CA");
        disp("*****************************************");
        system("placet -s Scripts/sextupole_tuning_ca_LTL_y.tcl machine $machine");

        disp("Time spent for CA sextupole tuning:");toc(timerNew);
        disp("total time:");toc(timer);
        timerNew=tic();
    endif

    disp("121 and DFS: CA to TAL1");
    disp("*****************************************");
    system("placet -s Main/bba_ca_tal1.tcl machine $machine");

    disp("Time spent for CA to TAL1 121 and DFS:");toc(timerNew);
    disp("total time:");toc(timer);
    timerNew=tic();

    if (sextuple_tuning_flag == 1)
        disp("Sextupole tuning: TAL1");
        disp("*****************************************");
        system(" placet -s Scripts/sextupole_tuning_tal1.tcl machine $machine");

        disp("Sextupole tuning x: TAL1-TAL2");
        disp("*****************************************");
        system("placet -s Scripts/sextupole_tuning_tal1_tal2_x.tcl machine $machine");
        
        disp("Sextupole tuning y: TAL1-TAL2");
        disp("*****************************************");
        system("placet -s Scripts/sextupole_tuning_tal1_tal2_y.tcl machine $machine");

        disp("Time spent for TAL1 sextupole tuning:");toc(timerNew);;
        disp("total time:");toc(timer);
        timerNew=tic();
    endif

    disp("121 and DFS: TAL2");
    disp("*****************************************");
    system("placet -s Main/bba_tal2.tcl machine $machine");

    disp("Time spent for TAL2 121 and DFS:");toc(timerNew);;
    disp("total time:");toc(timer);
    timerNew=tic();

    if (sextuple_tuning_flag == 1)
        disp("Sextupole tuning: TAL2");
        disp("*****************************************");
        system(" placet -s Scripts/sextupole_tuning_tal2.tcl machine $machine");

        disp("Time spent for TAL2 sextupole tuning:");toc(timerNew);;
        disp("total time:");toc(timer);
        timerNew=tic();
    endif

    disp("121 and DFS: BC2");
    disp("*****************************************");
    system("placet -s Main/bba_bc2.tcl machine $machine");
    disp("Time spent for BC2 121 and DFS:");toc(timerNew);
    disp("total time:");toc(timer);

    if (sextuple_tuning_flag == 1)
        disp("Sextupole tuning x: TAL2-BC2");
        disp("*****************************************");
        system("placet -s Scripts/sextupole_tuning_tal2_bc2.tcl machine $machine");

        disp("Sextupole tuning x: TAL2-BC2");
        disp("*****************************************");
        system("placet -s Scripts/sextupole_tuning_tal2_bc2_x.tcl machine $machine");
                    
        disp("Sextupole tuning y: TAL2-BC2");
        disp("*****************************************");
        system("placet -s Scripts/sextupole_tuning_tal2_bc2_y.tcl machine $machine");

        disp("Time spent for TAL2-BC2 sextupole tuning:");toc(timerNew);;
        disp("total time:");toc(timer);
        timerNew=tic();
    endif

    disp("total time:");toc(timer);

    system("placet -s Main/save_lattice.tcl machine $machine");
    system("cp movement_all.dat movement_all-$machine.dat")

}
