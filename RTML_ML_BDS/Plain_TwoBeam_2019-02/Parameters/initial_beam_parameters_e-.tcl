#
# define beam parameters at entrances of all sections
# if tracking starts in front of a section, the corresponding
# beam parameters as defined here should be used
# these values are close to what you would get at a certain position by tracking from the start
# but the values are not corrected for wake field effects etc.
# i.e. they are not necessarily what a real simulation will give
#
# beamXXX=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#          uncespr,echirp,energy]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV]
#
# Attention: before usage the parameters must be converted to Placet units
#

# at entrance of section 0010 (Matching DR to RTML)
set beam0010(betax)    20.0
set beam0010(alphax)    0.0
set beam0010(betay)     5.0
set beam0010(alphay)    0.0
set beam0010(sigmaz) 1800.0e-6
set beam0010(meanz)     0.0e-6
set beam0010(charge)    0.85e-9
set beam0010(uncespr)   1.1655e-3
set beam0010(echirp)    0.0
set beam0010(energy)    2.86e9

if {"$emitt_dr" == "perfect"} {
    puts "Using ex=700 nm and ey=5 nm from DR"
    set beam0010(emitnx)  700e-9
    set beam0010(emitny)    5e-9
} elseif {"$emitt_dr" == "nominal"} {
    puts "Using ex=860 nm and ey=29 nm from DR"
    set beam0010(emitnx)  860e-9
    set beam0010(emitny)   29e-9
} elseif {"$emitt_dr" == "dynamic"} {
    puts "Using ex=770 nm and ey=17 nm from DR"
    set beam0010(emitnx)  770e-9
    set beam0010(emitny)   17e-9
} else {
    puts "DR emitt_dr unspecified"
    exit
}

# at entrance of section 0280 (Dump and match RTML to Main Linac)
set beam0290(betax)     3.969038431111105e+01
set beam0290(alphax)   -1.498310794232508e+00
set beam0290(emitnx)  850.0e-9
set beam0290(betay)     1.789978887234355e+01
set beam0290(alphay)    6.807489722771091e-01
set beam0290(emitny)   10.0e-9
set beam0290(sigmaz)   70.0e-6
set beam0290(meanz)     0.0e-6
set beam0290(charge)    0.65e-9
set beam0290(uncespr)   1.52e-2
set beam0290(echirp)    0.0
set beam0290(energy)    9.0e9

# at entrance of ML
#set e_initial       9.0
#set match(emitt_x)  9.2
#set match(emitt_y)  0.1
#set match(e_spread) 1
#set match(sigma_z)  70.0
#set match(phase)    0.0
#set match(charge)   5.2e9
#set match(beta_x)   8.054208256047598
#set match(beta_y)   1.201443036029169
#set match(alpha_x)  2.455451375064132e-02
#set match(alpha_y)  6.250882009649877e-03

#  at entrance of BDS
#set match(emitt_x)   9.5
#set match(emitt_y)   0.30
#set match(phase)     0.0
#set match(e_spread) -1.0; # negative: uniform distribution; positive: gaussian
#set match(beta_x)    30.49309113139894
#set match(beta_y)    5.351885767617622
#set match(alpha_x)  -2.502710433712449
#set match(alpha_y)   0.5400454080769824
