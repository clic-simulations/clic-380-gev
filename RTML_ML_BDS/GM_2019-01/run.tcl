#
# Compact Linear Collider (CLIC) - 380 GeV - RTML to BDS
#
# This code was developed by C. Gohil from a code inherited from A. Latina, Y. Han,
# N. Blaskovic Kraljevic and F. Plassard.
#
# This runs a full simulation of the 380 GeV design of CLIC including the effects
# of ground motion. Electron and positron beams are tracked through the RTML, ML and
# BDS with PLACET and a luminosity calculation is done with GUINEA-PIG.
#
# To execute use: tclsh run.tcl <beam_seed> <bds_type> <gm_seed> <gm_iter>
# - beam_seed and gm_seed are any integer.
# - bds_type is either 43 for the L*=4.3 m BDS or 6 for the L*=6 m BDS.
# - gm_iter is the time step iteration of ground motion, t=gm_iter*delta_t.
#
set resultsdir ./Results
set beamdir    ./Beams

# Get simulation parameters from command line arguments
source Scripts/check_arguments.tcl
set beam_seed [lindex $argv 0]
set bds_type  [lindex $argv 1]
set gm_seed   [lindex $argv 2]
set gm_iter   [lindex $argv 3]

# Track the electron beam
exec placet main.tcl e- $beam_seed $bds_type $gm_seed $gm_iter >@stdout

# Track the positron beam
#
# This simulation mirrors the electron and positron beamlines,
# i.e. the e- beamline is used for both the e- and e+ beams.
exec placet main.tcl e+ -$beam_seed $bds_type $gm_seed $gm_iter >@stdout

# Calculate luminosity
exec placet guinea_pig.tcl >@stdout

exit
