set c 2.99792458e8
set f1 [expr 1e-9*$c/0.025]
set nbins 200
set RoverQ 1147.335
set lm_beta 0.4529
set charge 5.2428677e+10  ;#.4 nC
#~ set charge [expr $charge/2.]
set t_bunch [expr 1/(24*499.75e6)]
set pets_nodes 1
set p0 1.89993432

Machine m
Kwakelrt

m new_injector inj
m new_beamline bl1
Ktwiss "twiss"
Drift "twiss_knob" -len 0. -node 1 -kick twiss
for {set i 0} {$i <350} {incr i} {
source "../Lattices_PLACET3/new_singlecell_4beta_newQL.tcl"	
}

Kaccumulator "acc" -key bx by ax ay
m new_dump dmp -kick acc

BunchMulti -nparticles 50000 -bx 0.4 -by 4. -ax 0 -ay 0 -enx 150e-6 -eny 150e-6 \
          -sz 1e-3 -p0 $p0

#~ BunchMulti -nparticles 50000 -bx 10. -by 10. -ax 0 -ay 0 -enx 150e-6 -eny 150e-6 \
          #~ -sz 1e-3 -cut_z 3 -p0 $p0 -charge $charge -time_before_next $t_bunch \
          #~ -collect element s bx by ax ay enx eny sz p0 delta nparticles

Expose m m 
Expose "-name bx_in -machine m -beamline bl1 -component twiss_knob -kick twiss -prop bx"
Expose "-name by_in -machine m -beamline bl1 -component twiss_knob -kick twiss -prop by"
Expose "-name ax_in -machine m -beamline bl1 -component twiss_knob -kick twiss -prop ax"
Expose "-name ay_in -machine m -beamline bl1 -component twiss_knob -kick twiss -prop ay"
Expose "-name bx_out -machine m -dump dmp -kick acc -prop bx"
Expose "-name by_out -machine m -dump dmp -kick acc -prop by"
Expose "-name ax_out -machine m -dump dmp -kick acc -prop ax"
Expose "-name ay_out -machine m -dump dmp -kick acc -prop ay"

MulKnob QF m
QF AddProperties -beamline bl1 -component QF -prop K1L
QF PrintPropertyList
Expose QF QF

MulKnob QD m
QD AddProperties -beamline bl1 -component QD -prop K1L
QD PrintPropertyList
Expose QD QD

#~ exit

OctaveRPL {
  pkg load optim
  #~ format long
  global knobs=[0.4   0.7575];
  knobs=[0.64228   0.65224];
  knobs=[0.64222   0.65228];
  # new Q length
  knobs=[0.61346   0.69354];
  knobs=[0.61341   0.69355];
  # cell number= 10
  knobs=[0.61342   0.69355];
  # cell number= 350
  knobs=[0.61341   0.69355];

  
  Write("bx_in",knobs(1))
  Write("QF",knobs(2))
  Write("QD",-knobs(2))
  Track("m")
  #~ exit

  function S=shift(init,var)
    S=init.*(1. + var./100.);
  endfunction

  function M=merit(s_val)
    global knobs
    kvals=shift(knobs,s_val);
	Write("bx_in",kvals(1))
	Write("QF",kvals(2))
	Write("QD",-kvals(2))
    Track("m")
    bx_in=Read("bx_in")
    by_in=Read("by_in")
    #~ ax_in=Read("ax_in")
    #~ ay_in=Read("ay_in")
    bx_out=Read("bx_out")
    by_out=Read("by_out")
    ax_out=Read("ax_out")
    ay_out=Read("ay_out")
    #~ M=((bx_out-bx_in)/bx_in)**2 + ((by_out-by_in)/by_in)**2 + ((ax_out-ax_in)/ax_in)**2 + ((ay_out-ay_in)/ay_in)**2;
    M=((bx_out-bx_in)/bx_in)**2 + ((by_out-by_in)/by_in)**2 + ax_out**2 + ay_out**2;
  endfunction
  
  start_var=zeros(size(knobs));
  new_values=shift(knobs,fmins('merit',start_var))
}
