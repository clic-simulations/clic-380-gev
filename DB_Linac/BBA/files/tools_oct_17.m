1;
% file contains some functions used for manuplation of bunch file ...
% 

%----------------------------------------
%  smoothing 1d vector 3rd order
function smoothout=my_smooth(vector) 
  n=length(vector);
  tmp=[];
  for k=1:5
    tmp(1)=vector(1);
    a1=vector(1);
    a2=vector(2);
    a3=vector(3);
    tmp(2)=(a1+a2+a3)/3;
    
    for i=3:n-2
      sum=0;
      for j=1:5
	sum=sum+vector(i+j-3);
      end;
      tmp(i)=sum/5;
    end;
    a1=vector(n-2);
    a2=vector(n-1);
    a3=vector(n);
    tmp(n-1)=(a1+a2+a3)/3;
    tmp(n)=vector(n);
    vector=tmp;
   end;
  smoothout=vector;
end

%----------------------------------------
%  returns the histogram data for longitudinal coordinat
function hist=beam_histogram(beam,nslice) 
  B=load(beam);
  z=sort(B(:,4));
  minz=z(1);
  maxz=z(end);
  [count zl]=hist(z(2:end-1),nslice-2);
  zl=[minz zl maxz];
  count=[1 count 1];
  count=count./sum(count);
  count=my_smooth(count);
  hist=[zl' count'];   
end



%----------------------------------------
% calculates the variance of a vector
function suu=var1d(u) 
    um=mean(u);
    suu=sum((u-um).^2)/length(u);
endfunction


%----------------------------------------
% calculates the variance of two vector
function suv=var2d(u,v) 
    lu=length(u);
    lv=length(v);

    if (lu != lv)
        suv=0.0;
        return;
    end;

    um=mean(u);
    vm=mean(v);
    suv=sum((u-um).*(v-vm))/lu;
    
end


%----------------------------------------
% calculates the unprojected emittance of two vector
function emt=eps(u,v) 
    lu=length(u);
    lv=length(v);

    if (lu != lv)
        eps=0.0;
        return;
    end;
    
    ruu=var1d(u);
    rvv=var1d(v);
    ruv=var2d(u,v);
    eps=sqrt(ruu*rvv-ruv*ruv);
endfunction


%------------------------------------------------------------------------
%  this function calculates the transverse twiss parameters
function twissout=twisscalcfile(filein) 
   m0c2=0.00051099893;
   B=load(filein);
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/m0c2+1;
   z0=mean(z);
   x0=mean(x);
   y0=mean(y);
   xp0=mean(xp);
   yp0=mean(yp);
   
   sxx=var1d(x);
   sxpxp=var1d(xp);
   sxxp=var2d(x,xp);
   epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
   bx=sxx/epsx;
   ax=-sxxp/epsx;

   syy  =var1d(y);
   sypyp=var1d(yp);
   syyp =var2d(y,yp);
   epsy =sqrt(syy*sypyp-syyp*syyp);
   by   =syy/epsy;
   ay   =-syyp/epsy;

%     outpar=[x0,xp0,ax,bx,epsx,y0,yp0,ay,by,epsy,g0];
 
    pars=struct('x0', x0, 'xp0', xp0, 'alpha_x', ax, 'beta_x', bx, 'emit_x', epsx, 'y0', y0, 'yp0', yp0, 'alpha_y', ay, 'beta_y', by, 'emit_y', epsy);
   
   twissout=pars;
   
end


%------------------------------------------------------------------------
%  this function calculates the transverse twiss parameters
function [x0,xp0,ax,bx,epsx,y0,yp0,ay,by,epsy,g0]=twisscalcbeam(beam) 
   m0c2=0.00051099893;

   B=beam; 
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/m0c2+1;
   z0=mean(z);
   x0=mean(x);
   y0=mean(y);
   xp0=mean(xp);
   yp0=mean(yp);
   
   sxx=var1d(x);
   sxpxp=var1d(xp);
   sxxp=var2d(x,xp);
   epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
   bx=sxx/epsx;
   ax=-sxxp/epsx;

   syy  =var1d(y);
   sypyp=var1d(yp);
   syyp =var2d(y,yp);
   epsy =sqrt(syy*sypyp-syyp*syyp);
   by   =syy/epsy;
   ay   =-syyp/epsy;

   
end





%------------------------------------------------------------------------
%   this function normalizes the bunch by using phase advance at the stored position file_normed.ext
function normout=normalize_bunch(filein,mux,muy,fileout='normed') 
   m0c2=0.51099893e-3;
   B=load(filein);
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/m0c2+1;
   z0=mean(z);
   x0=mean(x)
   y0=mean(y)
   xp0=mean(xp);
   yp0=mean(yp);
   
   sxx=var1d(x);
   sxpxp=var1d(xp);
   sxxp=var2d(x,xp);
   epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
   bx=sxx/epsx
   ax=-sxxp/epsx;
   emtnx=epsx*g0*1e-6
   
   syy  =var1d(y);
   sypyp=var1d(yp);
   syyp =var2d(y,yp);
   epsy =sqrt(syy*sypyp-syyp*syyp);
   by   =syy/epsy
   ay   =-syyp/epsy;
   emtny=epsy*g0*1e-6
      
   gi=e/m0c2+1;
   
   txn=1.0 / sqrt(bx) * sqrt(gi) .* x  ;
   txpn= ax * txn + sqrt(bx) * sqrt(gi) .*  xp;    
      
   tyn=1.0/ sqrt(by) * sqrt(gi) .* y  ;
   typn= ay * tyn + sqrt(by) * sqrt(gi) .* yp;
   
   
   rx=sqrt(txn.*txn+txpn.*txpn);
   ry=sqrt(tyn.*tyn+typn.*typn);
    
   rx0=mean(rx)
   ry0=mean(ry)   
   
   tetx= atan2(txpn,txn) + mux;
   tety= atan2(typn,tyn) + muy;
   


   xn =rx.*cos(tetx);
   xpn=rx.*sin(tetx);
   
   yn =ry.*cos(tety);
   ypn=ry.*sin(tety);
    
   outdata0=[e,txn,tyn,z,txpn,typn];  
   outdata=[e,xn,yn,z,xpn,ypn];
   
   
   filecase=strcmp(fileout,'normed');

   if filecase >0 
    s=filein;
	npo=findstr(s,".");
	ext=s(1,(npo:end));
	name=s(1,(1:npo-1));
	fout=[name "_norm"];
	fout=[fout ext];
   else
    fout=fileout;
   end
   
   outfile0=sprintf('%s.%s',fout,"0");
   outfile=sprintf('%s',fout);
   
	fprintf('normalized_bunch outdata is %s\n\n',outfile);
%  	fid=fopen(fout,"wt");
%  	fprintf(fid,'%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n',outdata');
%  	fclose(fid);

%      save -text -ascii outfile outdata;
    save("-text", "-ascii",outfile0,"outdata0");
    save("-text", "-ascii",outfile,"outdata");
   
   
end






%------------------------------------------------------------------------
%   this function normalizes the bunch by using phase advance at the stored position file_normed.ext
function normout=normalize_in_out2(nslice,beam0_in,beam0_ou,beam_in,beam_ou,out_n_in,out_n_ou ) 
   m0c2=0.51099893e-3;
   
%   initial reference bunch   
   B0in=load(beam0_in);
   [z0in,id0in]=sort(B0in(:,4));
   B0in=B0in(id0in,:);
   minz0in=min(z0in);
   maxz0in=max(z0in);
   z0instp  =linspace(minz0in,maxz0in,nslice);  
   
%   exit reference bunch   
   B0ou=load(beam0_ou);
   [z0ou,id0ou]=sort(B0ou(:,4));
   B0ou=B0ou(id0ou,:);
   minz0ou=min(z0ou);
   maxz0ou=max(z0ou);
   z0oustp  =linspace(minz0ou,maxz0ou,nslice);  
 
%   initial bunch for normalization
   Bin=load(beam_in);
   [zin,idin]=sort(Bin(:,4));
   Bin=Bin(idin,:);
   minzin=min(zin);
   maxzin=max(zin);
   zinstp  =linspace(minzin,maxzin,nslice);  
   
%   exit  bunch  for normalization 
   Bou=load(beam_ou);
   [zou,idou]=sort(Bou(:,4));
   Bou=Bou(idou,:);
   minzou=min(zou);
   maxzou=max(zou);
   zoustp  =linspace(minzou,maxzou,nslice);  
   
        [sl_xin0_j,sl_xpin0_j,sl_alxin0_j,sl_btxin0_j,sl_epsxin0_j,sl_yin0_j,sl_ypin0_j,sl_alyin0_j,sl_btyin0_j,sl_epsyin0_j,sl_gin0_j]=twisscalcbeam(B0in);
        [sl_xou0_j,sl_xpou0_j,sl_alxou0_j,sl_btxou0_j,sl_epsxou0_j,sl_you0_j,sl_ypou0_j,sl_alyou0_j,sl_btyou0_j,sl_epsyou0_j,sl_gou0_j]=twisscalcbeam(B0ou);
   
%      fid1=fopen('look0.dat',"wt");
%      fid2=fopen('look1.dat',"wt");

    resultin=[];
    resultou=[];

    for j=1:nslice-1
        
        indxin0j  = find( z0in >= z0instp(j) & z0in <= z0instp(j+1));
        indxou0j  = find( z0ou >= z0oustp(j) & z0ou <= z0oustp(j+1));
        indxinj  = find( zin >= zinstp(j) & zin <= zinstp(j+1));
        indxouj  = find( zou >= zoustp(j) & zou <= zoustp(j+1));

        kin0_srt=indxin0j(1);
        kin0_end=indxin0j(end);
        kou0_srt=indxou0j(1);
        kou0_end=indxou0j(end);
    
        kin_srt=indxinj(1);
        kin_end=indxinj(end);
        kou_srt=indxouj(1);
        kou_end=indxouj(end);
    
        B0in_j=B0in(kin0_srt:kin0_end,:);
        B0ou_j=B0ou(kou0_srt:kou0_end,:);    
        
        Bin_j=Bin(kin_srt:kin_end,:);
        Bou_j=Bou(kou_srt:kou_end,:);    
        
%          [sl_xin0_j,sl_xpin0_j,sl_alxin0_j,sl_btxin0_j,sl_epsxin0_j,sl_yin0_j,sl_ypin0_j,sl_alyin0_j,sl_btyin0_j,sl_epsyin0_j,sl_gin0_j]=twisscalcbeam(B0in_j);
%          [sl_xou0_j,sl_xpou0_j,sl_alxou0_j,sl_btxou0_j,sl_epsxou0_j,sl_you0_j,sl_ypou0_j,sl_alyou0_j,sl_btyou0_j,sl_epsyou0_j,sl_gou0_j]=twisscalcbeam(B0ou_j);
        
%          printf ('beta  in out = %f <> %f <> %f \n', sl_btxin0_j,sl_btxou0_j, sl_btxin0_j-sl_btxou0_j)
%          printf ('alpha in out = %f <> %f <> %f \n', sl_alxin0_j,sl_alxou0_j, sl_alxin0_j-sl_alxou0_j)
%          initial bunch
        ein_j=Bin_j(:,1);
        gin_j=ein_j/m0c2+1;
        xin_j=Bin_j(:,2);
        yin_j=Bin_j(:,3);
        zin_j=Bin_j(:,4);
        xpin_j=Bin_j(:,5);
        ypin_j=Bin_j(:,6);
%          initial bunch slice 
        sl_ein_j=mean(ein_j);
        sl_gin_j=mean(gin_j);
        sl_xin_j=mean(xin_j);
        sl_yin_j=mean(yin_j);
        sl_zin_j=mean(zin_j);
        sl_xpin_j=mean(xpin_j);
        sl_ypin_j=mean(ypin_j);
        
%          exit bunch
        eou_j=Bou_j(:,1);
        gou_j=eou_j/m0c2+1;
        xou_j=Bou_j(:,2);
        you_j=Bou_j(:,3);
        zou_j=Bou_j(:,4);
        xpou_j=Bou_j(:,5);
        ypou_j=Bou_j(:,6);
 %          exit bunch slice 
        sl_eou_j=mean(eou_j);
        sl_gou_j=mean(gou_j);
        sl_xou_j=mean(xou_j);
        sl_you_j=mean(you_j);
        sl_zou_j=mean(zou_j);
        sl_xpou_j=mean(xpou_j);
        sl_ypou_j=mean(ypou_j);      
        
        
        
        
 %          initial bunch slice normalize       
                
        sl_xin_jn = sqrt(sl_gin_j/sl_btxin0_j)*sl_xin_j;
        sl_xpin_jn= sl_xin_jn*sl_alxin0_j + sqrt(sl_gin_j*sl_btxin0_j)*sl_xpin_j;
        
        sl_yin_jn = sqrt(sl_gin_j/sl_btyin0_j)*sl_yin_j;
        sl_ypin_jn= sl_yin_jn*sl_alyin0_j + sqrt(sl_gin_j*sl_btyin0_j)*sl_ypin_j;
        
        sl_rxin_jn=sqrt(sl_xin_jn**2+sl_xpin_jn**2);
        sl_ryin_jn=sqrt(sl_yin_jn**2+sl_ypin_jn**2);
        
        sl_muxin_jn=atan2(sl_xpin_jn,sl_xin_jn);
        sl_muyin_jn=atan2(sl_ypin_jn,sl_yin_jn);  
    
 %          exit bunch slice normalize
        sl_xou_jn = sqrt(sl_gou_j/sl_btxou0_j)*sl_xou_j;
        sl_xpou_jn= sl_xou_jn*sl_alxou0_j + sqrt(sl_gou_j*sl_btxou0_j)*sl_xpou_j;
        
        sl_you_jn = sqrt(sl_gou_j/sl_btyou0_j)*sl_you_j;
        sl_ypou_jn= sl_you_jn*sl_alyou0_j +sqrt(sl_gou_j*sl_btyou0_j)*sl_ypou_j;       
        
        sl_rxou_jn=sqrt(sl_xou_jn**2+sl_xpou_jn**2);
        sl_ryou_jn=sqrt(sl_you_jn**2+sl_ypou_jn**2);
        
        sl_muxou_jn=atan2(sl_xpou_jn,sl_xou_jn);
        sl_muyou_jn=atan2(sl_ypou_jn,sl_you_jn);  
        

        if (j==1)
            sl_muxin_jn0=sl_muxin_jn;
            sl_muyin_jn0=sl_muyin_jn;
            sl_muxou_jn0=sl_muxou_jn;
            sl_muyou_jn0=sl_muyou_jn;
            sl_muxinou_jn0=sl_muxou_jn0-sl_muxin_jn0;
            sl_muyinou_jn0=sl_muyou_jn0-sl_muyin_jn0;
        end
            dsl_muxin_jn=sl_muxin_jn-sl_muxin_jn0;
            dsl_muyin_jn=sl_muyin_jn-sl_muyin_jn0;
            dsl_muxou_jn=sl_muxou_jn-sl_muxou_jn0;
            dsl_muyou_jn=sl_muyou_jn-sl_muyou_jn0;
            
 %       transfer  initial bunch to final position 
        sl_xin_jn2 = cos(sl_muxinou_jn0-dsl_muxin_jn)*sl_xin_jn + sin(sl_muxinou_jn0-dsl_muxin_jn)*sl_xpin_jn;
        sl_xpin_jn2= sin(sl_muxinou_jn0-dsl_muxin_jn)*sl_xin_jn + cos(sl_muxinou_jn0-dsl_muxin_jn)*sl_xpin_jn;
        
        sl_yin_jn2 = cos(sl_muyinou_jn0-dsl_muyin_jn)*sl_yin_jn + sin(sl_muyinou_jn0-dsl_muyin_jn)*sl_ypin_jn;
        sl_ypin_jn2= sin(sl_muyinou_jn0-dsl_muyin_jn)*sl_yin_jn + cos(sl_muyinou_jn0-dsl_muyin_jn)*sl_ypin_jn; 
        
        sl_rxin_jn2=sqrt(sl_xin_jn2**2+sl_xpin_jn2**2);
        sl_ryin_jn2=sqrt(sl_yin_jn2**2+sl_ypin_jn2**2);
        
        xnin  = sl_rxin_jn2*cos(dsl_muxin_jn)/sl_rxin_jn;
        xpnin = sl_rxin_jn2*sin(dsl_muxin_jn)/sl_rxin_jn;
        
        ynin  = sl_ryin_jn2*cos(dsl_muyin_jn)/sl_ryin_jn;
        ypnin = sl_ryin_jn2*sin(dsl_muyin_jn)/sl_ryin_jn;
        
        xnou  = sl_rxou_jn*cos(dsl_muxou_jn)/sl_rxin_jn;
        xpnou = sl_rxou_jn*sin(dsl_muxou_jn)/sl_rxin_jn;

        ynou  = sl_ryou_jn*cos(dsl_muyou_jn)/sl_ryin_jn;
        ypnou = sl_ryou_jn*sin(dsl_muyou_jn)/sl_ryin_jn;
        
        resultin=[resultin;sl_ein_j,xnin,ynin,sl_zin_j,xpnin,ypnin,sqrt(xnin**2+xpnin**2),sqrt(ynin**2+ypnin**2)];
        resultou=[resultou;sl_eou_j,xnou,ynou,sl_zou_j,xpnou,ypnou,sqrt(xnou**2+xpnou**2),sqrt(ynou**2+ypnou**2)];
 
       
    end;
    
    resultin(:,2)=my_smooth(resultin(:,2));
    resultou(:,2)=my_smooth(resultou(:,2));
    resultin(:,3)=my_smooth(resultin(:,3));
    resultou(:,3)=my_smooth(resultou(:,3));
    resultin(:,5)=my_smooth(resultin(:,5));
    resultou(:,5)=my_smooth(resultou(:,5));
    resultin(:,6)=my_smooth(resultin(:,6));
    resultou(:,6)=my_smooth(resultou(:,6));
    resultin(:,7)=my_smooth(resultin(:,7));
    resultou(:,7)=my_smooth(resultou(:,7));
    resultin(:,8)=my_smooth(resultin(:,8));
    resultou(:,8)=my_smooth(resultou(:,8));    
    

        save("-text", "-ascii",out_n_in,"resultin");
        save("-text", "-ascii",out_n_ou,"resultou");
endfunction





%------------------------------------------------------------------------
%   this function normalizes the bunch by using phase advance at the stored position 
function normout=normalize_in_out3(nslice,beam0_in,beam0_ou,beam_in,beam_ou,out_n_in,out_n_ou ) 
   m0c2=0.51099893e-3;
   
%   initial reference bunch   
   Bin0=load(beam0_in);
%     [zin0,id0in]=sort(Bin0(:,4));
%     Bin0=Bin0(id0in,:);
   [mxin0,mxpin0,malxin0,mbtxin0,mepsxin0,myin0,mypin0,malyin0,mbtyin0,mepsyin0,mgin0]=twisscalcbeam(Bin0);
   ein0=Bin0(:,1);
   gin0=ein0/m0c2+1;
   xin0=Bin0(:,2);
   yin0=Bin0(:,3);
   zin0=Bin0(:,4);
   xpin0=Bin0(:,5);
   ypin0=Bin0(:,6);
   
%   exit reference bunch   
   Bou0=load(beam0_ou);
%     [zou0,id0ou]=sort(Bou0(:,4));
%     Bou0=Bou0(id0ou,:);
   [mxou0,mxpou0,malxou0,mbtxou0,mepsxou0,myou0,mypou0,malyou0,mbtyou0,mepsyou0,mgou0]=twisscalcbeam(Bou0);
   eou0=Bou0(:,1);
   gou0=eou0/m0c2+1;
   xou0=Bou0(:,2);
   you0=Bou0(:,3);
   zou0=Bou0(:,4);
   xpou0=Bou0(:,5);
   ypou0=Bou0(:,6);
 
%   initial bunch  for normalization
   Bin=load(beam_in);
%     [zin,idin]=sort(Bin(:,4));
%     Bin=Bin(idin,:);
   ein=Bin(:,1);
   gin=ein/m0c2+1;
   xin=Bin(:,2);
   yin=Bin(:,3);
   zin=Bin(:,4);
   xpin=Bin(:,5);
   ypin=Bin(:,6);
   
%   exit  bunch  for normalization
   Bou=load(beam_ou);
%     [zou,idou]=sort(Bou(:,4));
%     Bou=Bou(idou,:);
   eou=Bou(:,1);
   gou=eou/m0c2+1;
   xou=Bou(:,2);
   you=Bou(:,3);
   zou=Bou(:,4);
   xpou=Bou(:,5);
   ypou=Bou(:,6);
    
   
   xin_n = sqrt(gin./mbtxin0).*xin;
   xpin_n=  xin_n.*malxin0 + sqrt(gin.*mbtxin0).*xpin;
   rxin=sqrt(xin_n.^2+xpin_n.^2);  
   dtetxin=atan2(xpin_n(1),xin_n(1));
   tetxin=atan2(xpin_n,xin_n);
   xin_n=cos(tetxin-dtetxin);
   xpin_n=sin(tetxin-dtetxin);
   
   yin_n = sqrt(gin./mbtyin0).*yin;
   ypin_n=  yin_n.*malyin0 + sqrt(gin.*mbtyin0).*ypin;
   
   xou_n = sqrt(gou./mbtxou0).*xou;
   xpou_n=  xou_n.*malxou0 + sqrt(gou.*mbtxou0).*xpou;
   rxou=sqrt(xou_n.^2+xpou_n.^2);
   tetxou=atan2(xpou_n,xou_n);
%  tetxou0=atan2(xpou_n(1),xou_n(1));
%  dtetx=atan2(xpou_n(1),xou_n(1))-atan2(xpin_n(1),xin_n(1));
   dtetxou=atan2(xpou_n(1),xou_n(1));
   
%     dtetx=tetxou-tetxin;
   
   xou_n=cos(tetxou-dtetxou).*rxou./rxin;
   xpou_n=sin(tetxou-dtetxou).*rxou./rxin;
   
   
   
   you_n = sqrt(gou./mbtyou0).*you;
   ypou_n=  you_n.*malyou0 + sqrt(gou.*mbtyou0).*ypou;
   
   Bin_n=[ein xin_n yin_n zin xpin_n ypin_n];
   Bou_n=[eou xou_n you_n zou xpou_n ypou_n];
   
    save("-text", "-ascii",out_n_in,"Bin_n");
    save("-text", "-ascii",out_n_ou,"Bou_n");
   


endfunction

