set rfparamsbooster(gradientww)   1.489875899338e+07
set rfparamsbc1(gradientww)   1.327405601461e+07
set rfparamsbc2(gradientww)   7.192571273342e+07
set energy_wake_bc1  -3.531223661126e-05
set energy_wake_booster  -5.770721214867e-05
set energy_wake_bc2  -7.436381370271e-05
