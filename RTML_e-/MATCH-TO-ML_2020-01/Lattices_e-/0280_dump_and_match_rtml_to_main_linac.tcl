#
# Dump and matching of RTML to Main Linac
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_rtml_to_main_linac {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 10
set quad_synrad 0

set refenergy $beamparams(meanenergy)
set q0 $beamparams(charge)

set lquadm 0.3

set kqm1  0.380292757617274
set kqm2 -0.561616333993213
set kqm3 -4.714050813894366
set kqm4  9.042420490998818

set ld1 2.33573807942602e-1
set ld2 9.72003353939228
set ld3 5.99666849236622
set ld4 3.99495045973610e-21
set ld5 1.47809871786490e-9

SetReferenceEnergy $refenergy

Girder
Drift -name "matching-drift" -length $ld1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "matching-drift" -length $ld2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "matching-drift" -length $ld3 -six_dim $usesixdim
Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "matching-drift" -length $ld4 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "matching-quad" -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "matching-drift" -length $ld5 -six_dim $usesixdim

# Half Quadrupole
Quadrupole -name "" -s 0.0 -x 0 -y 0 -xp 0 -yp 0 -roll 0 -tilt 0 -tilt_deg 0 -length 0.215 -synrad 0 -six_dim 0 -thin_lens 0 -e0 -1 -aperture_x 1 -aperture_y 1 -aperture_losses 0 -aperture_shape "none" -tclcall_entrance "" -tclcall_exit "" -short_range_wake "" -strength 3.09139 -Kn 0 -type 0 -hcorrector "x" -hcorrector_step_size 0 -vcorrector "y" -vcorrector_step_size 0

}
