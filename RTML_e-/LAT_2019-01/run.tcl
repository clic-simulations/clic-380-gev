#
# CLIC 380 GeV: Main e- Beam (RTML)
#
# This code was developed by C. Gohil from a code inherited from A. Latina and Y. Han.
#
# Generates a list of elements that form the e- RTML. 
#
set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set wakedir    ./Wakefields

# Load variables and functions
source $scriptdir/set_parameters_e-.tcl
source $scriptdir/lattice_functions.tcl

puts "Setting up lattice"
source $scriptdir/beamline_and_wake.tcl

BeamlineList -file 0000_e-_rtml.tcl

exit
