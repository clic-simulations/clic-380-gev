proc otoCorrection {name beamline Start End beam beta0 nloop} {
    global sigma
    global machine
    Octave {
        disp("1-TO-1 CORRECTION");
        Start = placet_get_name_number_list("$beamline","$Start");
        End   = placet_get_name_number_list("$beamline","$End");

        for bin = 1:length(Bins)
            Bin = Bins(bin);
            nCorrector = length(Bin.Corrector);
            R0x = Response.R0x(Bin.Bpms,Bin.Corrector);
            R0y = Response.R0y(Bin.Bpms,Bin.Corrector);
            placet_test_no_correction("$beamline", "$beam", "None", 1, Start, Response.Bpms(Bin.Bpms(end)));
            printf("Now is bin %e\n",bin);

            for i=1:$nloop
                B0 = placet_get_bpm_readings("$beamline", Response.Bpms);
                B0 -= Response.B0;
                B0 = B0(Bin.Bpms,:);
                CorrectorX = -[ R0x ; $beta0 * eye(nCorrector) ] \ [ B0(:,1) ; zeros(nCorrector,1) ];
                CorrectorY = -[ R0y ; $beta0 * eye(nCorrector) ] \ [ B0(:,2) ; zeros(nCorrector,1) ];
                placet_element_vary_attribute("$beamline", Response.Corrector(Bin.Corrector), "strength_x", CorrectorX);
                placet_element_vary_attribute("$beamline", Response.Corrector(Bin.Corrector), "strength_y", CorrectorY);
                [E,B] = placet_test_no_correction("$beamline", "$beam", "None", 1, Start, Bin.last);
            end
            printf("The emittance of X is %e\n", E(end,2));
            printf("The emittance of Y is %e\n", E(end,6));
        end
        printf("*****************************************\n");
        save(['Emitt1-' "$name-$beamline-" num2str($sigma) '-' num2str($machine) '.dat'], 'E')
    }
}
