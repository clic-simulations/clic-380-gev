TclCall -script {
Octave {
        load("X_ca.dat");

	offset_X = round(10*X(1:5));
	offset_Y = round(10*X(6:10)); 
	Multipole_all = placet_get_name_number_list("rtml","Multi-CA*");

	N_Multipole = length(X)/2;
   
	Multipoles = Multipole_all(1:N_Multipole);
	placet_element_vary_attribute("rtml",Multipoles,"x", -offset_X);
	placet_element_vary_attribute("rtml",Multipoles,"y", -offset_Y);
}
}
