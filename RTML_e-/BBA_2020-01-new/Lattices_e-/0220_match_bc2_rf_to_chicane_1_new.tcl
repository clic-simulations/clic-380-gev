#
# matching of BC2 RF to BC2 chicane 1
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_bc2_rf_to_chicane_1 {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 100
    set quad_synrad   0

    set lquadm 0.3

    # set kqm1  0.6260896377748464
    # set kqm2 -1.1455472889573088
    # set kqm3  0.4128132094569047
    # set kqm4 -0.2872950092257217
    # set kqm5  1.9483056072681648
    # set kqm6 -2.02952949380762

    # set kqm1   1.5
    # set kqm2  -1.5
    # set kqm3   1.5
    # set kqm4  -1.5
    # set kqm5   1.5
    # set kqm6  -1.5
    # set kqm7   1.5

    # set ldm1 2
    # set ldm2 2
    # set ldm3 2
    # set ldm4 2
    # set ldm5 2
    # set ldm6 2
    # set ldm7 2
    # set ldm8 2

    set kqm1 1.9805
    set kqm2 -2.3204
    set kqm3 3.0415
    set kqm4 -0.73092
    set kqm5 2.1846
    set kqm6 -1.1503
    set kqm7 0.71314
    set ldm1 1.4628
    set ldm2 5.2076
    set ldm3 0.46718
    set ldm4 0.14189
    set ldm5 10.3815
    set ldm6 3.0589
    set ldm7 3.8264
    set ldm8 5.8292

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
    Drift -name "Drift220" -length $ldm1
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy]
    Dipole -name "D220-1"
    Drift -name "Drift220" -length $ldm2
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy]
    Dipole -name "D220-2"
    Drift -name "Drift220" -length $ldm3
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy]
    Dipole -name "D220-3"
    Drift -name "Drift220" -length $ldm4
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy]
    Dipole -name "D220-4"
    Drift -name "Drift220" -length $ldm5
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm5*$lquadm*$refenergy]
    Dipole -name "D220-5"
    Drift -name "Drift220" -length $ldm6
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm6*$lquadm*$refenergy]
    Dipole -name "D220-6"
    Drift -name "Drift220" -length $ldm7
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm7*$lquadm*$refenergy]
    Dipole -name "D220-7"
    Drift -name "Drift220" -length $ldm8
}
