## 1273 PETS first-stage CLIC decelerator "compact" lattice ##
set c 2.99792458e8
set f1 [expr 24*0.49975]
set nbins 200
set RoverQ [expr 2294.67/2]
set lm_beta 0.4529
set pets_app 11.5e-3 
set n_nodes 5
#~ set p0 1.99218842222222 (for 1273 PETS)
if { ! [info exists refp] } {
    set refp 1.91
} 
puts "initial refp= $refp"
## by Raul Costa, May 2023 -- revised July 2023 ##
#~ Girder
Quadrupole "halfquad" -length [expr 0.27/2] -K1L [expr -1.3871/2] -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
#~ Girder
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L -1.3871 -refp $refp 
Drift "drift" -length 0.11228 
PETS "PETS" -length 0.206349 -nodes $n_nodes -lmode_f $f1 -lmode_R $RoverQ -lmode_beta $lm_beta -nbins $nbins -ashape circ -radius $pets_app 
set refp [expr $refp - 0.0014094] 
Drift "drift" -length 0.088 
Drift "PETS_EmptySlot" -length 0.206349 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
Drift "bpm" -length 0.053250 
Quadrupole "quadrupole" -length 0.27 -K1L 1.3871 -refp $refp 
Drift "drift" -length 0.11228 
Drift "PETS_EmptySlot" -length 0.206349 
Drift "drift" -length 0.088 
Drift "PETS_EmptySlot" -length 0.206349 
Drift "drift" -length 0.088 
Drift "drift" -length 0.147276 
