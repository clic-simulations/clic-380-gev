#
# Compact Linear Collider (CLIC) - 380 GeV - RTML to BDS
#
# This code was developed by C. Gohil from a code inherited from A. Latina, Y. Han,
# N. Blaskovic Kraljevic and F. Plassard.
#
# This runs a full simulation of the 380 GeV design of CLIC. Electron and positron
# beams are tracked through the RTML, ML and BDS with PLACET and a luminosity
# calculation is done with GUINEA-PIG.
#
# THESE SCRIPTS ASSUME A DIPOLE GRID HAS ALREADY BEEN INSERTED INTO THE LATTICE FILES.
#
set resultsdir ./Results
set beamdir    ./Beams

# Get simulation parameters from command line arguments
source Scripts/check_arguments.tcl
set beam_seed  [lindex $argv 0]
set emitt_dr   [lindex $argv 1]
set bds_type   [lindex $argv 2]
set sf_seed    [lindex $argv 3]
set sf_iter    [lindex $argv 4]
set mitigation [lindex $argv 5]

# Track the electron beam
exec placet -s main.tcl e- $beam_seed $emitt_dr $bds_type $sf_seed $sf_iter \
                           $mitigation >@stdout

# Track the positron beam
#
# This simulation mirrors the electron and positron beamlines,
# i.e. the e- beamline is used for both the e- and e+ beams.
exec placet -s main.tcl e+ -$beam_seed $emitt_dr $bds_type $sf_seed $sf_iter \
                            $mitigation >@stdout

# Calculate luminosity
exec placet -s guinea_pig.tcl >@stdout

exit
