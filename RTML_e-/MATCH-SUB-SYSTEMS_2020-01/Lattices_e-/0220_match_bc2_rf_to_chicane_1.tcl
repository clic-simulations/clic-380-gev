#
# matching of BC2 RF to BC2 chicane 1
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_bc2_rf_to_chicane_1 {bparray} {
    upvar $bparray beamparams

    set usesixdim 1
    set numthinlenses 10
    set quad_synrad 0

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    set lquadm 0.3

    # From match_0220.tcl
    set kqm1 0.1878268913324539
    set kqm2 -0.3436641866871926
    set kqm3 0.1238439628370714
    set kqm4 -0.0861885027677165
    set kqm5 0.5844916821804494
    set kqm6 -0.6088588481422860
    set ldm1 3.85344476669227e+00
    set ldm2 9.57864209663127e-01
    set ldm3 9.35911621813729e+00
    set ldm4 2.04633501637627e+01
    set ldm5 3.09193396703442e+01
    set ldm6 2.49383490181243e-03
    set ldm7 2.14223407574243e+00

    #set kqm1 0.1842068703506505
    #set kqm2 -0.3473601281747410
    #set kqm3 0.1244212898383306
    #set kqm4 -0.0833300644099469
    #set kqm5 0.5928580764808334
    #set kqm6 -0.6206503041481647
    #set ldm1 3.74566621808366e+00
    #set ldm2 9.30210083634462e-01
    #set ldm3 9.24415165102153e+00
    #set ldm4 2.05731682483680e+01
    #set ldm5 3.14164137430266e+01
    #set ldm6 2.58201740906534e-03
    #set ldm7 2.09070042481231e+00

    Girder
    Drift -name "matching-drift" -length $ldm1
    Quadrupole -name "matching-quad" -length $lquadm -strength [expr $kqm1*$refenergy]
    Drift -name "matching-drift" -length $ldm2
    Quadrupole -name "matching-quad" -length $lquadm -strength [expr $kqm2*$refenergy]
    Drift -name "matching-drift" -length $ldm3
    Quadrupole -name "matching-quad" -length $lquadm -strength [expr $kqm3*$refenergy]
    Drift -name "matching-drift" -length $ldm4
    Quadrupole -name "matching-quad" -length $lquadm -strength [expr $kqm4*$refenergy]
    Drift -name "matching-drift" -length $ldm5
    Quadrupole -name "matching-quad" -length $lquadm -strength [expr $kqm5*$refenergy]
    Drift -name "matching-drift" -length $ldm6
    Quadrupole -name "matching-quad" -length $lquadm -strength [expr $kqm6*$refenergy]
    Drift -name "matching-drift" -length $ldm7
}
