set rfparamsbooster(gradientww)   1.834481358503e+07
set rfparamsbc1(gradientww)   2.230435984814e+07
set rfparamsbc2(gradientww)   7.454081479973e+07
set energy_wake_bc1  -3.531050891438e-05
set energy_wake_booster  -5.771364799783e-05
set energy_wake_bc2  -7.430675753713e-05
