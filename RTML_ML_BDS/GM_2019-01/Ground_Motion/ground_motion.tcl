#
# This code was developed by C. Gohil from code inherited from E. Marin.
#
# Implements ground motion in the 380 GeV design of CLIC.
#
source $gmdir/ground_motion_init.tcl

#
# Ground motion settings
#
set ground_motion_x       1
set ground_motion_y       1
set delta_T_short         0.02
set use_quad_stab_failure 0
set use_preisolator       1
set debug                 0
set time_step_index       1

# type: 
#  - 0: none,
#  - 1: ATL,
#  - 2: ground motion generator
# filtertype:
#  - 0: no filter,
#  - 1: use filters specified in x and y for the whole beamline
# preisolator:
#  - 0: no preisolator,
#  - 1: simple version F. Ramos et al,
#  - 2: mechanical feedback B. Caron et al.,
#  - 3: F. Ramos et al. including tilt motion
# t_start: start time after perfect beamline in seconds
array set groundmotion {
    type         2
    model        "D"
    filtertype   1
    filtertype_x "rec_quad_stab.dat"
    filtertype_y "rec_quad_stab.dat"
    preisolator  3
    t_start      0
}

# Move ground with respect to IP
set gm_ip0 1

# Move cavities with stabilisation
set gm_cav_stab 0

#
# Ground motion implentation
#
# Applies ground motion to a beamline by evolving the offset of each girder
# by one time step.
proc ground_motion {beamlinename type} {
    global delta_T_short ground_motion_x ground_motion_y
    if {$type==1} {
        ground_motion_long $beamlinename $delta_T_short \
                           $ground_motion_x $ground_motion_y
    } elseif {$type==2} {
        ground_motion_short $beamlinename
    }
}

# Short time ground motion model
proc ground_motion_short {beamlinename} {
    global debug time_step_index gm_ip0 gm_cav_stab
    BeamlineUse -name $beamlinename
    if {$debug} {
        GroundMotion -cav_stab $gm_cav_stab -ip0 $gm_ip0 \
                     -output_file gm_${beamlinename}_${time_step_index}.dat \
                     -output_file2 gm2_${beamlinename}_${time_step_index}.dat
    } else {
        GroundMotion -cav_stab $gm_cav_stab -ip0 $gm_ip0
    }
}

# Long time ground motion model
proc ground_motion_long {beamlinename {time_step -1} {use_x 1} {use_y 1}} {
    global atl_time groundmotion_long start_bds use_bds use_main_linac end_main_linac \
           atl_constant

    # ATL motion, time is given by tcl variable atl_time
    if {$time_step == -1} {
        set time_step $atl_time
    }
    puts "ATL motion applied $time_step"
    
    # determine which part to move:
    set start_element -1
    set end_element -1
    if {[info exists groundmotion_long] && $use_bds} {
        if {$groundmotion_long(ml)==0} {
            set start_element $start_bds
        }
        if {$groundmotion_long(bds)==0 && $use_main_linac} {
            set end_element $end_main_linac
        }
    }

    # move girders
    BeamlineUse -name $beamlinename
    GroundMotionATL -t $time_step -x $use_x -y $use_y -end_fixed 1 \
                    -start_element $start_element -end_element $end_element \
                    -a $atl_constant

}

# Function to setup ground motion in the RTML
proc set_rtml_ground_motion {gm_seed gm_iter particle} {
    global groundmotion sec_len use_bds
    set use_bds 0
    RandomReset -seed $gm_seed -stream Groundmotion
    if {$particle == "e-"} {
        ground_motion_init rtml $groundmotion(type) $groundmotion(model) 1 \
                           [expr -$sec_len(bds)-$sec_len(ml)]
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion rtml $groundmotion(type)
        }
    } elseif {$particle == "e+"} {
        ground_motion_init rtml $groundmotion(type) $groundmotion(model) -1 \
                           [expr $sec_len(bds)+$sec_len(ml)]
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion rtml $groundmotion(type)
        }
    }
}

# Function to setup ground motion in the ML
proc set_ml_ground_motion {gm_seed gm_iter particle} {
    global groundmotion sec_len use_bds
    set use_bds 0
    RandomReset -seed $gm_seed -stream Groundmotion
    if {$particle == "e-"} {
        ground_motion_init ml $groundmotion(type) $groundmotion(model) 1 -$sec_len(bds)
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion ml $groundmotion(type)
        }
    } elseif {$particle == "e+"} {
        ground_motion_init ml $groundmotion(type) $groundmotion(model) -1 $sec_len(bds)
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion ml $groundmotion(type)
        }  
    }
}

# Function to setup ground motion in the BDS
proc set_bds_ground_motion {gm_seed gm_iter particle} {
    global groundmotion sec_len use_preisolator use_bds start_cantilever end_cantilever
    set use_bds 0
    if {$use_preisolator} {
        set use_bds 1
        Octave {
            QD0 = placet_get_name_number_list("bds", "QD0");
            Tcl_SetVar("start_cantilever", QD0);
            Tcl_SetVar("end_cantilever", QD0+2);
        }
    }
    RandomReset -seed $gm_seed -stream Groundmotion
    if {$particle == "e-"} {
        ground_motion_init bds $groundmotion(type) $groundmotion(model) 1 0
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion bds $groundmotion(type)
        }
    } elseif {$particle == "e+"} {
        ground_motion_init bds $groundmotion(type) $groundmotion(model) -1 0
        for {set i 0} {$i <= $gm_iter} {incr i} {
            ground_motion bds $groundmotion(type)
        }
    }
}

# Saves the offset of every quadrupole in e- and e+ beamlines
proc write_quad_offsets {particle} {
    global gmdir sec_len
    if {$particle == "e-"} {
        set offsets_file [open $gmdir/quad_offsets.dat w]
        BeamlineUse -name rtml
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr -$sec_len(rtml_e-)-$sec_len(ml)-$sec_len(bds) + \
              [ElementGetAttribute $i -s]] [ElementGetOffset $i]"
        }
        BeamlineUse -name ml
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr -$sec_len(ml)-$sec_len(bds)+[ElementGetAttribute $i -s]] \
             [ElementGetOffset $i]"
        }
        BeamlineUse -name bds
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr -$sec_len(bds)+[ElementGetAttribute $i -s]] \
             [ElementGetOffset $i]"
        }
        close $offsets_file
    } elseif {$particle == "e+"} {
        set offsets_file [open $gmdir/quad_offsets.dat a]
        BeamlineUse -name rtml
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr $sec_len(rtml_e+)+$sec_len(ml)+$sec_len(bds) - \
              [ElementGetAttribute $i -s]] [ElementGetOffset $i]"
        }
        BeamlineUse -name ml
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr $sec_len(ml)+$sec_len(bds)-[ElementGetAttribute $i -s]] \
             [ElementGetOffset $i]"
        }
        BeamlineUse -name bds
        foreach i [QuadrupoleNumberList] {
            puts $offsets_file \
            "[expr $sec_len(bds)-[ElementGetAttribute $i -s]] \
             [ElementGetOffset $i]"
        }
        close $offsets_file
    }
}
