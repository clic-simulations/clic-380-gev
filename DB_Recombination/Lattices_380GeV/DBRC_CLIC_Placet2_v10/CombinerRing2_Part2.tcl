## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
 ## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
 ## Which means there will be a slight magnet missfocus for the other bunches. 
 set Pi [expr acos(-1)] 
 if {$Dipole_synrad} { 
 set refen 1.998709117
 } else { 
 set refen 2.0 
 } 
 ####START#### 
 Marker "CR2_P2.INJECTION" 
 Drift "CR2.SEPTUM" -len 1 
 Drift "CR2.DRFB6" -len 2.367622162 
 Drift "CR2.DSRFB6" -len 0.4 
 Drift "CR2.DRFB4A" -len 0.709135479 
 Drift "CR2.D1M" -len 1 
 Drift "CR2.D1M" -len 1 
 Drift "CR2.D1M" -len 1 
 Drift -name "CR2.D1CM" -len 0.01 
 Quadrupole "QUAD.CR2.QRFB3.EXT" -len 0.5 -K1L -0.2258082330193268 -refen $refen 
 Bpm "BPM.CR2_2.35" -len 0 
 Drift "CR2.DRFB3" -len 3.771233433 
 Quadrupole "QUAD.CR2.QRFB2.EXT" -len 0.5 -K1L 0.5591986720709291 -refen $refen 
 Bpm "BPM.CR2_2.36" -len 0 
 Drift "CR2.DRFB2" -len 1.193240969 
 Quadrupole "QUAD.CR2.QRFB1.EXT" -len 0.5 -K1L -1.079720421877582 -refen $refen 
 Bpm "BPM.CR2_2.37" -len 0 
 Drift "CR2.DRFB1" -len 0.217192186 
 Drift "CR2.DRRF" -len 2 
 Marker "CR2.MENTEXTKCK" 
 Drift "CR2.DL3IN" -len 0.4000389758 
 Quadrupole "QUAD.CR2.QL3IN.EXT" -len 0.5 -K1L -0.2499898253787109 -refen $refen 
 Bpm "BPM.CR2_2.38" -len 0 
 Drift "CR2.DL2IN" -len 1.103409345 
 Quadrupole "QUAD.CR2.QL2IN.EXT" -len 0.5 -K1L 0.8526680541738718 -refen $refen 
 Bpm "BPM.CR2_2.39" -len 0 
 Drift "CR2.DL1IN" -len 0.4000469975 
 Quadrupole "QUAD.CR2.QL1IN.EXT" -len 0.5 -K1L -0.6970772319488282 -refen $refen 
 Bpm "BPM.CR2_2.40" -len 0 
 Drift "CR2.DL0IN" -len 2.246504682 
 #### Missing length #### 
 Drift "CR2.MISSLENGTH" -len 0.202692445399988 
 ######################## 
 Marker "CR2MDACELLSTART" 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 ####################### 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SN1.5" -len 0.1 -K2L 2.1883972179249276 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Marker "CR2.DBA.HBM" 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -K2L 0.17585786940087478 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SN3.5" -len 0.1 -K2L -2.053256218936993 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SNN3.5" -len 0.1 -K2L -0.36501287976463404 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -K2L 0.17585791867669906 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SNN1.5" -len 0.1 -K2L -3.3041807355265043 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183728 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183728 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.039925864400000004 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SNN1.5" -len 0.1 -K2L -3.3042062447922875 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -K2L 0.17585927635254872 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SNN3.5" -len 0.1 -K2L -0.36501580005059064 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SN3.5" -len 0.1 -K2L -2.05327264601569 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -K2L 0.17585932562758458 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SN1.5" -len 0.1 -K2L 2.1884153393913777 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 ######################################## 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864928 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Marker "CR2MDACELLEND" 
 Drift "CR2.DTR1A" -len 1.1 
 Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
 Drift "CR2.DTR1B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
 Bpm "BPM.CR2_1.7" -len 0 
 Drift "CR2.DTR2A" -len 1.1 
 Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.84671680555554 -refen $refen 
 Drift "CR2.DTR2B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
 Bpm "BPM.CR2_1.8" -len 0 
 Drift "CR2.DTR3A" -len 1.5 
 Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458445 -refen $refen 
 Drift "CR2.DTR3B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
 Bpm "BPM.CR2_1.9" -len 0 
 Drift "CR2.DTR4A" -len 2.1328 
 Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
 Drift "CR2.DTR4B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR4" -len 0.25 -K1L 0.18700585960992677 -refen $refen 
 Bpm "BPM.CR2_1.10" -len 0 
 Quadrupole "QUAD.CR2.QTR4" -len 0.25 -K1L 0.18700585960992677 -refen $refen 
 Bpm "BPM.CR2_1.11" -len 0 
 Drift "CR2.DTR4B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
 Drift "CR2.DTR4A" -len 2.1328 
 Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
 Bpm "BPM.CR2_1.12" -len 0 
 Drift "CR2.DTR3B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458445 -refen $refen 
 Drift "CR2.DTR3A" -len 1.5 
 Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
 Bpm "BPM.CR2_1.13" -len 0 
 Drift "CR2.DTR2B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.84671680555554 -refen $refen 
 Drift "CR2.DTR2A" -len 1.1 
 Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
 Bpm "BPM.CR2_1.14" -len 0 
 Drift "CR2.DTR1B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
 Drift "CR2.DTR1A" -len 1.1 
 Marker "CR2MDACELLSTART" 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864928 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 ####################### 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SN1.6" -len 0.1 -K2L 2.553533169968345 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Marker "CR2.DBA.HBM" 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -K2L -0.05291278557499331 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SN3.6" -len 0.1 -K2L -2.022704279654237 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SNN3.6" -len 0.1 -K2L -0.12529383645800046 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967743999999 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -K2L -0.05291280040057932 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SNN1.6" -len 0.1 -K2L -1.7052837245897725 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.039925864400000004 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SNN1.6" -len 0.1 -K2L -1.705296889259376 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -K2L -0.05291320888365169 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SNN3.6" -len 0.1 -K2L -0.12529483882336354 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881359999999999 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SN3.6" -len 0.1 -K2L -2.0227204615253247 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -K2L -0.05291322370900049 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.1143160000000003 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SN1.6" -len 0.1 -K2L 2.5535543140020245 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 ######################################## 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Marker "CR2MDACELLEND" 
 Drift "CR2.DS1C_A" -len 2.179474654 
 Sextupole "SEXT.CR2.SXS1C" -len 0.1 -K2L 2.1606775839716965 -refen $refen 
 Drift "CR2.DS1C_B" -len 0.3 
 Quadrupole "QUAD.CR2.QS1C" -len 0.5 -K1L -0.647356904204382 -refen $refen 
 Bpm "BPM.CR2_1.15" -len 0 
 Drift "CR2.DS2C" -len 0.2003907272 
 Quadrupole "QUAD.CR2.QS2C" -len 0.5 -K1L 0.7905004870236609 -refen $refen 
 Bpm "BPM.CR2_1.16" -len 0 
 Drift "CR2.DS3C_A" -len 0.3 
 Sextupole "SEXT.CR2.SXS3C" -len 0.1 -K2L -0.42652765870388576 -refen $refen 
 Drift "CR2.DS3C_B" -len 2.777418619 
 Quadrupole "QUAD.CR2.QS3C" -len 0.5 -K1L -0.24686896328832697 -refen $refen 
 Drift "CR2.DS3C_C" -len 0.5 
 Quadrupole "QUAD.CR2.QS4C" -len 0.5 -K1L 0.24225644224167997 -refen $refen 
 Bpm "BPM.CR2_1.17" -len 0 
 Drift "CR2.DS4C" -len 0.2407160001 
 Drift "CR2.BC1" -len 0.4 
 Drift "CR2.BC2" -len 0.4 
 Drift "CR2.BC3" -len 0.4 
 Drift "CR2.BC1" -len 0.4 
 Drift "CR2.DS4C" -len 0.2407160001 
 Quadrupole "QUAD.CR2.QS4C" -len 0.5 -K1L 0.24225644224167997 -refen $refen 
 Drift "CR2.DS3C_C" -len 0.5 
 Quadrupole "QUAD.CR2.QS3C" -len 0.5 -K1L -0.24686896328832697 -refen $refen 
 Bpm "BPM.CR2_1.18" -len 0 
 Drift "CR2.DS3C_B" -len 2.777418619 
 Sextupole "SEXT.CR2.SXS3C" -len 0.1 -K2L -0.42652765870388576 -refen $refen 
 Drift "CR2.DS3C_A" -len 0.3 
 Quadrupole "QUAD.CR2.QS2C" -len 0.5 -K1L 0.7905004870236609 -refen $refen 
 Bpm "BPM.CR2_1.19" -len 0 
 Drift "CR2.DS2C" -len 0.2003907272 
 Quadrupole "QUAD.CR2.QS1C" -len 0.5 -K1L -0.647356904204382 -refen $refen 
 Bpm "BPM.CR2_1.20" -len 0 
 Drift "CR2.DS1C_B" -len 0.3 
 Sextupole "SEXT.CR2.SXS1C" -len 0.1 -K2L 2.1606775839716965 -refen $refen 
 Drift "CR2.DS1C_A" -len 2.179474654 
 Marker "CR2MDACELLSTART" 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 ####################### 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SN1.7" -len 0.1 -K2L 2.061155942197774 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Marker "CR2.DBA.HBM" 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -K2L 0.6398802696390732 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SN3.7" -len 0.1 -K2L -2.476615612181143 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SNN3.7" -len 0.1 -K2L -0.0912251185775646 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -K2L 0.6398804489179475 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SNN1.7" -len 0.1 -K2L -4.004626056401545 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.03992586439999999 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.08046148038247242 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.28064251000565993 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.08046148038247242 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SNN1.7" -len 0.1 -K2L -4.0046569703472334 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -K2L 0.6398853885125978 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SNN3.7" -len 0.1 -K2L -0.09122584835416526 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SN3.7" -len 0.1 -K2L -2.476635424445033 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -K2L 0.6398855677886037 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SN1.7" -len 0.1 -K2L 2.061173008378598 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 ######################################## 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Marker "CR2MDACELLEND" 
 Drift "CR2.DTR1A" -len 1.1 
 Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
 Drift "CR2.DTR1B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
 Bpm "BPM.CR2_1.7" -len 0 
 Drift "CR2.DTR2A" -len 1.1 
 Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.846716805555538 -refen $refen 
 Drift "CR2.DTR2B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
 Bpm "BPM.CR2_1.8" -len 0 
 Drift "CR2.DTR3A" -len 1.5 
 Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458447 -refen $refen 
 Drift "CR2.DTR3B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
 Bpm "BPM.CR2_1.9" -len 0 
 Drift "CR2.DTR4A" -len 2.1328 
 Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
 Drift "CR2.DTR4B" -len 0.2 
 Quadrupole "QUAD.CR2.QTR4" -len 0.25 -K1L 0.18700585960992677 -refen $refen 
 Bpm "BPM.CR2_1.10" -len 0 
 Quadrupole "QUAD.CR2.QTR4" -len 0.25 -K1L 0.18700585960992677 -refen $refen 
 Bpm "BPM.CR2_1.11" -len 0 
 Drift "CR2.DTR4B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
 Drift "CR2.DTR4A" -len 2.1328 
 Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
 Bpm "BPM.CR2_1.12" -len 0 
 Drift "CR2.DTR3B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458447 -refen $refen 
 Drift "CR2.DTR3A" -len 1.5 
 Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
 Bpm "BPM.CR2_1.13" -len 0 
 Drift "CR2.DTR2B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.846716805555538 -refen $refen 
 Drift "CR2.DTR2A" -len 1.1 
 Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
 Bpm "BPM.CR2_1.14" -len 0 
 Drift "CR2.DTR1B" -len 0.2 
 Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
 Drift "CR2.DTR1A" -len 1.1 
 Marker "CR2MDACELLSTART" 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 ####################### 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SN1.8" -len 0.1 -K2L 4.737798500969112 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Marker "CR2.DBA.HBM" 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -K2L 0.5490523803282401 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SN3.8" -len 0.1 -K2L -2.686372176642733 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SNN3.8" -len 0.1 -K2L -0.6081300062640962 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -K2L 0.5490525341519675 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SNN1.8" -len 0.1 -K2L -3.431439866013065 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
 Drift "CR2.DBA.DM1" -len 1.96118337075000 
 Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
 Drift "CR2.DBA.DM0" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
 Drift "CR2.DBA.D11B1" -len 0.3424874764 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.03992586439999999 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sextupole "SEXT.CR2.DBA.SNN1.8" -len 0.1 -K2L -3.431466353942955 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -K2L 0.5490567723917731 -refen $refen 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SNN3.8" -len 0.1 -K2L -0.6081348709083396 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -K1L -0.11881360000000002 -refen $refen 
 Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
 Sextupole "SEXT.CR2.DBA.SN3.8" -len 0.1 -K2L -2.6863936658716976 -refen $refen 
 Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
 Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
 Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -K2L 0.5490569262130396 -refen $refen 
 Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
 Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
 Drift "CR2.DBA.DS1C_B" -len 0.109313245 
 Sextupole "SEXT.CR2.DBA.SN1.8" -len 0.1 -K2L 4.737837727620266 -refen $refen 
 Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
 Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 ######################################## 
 Drift "CR2.DBA.D12" -len 0.2697746031 
 Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215879999999999 -refen $refen 
 Drift "CR2.DBA.D11B" -len 0.1628018956 
 Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864919 -refen $refen 
 Drift "CR2.DBA.D11A" -len 0.1730305995 
 Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
 Marker "CR2MDACELLEND" 
 #### Missing length (0.202692445399988) #### 
 Drift "CR2.MISSLENGTH" -len 0.202692445399988 
 ######################## 
 Drift "CR2.DL0" -len 1.246504682 
 Quadrupole "QUAD.CR2.QL0IN.IN1" -len 0.5 -K1L -0.3536192234913272 -refen $refen 
 Drift "CR2.DL0IN" -len 0.5 
 Quadrupole "QUAD.CR2.QL1IN.IN1" -len 0.5 -K1L 0.9809117348720798 -refen $refen 
 Bpm "BPM.CR2_2.63" -len 0 
 Drift "CR2.DL1IN" -len 0.4000469975 
 Quadrupole "QUAD.CR2.QL2IN.IN1" -len 0.5 -K1L -0.030500513033999792 -refen $refen 
 Bpm "BPM.CR2_2.64" -len 0 
 Drift "CR2.DL2IN" -len 1.103409345 
 Quadrupole "QUAD.CR2.QL3IN.IN1" -len 0.5 -K1L -0.2614385592457404 -refen $refen 
 Bpm "BPM.CR2_2.65" -len 0 
 Drift "CR2.DL3IN" -len 0.4000389758 
 #### RF CORRECTOR #### 
 ## 0.1357244419 mm delay from oscillations in the orbit 
 ## Machine will have to take this into consideration but its easier to fix here for RF 
 Drift "CR2.RFK1" -len 0 
 #Kalign al1 -cz 0 
 Kalign al1 -fixed 1 -cz -0.0001357244419 
 Drift "CR2.al1" -len 0 -node 1 -kick al1 
 Drift "DRRF" -len 1 
 Rfmultipole "RFMulti.CR2.RFKICK1" -len 0 -strength [expr -0.004*$refen] -freq [expr 0.9995*3.] -phase [expr -$Pi/2] -refen $refen 
 Drift "DRRF" -len 1 
 ###################### 
 Drift "CR2.DRFB1" -len 0.217192186 
 Quadrupole "QUAD.CR2.QRFB1.IN1" -len 0.5 -K1L -0.22582311699328744 -refen $refen 
 Drift "CR2.DRFB2" -len 1.193240969 
 Quadrupole "QUAD.CR2.QRFB2.IN1" -len 0.5 -K1L 0.39708610074064277 -refen $refen 
 Drift "CR2.DRFB3" -len 3.771233433 
 Quadrupole "QUAD.CR2.QRFB3.IN1" -len 0.5 -K1L -0.08240914765491558 -refen $refen 
 Drift -name "CR2.D1CM" -len 0.01 
 Drift "CR2.D1M" -len 1 
 Sextupole "SEXT.CR2.SRFB4.IN1" -len 0.5 -K2L -4.717961074837193 -refen $refen 
 Drift "CR2.DS1" -len 0.5 
 Sextupole "SEXT.CR2.SRFB5.IN1" -len 0.5 -K2L 2.9954518485987762 -refen $refen 
 Drift "CR2.DS1" -len 0.5 
 Drift "CR2.DRFB4A" -len 0.709135479 
 Sextupole "SEXT.CR2.SRFB6.IN1" -len 0.4 -K2L 3.0040021747304624 -refen $refen 
 Drift "CR2.DRFB6" -len 2.367622162 
 Drift "CR2.SEPTUM" -len 1 
 #### Beamline under construction #################################### 
 #### Injection needs to be done into after the sextupole section #### 
 Drift "CR2.SEPTUM" -len 1 
 Drift "CR2.DRFB6" -len 2.367622162 
 Sextupole "SEXT.CR2.SRFB6.IN2" -len 0.4 -K2L -3.4377743532350404 -refen $refen 
 Drift "CR2.DRFB4A" -len 0.709135479 
 Drift "CR2.DS1" -len 0.5 
 Sextupole "SEXT.CR2.SRFB5.IN2" -len 0.5 -K2L 0.29120223716942567 -refen $refen 
 Drift "CR2.DS1" -len 0.5 
 Sextupole "SEXT.CR2.SRFB4.IN2" -len 0.5 -K2L 1.4264823718127457 -refen $refen 
 ##################################################################### 
 Marker "CR2_P2.EXTRACTION" 
 
