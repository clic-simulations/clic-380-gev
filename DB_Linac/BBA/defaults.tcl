## constants
set clight 299792458.0
set Z0 376.730313461771
set pi [expr acos(-1.)]
set eC 1.6021766e-19
set mc2 0.510998910e-3

# #  some file names

set script_dir "../files"



array set args {
    sigma  200.0
    sigmap 200.0
    bpmres 10.0
    dphase1 20
    dphase2 0
    dgrad1 10
    dgrad2 0
    dcharge 0.8
    machine all
    nm 10
    wgt1 -1
    wgt2 -1
    beta0 5
    beta1 6
    beta2 6
    nbins 1
    noverlap 0.0
}   

array set args $argv

set nm $args(nm)
set sigma $args(sigma)
set sigmap $args(sigmap)
set bpmres $args(bpmres)
set machine $args(machine)
set dphase1 $args(dphase1)
set dphase2 $args(dphase2)
set dgrad1 $args(dgrad1)
set dgrad2 $args(dgrad2)
set dcharge $args(dcharge)
set wgt1 $args(wgt1)
set wgt2 $args(wgt2)
set beta0 $args(beta0)
set beta1 $args(beta1)
set beta2 $args(beta2)
set nbins $args(nbins)
set noverlap $args(noverlap)


set outfiles outfiles

# if {[file exist $outfiles]} {
#     exec rm -rf $outfiles
#     exec mkdir -p $outfiles
# } else {
    exec mkdir -p $outfiles
# }

cd $outfiles


# load related octave and tcl scripts

source $script_dir/dbl_structure_parameters.tcl

Octave {
    scriptdir="$script_dir/";
    source([scriptdir,"generate_bunch.m"]);
    source([scriptdir,"tools_oct_17.m"]);
    source([scriptdir,"wake_init.m"]);
}

source $script_dir/wake_init.tcl
source $script_dir/make_beam.tcl
source $script_dir/match_dec_17.tcl

#########################################################################################
# change font color
#----------------------------------------------------------------------------------------
proc color {foreground text} {
    return [exec tput setaf $foreground] $text[exec tput sgr0]
}

# print ELLAPSED time
proc print_time {t0 } {
    set ttot [expr [clock clicks -milliseconds] - $t0]
    set sfac  [expr 1000]
    set mfac  [expr $sfac*60]
    set hfac  [expr $mfac*60]

    set hour [expr floor($ttot/$hfac)]
    set ttot [expr $ttot-$hour*$hfac]
    set minu [expr floor($ttot/$mfac)]
    set ttot [expr $ttot-$minu*$mfac]
    set seco [expr floor($ttot/$sfac)]
    set ttot [expr $ttot-$seco*$sfac]
    set ms   [expr $ttot]

    puts "\n******* ELLAPSED TIME **************"
    puts [format "*   %4.0fh:%4.0fm:%4.0fs:%4.0fms       *"   $hour $minu $seco $ms]
    puts "************************************"
}

