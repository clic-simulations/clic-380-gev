#
# This code was developed by C. Gohil from a simulation inherited from
# N. Blaskovic Kraljevic.
#

# Create new folder to hold results
set name results
exec mkdir -p $name
set script_dir [pwd]
puts "Using pwd: [pwd]"
cd $name

Random -type gaussian gcav

# Initial beam
source $script_dir/clic_beam.tcl
set nm [lindex $argv 0]; # Number of machines to perform BBA on
set n_slice 25
set n_part 9
set n_total 500
set match(n_total) $n_total

set e_initial 9.0
set e0 $e_initial

set charge $match(charge)

# ML cavity paramters
set phase 8
set frac_lambda 0.25
set scale 1.0
source $script_dir/clic_basic_single.tcl

# Old lattice
#source $script_dir/lattice4a.tcl
#Linac::choose_linac $script_dir/phase.def $phase
#Linac::put_linac
#TclCall -script {save_beam}
#BeamlineSet -name test

# New lattice with 574 quads
array set match {
    alpha_x -8.4047e-17
    beta_x 8.09953
    alpha_y -2.80157e-17
    beta_y 1.19147
}
BeamlineNew
source $script_dir/1000_db_ml.tcl
TclCall -script {save_beam}
BeamlineSet -name test

WriteGirderLength -file girder.data -beginning_only 1

set cav0 [CavityGetPhaseList]
set grad0 [CavityGetGradientList]

# Generate beams
make_beam_slice_energy_gradient beam0 $n_slice $n_part 1.0 1.0
make_beam_slice_energy_gradient beam0a 1 1 1.0 1.0
make_beam_slice_energy_gradient beam1 $n_slice $n_part 1.0 0.9
make_beam_slice_energy_gradient beam2a $n_slice $n_part 0.95 1.0

set match(sigma_z) 60.0
make_beam_slice_energy_gradient beam2 $n_slice $n_part 0.95 1.0
make_beam_slice_energy_gradient beam3 $n_slice $n_part 0.95 0.9
make_beam_slice_energy_gradient beam3a 1 1 0.95 0.9
make_beam_slice_energy_gradient beam4 $n_slice $n_part 0.95 0.8

#
# Main BBA tracking
#
set method dfs
set wgt 1000
set bin_iter 3
foreach {sname} {
    phase8_nm1_nslice25
} {
    proc save_beam {} {
        puts "Saving premisaligment..."
        SaveAllPositions -file premisaligment
    }
    TestNoCorrection -beam beam0 -emitt_file $sname.premisalignment -survey Zero -machines $nm

    foreach err {everything_nojitter} {
        # straight girder_scatter girder_flo roll bpm cav tilt res real jitter everything everything_nojitter none
        set mm $sname.$err
        set jitter 0.0
        set phase_err 0.0
        set gradient_err 0.0
        set bpm_scale 0.0
        #survey_data_reset
        #set survey_scale 1.0
        #set survey_scale 0.0
	
        switch $err {
            straight {
                set bpm_res 0.0
            }
            none {
                # Added to compare with straight (Neven, 191118)
                set bpm_res 0.0
                set jitter 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0 \
                    -cavity_yp 0.0 \
            }
            cav {
                # CLIC CDR (Neven, 220318)
                set bpm_res 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 14.0 \
                    -cavity_realign_y 0 \
                    -cavity_yp 0.0 \
            }
            real {
                # CLIC CDR (Neven, 220318)
                set bpm_res 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 3.5 \
                    -cavity_yp 0.0 \
            }
            quad {
                set bpm_res 0.0
                SurveyErrorSet -quadrupole_y 20.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0 \
                    -cavity_yp 0.0 \
            }
            bpm {
                # CLIC CDR (Neven, 220318)
                set bpm_res 0.0
                set jitter 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 14.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0 \
                    -cavity_yp 0.0 \
            }
            res {
                # CLIC CDR (Neven, 220318)
                set bpm_res 0.1
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0 \
                    -cavity_yp 0.0 \
            }
            tilt {
                # CLIC CDR (Neven, 220318)
                set bpm_res 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0 \
                    -cavity_yp 141.0
            }
            all {
                set bpm_res 0.1
                SurveyErrorSet -quadrupole_y 20.0 \
                    -bpm_y 20.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y [expr 20.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            everything {
                # CLIC CDR (Neven, 200318)
                set bpm_res 0.1
                set jitter 0.1
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 14.0 \
                            -quadrupole_roll 100.0 \
                    -cavity_y 14.0 \
                    -cavity_realign_y 3.5 \
                    -cavity_yp 141
            }
            everything_nojitter {
                # For DS (Neven, 030418)
                set bpm_res 0.1
                set jitter 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 14.0 \
                            -quadrupole_roll 100.0 \
                    -cavity_y 14.0 \
                    -cavity_realign_y 3.5 \
                    -cavity_yp 141
            }
            girder {
                # CLIC CDR (Neven, 220318)
                set bpm_res 0.0
                set jitter 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0.0 \
                    -cavity_yp 0.0
            }
            girder2 {
                set bpm_res 0.0
                set jitter 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0.0 \
                    -cavity_yp 0.0
            }
            girder3 {
                set bpm_res 0.0
                set jitter 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0.0 \
                    -cavity_yp 0.0
            }
            girder_scatter {
                # CLIC CDR (Neven, 230318)
                set bpm_res 0.0
                set jitter 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0.0 \
                    -cavity_yp 0.0
            }
            girder_flo {
                # CLIC CDR (Neven, 230318)
                set bpm_res 0.0
                set jitter 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0.0 \
                    -cavity_yp 0.0
            }
            everything_scale {
                set bpm_res 0.1
                set jitter 0.1
                set bpm_scale 0.01
                SurveyErrorSet -quadrupole_y 17.0 \
                    -bpm_y 14.0 \
                            -quadrupole_roll 100 \
                    -cavity_y [expr 20.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            everything_scale2 {
                set bpm_res 0.1
                set jitter 0.1
                set bpm_scale 0.02
                SurveyErrorSet -quadrupole_y 17.0 \
                    -bpm_y 14.0 \
                            -quadrupole_roll 100 \
                    -cavity_y [expr 20.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            full {
                set bpm_res 0.1
                set jitter 0.1
                SurveyErrorSet -quadrupole_y 17.0 \
                    -quadrupole_roll 100.0 \
                    -bpm_y 14.0 \
                    -cavity_y [expr 7.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            roll {
                # CLIC CDR (Neven, 220318)
                set bpm_res 0.0
                set jitter 0.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -quadrupole_roll 100.0 \
                    -bpm_y 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0.0 \
                    -cavity_yp 0.0
            }
            tous_null {
                set bpm_res 0.1
                set jitter 0.1
                SurveyErrorSet -quadrupole_y 20.0 \
                    -bpm_y 20.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y [expr 20.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            tous_phase {
                set bpm_res 0.1
                set jitter 0.1
                set phase_err 6.0
                SurveyErrorSet -quadrupole_y 20.0 \
                    -bpm_y 20.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y [expr 20.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            tous_phase1 {
                set bpm_res 0.1
                set jitter 0.1
                set phase_err 1.0
                SurveyErrorSet -quadrupole_y 20.0 \
                    -bpm_y 20.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y [expr 20.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            tous_phase2 {
                set bpm_res 0.1
                set jitter 0.1
                set phase_err 2.0
                SurveyErrorSet -quadrupole_y 20.0 \
                    -bpm_y 20.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y [expr 20.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            tous_gradient {
                set bpm_res 0.1
                set jitter 0.1
                set gradient_err 0.002
                SurveyErrorSet -quadrupole_y 20.0 \
                    -bpm_y 20.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y [expr 20.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            tous {
                set bpm_res 0.1
                set jitter 0.1
                set phase_err 6.0
                set gradient_err 0.002
                SurveyErrorSet -quadrupole_y 20.0 \
                    -bpm_y 20.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y [expr 20.0/sqrt(2.0)] \
                    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
                    -cavity_yp [expr 200.0/sqrt(2.0)]
            }
            jitter {
                # Jitter updated to match BPM resolution in res (Neven, 220318)
                set bpm_res 0.0
                set jitter 0.1
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 0.0 \
                    -cavity_realign_y 0 \
                    -cavity_yp 0.0
            }

            jitter01 {
                set bpm_res 0.1
                set jitter 0.1
                SurveyErrorSet -quadrupole_y 14.0 \
                    -bpm_y 14.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 14.0 \
                    -cavity_realign_y 14.0 \
                    -cavity_yp 141.0
            }
            jitter2 {
                set bpm_res 0.0
                set jitter -1.0
                SurveyErrorSet -quadrupole_y 0.0 \
                    -bpm_y 0.0 \
                            -quadrupole_roll 0.0 \
                    -cavity_y 10.0 \
                    -cavity_realign_y 0 \
                    -cavity_yp 0.0
            }
            default {
                puts "I do not know misalignment method $err"
                exit
            }
        }
        
        proc my_survey {} {
            global i err
            global cav0 grad0
            global phase_err gradient_err bpm_scale
            
            Clic
            if {$err=="full"} {
                InterGirderMove -scatter_y 12.0 -flo_y 5.0
            }
            if {$err=="everything"} {
                InterGirderMove -scatter_y 12.0 -flo_y 5.0
            }
            if {$err=="everything_nojitter"} {
                InterGirderMove -scatter_y 12.0 -flo_y 5.0
            }
            if {$err=="girder"} {
                InterGirderMove -scatter_y 12.0 -flo_y 5.0
            }
            if {$err=="girder2"} {
                InterGirderMove -scatter_y 12.0 -flo_y 0.0
            }
            if {$err=="girder3"} {
                InterGirderMove -scatter_y 0.0 -flo_y 5.0
            }
            if {$err=="girder_scatter"} {
                InterGirderMove -scatter_y 12.0 -flo_y 0.0
            }
            if {$err=="girder_flo"} {
                InterGirderMove -scatter_y 0.0 -flo_y 5.0
            }
            
            #apply_survey_data_switch girder.data

            set cav {}
            set grad {}
            set fcav [open cav.dat.$i w]
            foreach c $cav0 g $grad0 {
                set cc [expr $c+[gcav]*$phase_err]
                set gg [expr $g+[gcav]*$gradient_err]
                puts $fcav "$gg $cc"
                lappend cav $cc
                lappend grad $gg
            }
            close $fcav
            CavitySetPhaseList $cav
            CavitySetGradientList $grad
            SaveAllPositions -file buc.$i -cav_bpm 1
            puts "misalignment done"
        }

        proc save_beam {} {
            global i
            puts "Saving b121.$i..."
            SaveAllPositions -file b121.$i -cav_bpm 1
            incr i
        }

        set i 0
        Zero
        CavitySetPhaseList $cav0
        CavitySetGradientList $grad0
        TestSimpleCorrection -beam beam0 -emitt_file $mm.121 -survey my_survey -machines $nm -bpm_res $bpm_res

        proc save_beam {} {
            global i
            puts "Saving bl.$i..."
            BpmRealign
            SaveAllPositions -file bl.$i -cav_bpm 1
            incr i
        }
        
        proc my_survey {} {
            global i
            puts "Running my_survey for switch method, i=$i..."
            global cav0 bpm_scale
            puts "Reading b121.$i..."
            ReadAllPositions -file b121.$i -cav_bpm 1

            SetBpmScaleError -sigma_y $bpm_scale
            set fcav [open cav.dat.$i r]
            set cav ""
            set grad ""
            foreach c $cav0 {
                gets $fcav cc
                lappend cav [lindex $cc 1]
                lappend grad [lindex $cc 0]
            }
            close $fcav
            CavitySetPhaseList $cav
            CavitySetGradientList $grad
        }

        global i
        set i 0
        switch $method {
            none {
                TestNoCorrection -beam beam0 -emitt_file $mm.zero -survey Zero -machines $nm
                set i 0
                TestNoCorrection -beam beam0 -emitt_file $mm.survey -survey my_survey -machines $nm
        return
            }
            nlc {
                TestSimpleAlignment -beam beam0 -emitt_file $mm \
                    -survey Nlc -jitter_y $jitter \
                    -machines $nm -position_wgt 0.01 -binlength 36 \
                    -binoverlap 18 -bpm_res $bpm_res
            }
            ball {
                TestBallisticCorrection -beam beam0 -emitt_file $mm \
                    -survey Nlc -jitter_y $jitter \
                    -machines $nm -binlength 12 -bpm_res $bpm_res
                # Added (Neven, 130318):
                EnergySpreadPlot -beam beam0 -file espread
                return
            }
            dfs {
                puts "Running dfs: $mm.$wgt..."
                set mm $mm.$wgt
                Zero
                CavitySetPhaseList $cav0
                CavitySetGradientList $grad0
                TestMeasuredCorrection \
                    -beam0 beam0 -beam1 beam3 -cbeam0 beam0a -cbeam1 beam3a\
                    -jitter_y [expr 1.0*$jitter] \
                    -emitt_file $mm.$method -survey my_survey -machines $nm \
                    -wgt1 $wgt -bin_iteration $bin_iter -correct_full_bin 1 \
                    -binlength 36 -binoverlap 18 -bpm_res $bpm_res
                puts "dfs complete"
            }
            dfs2 {
                set mm $method.$wgt
                Zero
                CavitySetPhaseList $cav0
                CavitySetGradientList $grad0
                set qtmp0 [QuadrupoleGetStrengthList]
                set qtmp1 ""
                foreach qq $qtmp0 {
                    lappend qtmp1 $qq
                }
                TestMeasuredCorrection \
                    -beam0 beam0 -beam1 beam0 \
                    -jitter_y [expr 1.0*$jitter] \
                    -quad0 $qtmp0 -quad1 -$qtmp1 \
                    -emitt_file $mm.$err -survey my_survey -machines $nm \
                    -wgt1 10000 -bin_iteration $bin_iter -correct_full_bin 1 \
                    -binlength 36 -binoverlap 18 -bpm_res $bpm_res
            }
            default {
                puts "I do not know correction method $method"
                exit
            }
        }
        
        proc save_beam {} {
            global i
            puts "Saving bx.$i..."
            SaveAllPositions -file bx.$i -cav_bpm 1
            BeamSaveAll -file beam.$i
            incr i
        }
        
        proc my_survey {} {
            global j
            puts "Running my_survey for TestRFAlignment, j=$j..."
            global cav0
            puts "Reading bl.$j..."
            ReadAllPositions -file bl.$j -cav_bpm 1

            set fcav [open cav.dat.$j r]
            set cav ""
            set grad ""
            foreach c $cav0 {
                gets $fcav cc
                lappend cav [lindex $cc 1]
                lappend grad [lindex $cc 0]
            }
            close $fcav
            CavitySetPhaseList $cav
            CavitySetGradientList $grad
            incr j
        }
        
        Zero

        CavitySetPhaseList $cav0
        CavitySetGradientList $grad0
        global i j
        set i 0
        set j 0
        puts "Running TestRfAlignment: $mm.rf..."
        TestRfAlignment -beam beam0 -emitt_file $mm.rf -machines $nm \
            -survey my_survey -testbeam beam0
    }
}
