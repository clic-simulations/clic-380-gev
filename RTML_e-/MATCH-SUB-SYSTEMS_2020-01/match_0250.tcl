# CLIC Main Beam RTML
# This script assumes Scripts/create_wake.tcl has been run.

set tcl_precision 15
ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set wakedir    ./Wakefields
set beamdir    ./Beams

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
if {$beamparams(usewakefields)==1} {
   source $paramsdir/rf_parameters_ww.tcl
}
create_particles_file $beamdir/particles.in beamparams
source $scriptdir/beamline_and_wake.tcl
make_particle_beam beam0 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

Octave {                                                                          
    disp("Tracking to matching section");                                           
    [E, B] = placet_test_no_correction("0010", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0020", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0030", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0040", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0050", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0060", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0070", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0080", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0090", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0100", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0110", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0120", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0130", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0140", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0150", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0160", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0170", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0180", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0190", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0200", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0210", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0220", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0230", "beam0", "None");
    placet_set_beam("beam0", B);
    [E, B] = placet_test_no_correction("0240", "beam0", "None");
    placet_set_beam("beam0", B);
}

# Target twiss parameters at the end of the matching section
array set twiss1 {
    beta_x   19.1344706573962
    beta_y   48.7420059716351
    alpha_x  0.205912699573941
    alpha_y  -1.67763258853367
}

# Matching
Octave {
    pkg load optim;
    format long;

    global Correctors1 = placet_get_name_number_list("0250","matching-sbend");

    global twiss1 = zeros(4,4);
    twiss1(1:2,1:2) = [  $twiss1(beta_x), -($twiss1(alpha_x)); -($twiss1(alpha_x)), (1+($twiss1(alpha_x))*($twiss1(alpha_x)))/($twiss1(beta_x)) ];
    twiss1(3:4,3:4) = [  $twiss1(beta_y), -($twiss1(alpha_y)); -($twiss1(alpha_y)), (1+($twiss1(alpha_y))*($twiss1(alpha_y)))/($twiss1(beta_y)) ];

    function merit = TwissMatching(beamline)
        global Correctors;                  
        global Correctors1;
        global Correctors2;
        global twiss1;
                      
        Sbends = placet_element_get_attribute("0250",Correctors1,"e0")
                
        [E, B] = placet_test_no_correction("0250", "beam0", "None"); 
        emitt   = placet_get_emittance(B)
        twiss   = placet_get_twiss_matrix(B);
        beta_x  = twiss(1,1)/twiss1(1,1)     
        beta_y  = twiss(3,3)/twiss1(3,3)
        alpha_x = twiss(1,2)/twiss1(1,2)
        alpha_y = twiss(3,4)/twiss1(3,4)
        x       = mean(B(:,2))          
        y       = mean(B(:,3))    
        xp      = mean(B(:,5))    
        yp      = mean(B(:,6))    
        sz      = std(B(:,4))
                                
        #merit = sum((1.0-emitt./[7.7, 0.057]).**2);
        #merit = sum((1.0 - [beta_x, beta_y]).**2);
        #merit = sum((1.0 - [alpha_x, alpha_y]).**2);
        merit = x**2 + y**2 + xp**2 + yp**2 + sum((1.0 - [beta_x, beta_y]).**2) + 1e2*sum((1.0-emitt./[7.7, 0.057]).**2);
 
    endfunction
               
    Leverages1   = ["e0"; "e0"];
    Constraints1 = [8.9 9.1; 8.9 9.1];

    Correctors  = Correctors1;
    Leverages   = Leverages1;
    Constraints = Constraints1;

    [optimum, merit] = placet_optimize_constraint("0250", "TwissMatching",
                                                  Correctors, Leverages, Constraints);

    #[E, B] = placet_test_no_correction("0250", "beam0", "None"); 
    #emitt   = placet_get_emittance(B)
    #twiss   = placet_get_twiss_matrix(B);
    #beta_x  = twiss(1,1)/twiss1(1,1)     
    #beta_y  = twiss(3,3)/twiss1(3,3)
    #alpha_x = twiss(1,2)/twiss1(1,2)
    #alpha_y = twiss(3,4)/twiss1(3,4)
    #x       = mean(B(:,2))          
    #y       = mean(B(:,3))    
    #xp      = mean(B(:,5))    
    #yp      = mean(B(:,6))    
    #sz      = std(B(:,4))
}
