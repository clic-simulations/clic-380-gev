##------> Author: Raul Costa [raul.costa@cern.ch]  (Dec 2023 version) <------------------##
## Placet2 lattice generated from Eduardo Marin's design for Placet1
## Later adapted to be defined by K1L/K2L instead of S1/S2
## Sextupole strength optimised for low emittance/T566
## Low energy version for 380GeV stage
#~ if {$Dipole_synrad} {
#~ set refen 1.905993977 
#~ } else { 
#~ set refen 1.906
#~ } 
## Final Element (SeptQ) result=DL.SEPTQ	4.430489288	-0.01929310768	-0.01334974458	8.206053748e-05	
Kalign DL_inj -fixed 1 -cx 0.01929310768 -cxp 0.01334974458
Marker "DL_LONG.CENTERINJ" -len 0. -nodes 1 -kick DL_inj 
#~ Sbend "BEND.SEPTQ" -len 0.500001425441655 -angle -0.005849061169 -E1 -0.002924530584 -E2 -0.002924530584 -K1L -0.5000007127 -refen $refen 
#~ set refen [expr $refen-14.1e-6*-0.005849061169*-0.005849061169/0.500001425441655*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DL1IN" -len 1.1 
Sbend "BEND.SEPT1" -len 1.00063486103645 -angle -0.0872664626 -E1 -0.0436332313 -E2 -0.0436332313 -K1L 0.9956481989 -refen $refen 
set refen [expr $refen-14.1e-6*-0.0872664626*-0.0872664626/1.00063486103645*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DL2IN" -len 0.3 
Quadrupole "QUAD.QL1IN" -len 0.4 -K1L -0.647036 -refen $refen 
Drift "DL3IN" -len 0.4 
Sbend "BEND.SEPT2" -len 2.58202788155376 -angle -0.2714151852 -E1 -0.1357075926 -E2 -0.1357075926 -K1L -1.249456954 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2714151852*-0.2714151852/2.58202788155376*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D33" -len 0.192971 
Quadrupole "QUAD.QL2IN" -len 0.4 -K1L 1.14802 -refen $refen 
Drift "D32A" -len 0.11111 
Sextupole "SEXT.DL.SXL2" -len 0.1 -K2L -1.6036571002551785 -refen $refen 
Drift "D32B" -len 0.43333 
Quadrupole "QUAD.QL3IN" -len 0.4 -K1L -1.943224 -refen $refen 
Drift "D33A" -len 0.0464855 
Sextupole "SEXT.DL.SXL3" -len 0.1 -K2L 4.259153048146563 -refen $refen 
Drift "D33B" -len 0.0464855 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3CA" -len 0.08511145 
Sextupole "SEXT.DL.SXL4" -len 0.1 -K2L -4.5495950373050045 -refen $refen 
Drift "DS3CB" -len 0.08511145 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -K2L 1.0546342022452768 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -K2L -3.0283710034602973 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981251 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981251 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SN2" -len 0.1 -K2L -3.0283710034602973 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SN3" -len 0.1 -K2L 1.0546342022452768 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -K2L 1.0546342022452768 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -K2L -3.0283710034602973 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981254 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981254 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SN2" -len 0.1 -K2L -3.0283710034602973 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SN3" -len 0.1 -K2L 1.0546342022452768 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -K2L 1.0546342022452768 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -K2L -3.028371003460297 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981251 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981251 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -K2L 3.0283710034602973 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964997999999 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -K2L -1.054634202245277 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -K2L -1.054634202245277 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -K2L 3.0283710034602973 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981254 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981254 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -K2L 3.0283710034602978 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -K2L -1.0546342022452768 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -K2L -1.0546342022452768 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -K2L 3.0283710034602973 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981251 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981251 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -K2L 3.0283710034602973 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -K2L -1.0546342022452768 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -K2L -1.0546342022452768 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964997999999 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -K2L 3.0283710034602973 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981251 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981251 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -K2L 3.0283710034602973 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -K2L -1.054634202245277 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -K2L -1.054634202245277 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -K2L 3.0283710034602973 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981254 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981254 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -K2L 3.028371003460297 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -K2L -1.0546342022452768 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -K2L -1.0546342022452768 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -K2L 3.028371003460297 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981251 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981251 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -K2L 3.0283710034602973 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -K2L -1.0546342022452768 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -K2L -1.0546342022452768 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -K1L 0.9511964997999999 -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -K2L 3.0283710034602973 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -K2L -5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -K1L 0.04695405543000001 -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -K2L -1.7331554089981254 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981254 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SN2" -len 0.1 -K2L -3.0283710034602973 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SN3" -len 0.1 -K2L 1.0546342022452766 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -K2L 1.0546342022452766 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -K2L -3.0283710034602973 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543000001 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981251 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981251 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SN2" -len 0.1 -K2L -3.0283710034602978 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SN3" -len 0.1 -K2L 1.0546342022452768 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -K2L 1.0546342022452768 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -K2L -3.0283710034602973 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981254 -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L 1.7331554089981254 -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SNN1" -len 0.1 -K2L 5.401429829762652 -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SNN2" -len 0.1 -K2L -3.0283710034602973 -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SNN3" -len 0.1 -K2L 1.0546342022452768 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3CB" -len 0.08511145 
Sextupole "SEXT.DL.SXL4" -len 0.1 -K2L -4.5495950373050045 -refen $refen 
Drift "DS3CA" -len 0.08511145 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D33B" -len 0.0464855 
Sextupole "SEXT.DL.SXL3" -len 0.1 -K2L 4.259153048146563 -refen $refen 
Drift "D33A" -len 0.0464855 
Quadrupole "QUAD.QL3IN" -len 0.4 -K1L -1.943224 -refen $refen 
Drift "D32B" -len 0.43333 
Sextupole "SEXT.DL.SXL2" -len 0.1 -K2L -1.603657100255179 -refen $refen 
Drift "D32A" -len 0.11111 
Quadrupole "QUAD.QL2IN" -len 0.4 -K1L 1.14802 -refen $refen 
Drift "D33" -len 0.192971 
Sbend "BEND.SEPT2" -len 2.58202788155376 -angle -0.2714151852 -E1 -0.1357075926 -E2 -0.1357075926 -K1L -1.249456954 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2714151852*-0.2714151852/2.58202788155376*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DL3IN" -len 0.4 
Quadrupole "QUAD.QL1IN" -len 0.4 -K1L -0.647036 -refen $refen 
Drift "DL2IN" -len 0.3 
Sbend "BEND.SEPT1" -len 1.00063486103645 -angle -0.0872664626 -E1 -0.0436332313 -E2 -0.0436332313 -K1L 0.9956481989 -refen $refen 
set refen [expr $refen-14.1e-6*-0.0872664626*-0.0872664626/1.00063486103645*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DL1IN" -len 1.1 
#~ Sbend "BEND.SEPTQ" -len 0.500001425441655 -angle -0.005849061169 -E1 -0.002924530584 -E2 -0.002924530584 -K1L -0.5000007127 -refen $refen 
#~ set refen [expr $refen-14.1e-6*-0.005849061169*-0.005849061169/0.500001425441655*$refen*$refen*$refen*$refen*$Dipole_synrad] 
## Final Element (SeptQ) result=DL.SEPTQ	4.430489288	-0.01929310768	-0.01334974458	8.206053748e-05	
Kalign DL_ext -fixed 1 -cx -0.01929310768 -cxp 0.01334974458 
Marker "DL_LONG.CENTEREXT" -len 0. -nodes 1 -kick DL_ext 
