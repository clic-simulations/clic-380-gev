## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
 ## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
 ## Which means there will be a slight magnet missfocus for the other bunches. 
 if {$Dipole_synrad} { 
 set refen 1.905300698 
 } else { 
 set refen 1.906 
 } 
 ####START#### 
 Marker "TL2.INJECTION" 
 ### The beam exits CR1 with an offset in the combiner ring's referencial, 
 ### This kicker puts sets the referencial so that the beam is centered (use fixed for multibunch studies) 
 #Kalign TL2offset -cx 0 -cxp 0 -cz 0 
 Kalign TL2offset -fixed 1 -cx -0.01719855576 -cxp 0.0001020892625 -cz -1.761748864e-05 
 #Kalign TL2offset -fixed 1 -cx -0.017198 -cxp 1.1931e-04 -cz 1.1760e-05 
 Marker "TL2_offset" -nodes 1 -kick TL2offset 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 2.005723015 
 Sbend "BEND.TL2.BEND10" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D122" -len 0.667347296 
 Sextupole "SEXT.TL2.SX125" -len 0.3 -K2L 2.0263244664256375 -refen $refen 
 Drift "TL2.D128" -len 0.258673648 
 Quadrupole "QUAD.TL2.Q13" -len 0.75 -K1L 0.5040779068400361 -refen $refen 
 Bpm "BPM.TL2.1" -len 0 
 Drift "TL2.D142" -len 0.1747711281 
 Sextupole "SEXT.TL2.SX145" -len 0.3 -K2L -2.8559221190506503 -refen $refen 
 Drift "TL2.D148" -len 0.1747711281 
 Quadrupole "QUAD.TL2.Q15" -len 0.75 -K1L -0.5757477388026118 -refen $refen 
 Bpm "BPM.TL2.2" -len 0 
 Drift "TL2.D162" -len 0.2366852486 
 Drift "TL2.D168" -len 0.2366852486 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 0.500013974 
 Sbend "BEND.TL2.BEND17" -len 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D182" -len 0.05000058615 
 Sextupole "SEXT.TL2.SX185" -len 0.3 -K2L -0.48046659359216654 -refen $refen 
 Drift "TL2.D188" -len 0.05000058615 
 Quadrupole "QUAD.TL2.Q20A" -len 0.375 -K1L 0.2449152218713822 -refen $refen 
 Bpm "BPM.TL2.3" -len 0 
 Marker "TL2.MIDCELLONE" 
 Quadrupole "QUAD.TL2.Q20B" -len 0.375 -K1L 0.2449152218713822 -refen $refen 
 Bpm "BPM.TL2.4" -len 0 
 Drift "TL2.D222" -len 0.1536564076 
 Sextupole "SEXT.TL2.SX225" -len 0.3 -K2L -1.5782888816505378 -refen $refen 
 Drift "TL2.D228" -len 0.1536564076 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 0.500013974 
 Sbend "BEND.TL2.BEND23" -len 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D242" -len 0.0500000253 
 Drift "TL2.D248" -len 0.0500000253 
 Quadrupole "QUAD.TL2.Q25" -len 0.75 -K1L -0.5757480317779982 -refen $refen 
 Bpm "BPM.TL2.5" -len 0 
 Drift "TL2.D262" -len 0.1562568231 
 Sextupole "SEXT.TL2.SX265" -len 0.3 -K2L 6.962759461581642 -refen $refen 
 Drift "TL2.D268" -len 0.1562568231 
 Quadrupole "QUAD.TL2.Q27" -len 0.75 -K1L 0.504078163345463 -refen $refen 
 Bpm "BPM.TL2.6" -len 0 
 Drift "TL2.D282" -len 0.3136946693 
 Sextupole "SEXT.TL2.SX285" -len 0.3 -K2L -7.11200518205087 -refen $refen 
 Drift "TL2.D288" -len 0.7773893387 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 2.005723015 
 Sbend "BEND.TL2.BEND30" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Bpm "BPM.TL2.D302" -len 0.4980886966 
 Sextupole "SEXT.TL2.SXC305" -len 0.3 -K2L 10.056534304784421 -refen $refen 
 Bpm "BPM.TL2.D308" -len 0.06602956553 
 Quadrupole "QUAD.TL2.Q31" -len 0.75 -K1L 0.21647130599420328 -refen $refen 
 Bpm "BPM.TL2.9" -len 0 
 Bpm "BPM.TL2.D312" -len 0.9874829865 
 Sextupole "SEXT.TL2.SXC315" -len 0.3 -K2L -4.028591803471267 -refen $refen 
 Bpm "BPM.TL2.D318" -len 0.2291609955 
 Quadrupole "QUAD.TL2.Q32" -len 0.75 -K1L -0.49537237654850036 -refen $refen 
 Bpm "BPM.TL2.12" -len 0 
 Bpm "BPM.TL2.D322" -len 1.245610625 
 Sextupole "SEXT.TL2.SXC325" -len 0.3 -K2L -4.4388966119525 -refen $refen 
 Bpm "BPM.TL2.D328" -len 0.3152035415 
 Quadrupole "QUAD.TL2.Q33" -len 0.75 -K1L 0.7089964008874374 -refen $refen 
 Bpm "BPM.TL2.15" -len 0 
 Bpm "BPM.TL2.D332" -len 1.58013019 
 Sextupole "SEXT.TL2.SXC335" -len 0.3 -K2L -28.574457205814515 -refen $refen 
 Bpm "BPM.TL2.D338" -len 0.4267100632 
 Quadrupole "QUAD.TL2.Q34" -len 0.75 -K1L -0.44754543299542593 -refen $refen 
 Bpm "BPM.TL2.18" -len 0 
 Bpm "BPM.TL2.D342" -len 1.673352339 
 Sextupole "SEXT.TL2.SXC345" -len 0.3 -K2L -4.576755930314475 -refen $refen 
 Bpm "BPM.TL2.D348" -len 0.457784113 
 Quadrupole "QUAD.TL2.Q35" -len 0.75 -K1L 0.5707531209017225 -refen $refen 
 Bpm "BPM.TL2.21" -len 0 
 Bpm "BPM.TL2.D352" -len 0.457784113 
 Sextupole "SEXT.TL2.SXC355" -len 0.3 -K2L -4.576755930314475 -refen $refen 
 Bpm "BPM.TL2.D358" -len 1.673352339 
 Quadrupole "QUAD.TL2.Q36" -len 0.75 -K1L -0.44754543299542593 -refen $refen 
 Bpm "BPM.TL2.24" -len 0 
 Bpm "BPM.TL2.D362" -len 0.4267100632 
 Sextupole "SEXT.TL2.SXC365" -len 0.3 -K2L -28.574457205814515 -refen $refen 
 Bpm "BPM.TL2.D368" -len 1.58013019 
 Quadrupole "QUAD.TL2.Q37" -len 0.75 -K1L 0.7089964008874374 -refen $refen 
 Bpm "BPM.TL2.27" -len 0 
 Bpm "BPM.TL2.D372" -len 0.3152035415 
 Sextupole "SEXT.TL2.SXC375" -len 0.3 -K2L -4.4388966119525 -refen $refen 
 Bpm "BPM.TL2.D378" -len 1.245610625 
 Quadrupole "QUAD.TL2.Q38" -len 0.75 -K1L -0.49537237654850036 -refen $refen 
 Bpm "BPM.TL2.30" -len 0 
 Bpm "BPM.TL2.D382" -len 0.2291609955 
 Sextupole "SEXT.TL2.SXC385" -len 0.3 -K2L -4.028591803471267 -refen $refen 
 Bpm "BPM.TL2.D388" -len 0.9874829865 
 Quadrupole "QUAD.TL2.Q39" -len 0.75 -K1L 0.21647130599420328 -refen $refen 
 Bpm "BPM.TL2.33" -len 0 
 Bpm "BPM.TL2.D392" -len 0.06602956553 
 Sextupole "SEXT.TL2.SXC395" -len 0.3 -K2L 10.056534304784421 -refen $refen 
 Bpm "BPM.TL2.D398" -len 0.4980886966 
 Marker "TL2.INTEROMSEND" 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 2.005723015 
 Sbend "BEND.TL2.BEND40" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D422" -len 0.7773893387 
 Sextupole "SEXT.TL2.SX425" -len 0.3 -K2L 7.098868394091101 -refen $refen 
 Drift "TL2.D428" -len 0.3136946693 
 Quadrupole "QUAD.TL2.Q43" -len 0.75 -K1L 0.5040846790677052 -refen $refen 
 Bpm "BPM.TL2.36" -len 0 
 Drift "TL2.D442" -len 0.1562568231 
 Sextupole "SEXT.TL2.SX445" -len 0.3 -K2L -3.102952640289646 -refen $refen 
 Drift "TL2.D448" -len 0.1562568231 
 Quadrupole "QUAD.TL2.Q45" -len 0.75 -K1L -0.5757554739060834 -refen $refen 
 Bpm "BPM.TL2.37" -len 0 
 Drift "TL2.D462" -len 0.0500000253 
 Drift "TL2.D468" -len 0.0500000253 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 0.500013974 
 Sbend "BEND.TL2.BEND47" -len 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D482" -len 0.1536564076 
 Sextupole "SEXT.TL2.SX485" -len 0.3 -K2L -0.9345875539379843 -refen $refen 
 Drift "TL2.D488" -len 0.1536564076 
 Quadrupole "QUAD.TL2.Q50A" -len 0.375 -K1L 0.24491851227620587 -refen $refen 
 Bpm "BPM.TL2.38" -len 0 
 Marker "TL2.MIDCELLTWO" 
 Quadrupole "QUAD.TL2.Q50B" -len 0.375 -K1L 0.24491851227620587 -refen $refen 
 Bpm "BPM.TL2.39" -len 0 
 Drift "TL2.D522" -len 0.1536564076 
 Sextupole "SEXT.TL2.SX525" -len 0.3 -K2L 2.7073460215777136 -refen $refen 
 Drift "TL2.D528" -len 0.1536564076 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 0.500013974 
 Sbend "BEND.TL2.BEND53" -len 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D542" -len 0.0500000253 
 Drift "TL2.D548" -len 0.0500000253 
 Quadrupole "QUAD.TL2.Q55" -len 0.75 -K1L -0.5757557668735978 -refen $refen 
 Bpm "BPM.TL2.40" -len 0 
 Drift "TL2.D562" -len 0.1562568231 
 Sextupole "SEXT.TL2.SX565" -len 0.3 -K2L 3.3670342547460144 -refen $refen 
 Drift "TL2.D568" -len 0.1562568231 
 Quadrupole "QUAD.TL2.Q57" -len 0.75 -K1L 0.50408493556624 -refen $refen 
 Bpm "BPM.TL2.41" -len 0 
 Drift "TL2.D582" -len 0.3136946693 
 Sextupole "SEXT.TL2.SX585" -len 0.3 -K2L -2.4500758422584603 -refen $refen 
 Drift "TL2.D588" -len 0.7773893387 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 2.005723015 
 Sbend "BEND.TL2.BEND60" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Marker "TL2.OMSEND" 
 Drift "TL2.D71" -len 0.8 
 Quadrupole "QUAD.TL2.Q72" -len 0.75 -K1L 0.7165680367936128 -refen $refen 
 Bpm "BPM.TL2.42" -len 0 
 Drift "TL2.D73" -len 0.8 
 Quadrupole "QUAD.TL2.Q74" -len 0.75 -K1L -0.7000235069383508 -refen $refen 
 Bpm "BPM.TL2.43" -len 0 
 Drift "TL2.D75" -len 1.6 
 Quadrupole "QUAD.TL2.Q76" -len 0.75 -K1L 0.6030743956353652 -refen $refen 
 Bpm "BPM.TL2.44" -len 0 
 Drift "TL2.D77" -len 0.8 
 Quadrupole "QUAD.TL2.Q78" -len 0.75 -K1L -0.2606155769240615 -refen $refen 
 Bpm "BPM.TL2.45" -len 0 
 Drift "TL2.D79" -len 0.8 
 #### Start of FODO main sequence ### 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.70" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.71" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.72" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.73" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.74" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.75" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.76" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.77" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.78" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.79" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.80" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.81" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.82" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.83" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.84" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.85" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.86" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.87" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.88" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.89" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.90" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.91" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.92" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.93" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.94" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.95" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.96" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.97" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.98" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.99" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.100" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.101" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.102" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.103" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.104" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.105" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.106" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.107" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.108" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.109" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.110" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.111" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.112" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.113" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.114" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.115" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.116" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.117" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.118" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.119" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.120" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.121" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.122" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.123" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Marker "TL2.MFODO" 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.124" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -K1L -0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.125" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -K1L 0.29999999999999993 -refen $refen 
 Bpm "BPM.TL2.126" -len 0 
 Drift "TL2.DLFODO" -len 3.25 
 Drift "TL2.D91" -len 0.8 
 Quadrupole "QUAD.TL2.Q92" -len 0.75 -K1L -0.9124435264740295 -refen $refen 
 Bpm "BPM.TL2.127" -len 0 
 Drift "TL2.D93" -len 0.8 
 Quadrupole "QUAD.TL2.Q94" -len 0.75 -K1L 0.7834140767926763 -refen $refen 
 Bpm "BPM.TL2.128" -len 0 
 Drift "TL2.D95" -len 1.2 
 Quadrupole "QUAD.TL2.Q96" -len 0.75 -K1L -0.7596474469880714 -refen $refen 
 Bpm "BPM.TL2.129" -len 0 
 Drift "TL2.D97" -len 0.8 
 Quadrupole "QUAD.TL2.Q98" -len 0.75 -K1L 0.21844939048490042 -refen $refen 
 Bpm "BPM.TL2.130" -len 0 
 Drift "TL2.D99" -len 0.8 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 2.005723015 
 Sbend "BEND.TL2.BEND110" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D1122" -len 1.60479140 
 Sextupole "SEXT.TL2.SX1125" -len 0.3 -K2L 1.8089964672394454 -refen $refen 
 Drift "TL2.D1128" -len 0.727395700 
 Quadrupole "QUAD.TL2.Q113" -len 0.75 -K1L 0.42911011041355884 -refen $refen 
 Bpm "BPM.TL2.131" -len 0 
 Drift "TL2.D1142" -len 0.2203857562 
 Sextupole "SEXT.TL2.SX1145" -len 0.3 -K2L 0.3320845765660412 -refen $refen 
 Drift "TL2.D1148" -len 0.2203857562 
 Quadrupole "QUAD.TL2.Q115" -len 0.75 -K1L -0.5104849819752184 -refen $refen 
 Bpm "BPM.TL2.132" -len 0 
 Drift "TL2.D1162" -len 0.05000000025 
 Drift "TL2.D1168" -len 0.05000000025 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 0.5000086579 
 Sbend "BEND.TL2.BEND117" -len 0.500017315941417 -angle 0.02038562618 -E1 0.01019281309 -E2 0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*0.02038562618*0.02038562618/0.500017315941417*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D1182" -len 0.1905674726 
 Sextupole "SEXT.TL2.SX1185" -len 0.3 -K2L -4.694014263194243 -refen $refen 
 Drift "TL2.D1188" -len 0.1905674726 
 Quadrupole "QUAD.TL2.Q120A" -len 0.375 -K1L 0.20351471015138278 -refen $refen 
 Bpm "BPM.TL2.133" -len 0 
 Marker "TL2.MIDCELL11" 
 Quadrupole "QUAD.TL2.Q120B" -len 0.375 -K1L 0.20351471015138278 -refen $refen 
 Bpm "BPM.TL2.134" -len 0 
 Drift "TL2.D1222" -len 0.1905674726 
 Sextupole "SEXT.TL2.SX1225" -len 0.3 -K2L 3.105297017400643 -refen $refen 
 Drift "TL2.D1228" -len 0.1905674726 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 0.5000086579 
 Sbend "BEND.TL2.BEND123" -len 0.500017315941417 -angle 0.02038562618 -E1 0.01019281309 -E2 0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*0.02038562618*0.02038562618/0.500017315941417*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D1242" -len 0.05000000025 
 Drift "TL2.D1248" -len 0.05000000025 
 Quadrupole "QUAD.TL2.Q125" -len 0.75 -K1L -0.5104851433929286 -refen $refen 
 Bpm "BPM.TL2.135" -len 0 
 Drift "TL2.D1262" -len 0.2203857562 
 Sextupole "SEXT.TL2.SX1265" -len 0.3 -K2L -0.026795124638435922 -refen $refen 
 Drift "TL2.D1268" -len 0.2203857562 
 Quadrupole "QUAD.TL2.Q127" -len 0.75 -K1L 0.42911024610016385 -refen $refen 
 Bpm "BPM.TL2.136" -len 0 
 Drift "TL2.D1282" -len 0.727395700 
 Sextupole "SEXT.TL2.SX1285" -len 0.3 -K2L 0.030222640202408613 -refen $refen 
 Drift "TL2.D1288" -len 1.60479140 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 2.005723015 
 Sbend "BEND.TL2.BEND130" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Bpm "BPM.TL2.D1302" -len 0.1024133036 
 Sextupole "SEXT.TL2.SXC1305" -len 0.3 -K2L 1.4342714841442439 -refen $refen 
 Bpm "BPM.TL2.D1308" -len 0.1024133036 
 Quadrupole "QUAD.TL2.Q131" -len 0.75 -K1L -0.6431324372962983 -refen $refen 
 Bpm "BPM.TL2.139" -len 0 
 Bpm "BPM.TL2.D1312" -len 0.0911315578 
 Sextupole "SEXT.TL2.SXC1315" -len 0.3 -K2L -8.287651194602185 -refen $refen 
 Bpm "BPM.TL2.D1318" -len 0.0911315578 
 Quadrupole "QUAD.TL2.Q132" -len 0.75 -K1L 0.8176585037618992 -refen $refen 
 Bpm "BPM.TL2.142" -len 0 
 Bpm "BPM.TL2.D1322" -len 1.292423292 
 Sextupole "SEXT.TL2.SXC1325" -len 0.3 -K2L -0.9185139681012899 -refen $refen 
 Bpm "BPM.TL2.D1328" -len 0.330807764 
 Quadrupole "QUAD.TL2.Q133" -len 0.75 -K1L -0.02174474928342903 -refen $refen 
 Bpm "BPM.TL2.145" -len 0 
 Bpm "BPM.TL2.D1332" -len 0.480449320 
 Sextupole "SEXT.TL2.SXC1335" -len 0.3 -K2L -0.09500141447115633 -refen $refen 
 Bpm "BPM.TL2.D1338" -len 1.741347962 
 Quadrupole "QUAD.TL2.Q134" -len 0.75 -K1L 0.6382142984803992 -refen $refen 
 Bpm "BPM.TL2.148" -len 0 
 Bpm "BPM.TL2.D1342" -len 0.1086306923 
 Sextupole "SEXT.TL2.SXC1345" -len 0.3 -K2L 5.454474778565421 -refen $refen 
 Bpm "BPM.TL2.D1348" -len 0.6258920768 
 Quadrupole "QUAD.TL2.Q135" -len 0.75 -K1L -0.7372074408272478 -refen $refen 
 Bpm "BPM.TL2.151" -len 0 
 Bpm "BPM.TL2.D1352" -len 0.6258920768 
 Sextupole "SEXT.TL2.SXC1355" -len 0.3 -K2L -4.96177578225521 -refen $refen 
 Bpm "BPM.TL2.D1358" -len 0.1086306923 
 Quadrupole "QUAD.TL2.Q136" -len 0.75 -K1L 0.3917670154145769 -refen $refen 
 Bpm "BPM.TL2.154" -len 0 
 Bpm "BPM.TL2.D1362" -len 1.741347962 
 Sextupole "SEXT.TL2.SXC1365" -len 0.3 -K2L -16.234605370078153 -refen $refen 
 Bpm "BPM.TL2.D1368" -len 0.480449320 
 Quadrupole "QUAD.TL2.Q137" -len 0.75 -K1L -0.6592113427302688 -refen $refen 
 Bpm "BPM.TL2.157" -len 0 
 Bpm "BPM.TL2.D1372" -len 0.330807764 
 Sextupole "SEXT.TL2.SXC1375" -len 0.3 -K2L 17.706861946036053 -refen $refen 
 Bpm "BPM.TL2.D1378" -len 1.292423292 
 Quadrupole "QUAD.TL2.Q138" -len 0.75 -K1L -0.197559413835395 -refen $refen 
 Bpm "BPM.TL2.160" -len 0 
 Bpm "BPM.TL2.D1382" -len 0.0911315578 
 Sextupole "SEXT.TL2.SXC1385" -len 0.3 -K2L 8.428824401826072 -refen $refen 
 Bpm "BPM.TL2.D1388" -len 0.0911315578 
 Quadrupole "QUAD.TL2.Q139" -len 0.75 -K1L 0.4609441449342395 -refen $refen 
 Bpm "BPM.TL2.163" -len 0 
 Bpm "BPM.TL2.D1392" -len 0.1024133036 
 Sextupole "SEXT.TL2.SXC1395" -len 0.3 -K2L -3.4069734154373204 -refen $refen 
 Bpm "BPM.TL2.D1398" -len 0.1024133036 
 Marker "TL2.INTEROMSEND" 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 2.005723015 
 Sbend "BEND.TL2.BEND140" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D1422" -len 1.60479140 
 Sextupole "SEXT.TL2.SX1425" -len 0.3 -K2L 2.7817747578105254 -refen $refen 
 Drift "TL2.D1428" -len 0.727395700 
 Quadrupole "QUAD.TL2.Q143" -len 0.75 -K1L 0.13216531108804833 -refen $refen 
 Bpm "BPM.TL2.166" -len 0 
 Drift "TL2.D1442" -len 0.2203857562 
 Sextupole "SEXT.TL2.SX1445" -len 0.3 -K2L -2.180417000538101 -refen $refen 
 Drift "TL2.D1448" -len 0.2203857562 
 Quadrupole "QUAD.TL2.Q145" -len 0.75 -K1L -0.5435020204501555 -refen $refen 
 Bpm "BPM.TL2.167" -len 0 
 Drift "TL2.D1462" -len 0.05000000025 
 Drift "TL2.D1468" -len 0.05000000025 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 0.5000086579 
 Sbend "BEND.TL2.BEND147" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D1482" -len 0.1905674726 
 Sextupole "SEXT.TL2.SX1485" -len 0.3 -K2L 0.6860802477519287 -refen $refen 
 Drift "TL2.D1488" -len 0.1905674726 
 Quadrupole "QUAD.TL2.Q150A" -len 0.375 -K1L 0.161217168641821 -refen $refen 
 Bpm "BPM.TL2.168" -len 0 
 Marker "TL2.MIDCELL12" 
 Quadrupole "QUAD.TL2.Q150B" -len 0.375 -K1L 0.161217168641821 -refen $refen 
 Bpm "BPM.TL2.169" -len 0 
 Drift "TL2.D1522" -len 0.0001175 
 Sextupole "SEXT.TL2.SX1525" -len 0.3 -K2L 0.23166156862996262 -refen $refen 
 Drift "TL2.D1528" -len 0.0001175 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 0.5000086579 
 Sbend "BEND.TL2.BEND153" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "TL2.D1542" -len 0.0976775 
 Drift "TL2.D1548" -len 0.0976775 
 Quadrupole "QUAD.TL2.Q155" -len 0.75 -K1L 0.5309523313282983 -refen $refen 
 Bpm "BPM.TL2.170" -len 0 
 Drift "TL2.D1562" -len 0.042885 
 Sextupole "SEXT.TL2.SX1565" -len 0.3 -K2L -0.3335686493232526 -refen $refen 
 Drift "TL2.D1568" -len 0.042885 
 Quadrupole "QUAD.TL2.Q157" -len 0.75 -K1L -0.9763405816507942 -refen $refen 
 Bpm "BPM.TL2.171" -len 0 
 Drift "TL2.D1582" -len 0.8837 
 Sextupole "SEXT.TL2.SX1585" -len 0.3 -K2L -0.06469538389836345 -refen $refen 
 Drift "TL2.D1588" -len 1.9174 
 # WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
 # WARNING: original length was 2.005723015 
 Sbend "BEND.TL2.BEND160" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 ### Offset needed for CR2 Injection (use fixed for multibunch studies) 
 ### b2 going for -0.02286596894359623 -0.0006579938618535864 -0.0001042623843514314 
 #Kalign CR2offset -cx -0.02286596894359623 -cxp -0.0006579938618535864 -cz -0.0001042623843514314 
 Kalign CR2offset -fixed 1 -cx -0.0225759468943962 -cxp -6.22745187753586e-04 -cz -2.54805275751431e-04 
 #Kalign CR2offset -fixed 1 -cx ?? -cxp ?? -cz ?? 
 Drift "CR2_offset" -nodes 1 -kick CR2offset 
 Marker "TL2.EXTRACTION" 
 
