##################################################################
## Placet2 lattice file, "clean" version of TurnAround_auto.tcl
## Author: Raul Costa (raul.costa@cern.ch)
## File generated in 2019-02-11 
##################################################################
Drift	"START_D"	-len	1	
Sbend	"PEM_B1"	-len	1.005115441	-angle	-0.1748804447	-E1	0	-E2	-0.1748804447	
Drift	"PEM_D12"	-len	2.538722248	
Sbend	"PEM_B2"	-len	1.005115441	-angle	0.1748804447	-E1	0.1748804447	-E2	0	
Drift	"PEM_D23"	-len	1	
Sbend	"PEM_B3"	-len	1.005115441	-angle	0.1748804447	-E1	0	-E2	0.1748804447	
Drift	"PEM_D34"	-len	2.538722248	
Sbend	"PEM_B4"	-len	1.005115441	-angle	-0.1748804447	-E1	-0.1748804447	-E2	0	
Drift	"MPT_D1"	-len	0.8922428231	
Quadrupole	"MPT_Q1"	-len	0.3	-K1L	0.02651539578	
Drift	"MPT_D2"	-len	0.7274886118	
Quadrupole	"MPT_Q2"	-len	0.3	-K1L	0.02230027948	
Drift	"MPT_D3"	-len	3.72849019	
Quadrupole	"MPT_Q3"	-len	0.3	-K1L	-0.4377810921	
Drift	"MPT_D4"	-len	0.1125578543	
Quadrupole	"TAL_QL1"	-len	0.3	-K1L	0.05998270813	
Drift	"TAL_DL01"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	
Drift	"TAL_DL02"	-len	1.2	
Quadrupole	"TAL_QL2"	-len	0.3	-K1L	1.009913347	
Drift	"TAL_DL03"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	
Drift	"TAL_DL04"	-len	0.2	
Quadrupole	"TAL_QL3"	-len	0.3	-K1L	-1.091704012	
Drift	"TAL_DL05"	-len	0.35	
Quadrupole	"TAL_QL4"	-len	0.3	-K1L	1.496043936	
Drift	"TAL_DL06"	-len	0.1	
Sextupole	"TAL_SL"	-len	0.1	-K2L	-2.6	
Drift	"TAL_DL07"	-len	1	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	
Drift	"TAL_DL08"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	
Drift	"TAL_DL09"	-len	0.2	
Quadrupole	"TAL_QL5"	-len	0.3	-K1L	-0.2425113337	
Drift	"TAL_DL10"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	
Drift	"TAL_DL11"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	
Drift	"TAL_DL12"	-len	1	
Sextupole	"TAL_SL"	-len	0.1	-K2L	-2.6	
Drift	"TAL_DL13"	-len	0.1	
Quadrupole	"TAL_QL6"	-len	0.3	-K1L	1.496043936	
Drift	"TAL_DL14"	-len	0.35	
Quadrupole	"TAL_QL7"	-len	0.3	-K1L	-1.091704012	
Drift	"TAL_DL15"	-len	0.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	
Drift	"TAL_DL16"	-len	0.2	
Quadrupole	"TAL_QL8"	-len	0.3	-K1L	1.009913347	
Drift	"TAL_DL17"	-len	1.2	
Sbend	"TAL_BL"	-len	0.8005714432	-angle	-0.1308996939	-E1	-0.06544984695	-E2	-0.06544984695	
Drift	"TAL_DL18"	-len	0.2	
Quadrupole	"TAL_QL9"	-len	0.3	-K1L	-0.2190154096	
Drift	"TAL_DM1"	-len	2	
Quadrupole	"TAL_QM1"	-len	0.3	-K1L	-0.3550502363	
Drift	"TAL_DM2"	-len	2.65	
Quadrupole	"TAL_QM2"	-len	0.3	-K1L	0.3991282895	
Drift	"TAL_DM3"	-len	2.65	
Quadrupole	"TAL_QM3"	-len	0.3	-K1L	-0.3574829148	
Drift	"TAL_DM4"	-len	2	
Quadrupole	"TAL_QR1A"	-len	0.3	-K1L	-0.2067952833	
Drift	"TAL_DR01"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR02"	-len	1.2	
Quadrupole	"TAL_QR2"	-len	0.3	-K1L	0.976659716	
Drift	"TAL_DR03"	-len	0.26	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR04"	-len	0.2	
Quadrupole	"TAL_QR3"	-len	0.3	-K1L	-1.046061342	
Drift	"TAL_DR05"	-len	0.33	
Quadrupole	"TAL_QR4"	-len	0.3	-K1L	1.499922127	
Drift	"TAL_DR06"	-len	0.1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	
Drift	"TAL_DR07"	-len	1	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR08"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR09"	-len	0.2	
Quadrupole	"TAL_QR5"	-len	0.3	-K1L	-0.2263675601	
Drift	"TAL_DR10"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR11"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR12"	-len	1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	
Drift	"TAL_DR13"	-len	0.1	
Quadrupole	"TAL_QR6"	-len	0.3	-K1L	1.499922127	
Drift	"TAL_DR14"	-len	0.33	
Quadrupole	"TAL_QR7"	-len	0.3	-K1L	-1.046061342	
Drift	"TAL_DR15"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR16"	-len	0.26	
Quadrupole	"TAL_QR8"	-len	0.3	-K1L	0.976659716	
Drift	"TAL_DR17"	-len	1.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR18"	-len	0.2	
Quadrupole	"TAL_QR9"	-len	0.3	-K1L	-0.2238900167	
Quadrupole	"TAL_QR1"	-len	0.3	-K1L	-0.2238900167	
Drift	"TAL_DR01"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR02"	-len	1.2	
Quadrupole	"TAL_QR2"	-len	0.3	-K1L	0.976659716	
Drift	"TAL_DR03"	-len	0.26	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR04"	-len	0.2	
Quadrupole	"TAL_QR3"	-len	0.3	-K1L	-1.046061342	
Drift	"TAL_DR05"	-len	0.33	
Quadrupole	"TAL_QR4"	-len	0.3	-K1L	1.499922127	
Drift	"TAL_DR06"	-len	0.1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	
Drift	"TAL_DR07"	-len	1	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR08"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR09"	-len	0.2	
Quadrupole	"TAL_QR5"	-len	0.3	-K1L	-0.2263675601	
Drift	"TAL_DR10"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR11"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR12"	-len	1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	
Drift	"TAL_DR13"	-len	0.1	
Quadrupole	"TAL_QR6"	-len	0.3	-K1L	1.499922127	
Drift	"TAL_DR14"	-len	0.33	
Quadrupole	"TAL_QR7"	-len	0.3	-K1L	-1.046061342	
Drift	"TAL_DR15"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR16"	-len	0.26	
Quadrupole	"TAL_QR8"	-len	0.3	-K1L	0.976659716	
Drift	"TAL_DR17"	-len	1.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR18"	-len	0.2	
Quadrupole	"TAL_QR9"	-len	0.3	-K1L	-0.2238900167	
Quadrupole	"TAL_QR1"	-len	0.3	-K1L	-0.2238900167	
Drift	"TAL_DR01"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR02"	-len	1.2	
Quadrupole	"TAL_QR2"	-len	0.3	-K1L	0.976659716	
Drift	"TAL_DR03"	-len	0.26	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR04"	-len	0.2	
Quadrupole	"TAL_QR3"	-len	0.3	-K1L	-1.046061342	
Drift	"TAL_DR05"	-len	0.33	
Quadrupole	"TAL_QR4"	-len	0.3	-K1L	1.499922127	
Drift	"TAL_DR06"	-len	0.1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	
Drift	"TAL_DR07"	-len	1	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR08"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR09"	-len	0.2	
Quadrupole	"TAL_QR5"	-len	0.3	-K1L	-0.2263675601	
Drift	"TAL_DR10"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR11"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR12"	-len	1	
Sextupole	"TAL_SR"	-len	0.1	-K2L	2.3	
Drift	"TAL_DR13"	-len	0.1	
Quadrupole	"TAL_QR6"	-len	0.3	-K1L	1.499922127	
Drift	"TAL_DR14"	-len	0.33	
Quadrupole	"TAL_QR7"	-len	0.3	-K1L	-1.046061342	
Drift	"TAL_DR15"	-len	0.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR16"	-len	0.26	
Quadrupole	"TAL_QR8"	-len	0.3	-K1L	0.976659716	
Drift	"TAL_DR17"	-len	1.2	
Sbend	"TAL_BR"	-len	0.8010162943	-angle	0.1745329252	-E1	0.0872664626	-E2	0.0872664626	
Drift	"TAL_DR18"	-len	0.2	
Quadrupole	"TAL_QR9C"	-len	0.3	-K1L	-0.2406355661	
Drift	"MTB_D1"	-len	7.946950693	
Quadrupole	"MTB_Q1"	-len	0.3	-K1L	0.009953216451	
Drift	"MTB_D2"	-len	7.8206454	
Quadrupole	"MTB_Q2"	-len	0.3	-K1L	-0.3670876175	
Drift	"MTB_D3"	-len	0.1885032606	
Quadrupole	"MTB_Q3"	-len	0.3	-K1L	0.3969302332	
Drift	"MTB_D4"	-len	0.1220094165	
Sbend	"BCPC_B1"	-len	0.7511653817	-angle	-0.09650349724	-E1	0	-E2	-0.09650349724	
Drift	"BCPC_D12"	-len	8.0373968	
Sbend	"BCPC_B2"	-len	0.7511653817	-angle	0.09650349724	-E1	0.09650349724	-E2	0	
Drift	"BCPC_D23"	-len	1	
Sbend	"BCPC_B3"	-len	0.7511653817	-angle	0.09650349724	-E1	0	-E2	0.09650349724	
Drift	"BCPC_D34"	-len	8.0373968	
Sbend	"BCPC_B4"	-len	0.7511653817	-angle	-0.09650349724	-E1	-0.09650349724	-E2	0	
Drift	"END_D"	-len	1	
