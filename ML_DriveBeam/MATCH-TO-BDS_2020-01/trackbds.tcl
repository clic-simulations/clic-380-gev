#
# CLIC 380 GeV BDS
#
ParallelThreads -num 10

# Directories containing files needed by this similation
set script_dir ./scripts
set lattice_dir ./lattices

set scale 1.0
source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl

#
# Parameters
#
set e_initial 190.0
set e0 190.0
set match(emitt_x) 9.5
set match(emitt_y) 0.3
set match(charge) 5.2e9
set match(sigma_z) 70.0
set match(phase) 0.0
set match(e_spread) -1
set match(alpha_x) 0
set match(alpha_y) 0
set match(beta_x) 33.07266007
set match(beta_y) 8.962361942
set charge $match(charge)

#
# Lattice
#
set synrad 1
set quad_synrad 1
set mult_synrad 1
set sbend_synrad 1
set sbend_sixdim 1

set kFQF 0.0734950722
set kFQD -0.0812017998
set kFQF2 -0.0780444232
set kFQD2 0.01289982894
set kTQF 0.1149806684
set kTQD -0.1179863805

# Set up the BDS lattice with L*=4.3 m
BeamlineNew
source $lattice_dir/bds43.tcl
BeamlineSet -name bds43

# Set up the BDS lattice with L*=6 m
BeamlineNew
source $lattice_dir/bds6.tcl
BeamlineSet -name bds6

#
# Beam
#
set n_slice 50
set n 2000
set n_total [expr $n_slice*$n]
make_beam_many beam0 $n_slice $n
#BeamRead -file beam_ml.dat -beam beam0

#
# Tracking
#
FirstOrder 1
Octave {	
    
    # Track the beam through the BDS
    [emitt, beam] = placet_test_no_correction("bds43", "beam0", "None", "%ex %ey %sex %sey %x %y %xp %yp %Env %sx %sy %sxp %syp %E %dE %s %n %name");
    save -text emitt.dat emitt;
    save_beam("particles.out", beam);

    # Display the emittance at the IP
    bds = placet_get_emittance(beam)

    # Display the Twiss parameters at the IP
    twiss = placet_get_twiss_matrix(beam);
    beta_x = twiss(1,1)
    beta_y = twiss(3,3)
    alpha_x = -twiss(1,2)
    alpha_y = -twiss(3,4)
    disp("");

    # Display the offset and angle of the beam at the IP
    x = mean(beam(:,2))
    y = mean(beam(:,3))
    xp = mean(beam(:,5))
    yp = mean(beam(:,6))

}

exit
