set rfparamsbooster(gradientww)   1.834530652708e+07
set rfparamsbc1(gradientww)   2.230412045615e+07
set rfparamsbc2(gradientww)   6.764236132039e+07
set energy_wake_bc1  -3.531056152295e-05
set energy_wake_booster  -5.768493690441e-05
set energy_wake_bc2  -7.702472188307e-05
