# Try to fit rolls
# Assuming matrix RollRespMat is already loaded in load_machine_model.tcl

Octave {
disp("Starting roll correction")
disp("Turning multipoles OFF")
MStemp = placet_element_get_attribute("test", MI, "strength");
placet_element_set_attribute("test", MI, "strength", 0.0);

d = 200; # quadrupoles offset +/- um
inds = [QI, BI];

# Perform correction
for i = 1:1
   yvec = [];

   # Measure response
   for j = 1:length(QI)
      xtemp = placet_element_get_attribute("test", QI(j), "x");
      ytemp = placet_element_get_attribute("test", QI(j), "y");

      # displace quad j in x:
      placet_element_set_attribute("test", QI(j), "x", xtemp+d);
      placet_test_no_correction("test", "beam0t", "None");
      Bpos1 = placet_get_bpm_readings("test");

      placet_element_set_attribute("test", QI(j), "x", xtemp-d);
      placet_test_no_correction("test", "beam0t", "None");
      Bpos2 = placet_get_bpm_readings("test");
      placet_element_set_attribute("test", QI(j), "x", xtemp);
 
      dx = (Bpos1(:,1) - Bpos2(:,1));
      dy = (Bpos1(:,2) - Bpos2(:,2));
      yvec = [yvec; dx; dy];

      # displace quad j in y:
      placet_element_set_attribute("test", QI(j), "y", ytemp+d);
      placet_test_no_correction("test", "beam0t", "None");
      Bpos1 = placet_get_bpm_readings("test");
   
      placet_element_set_attribute("test", QI(j), "y", ytemp-d);
      placet_test_no_correction("test", "beam0t", "None");
      Bpos2 = placet_get_bpm_readings("test");
      placet_element_set_attribute("test", QI(j), "y", ytemp);
 
      dx = (Bpos1(:,1) - Bpos2(:,1));
      dy = (Bpos1(:,2) - Bpos2(:,2));
      yvec = [yvec; dx; dy];         
   end

   
   # Fit to find BPM and quadrupole rolls:
   ROLLS = pinv(RollRespMat)*yvec
   [ROLLS, placet_element_get_attribute("test", inds, "roll")];

   disp("Correcting quadrupole and BPM rolls")
   # Correct quadrupole rolls
   quad_rolls = placet_element_get_attribute("test", QI, "roll");
   placet_element_set_attribute("test", QI, "roll", quad_rolls - ROLLS(1:length(QI)));

   # BPM rolls
   BPM_rolls = placet_element_get_attribute("test", BI, "roll");
   placet_element_set_attribute("test", BI(2:end), "roll", BPM_rolls(2:end) - ROLLS(length(QI)+2:end));
end
disp("Turning multipoles ON")
placet_element_set_attribute("test", MI, "strength", MStemp);
}
