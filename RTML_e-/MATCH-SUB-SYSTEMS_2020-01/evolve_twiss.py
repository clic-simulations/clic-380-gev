from optics import gamma, quadrupole_matrix, evolve_twiss_matrix
from numpy import matrix

#
# BC2 RF
#
bx2 = 8.65865
by2 = 2.5356
ax2 = -6.0008e-17
ay2 = 0
gx2 = gamma(ax2, bx2)
gy2 = gamma(ay2, by2)

k = 1.12602538753347
l = 0.2

t2 = matrix([bx2, ax2, gx2, by2, ay2, gy2]).T
M = evolve_twiss_matrix(quadrupole_matrix(k, l))
t1 = M.I*t2

print 't1 =', t1
print 't2 =', M*t1

#
# ML Cavity
#
bx2 = 8.09953
by2 = 1.19147
ax2 = -8.4047e-17
ay2 = -2.80157e-17
gx2 = gamma(ax2, bx2)
gy2 = gamma(ay2, by2)

k = 6.1827894/(9*0.43)
l = 0.215

t2 = matrix([bx2, ax2, gx2, by2, ay2, gy2]).T
M = evolve_twiss_matrix(quadrupole_matrix(k, l))
t1 = M.I*t2

print 't1 =', t1
print 't2 =', M*t1
