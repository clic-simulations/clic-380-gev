#
# matching of BC2 RF to BC2 chicane 1
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_bc2_rf_to_chicane_1 {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 100
    set quad_synrad   0

    set lquadm 0.3

    # set kqm1  0.6260896377748464
    # set kqm2 -1.1455472889573088
    # set kqm3  0.4128132094569047
    # set kqm4 -0.2872950092257217
    # set kqm5  1.9483056072681648
    # set kqm6 -2.02952949380762

    # set kqm1 1.0978
    # set kqm2 -0.82369
    # set kqm3 1.08438
    # set kqm4 -0.81562
    # set kqm5 0.99802
    # set kqm6 2.4785

    # set ldm1 3.8509
    # set ldm2 4.4969
    # set ldm3 6.1085
    # set ldm4 4.555
    # set ldm5 3.3911
    # set ldm6 4.45
    # set ldm7 2.1356

    # set kqm1 1.6632
    # set kqm2 -2.2957
    # set kqm3 3.0661
    # set kqm4 -0.52144
    # set kqm5 2.2948
    # set kqm6 -1.27
    # set kqm7 0.55624

    # set ldm1 0.66462
    # set ldm2 7.6222
    # set ldm3 0.29831
    # set ldm4 0.038013
    # set ldm5 8.6876
    # set ldm6 1.7417
    # set ldm7 6.5672
    # set ldm8 5.3896

    # set kqm1 1.9805
    # set kqm2 -2.3204
    # set kqm3 3.0415
    # set kqm4 -0.73092
    # set kqm5 2.1846
    # set kqm6 -1.1503
    # set kqm7 0.71314
    # set ldm1 1.4628
    # set ldm2 5.2076
    # set ldm3 0.46718
    # set ldm4 0.14189
    # set ldm5 10.3815
    # set ldm6 3.0589
    # set ldm7 3.8264
    # set ldm8 5.8292

    set kqm1 2.0074
    set kqm2 -2.2007
    set kqm3 3.0368
    set kqm4 -0.88395
    set kqm5 2.2424
    set kqm6 -1.0532
    set kqm7 0.66859
    set ldm1 1.2984
    set ldm2 5.3372
    set ldm3 0.53595
    set ldm4 0.29497
    set ldm5 8.7479
    set ldm6 3.696
    set ldm7 3.9284
    set ldm8 7.4935


    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
    Drift -name "Drift220" -length $ldm1
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy]
    Dipole -name "D220-1"
    Drift -name "Drift220" -length $ldm2
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy]
    Dipole -name "D220-2"
    Drift -name "Drift220" -length $ldm3
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy]
    Dipole -name "D220-3"
    Drift -name "Drift220" -length $ldm4
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy]
    Dipole -name "D220-4"
    Drift -name "Drift220" -length $ldm5
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm5*$lquadm*$refenergy]
    Dipole -name "D220-5"
    Drift -name "Drift220" -length $ldm6
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm6*$lquadm*$refenergy]
    Dipole -name "D220-6"
    Drift -name "Drift220" -length $ldm7
    Bpm
    Quadrupole -name "Q220" -length $lquadm -strength [expr $kqm7*$lquadm*$refenergy]
    Dipole -name "D220-7"
    Drift -name "Drift220" -length $ldm8
}
