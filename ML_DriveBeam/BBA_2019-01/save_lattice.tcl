#
# Generates a lattice file with displaced elements after tuning
#

BeamlineNew
source 1000_db_ml.tcl
BeamlineSet -name ml

set save_all_file [lindex $argv 0]

ReadAllPositions -file ./results/$save_all_file -cav_bpm 1

BeamlineList -file lattice.$save_all_file.tcl
