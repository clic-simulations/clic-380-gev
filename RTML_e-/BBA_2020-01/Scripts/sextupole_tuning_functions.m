%
% Contains functions used to perform sextupole tuning.
%
% Notes:
% - The files X_*-<machine>.dat contain the amount to displace the sextupoles
%   from their current position.
% - The offset of the sextupoles is set in the tracking scripts Main/sext_*.tcl.
%

function [ex, ey] = read_emittance(filename)
    % reads emittance from plain tracking and adds an error
    E = load(filename).E;
    ex = E(end,2)*(1 + 0.01*randn());
    ey = E(end,6)*(1 + 0.01*randn());
end

function demitt = correction1(X)
    global machine;
    save(['X_ca-' num2str(machine) '.dat'], 'X');
    success = system(['placet -s Main/sext_ca.tcl machine ' num2str(machine)]);
    if(success == 0)
        [ex, ey] = read_emittance(['Emitt0-CA-rtml-30-' num2str(machine) '.dat']);
        demitt = ex/7.0 + ey/0.05;
    else
        % there was an error in tracking
        demitt = 100000000;
    end
end

function demitt = correction2(X)
    global machine;
    save(['X_tal1-' num2str(machine) '.dat'], 'X');
    success = system(['placet -s Main/sext_tal1.tcl machine ' num2str(machine)]);
    if(success == 0)
        [ex, ey] = read_emittance(['Emitt0-TAL1-rtml-30-' num2str(machine) '.dat']);
        demitt = ex/7.0 + ey/0.05;
    else
        % there was an error in tracking
        demitt = 100000000;
    end
end

function demitt = correction3(X)
    global machine;
    save(['X_tal2-' num2str(machine) '.dat'], 'X');
    success = system(['placet -s Main/sext_tal2.tcl machine ' num2str(machine)]);
    if(success == 0)
        [ex, ey] = read_emittance(['Emitt0-TAL2-rtml-30-' num2str(machine) '.dat']);
        demitt = ex/7.0 + ey/0.05;
    else
        % there was an error in tracking
        demitt = 100000000;
    end
end

function demitt = correction4(Xh)
    global machine;
    X = load(['X_tal1-' num2str(machine) '.dat']).X;
    X(1:5) = 5*Xh; % only optimize horizontal offset
    save(['X_tal1-' num2str(machine) '.dat'], 'X');
    success = system(['placet -s Main/sext_tal1_all.tcl machine ' num2str(machine)]);
    if(success == 0)
        [ex, ey] = read_emittance(['Emitt0-ALL-rtml-30-' num2str(machine) '.dat']);
        demitt = ex/7.0;
    else
        % there was an error in tracking
        demitt = 100000000;
    end
end

function demitt = correction5(Xh)
    global machine;
    X = load(['X_tal1-' num2str(machine) '.dat']).X;
    X(6:10) = 5*Xh; % only optimize vertical offset
    save(['X_tal1-' num2str(machine) '.dat'], 'X');
    success = system(['placet -s Main/sext_tal1_all.tcl machine ' num2str(machine)]);
    if(success == 0)
        [ex, ey] = read_emittance(['Emitt0-ALL-rtml-30-' num2str(machine) '.dat']);
        demitt = ey/0.05;
    else
        % there was an error in tracking
        demitt = 100000000;
    end
end

function demitt = correction6(Xh)
    global machine;
    X = load(['X_tal2-' num2str(machine) '.dat']).X;
    X(1:5) = 5*Xh; % only optimize horizontal offset
    save(['X_tal2-' num2str(machine) '.dat'], 'X');
    success = system(['placet -s Main/sext_tal2_all.tcl machine ' num2str(machine)]);
    if(success == 0)
        [ex, ey] = read_emittance(['Emitt0-ALL-rtml-30-' num2str(machine) '.dat']);
        demitt = ex/7.0;
    else
        % there was an error in tracking
        demitt = 100000000;
    end
end

function demitt = correction7(Xh)
    global machine;
    X = load(['X_tal2-' num2str(machine) '.dat']).X;
    X(6:10) = 5*Xh; % only optimize vertical offset
    save(['X_tal2-' num2str(machine) '.dat'], 'X');
    success = system(['placet -s Main/sext_tal2_all.tcl machine ' num2str(machine)]);
    if(success == 0)
        [ex, ey] = read_emittance(['Emitt0-ALL-rtml-30-' num2str(machine) '.dat']);
        demitt = ey/0.05;
    else
        % there was an error in tracking
        demitt = 100000000;
    end
end
