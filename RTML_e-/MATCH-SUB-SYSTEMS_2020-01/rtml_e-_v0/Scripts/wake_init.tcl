## constants
set clight 299792458.0
set Z0 376.730313461771

Octave {
    function Wt = W_transv(s,a,g,l,delta,delta_g) % V/pC/m/m

        ax1=a;
        gx1=g;
        s01=0.169*ax1^1.79*gx1^0.38*l^-1.17;
        s01b=s01/(pi*ax1^4);
       
        ax2=a*(1.0-0.5*delta);
        gx2=g-0.5*delta_g;
        s02=0.169*ax2^1.79*gx2^0.38*l^-1.17;
        s02b=s02/(pi*ax2^4);
       
        ax3=a*(1.0+0.5*delta);
        gx3=g+0.5*delta_g;
        s03=0.169*ax3^1.79*gx3^0.38*l^-1.17;
        s03b=s03/(pi*ax3^4);
       
        ax4=a*(1.0-delta);
        gx4=g-delta_g;
        s04=0.169*ax4^1.79*gx4^0.38*l^-1.17;
        s04b=0.5*s04/(pi*ax4^4);
       
        ax5=a*(1.0+delta);
        gx5=g+delta_g;
        s05=0.169*ax5^1.79*gx5^0.38*l^-1.17;
        s05b=0.5*s05/(pi*ax5^4);

        ss01=sqrt(s/s01);
        ss02=sqrt(s/s02);
        ss03=sqrt(s/s03);
        ss04=sqrt(s/s04);
        ss05=sqrt(s/s05);


        Wt = 0.25 * 4 * $Z0 * $clight * ( s01b*(1.0-(1.0+ss01).*exp(-ss01))+...
                                     s02b*(1.0-(1.0+ss02).*exp(-ss02))+...
                                     s03b*(1.0-(1.0+ss03).*exp(-ss03))+...
                                     s04b*(1.0-(1.0+ss04).*exp(-ss04))+...
                                     s05b*(1.0-(1.0+ss05).*exp(-ss05))) / 1e12;

    endfunction

    function Wl = W_long(s,a,g,l,delta,delta_g) % V/pC/m

        ax1=a;
        ax1b=1/(pi*ax1*ax1);
        gx1=g;
        s01=0.41*ax1^1.8*gx1^1.6*l^-2.4;

        ax2=a*(1.0-0.5*delta);
        ax2b=1/(pi*ax2*ax2);
        gx2=g-0.5*delta_g;
        s02=0.41*ax2^1.8*gx2^1.6*l^-2.4;

        ax3=a*(1.0+0.5*delta);
        ax3b=1/(pi*ax3*ax3);
        gx3=g+0.5*delta_g;
        s03=0.41*ax3^1.8*gx3^1.6*l^-2.4;

        ax4=a*(1.0-delta);
        ax4b=0.5/(pi*ax4*ax4);
        gx4=g-delta_g;
        s04=0.41*ax4^1.8*gx4^1.6*l^-2.4;

        ax5=a*(1.0+delta);
        ax5b=0.5/(pi*ax5*ax5);
        gx5=g+delta_g;
        s05=0.41*ax5^1.8*gx5^1.6*l^-2.4;

        Wl = $Z0 * $clight * (ax1b*exp(-sqrt(s/s01)) +...
                              ax2b*exp(-sqrt(s/s02)) +...
                              ax3b*exp(-sqrt(s/s03)) +...
                              ax4b*exp(-sqrt(s/s04)) +...
                              ax5b*exp(-sqrt(s/s05)))/ 1e12 /4.0;
    endfunction
}
