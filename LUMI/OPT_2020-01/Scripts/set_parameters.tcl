#
# This script set all the parameters used in the simulation
#
set beamparams(nbunches)      1
set beamparams(nslice)        50
set beamparams(nmacro)        2000
set beamparams(nsigmabunch)   4
set beamparams(nsigmawake)    5
set beamparams(useisr)        1
set beamparams(usewakefields) 1

# Load parameters
source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_$particle.tcl
source $paramsdir/rf_parameters.tcl
if {$beamparams(usewakefields)==1} {
   source $paramsdir/rf_parameters_ww.tcl
}

#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigma]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1]
# hence, in the array the values are stored in Placet units
set beamparams(betax)  $beam0010(betax)
set beamparams(alphax) $beam0010(alphax)
set beamparams(emitnx) [expr $beam0010(emitnx)*$placetunits(emittance)]
set beamparams(betay)  $beam0010(betay)
set beamparams(alphay) $beam0010(alphay)
set beamparams(emitny) [expr $beam0010(emitny)*$placetunits(emittance)]
set beamparams(sigmaz) [expr $beam0010(sigmaz)*$placetunits(xyz)]
set beamparams(meanz)  [expr $beam0010(meanz)*$placetunits(xyz)]
set beamparams(charge) [expr $beam0010(charge)*$placetunits(charge)]
set beamparams(uncespread) $beam0010(uncespr)
set beamparams(echirp) [expr $beam0010(echirp)/$placetunits(xyz)]
set beamparams(energy) [expr $beam0010(energy)*$placetunits(energy)]
set beamparams(startenergy) [expr $beam0010(energy)*$placetunits(energy)]
set beamparams(meanenergy)  [expr $beam0010(energy)*$placetunits(energy)]

set bpmres 1
set charge $beam0010(charge)  

# random misalignment of beam
set gamma  [expr $beamparams(startenergy)/0.0005109989]
set emitx  [expr $beamparams(emitnx)/($placetunits(emittance)*$gamma)]
set emity  [expr $beamparams(emitny)/($placetunits(emittance)*$gamma)]
set betax  $beamparams(betax)
set alphax $beamparams(alphax)
set betay  $beamparams(betay)
set alphay $beamparams(alphay)
Octave {
    ex  = $emitx;
    bx  = $betax;
    ax  = $alphax;
    ey  = $emity;
    by  = $betay;
    ay  = $alphay;
    xn  = 0.0*sqrt(ex)*randn(1,1);
    xpn = 0.0*sqrt(ex)*randn(1,1);
    yn  = 0.0*sqrt(ey)*randn(1,1);
    ypn = 0.0*sqrt(ey)*randn(1,1);
    x   = (sqrt(bx)*xn)*1e6;
    xp  = (-ax/sqrt(bx)*xn+xpn/sqrt(bx))*1e6;
    y   = (sqrt(by)*yn)*1e6;
    yp  = (-ay/sqrt(by)*yn+ypn/sqrt(by))*1e6;
    Tcl_SetVar("bm_x",  x);
    Tcl_SetVar("bm_xp", xp);
    Tcl_SetVar("bm_y",  y);
    Tcl_SetVar("bm_yp", yp);
}
set beamparams(meanx)  $bm_x
set beamparams(meanxp) $bm_xp
set beamparams(meany)  $bm_y
set beamparams(meanyp) $bm_yp

# Parameters needed for the BDS lattice
set e0           190.0
set synrad       1
set quad_synrad  1
set mult_synrad  1
set sbend_synrad 1
set sbend_sixdim 1

# This setting is needed when simulating a particle (opposed to a sliced) beam
FirstOrder 1
