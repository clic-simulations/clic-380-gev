set l 2.2
set f 1.5
set scale 3

exec tclsh join.tcl $l $f $scale
exec ./placet autonew.tcl 0.072
foreach phase {0 1 2 3 4 5 6 7 8 9 10 11 12} {
    exec ./placet twiss.tcl $phase
    set auto [lindex [exec tail -3 auto] 5]
    set quad [lindex [exec tail -3 quadrupole] 5]
    set cav [lindex [exec tail -3 cavity] 5]
    set bpm [lindex [exec tail -3 bpm] 5]
    puts "$l $f $scale $phase $auto $quad $cav $bpm"
}
