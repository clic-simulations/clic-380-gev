Girder
Bpm
Quadrupole -name "FQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.1593135711*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Dipole
Girder
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQD" -synrad $quad_synrad -length 0.5 -strength [expr -0.06708030646*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "FDD" -length 0.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 0.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQF2" -synrad $quad_synrad -length 0.5 -strength [expr -0.1182922355*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Dipole
Girder
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "FDD" -length 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "FQD2" -synrad $quad_synrad -length 0.5 -strength [expr 0.02788484518*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Bpm
Quadrupole -name "TQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.1149808129*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Dipole
Girder
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.78125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.4375 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.5625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.65625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.34375 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD" -synrad $quad_synrad -length 0.125 -strength [expr -0.01474936495*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQD" -synrad $quad_synrad -length 0.875 -strength [expr -0.10324555465*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.09375 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.90625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.6875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.53125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.46875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.75 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF2A" -synrad $quad_synrad -length 0.25 -strength [expr 0.05168697685*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQF2A" -synrad $quad_synrad -length 0.25 -strength [expr 0.05168697685*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF2B" -synrad $quad_synrad -length 0.5 -strength [expr 0.1033739537*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 0.25 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.774479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.225520833 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.798958334 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.201041666 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.823437501 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD2A" -synrad $quad_synrad -length 0.176562499 -strength [expr -0.0300137259639*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQD2A" -synrad $quad_synrad -length 0.323437501 -strength [expr -0.0549808967161*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD2B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08499462268*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 0.176562499 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.847916668 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.152083332 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.872395835 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.127604165 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.896875002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF3A" -synrad $quad_synrad -length 0.103124998 -strength [expr 0.0116745914015*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQF3A" -synrad $quad_synrad -length 0.396875002 -strength [expr 0.0449294892185*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF3B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05660408062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 0.103124998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.921354169 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.078645831 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.945833336 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.054166664 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.970312503 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD3A" -synrad $quad_synrad -length 0.029687497 -strength [expr -0.00470596565645*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQD3A" -synrad $quad_synrad -length 0.470312503 -strength [expr -0.0745524113036*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD3B" -synrad $quad_synrad -length 0.5 -strength [expr -0.07925837696*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD2" -length 0.029687497 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.99479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.00520832999997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.019270837 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD2" -length 0.980729163 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD2" -length 0.043750004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF4A" -synrad $quad_synrad -length 0.5 -strength [expr 0.1039004288*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF4B" -synrad $quad_synrad -length 0.456249996 -strength [expr 0.0948091404488*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQF4B" -synrad $quad_synrad -length 0.043750004 -strength [expr 0.00909128835121*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 0.956249996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.262500004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.737499996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.481250004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.518749996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.700000004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.299999996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.918750004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD4A" -synrad $quad_synrad -length 0.081249996 -strength [expr -0.014607599465*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQD4A" -synrad $quad_synrad -length 0.418750004 -strength [expr -0.075285324745*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD4B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08989292421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 0.081249996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.137500004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.862499996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.356250004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.643749996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.575000004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.424999996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.793750004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF5A" -synrad $quad_synrad -length 0.206249996 -strength [expr 0.0237791299565*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQF5A" -synrad $quad_synrad -length 0.293750004 -strength [expr 0.0338672468135*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF5B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05764637677*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DD" -length 0.206249996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.012500004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.987499996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.231250004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.768749996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.450000004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DD" -length 0.549999996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DD" -length 0.668750004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD5" -synrad $quad_synrad -length 0.331249996 -strength [expr -0.0366898972169*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQD5" -synrad $quad_synrad -length 0.668750004 -strength [expr -0.0740720579831*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.331249996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.213141778 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.786858222 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.757533552 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.242466448 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.301925326 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.698074674 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.8463171 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF6A" -synrad $quad_synrad -length 0.1536829 -strength [expr 0.0080300934576*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQF6A" -synrad $quad_synrad -length 0.3463171 -strength [expr 0.0180954333824*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF6B" -synrad $quad_synrad -length 0.5 -strength [expr 0.02612552684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.1536829 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.390708874 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.609291126 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.935100648 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.0648993519999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.479492422 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.520507578 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.0238841960001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD6A" -synrad $quad_synrad -length 0.5 -strength [expr -0.02002336697*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD6B" -synrad $quad_synrad -length 0.476115804 -strength [expr -0.0190668829274*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQD6B" -synrad $quad_synrad -length 0.0238841960001 -strength [expr -0.000956484042586*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.976115804 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.56827597 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.43172403 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.112667744 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.887332256 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.657059518 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.342940482 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.201451292 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF7A" -synrad $quad_synrad -length 0.5 -strength [expr 0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF7B" -synrad $quad_synrad -length 0.298548708 -strength [expr 0.0119149257808*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQF7B" -synrad $quad_synrad -length 0.201451292 -strength [expr 0.00803981771921*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.798548708 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.745843066 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.254156934 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.29023484 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.70976516 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.834626614 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.165373386 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.379018388 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD7A" -synrad $quad_synrad -length 0.5 -strength [expr -0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQD7B" -synrad $quad_synrad -length 0.120981612 -strength [expr -0.00482831407135*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQD7B" -synrad $quad_synrad -length 0.379018388 -strength [expr -0.0151264294286*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.620981612 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.923410162 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.076589838 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.467801936 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.532198064 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.01219371 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.98780629 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.556585484 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF8A" -synrad $quad_synrad -length 0.443414516 -strength [expr 0.0178130801973*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQF8A" -synrad $quad_synrad -length 0.056585484 -strength [expr 0.00227318170272*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "TQF8B" -synrad $quad_synrad -length 0.5 -strength [expr 0.0200862619*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL" -length 0.443414516 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.100977258 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.899022742 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.645369032 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.354630968 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.189760806 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DDL" -length 0.810239194 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL" -length 0.73415258 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD8" -synrad $quad_synrad -length 0.26584742 -strength [expr -0.0275952029474*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQD8" -synrad $quad_synrad -length 0.73415258 -strength [expr -0.0762057026526*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDL89" -length 0.26584742 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL89" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL89" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL89" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDL89" -length 0.528544354 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQF9" -synrad $quad_synrad -length 0.471455646 -strength [expr -0.0718722937141*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQF9" -synrad $quad_synrad -length 0.528544354 -strength [expr -0.0805753316859*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DDLE" -length 0.471455646 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDLE" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DDLE" -length 0.549011171 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "TQD9" -synrad $quad_synrad -length 0.450988829 -strength [expr 0.0607891689544*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "TQD9" -synrad $quad_synrad -length 0.549011171 -strength [expr 0.0740016840456*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988829 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 0.549011171 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.450988829 -strength [expr 0.000806605433266*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.799011171 -strength [expr 0.00142905258473*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.200988829 -strength [expr 0.000359473829666*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.0 -strength [expr 0.0017885264144*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.0490111709999 -strength [expr 8.76577739341e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "YGIRDER" -length 0.325988829 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "YGIRDER" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "YGIRDER" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "YGIRDER" -length 0.174011171 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.200988829 -angle 1.53787370981e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.450052838 -angle 3.44359649752e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.549947162 -angle 4.20794173702e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.101094505 -angle 7.73528470308e-07 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.273905495 -angle 2.09579836764e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.377136172 -angle 2.88567183968e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.622863828 -angle 4.76586639485e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.0281778389999 -angle 2.15603812474e-07 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.346822161 -angle 2.65372302548e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.304219506 -angle 2.32774718185e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.695780494 -angle 5.32379105269e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.955261173 -angle 7.30921738918e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.0447388270001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.580261173 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.419738827 -angle 3.21164768331e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.23130284 -angle 1.76982252402e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.76869716 -angle 5.88171571052e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.882344507 -angle 6.75129273134e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.117655493 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.507344507 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.492655493 -angle 3.76957234114e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.158386174 -angle 1.21189786618e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.841613826 -angle 6.43964036835e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.809427841 -angle 6.19336807351e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.190572159 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.434427841 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.565572159 -angle 4.32749699898e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.0854695079999 -angle 6.53973208348e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.914530492 -angle 6.99756502619e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.736511175 -angle 5.63544341567e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.263488825 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM2" -length 0.0490111749999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.638488825 -angle 4.88542165681e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.0125528419999 -angle 9.60485505145e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.987447158 -angle 7.55548968402e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.663594509 -angle 5.07751875784e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.336405491 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.288594509 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.711405491 -angle 5.44334631464e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.939636176 -angle 7.18966212722e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.060363824 -angle 4.61876107319e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.590677843 -angle 4.51959410001e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.409322157 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.215677843 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.784322157 -angle 6.00127097248e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.86671951 -angle 6.63173746938e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.13328049 -angle 1.01980076515e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.517761177 -angle 3.96166944217e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.482238823 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.142761177 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.857238823 -angle 6.55919563031e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.793802844 -angle 6.07381281155e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.206197156 -angle 1.57772542299e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.444844511 -angle 3.40374478434e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.555155489 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.069844511 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.930155489 -angle 7.11712028815e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.720886178 -angle 5.51588815372e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.279113822 -angle 2.13565008082e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.371927845 -angle 2.84582012651e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.00307215499993 -angle 2.35067114444e-08 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.647969512 -angle 4.95796349588e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.352030488 -angle 2.69357473865e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.299011179 -angle 2.28789546867e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.0759888209999 -strength [expr -0.0019969997893*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.549011179 -strength [expr -0.0144281118507*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.450988821 -strength [expr -0.0118520667733*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.174011179 -strength [expr -0.0045730448667*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.200988821 -strength [expr -0.0052820221173*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.424011179 -strength [expr -0.0111430895227*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.575988821 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.0490111790001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.950988821 -strength [expr -0.00170086862616*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.299011179 -strength [expr -0.000534789391843*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.700988821 -strength [expr -0.00125373702256*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.549011179 -strength [expr -0.000981920995443*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988821 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 0.549011179 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*463.8761985*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 0.450988821 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 0.549011179 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.450988821 -strength [expr -0.000806605418957*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.799011179 -strength [expr -0.00142905259904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.200988821 -strength [expr -0.000359473815357*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.0 -strength [expr -0.0017885264144*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.0490111790001 -strength [expr -8.76577882426e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.325988821 -strength [expr -0.0085670444453*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.299011179 -strength [expr -0.0078580671947*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.0759888209999 -strength [expr -0.0019969997893*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.549011179 -strength [expr -0.0144281118507*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.450988821 -strength [expr -0.0118520667733*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.174011179 -strength [expr -0.0045730448667*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.200988821 -angle -3.41394910375e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.450052846 -angle -7.64449237822e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.549947154 -angle -9.34127372939e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.101094513 -angle -1.71716775258e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.273905487 -angle -4.65249453777e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.37713618 -angle -6.4059469442e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.62286382 -angle -1.05798191634e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.0281778470002 -angle -4.7862231856e-08 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.346822153 -angle -5.89103997179e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.304219514 -angle -5.16740151018e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.695780486 -angle -1.18183645974e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.955261181 -angle -1.62258429921e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.0447388189998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.580261181 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.419738819 -angle -7.12958540581e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.231302848 -angle -3.92885607615e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.768697152 -angle -1.30569100315e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.882344515 -angle -1.49872975581e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.117655485 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.507344515 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.492655485 -angle -8.36813083984e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.158386182 -angle -2.69031064213e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.841613818 -angle -1.42954554655e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.809427849 -angle -1.37487521241e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.190572151 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.434427849 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.565572151 -angle -9.60667627386e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.0854695160003 -angle -1.45176520811e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.914530484 -angle -1.55340008995e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.736511183 -angle -1.25102066901e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.263488817 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM2" -length 0.0490111830003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.638488817 -angle -1.08452217079e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.0125528500003 -angle -2.13219774089e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.98744715 -angle -1.67725463335e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.663594517 -angle -1.12716612561e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.336405483 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.288594517 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.711405483 -angle -1.20837671419e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.939636184 -angle -1.59604404477e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.0603638159997 -angle -1.02532565993e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.590677851 -angle -1.0033115822e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.409322149 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.215677851 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.784322149 -angle -1.33223125759e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.866719518 -angle -1.47218950137e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.133280482 -angle -2.26387109395e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.517761185 -angle -8.79457038801e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.482238815 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.142761185 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.857238815 -angle -1.45608580099e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.793802852 -angle -1.34833495796e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.206197148 -angle -3.50241652798e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.444844519 -angle -7.55602495399e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.555155481 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.0698445190004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.930155481 -angle -1.5799403444e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.720886186 -angle -1.22448041456e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.279113814 -angle -4.740961962e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.371927853 -angle -6.31747951997e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.00307214699956 -angle -5.21827703827e-09 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.64796952 -angle -1.10062587116e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.35203048 -angle -5.97950739602e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.299011187 -angle -5.07893408595e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.0759888129996 -strength [expr 0.000638530569209*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.549011187 -strength [expr 0.00461331624879*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.450988813 -strength [expr 0.00378963866001*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.174011187 -strength [expr 0.00146220815799*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.200988813 -strength [expr 0.00168889993281*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.424011187 -strength [expr 0.00356294688519*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.575988813 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.0490111870004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.950988813 -strength [expr 0.00170086861185*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.299011187 -strength [expr 0.000534789406151*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.700988813 -strength [expr 0.00125373700825*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.549011187 -strength [expr 0.000981921009751*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988813 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 0.549011187 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DUTIL" -length 0.450988813 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 0.549011187 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.450988813 -strength [expr 0.000806605404649*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.799011187 -strength [expr 0.00142905261335*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.200988813 -strength [expr 0.000359473801049*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.0 -strength [expr 0.0017885264144*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.0490111870004 -strength [expr 8.76578025512e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.325988813 -strength [expr 0.00273926929641*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.299011187 -strength [expr 0.00251257752159*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.0759888129996 -strength [expr 0.000638530569209*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.549011187 -strength [expr 0.00461331624879*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.450988813 -strength [expr 0.00378963866001*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.174011187 -strength [expr 0.00146220815799*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.200988813 -angle -3.41394896786e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.450052854 -angle -7.64449251411e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.549947146 -angle -9.3412735935e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.101094521 -angle -1.71716788847e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.273905479 -angle -4.65249440188e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.377136188 -angle -6.40594708009e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.622863812 -angle -1.05798190275e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.0281778550002 -angle -4.78622454448e-08 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.346822145 -angle -5.89103983591e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.304219522 -angle -5.16740164606e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.695780478 -angle -1.18183644615e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.955261189 -angle -1.6225843128e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.0447388109999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.580261189 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.419738811 -angle -7.12958526993e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.231302856 -angle -3.92885621204e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.768697144 -angle -1.30569098956e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.882344523 -angle -1.4987297694e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.117655477 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.507344523 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.492655477 -angle -8.36813070395e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.15838619 -angle -2.69031077802e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.84161381 -angle -1.42954553296e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.809427857 -angle -1.374875226e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.190572143 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.434427857 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.565572143 -angle -9.60667613798e-07 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.0854695239999 -angle -1.45176534399e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.914530476 -angle -1.55340007636e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.736511191 -angle -1.2510206826e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.263488809 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM2" -length 0.0490111909999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.638488809 -angle -1.0845221572e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.0125528579999 -angle -2.13219909968e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.987447142 -angle -1.67725461976e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.663594525 -angle -1.12716613919e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.336405475 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.288594525 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.711405475 -angle -1.2083767006e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.939636192 -angle -1.59604405836e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.0603638080003 -angle -1.02532552406e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.590677859 -angle -1.00331159579e-06 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.409322141 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.215677859 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.784322141 -angle -1.332231244e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.866719526 -angle -1.47218951495e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.133280474 -angle -2.26387095808e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.517761193 -angle -8.79457052389e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.482238807 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.142761193 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.857238807 -angle -1.45608578741e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.79380286 -angle -1.34833497155e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.20619714 -angle -3.5024163921e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.444844527 -angle -7.55602508986e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.555155473 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.0698445269995 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.930155473 -angle -1.57994033081e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.720886194 -angle -1.22448042815e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.279113806001 -angle -4.74096182613e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.371927860999 -angle -6.31747965584e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 0.00307213900055 -angle -5.21826345134e-09 -e0 $e0 -E1 -9.598727202e-06 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4A" -synrad $sbend_synrad -length 0.647969527999 -angle -1.10062588475e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 0.352030472001 -angle -5.97950726015e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 1.0 -angle -1.69857661076e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B4B" -synrad $sbend_synrad -length 0.299011194999 -angle -5.07893422182e-07 -e0 $e0 -E1 0.0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.0759888050006 -strength [expr -0.00199699936884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.549011194999 -strength [expr -0.0144281122712*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.450988805001 -strength [expr -0.0118520663528*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.174011194999 -strength [expr -0.00457304528716*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.200988805001 -strength [expr -0.00528202169684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.424011194999 -strength [expr -0.0111430899432*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.575988805001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.0490111949994 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.950988805001 -strength [expr -0.00170086859754*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.299011194999 -strength [expr -0.000534789420458*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.700988805001 -strength [expr -0.00125373699394*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.549011194999 -strength [expr -0.000981921024058*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988805001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 0.549011194999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC4" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-458.648884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 0.450988805001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 0.549011194999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.450988805001 -strength [expr -0.000806605390342*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.799011194999 -strength [expr -0.00142905262766*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.200988805001 -strength [expr -0.000359473786742*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.0 -strength [expr -0.0017885264144*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDEC" -synrad $quad_synrad -length 0.0490111949994 -strength [expr -8.76578168577e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.325988805001 -strength [expr -0.00856704402484*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.299011194999 -strength [expr -0.00785806761516*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.0759888050006 -strength [expr -0.00199699936884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.549011194999 -strength [expr -0.0144281122712*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.450988805001 -strength [expr -0.0118520663528*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.174011194999 -strength [expr -0.00457304528716*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.200988805001 -angle 1.53787352618e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.450052861999 -angle 3.44359668115e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.549947138001 -angle 4.20794155339e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.101094528999 -angle 7.73528653941e-07 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.273905471001 -angle 2.09579818401e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.377136195999 -angle 2.88567202332e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.622863804001 -angle 4.76586621122e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.0281778629992 -angle 2.15603996106e-07 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.346822137001 -angle 2.65372284184e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.304219529999 -angle 2.32774736548e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.695780470001 -angle 5.32379086905e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.955261196999 -angle 7.30921757281e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.0447388030009 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.580261196999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.419738803001 -angle 3.21164749968e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.231302863999 -angle 1.76982270765e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.768697136001 -angle 5.88171552689e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.882344530999 -angle 6.75129291497e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.117655469001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.507344530999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.492655469001 -angle 3.76957215751e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.158386197999 -angle 1.21189804981e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.841613802001 -angle 6.43964018472e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.809427864999 -angle 6.19336825714e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.190572135001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.434427864999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.565572135001 -angle 4.32749681535e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.085469531999 -angle 6.53973391978e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.914530468001 -angle 6.99756484256e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.736511198999 -angle 5.6354435993e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.263488801001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM2" -length 0.0490111989989 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.638488801001 -angle 4.88542147318e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.0125528659989 -angle 9.60487341433e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.987447134001 -angle 7.55548950039e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.663594532999 -angle 5.07751894147e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.336405467001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.288594532999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.711405467001 -angle 5.44334613102e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.939636199999 -angle 7.18966231084e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.0603638000013 -angle 4.61875923691e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.590677866999 -angle 4.51959428363e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.409322133001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.215677866999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.784322133001 -angle 6.00127078885e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.866719533999 -angle 6.63173765301e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.133280466001 -angle 1.01980058153e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.517761200999 -angle 3.9616696258e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.482238799001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.142761200999 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.857238799001 -angle 6.55919544669e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.793802867999 -angle 6.07381299517e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.206197132001 -angle 1.57772523936e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.444844534999 -angle 3.40374496797e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.555155465001 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.0698445349985 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.930155465001 -angle 7.11712010452e-06 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.720886201999 -angle 5.51588833734e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.279113798002 -angle 2.13564989719e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.371927868998 -angle 2.84582031013e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 0.00307213100155 -angle 2.35065278198e-08 -e0 $e0 -E1 4.323916138e-05 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3A" -synrad $sbend_synrad -length 0.647969535998 -angle 4.95796367951e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 0.352030464002 -angle 2.69357455503e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 1.0 -angle 7.65153823453e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "B3B" -synrad $sbend_synrad -length 0.299011202998 -angle 2.2878956523e-06 -e0 $e0 -E1 0.0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DSXL" -length 0.0759887970016 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DSXL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DSXL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DSXL" -length 0.424011202998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.575988797002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM" -length 0.0490112029984 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.950988797002 -strength [expr 0.00170086858324*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.299011202998 -strength [expr 0.000534789434764*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.700988797002 -strength [expr 0.00125373697964*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFEC" -synrad $quad_synrad -length 0.549011202998 -strength [expr 0.000981921038364*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DUTIL" -length 0.450988797002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DUTIL" -length 0.549011202998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D1BC" -length 0.1 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.350988797002 -strength [expr 0.0362585369069*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.149011202998 -strength [expr 0.0153934491631*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.225988797002 -strength [expr 0.0233455403894*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.274011202998 -strength [expr 0.0283064456806*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BC" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.100988797002 -strength [expr -0.00849869234111*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.399011202998 -strength [expr -0.0335787092789*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.100988797002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM0" -length 0.0240112029984 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.475988797002 -strength [expr -0.0400567435561*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.0240112029984 -strength [expr -0.00202065806389*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.350988797002 -strength [expr -0.0295373931511*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.149011202998 -strength [expr -0.0125400084689*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.225988797002 -strength [expr -0.0190180427461*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.274011202998 -strength [expr -0.0230593588739*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BC" -length 0.725988797002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BC" -length 0.907526202998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.0924737970016 -strength [expr 0.00251406586419*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.407526202998 -strength [expr 0.0110793300258*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "DMM0" -length 0.0924737970016 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DMM0" -length 0.0325262029984 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.467473797002 -strength [expr 0.0127091127817*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.0325262029984 -strength [expr 0.000884283108311*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D4BC" -length 0.1925765 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SEXIT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -20*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.0588922970016 -strength [expr -0.00714559101428*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.441107702998 -strength [expr -0.0535210103757*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.558892297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 0.0248677029984 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.475132297002 -strength [expr 0.0419020631952*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.0248677029983 -strength [expr 0.00219309036479*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.975132297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 0.935842702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.0641572970017 -strength [expr -0.00871159075131*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.435842702998 -strength [expr -0.0591808482887*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.0641572970017 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 0.846817702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.153182297002 -strength [expr 0.0135091938179*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.346817702998 -strength [expr 0.0305859597421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.153182297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 0.430577702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.0694222970017 -strength [expr -0.00842322963956*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.430577702998 -strength [expr -0.0522433717504*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.137412297002 -strength [expr -0.0166726740966*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.362587702998 -strength [expr -0.0439939272934*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.137412297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 0.446347702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.0536522970017 -strength [expr 0.00473161255027*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.446347702998 -strength [expr 0.0393635410097*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.553652297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 0.357322702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.142677297002 -strength [expr -0.0193734193782*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.357322702998 -strength [expr -0.0485190196618*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.642677297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 0.268297702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.231702297002 -strength [expr 0.020433896733*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.268297702998 -strength [expr 0.023661256827*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.731702297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 0.852057702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.147942297002 -strength [expr -0.0179503127218*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.352057702998 -strength [expr -0.0427162886682*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.147942297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BCOL" -length 0.0680627029983 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.215932297002 -strength [expr -0.0261997571789*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.284067702998 -strength [expr -0.0344668442111*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.715932297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 0.867827702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.132172297002 -strength [expr 0.0116563154653*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.367827702998 -strength [expr 0.0324388380947*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.132172297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 0.778802702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.221197297002 -strength [expr -0.030035248005*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.278802702998 -strength [expr -0.037857191035*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.221197297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 0.689777702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.310222297002 -strength [expr 0.0273585996481*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.189777702998 -strength [expr 0.0167365539119*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.310222297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 0.273537702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.226462297002 -strength [expr -0.0274773958041*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.273537702998 -strength [expr -0.0331892055859*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.294452297002 -strength [expr -0.0357268402611*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.205547702998 -strength [expr -0.0249397611289*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.294452297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 0.289307702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.210692297002 -strength [expr 0.0185810183804*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.289307702998 -strength [expr 0.0255141351796*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.710692297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 0.200282702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.299717297002 -strength [expr -0.0406970766318*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.200282702998 -strength [expr -0.0271953624082*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.799717297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 0.111257702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.388742297002 -strength [expr 0.0342833025631*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.111257702998 -strength [expr 0.00981185099688*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.888742297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 0.695017702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.304982297002 -strength [expr -0.0370044788864*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.195017702998 -strength [expr -0.0236621225036*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "D3BCOL" -length 0.0889772970017 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3BCOL" -length 0.127027702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.372972297002 -strength [expr -0.0452539233434*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.127027702998 -strength [expr -0.0154126780466*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1BCOL" -length 0.872972297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1BCOL" -length 0.710787702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.289212297002 -strength [expr 0.0255057212955*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.210787702998 -strength [expr 0.0185894322645*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Bpm
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D2BCOL" -length 0.289212297002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2BCOL" -length 0.621762702998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "BTFD0" -length 0.02536345 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 0.352873847002 -strength [expr -0.0170789953902*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 1.0 -strength [expr -0.04839972*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 1.0 -strength [expr -0.04839972*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 0.147126152998 -strength [expr -0.00712086460979*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD1" -length 0.852873847002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD1" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD1" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD1" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD1" -length 0.503472652998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 0.496527347002 -strength [expr 0.00604580833813*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 1.0 -strength [expr 0.012176184*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 1.0 -strength [expr 0.012176184*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 0.0034726529982 -strength [expr 4.22836618742e-05*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD2" -length 0.996527347002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD2" -length 0.0010226529982 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 0.998977347002 -strength [expr 0.020171598337*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 1.0 -strength [expr 0.020192248*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 0.501022652998 -strength [expr 0.010116773663*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD3" -length 0.498977347002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD3" -length 0.489312652998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 0.510687347002 -strength [expr -0.0136105736149*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 1.0 -strength [expr -0.02665148*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 0.989312652998 -strength [expr -0.0263666463851*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "BTFD4" -length 0.0106873470017 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "BTFD4" -length 0.0155886029983 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "LMD4" -length 0.984411397002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD4" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD4" -length 0.984560466998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD11" -synrad $quad_synrad -length 0.0154395330019 -strength [expr 0.00185372821502*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QMD11" -synrad $quad_synrad -length 1.0 -strength [expr 0.120063749*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QMD11" -synrad $quad_synrad -length 0.984560466998 -strength [expr 0.118210020785*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD5" -length 0.0154395330019 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD5" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD5" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD5" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD5" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD5" -length 0.229428129998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD12" -synrad $quad_synrad -length 0.770571870002 -strength [expr -0.0706978837126*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QMD12" -synrad $quad_synrad -length 1.0 -strength [expr -0.09174729375*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QMD12" -synrad $quad_synrad -length 0.229428129998 -strength [expr -0.0210494100374*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD6" -length 0.770571870002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD6" -length 0.429428129998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD13" -synrad $quad_synrad -length 0.570571870002 -strength [expr 0.0479954334288*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QMD13" -synrad $quad_synrad -length 1.0 -strength [expr 0.0841181207*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QMD13" -synrad $quad_synrad -length 0.429428129998 -strength [expr 0.0361226872712*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD7" -length 0.570571870002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD7" -length 0.629428129998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QMD14" -synrad $quad_synrad -length 0.370571870002 -strength [expr -0.0204801825717*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QMD14" -synrad $quad_synrad -length 1.0 -strength [expr -0.0552664253*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QMD14" -synrad $quad_synrad -length 0.629428129998 -strength [expr -0.0347862427283*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LMD8" -length 0.370571870002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LMD8" -length 0.534028129998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF8" -synrad $quad_synrad -length 0.465971870002 -strength [expr -0.00394696003925*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF8" -synrad $quad_synrad -length 1.0 -strength [expr -0.00847038264183*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF8" -synrad $quad_synrad -length 0.515228129998 -strength [expr -0.00436417940892*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D8" -length 0.484771870002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D8" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D8" -length 0.659228129998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD7" -synrad $quad_synrad -length 0.340771870002 -strength [expr -0.00223613845421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD7" -synrad $quad_synrad -length 1.0 -strength [expr -0.00656198075914*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD7" -synrad $quad_synrad -length 0.640428129998 -strength [expr -0.00420247706666*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.054771870002 -angle 2.07715448258e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.048348129998 -angle 1.83354219869e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.951651870002 -angle 3.60902037407e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.151468129998 -angle 5.74423887995e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.848531870002 -angle 3.21795070594e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.254588129998 -angle 9.65493556122e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.745411870002 -angle 2.82688103781e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.357708129998 -angle 1.35656322425e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.642291870002 -angle 2.43581136969e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.460828129998 -angle 1.74763289238e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.539171870002 -angle 2.04474170156e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.563948129998 -angle 2.1387025605e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.436051870002 -angle 1.65367203343e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.667068129998 -angle 2.52977222863e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.332931870002 -angle 1.26260236531e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.770188129998 -angle 2.92084189676e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.229811870002 -angle 8.71532697181e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.873308129998 -angle 3.31191156488e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.126691870002 -angle 4.80463029054e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 1.0 -angle 3.79237459394e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB4" -synrad $sbend_synrad -length 0.976428129998 -angle 3.70298123301e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.0235718700021 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LX0" -length 0.281228129998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD6C" -synrad $quad_synrad -length 0.718771870002 -strength [expr -0.0167116879953*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD6C" -synrad $quad_synrad -length 1.0 -strength [expr -0.0232503367101*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD6C" -synrad $quad_synrad -length 0.262428129998 -strength [expr -0.00610154238464*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D7" -length 0.737571870002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D7" -length 0.838428129998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.161571870002 -strength [expr -0.00540372679584*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD6B" -synrad $quad_synrad -length 1.0 -strength [expr -0.0334447252221*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.819628129998 -strength [expr -0.0274122375921*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.180371870002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LX0" -length 0.124428129998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.875571870002 -strength [expr -0.0292832606044*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD6B" -synrad $quad_synrad -length 1.0 -strength [expr -0.0334447252221*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.105628129998 -strength [expr -0.00353270378349*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D6" -length 0.894371870002 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D6" -length 0.377084129998 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF6" -synrad $mult_synrad -type 3 -length 0.622915870003 -strength [expr 0.638681315113*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SF6" -synrad $mult_synrad -type 3 -length 0.376828129997 -strength [expr 0.386365313887*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.318371870003 -strength [expr 0.00580077401289*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF5B" -synrad $quad_synrad -length 1.0 -strength [expr 0.0182201210579*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.662828129997 -strength [expr 0.0120768087692*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.0323718700029 -strength [expr 0.000589819390324*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF5B" -synrad $quad_synrad -length 1.0 -strength [expr 0.0182201210579*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.948828129997 -strength [expr 0.0172877633917*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D5OD" -length 0.0511718700029 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5OD" -length 0.394511179997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD5L" -synrad $mult_synrad -type 3 -length 0.605488820003 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SD5L" -synrad $mult_synrad -type 3 -length 0.394511179997 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD5" -synrad $mult_synrad -type 3 -length 0.605488820003 -strength [expr 7.13637785984*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SD5" -synrad $mult_synrad -type 3 -length 0.394511179997 -strength [expr 4.64976521016*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD5R" -synrad $mult_synrad -type 3 -length 0.605488820003 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SD5R" -synrad $mult_synrad -type 3 -length 0.394511179997 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D5ODT" -length 0.605488820003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D5ODT" -length 0.394511179997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.605488820003 -strength [expr 0.0100891748458*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF5A" -synrad $quad_synrad -length 1.0 -strength [expr 0.0166628590198*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.375711179997 -strength [expr 0.00626042242445*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.319488820003 -strength [expr 0.00532359716611*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF5A" -synrad $quad_synrad -length 1.0 -strength [expr 0.0166628590198*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.661711179997 -strength [expr 0.0110260001041*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF5" -synrad $mult_synrad -type 3 -length 0.0334888200032 -strength [expr -0.0321912007332*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SF5" -synrad $mult_synrad -type 3 -length 0.966255179997 -strength [expr -0.928814883767*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D4OD" -length 0.0337448200032 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D4OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D4OD" -length 0.784149723997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT" -length 0.215850276003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "OCTDRIFT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "OCTDRIFT" -length 0.784149723997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD4B" -synrad $quad_synrad -length 0.215850276003 -strength [expr -0.00237389316012*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD4B" -synrad $quad_synrad -length 1.0 -strength [expr -0.010997869468*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD4B" -synrad $quad_synrad -length 0.765349723997 -strength [expr -0.00841721636189*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.234650276003 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LX0" -length 0.0701497239967 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD4" -synrad $mult_synrad -type 3 -length 0.929850276003 -strength [expr 1.58972225347*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SD4" -synrad $mult_synrad -type 3 -length 0.0698937239966 -strength [expr 0.119494085534*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD4A" -synrad $quad_synrad -length 0.625306276004 -strength [expr -0.00690732711424*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD4A" -synrad $quad_synrad -length 1.0 -strength [expr -0.0110463102312*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD4A" -synrad $quad_synrad -length 0.355893723996 -strength [expr -0.00393131248459*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D3" -length 0.644106276004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D3" -length 0.282136103996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.717863896004 -angle -8.47083417086e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.273736103996 -angle -3.23010135548e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.726263896004 -angle -8.56995464123e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.265336103996 -angle -3.13098088511e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.734663896003 -angle -8.6690751116e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.256936103997 -angle -3.03186041474e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.743063896003 -angle -8.76819558197e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.248536103997 -angle -2.93273994436e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.751463896003 -angle -8.86731605234e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.240136103997 -angle -2.83361947399e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.759863896003 -angle -8.96643652272e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.231736103997 -angle -2.73449900362e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.768263896003 -angle -9.06555699309e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.223336103997 -angle -2.63537853325e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.776663896003 -angle -9.16467746346e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.214936103997 -angle -2.53625806288e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.785063896003 -angle -9.26379793383e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.206536103997 -angle -2.43713759251e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.793463896003 -angle -9.3629184042e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 1.0 -angle -1.18000559967e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3" -synrad $sbend_synrad -length 0.198136103997 -angle -2.33801712213e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF3B" -synrad $quad_synrad -length 0.497063896003 -strength [expr 0.00133803681674*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF3B" -synrad $quad_synrad -length 1.0 -strength [expr 0.00269188091813*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF3B" -synrad $quad_synrad -length 0.484136103997 -strength [expr 0.00130323674013*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.211063896003 -angle -2.49056579094e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.617736103997 -angle -7.28932061606e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.382263896003 -angle -4.51073537694e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.446536103997 -angle -5.26915103006e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.553463896003 -angle -6.53090496294e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.275336103997 -angle -3.24898144406e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.724663896003 -angle -8.55107454894e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.104136103997 -angle -1.22881185805e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.895863896003 -angle -1.05712441349e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.932936103997 -angle -1.10086982651e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.0670638960032 -angle -7.91357727947e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.761736103997 -angle -8.98852867905e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.238263896003 -angle -2.81152731395e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.590536103997 -angle -6.96835909305e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.409463896003 -angle -4.83169689995e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.419336103997 -angle -4.94818950705e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.580663896003 -angle -6.85186648595e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.248136103997 -angle -2.92801992105e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.751863896003 -angle -8.87203607195e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 0.0769361039968 -angle -9.07850335045e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF3A" -synrad $quad_synrad -length 0.618263896003 -strength [expr 0.00684684022863*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF3A" -synrad $quad_synrad -length 1.0 -strength [expr 0.0110743005906*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF3A" -synrad $quad_synrad -length 0.362936103997 -strength [expr 0.00401926351082*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.332263896003 -angle -3.92073257729e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.496536103997 -angle -5.85915382971e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.503463896003 -angle -5.9409021633e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.325336103997 -angle -3.8389842437e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.674663896003 -angle -7.9610717493e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.154136103997 -angle -1.8188146577e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.845863896003 -angle -9.9812413353e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.982936103997 -angle -1.15987010647e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.0170638960035 -angle -2.013549283e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.811736103997 -angle -9.5785314787e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.188263896003 -angle -2.2215245143e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.640536103997 -angle -7.5583618927e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.359463896003 -angle -4.2416941003e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.469336103997 -angle -5.5381923067e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.530663896003 -angle -6.26186368631e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.298136103997 -angle -3.51802272069e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.701863896003 -angle -8.28203327231e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.0 -angle -1.1800055993e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.126936103997 -angle -1.49785313469e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.873063896003 -angle -1.03022028583e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB2" -synrad $sbend_synrad -length 0.955736103997 -angle -1.12777395417e-05 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.0442638960035 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LX0" -length 0.260536103997 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD2" -synrad $quad_synrad -length 0.739463896004 -strength [expr -0.00965933466269*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD2" -synrad $quad_synrad -length 1.0 -strength [expr -0.0130626183576*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD2" -synrad $quad_synrad -length 0.241736103996 -strength [expr -0.00315770646975*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.453463896004 -angle -1.60755028716e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.375336103996 -angle -1.330583685e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.624663896004 -angle -2.21446213083e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.204136103996 -angle -7.23671841333e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.795863896004 -angle -2.8213739745e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.0329361039962 -angle -1.16759997662e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.967063896004 -angle -3.42828581817e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.861736103996 -angle -3.05489396983e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.138263896004 -angle -4.90151846009e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.690536103996 -angle -2.44798212616e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.309463896004 -angle -1.09706368968e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.519336103996 -angle -1.84107028248e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.480663896004 -angle -1.70397553335e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.348136103996 -angle -1.23415843881e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.651863896004 -angle -2.31088737702e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.176936103996 -angle -6.27246595142e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.823063896004 -angle -2.91779922069e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.00573610399624 -angle -2.03347514711e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.994263896004 -angle -3.52471106436e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFB1" -synrad $sbend_synrad -length 0.834536103996 -angle -2.95846872364e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.165463896004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LX0" -length 0.139336103996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.860663896004 -angle -3.05109294337e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.968136103996 -angle -3.43208684463e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0318638960039 -angle -1.12958971205e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.796936103996 -angle -2.82517500096e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.203063896004 -angle -7.19870814876e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.625736103996 -angle -2.21826315729e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.374263896004 -angle -1.32678265855e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.454536103996 -angle -1.61135131362e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.545463896004 -angle -1.93369450222e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.283336103996 -angle -1.00443946995e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.716663896004 -angle -2.54060634589e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.112136103996 -angle -3.97527626276e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.887863896004 -angle -3.14751818956e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.940936103996 -angle -3.33566159844e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0590638960039 -angle -2.09384217395e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.769736103996 -angle -2.72874975477e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.230263896004 -angle -8.16296061066e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.598536103996 -angle -2.1218379111e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.401463896004 -angle -1.42320790474e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.427336103996 -angle -1.51492606743e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.267863896004 -angle -9.49589783742e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.560936103996 -angle -1.98854418842e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.439063896004 -angle -1.55650162741e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.389736103996 -angle -1.38163234475e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.610263896004 -angle -2.16341347108e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.218536103996 -angle -7.7472050108e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.781463896004 -angle -2.77032531476e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.047336103996 -angle -1.67808657409e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.952663896004 -angle -3.37723715843e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.876136103996 -angle -3.10594262957e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.123863896004 -angle -4.39103186262e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.704936103996 -angle -2.4990307859e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.295063896004 -angle -1.04601502993e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.533736103996 -angle -1.89211894223e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.466263896004 -angle -1.6529268736e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.362536103996 -angle -1.28520709856e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.637463896004 -angle -2.25983871728e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.191336103996 -angle -6.78295254889e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.808663896004 -angle -2.86675056095e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.020136103996 -angle -7.13834112184e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.675063896004 -angle -2.39313243995e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.153736103996 -angle -5.45001532214e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.846263896004 -angle -3.00004428362e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.982536103996 -angle -3.48313550438e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0174638960041 -angle -6.19103114574e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.811336103996 -angle -2.87622366071e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.188663896004 -angle -6.68822155128e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.640136103996 -angle -2.26931181704e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.359863896004 -angle -1.2757339988e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.468936103996 -angle -1.66239997336e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.531063896004 -angle -1.88264584247e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.297736103996 -angle -1.05548812969e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.702263896004 -angle -2.48955768614e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.126536103996 -angle -4.48576286023e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.873463896004 -angle -3.09646952981e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.955336103996 -angle -3.38671025819e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0446638960041 -angle -1.58335557648e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.784136103996 -angle -2.77979841452e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.215863896004 -angle -7.65247401319e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.612936103996 -angle -2.17288657085e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0822638960042 -angle -2.91629280324e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.746536103996 -angle -2.64650469184e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.253463896004 -angle -8.98541123995e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.575336103996 -angle -2.03959284817e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.424663896004 -angle -1.50545296767e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.404136103996 -angle -1.4326810045e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.595863896004 -angle -2.11236481134e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.232936103996 -angle -8.25769160827e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.767063896004 -angle -2.71927665501e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.0617361039958 -angle -2.18857317156e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.938263896004 -angle -3.32618849868e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.890536103996 -angle -3.15699128932e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.109463896004 -angle -3.88054526515e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.719336103996 -angle -2.55007944565e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.280663896004 -angle -9.94966370186e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.548136103996 -angle -1.94316760198e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.451863896004 -angle -1.60187821386e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.376936103996 -angle -1.33625575831e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.623063896004 -angle -2.20879005753e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 0.205736103996 -angle -7.29343914637e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.489463896004 -angle -1.73517193653e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.339336103996 -angle -1.20296203563e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.660663896004 -angle -2.3420837802e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.168136103996 -angle -5.96050191961e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.831863896004 -angle -2.94899562387e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.996936103996 -angle -3.53418416413e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.00306389600428 -angle -1.08616517101e-08 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.825736103996 -angle -2.92727232045e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.174263896004 -angle -6.17773495381e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.654536103996 -angle -2.32036047678e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.345463896004 -angle -1.22468533905e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.483336103996 -angle -1.71344863311e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.516663896004 -angle -1.83159718272e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.312136103996 -angle -1.10653678944e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.687863896004 -angle -2.43850902639e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.140936103996 -angle -4.9962494577e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.859063896004 -angle -3.04542087007e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.969736103996 -angle -3.43775891793e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.0302638960043 -angle -1.07286897901e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 0.798536103996 -angle -2.83084707426e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.201463896004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "LX0" -length 0.103336103996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.896663896004 -angle -3.17871459274e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.932136103996 -angle -3.30446519526e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.0678638960044 -angle -2.40580620577e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.760936103996 -angle -2.69755335159e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.239063896004 -angle -8.47492464248e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.589736103996 -angle -2.09064150792e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.410263896004 -angle -1.45440430792e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.418536103996 -angle -1.48372966425e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.581463896004 -angle -2.06131615159e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.247336103996 -angle -8.76817820575e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.752663896004 -angle -2.66822799526e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.0761361039956 -angle -2.69905976904e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.923863896004 -angle -3.27513983893e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.904936103996 -angle -3.20803994907e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.0950638960044 -angle -3.37005866767e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.733736103996 -angle -2.6011281054e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.266263896004 -angle -9.43917710438e-07 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.562536103996 -angle -1.99421626173e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.437463896004 -angle -1.55082955411e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.0 -angle -3.54504581584e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 0.391336103996 -angle -1.38730441806e-06 -e0 $e0 -E1 0.0 -E2 0.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "D2OD" -length 0.608663896004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D2OD" -length 0.696782793996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT" -length 0.303217206004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "OCTDRIFT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "OCTDRIFT" -length 0.696782793996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF1" -synrad $mult_synrad -type 3 -length 0.303217206004 -strength [expr -0.35588218548*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SF1" -synrad $mult_synrad -type 3 -length 0.306382793996 -strength [expr -0.35959759592*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.388817206004 -strength [expr 0.0102360401195*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.0 -strength [expr 0.0263260986434*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.0 -strength [expr 0.0263260986434*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.0 -strength [expr 0.0263260986434*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.607110793996 -strength [expr 0.0159828586502*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D1OD" -length 0.392889206004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D1OD" -length 0.864710793996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "OCTDRIFT" -length 0.135289206004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "OCTDRIFT" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "OCTDRIFT" -length 0.864710793996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "DEC0" -length 0.135289206004 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "DEC0" -length 0.474310793996 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD0" -synrad $mult_synrad -type 3 -length 0.220889206005 -strength [expr 0.958526270868*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Multipole -name "SD0" -synrad $mult_synrad -type 3 -length 0.388710793995 -strength [expr 1.68677100413*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm
Quadrupole -name "QD0" -synrad $quad_synrad -length 0.306489206005 -strength [expr -0.0235032590572*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.0 -strength [expr -0.0766854381711*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.0 -strength [expr -0.0766854381711*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.0 -strength [expr -0.0766854381711*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Quadrupole -name "QD0" -synrad $quad_synrad -length 0.0463107939953 -strength [expr -0.00355136352958*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole
Girder
Drift -name "D0" -length 0.953689206005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D0" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D0" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D0" -length 1.0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "DIPOLE_GRID" -length 0.0 -strength_x 0.0 -strength_y 0.0
Drift -name "D0" -length 0.343990793995 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
