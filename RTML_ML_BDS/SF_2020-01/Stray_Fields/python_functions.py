#----------------------------------------------------------------------------------#
# This code was developed by C. Gohil.                                             #
# Last modified: 15/04/20.                                                         #
#----------------------------------------------------------------------------------#
from __future__ import print_function
from math import ceil
from os.path import isfile, exists

import numpy as np
from scipy import special, interpolate

# Lengths of different sections [m]
L_RTML = {'e-': 9525.44, 'e+': 9234.1}
L_ML   = 3483.97
L_BDS  = {'6': 1949.11, 6: 1949.11}

# Markers for the start or end position of different sections [m]
L_RTML_VT_START      = 1970.0
L_RTML_VT_END        = 3435.0
L_RTML_TL_START      = 3505.5
L_RTML_TL_END        = 7159.5
L_BDS_COLL_START     = 200.0
L_BDS_COLL_END       = 850.0
L_BDS_FF_SBEND_START = 1300.0

#
# FILE INPUT/OUTPUT FUNCTIONS
#
def check_file_exists(path):
    '''
    This function checks whether or not a file exists. If it does not exist this
    function exits the programme.
    '''
    if not isfile(path):
        print('file %s not found' % (path))
        exit()
    return

def is_number(number_string):
    '''
    This function checks if a string contains a number.
    '''
    # Check a string argument has been passed
    if type(number_string) != str:
        print('a string must be passed')
        exit()

    # Check if the string is a number or not
    try:
        float(number_string)
        return True
    except:
        return False

def read_file(filename, column_names=[], delimiter=' ', no_message=False):
    '''
    This function reads a file.

    Returns either:
    - if column_names is not specified two dimensional numpy array with the
      first index corresponding to a column in the data and the second index
      corresponding to a row.
    - if column_names is specified a dictionary of numpy arrays corresponding
      each column in the data file.

    There must be no missing data in the rows, i.e. each row contains
    data for every column.
    '''
    check_file_exists(filename)

    if not no_message:
        print('Reading', filename)

    # Read the file
    data_file = open(filename, 'r')
    raw_lines = data_file.readlines()
    data_file.close()

    # Remove comments from the file
    lines = []
    for line in raw_lines:
        if line[0] != '#':
            lines.append(line)

    # Get the number of rows
    num_rows = len(lines)

    # Get the number of columns
    num_cols = len(lines[0].split(delimiter))

    # See if each column is a float
    is_numbers = [is_number(elem) for elem in lines[0].split(delimiter)]

    # Check if each column is a complex number
    is_complex = ['j' in elem for elem in lines[0].split(delimiter)]
    if True in is_complex:
        is_complex = True
    else:
        is_complex = False

    # Initialise a numpy array to hold the columns
    if False in is_numbers:
        # Numpy array containing floats and strings
        data = np.empty([num_cols, num_rows]).astype(object)
    elif is_complex:
        # Numpy array containing complex numbers
        data = np.empty([num_cols, num_rows], dtype=np.complex_)
    else:
        # Numpy array containing only floats
        data = np.empty([num_cols, num_rows])

    # Check if we should be using a comma delimiter
    if ',' in lines[0] and delimiter == ' ':
        print('a space delimiter has been set but lines in data ' + \
              'file contain a comma. Use delimiter=","')
        exit()

    # Check if we should use delimiter==None
    if '    ' in lines[0] and delimiter == '    ':
        print('a space delimiter has been set line lines in data ' + \
              'file contain a tab. Use delimiter=None')
        exit()

    # Loop through all the rows in the data file
    for row in range(num_rows):
        line = lines[row]
        elem = line.split(delimiter)

        # Check there is no data missing
        if len(elem) != num_cols:
            print('there is missing data in the file')
            exit()

        # Add the data to the array
        for col in range(num_cols):
            if is_numbers[col]:
                data[col][row] = float(elem[col])
            elif is_complex:
                data[col][row] = complex(elem[col])
            else:
                data[col][row] = elem[col].rstrip()

     # If column names have been specified return the data as a dictionary
    if column_names != []:
        data = lists_to_dictionary(data, column_names)

    if is_complex:
        return data.astype(np.complex_)
    else:
        return data

def reconfigure_arrays(direction, arrays, dims=None):
    '''
    This function rearranges data used to make 2D histogram plots.
    - direction is 'uck' or 'pack'.
    - x is the x-coordinates of the data.
    - y is the y-coordinates of the data.
    - z is a 2D array containing the data.
    '''
    if direction == 'pack':
        len_x = len(arrays[0])
        len_y = len(arrays[1])
        x = [np.insert(np.repeat(arrays[0], len_y), 0, len_x)]
        y = [np.insert(np.tile(arrays[1],   len_x), 0, len_y)]
        z = [np.insert(z, 0, len_x*len_y) for z in arrays[2:]]

    elif direction == 'uck':
        if dims == None:
            len_x = int(arrays[0][0])
            len_y = int(arrays[1][0])
            len_z = int(arrays[2][0])
            x = [arrays[0][1:][::len_y]]
            y = [np.unique(arrays[1][1:])]
            z = [z[1:].reshape(len_y, len_x) for z in arrays[2:]]
        else:
            if len(dims) != 3:
                print('three lengths must be given for the number of x ' + \
                      'points, y points and z points')
                exit()
            len_x = int(dims[0])
            len_y = int(dims[1])
            len_z = int(dims[2])
            x = [arrays[0][::len_y]]
            y = [np.unique(arrays[1])]
            z = [z.reshape(len_y, len_x) for z in arrays[2:]]
    else:
        print('direction unknown')
        exit()

    return x + y + z

#
# PASSIVE SHIELDING
#

# Material properties (values taken from Wikipedia).
# conductivity is in [S/m].
# resistivity is in [Ohm m].
copper = {
    'rel_permeability': 1,
    'conductivity'    : 5.96e7,
    'resistivity'     : 1.68e-8,
}
steel = {
    'rel_permeability': 1,
    'conductivity'    : 1.46e6,
    'resistivity'     : 6.9e-7,
}
mu_metal = {
    'rel_permeability': 50000,
    'conductivity'    : 1.7e6,
    'resistivity'     : 5.9e-7,
}

def setup_beam_pipe(shells, inner_radius):
    '''
    Returns the material properties of the beam np.pipe in CLIC.
    - shells is a list of lists containing the material name and thickness [in cm] of
      the shells of the beam np.pipe.
    - beam_np.pipe is returned as a list. Each item contains a dictionary with
      material properties.
    - conducitivity is in [S/m].
    - radii are in [cm].
    '''
    beam_np.pipe = []

    # Loop through the different shells forming the beam np.pipe
    for [name, thickness] in shells:
        if name == 'steel':
            conductivity     = steel['conductivity']
            rel_permeability = steel['rel_permeability']
        elif name == 'copper':
            conductivity     = copper['conductivity']
            rel_permeability = copper['rel_permeability']
        elif name == 'mu-metal' or name == 'mu_metal':
            conductivity     = mu_metal['conductivity']
            rel_permeability = mu_metal['rel_permeability']
        else:
            print('material type unknown')
            exit()

        # Calculate the outer radius of the shell
        outer_radius = inner_radius + thickness

        # Create a dictionary containing the properties of the shell
        shell = {'conductivity'    : conductivity,
                 'rel_permeability': rel_permeability,
                 'inner_radius'    : inner_radius,
                 'outer_radius'    : outer_radius}

        # Add the shell to the beam np.pipe
        beam_np.pipe.append(shell)

        # Set the new inner radius for the next shell
        inner_radius = outer_radius

    return beam_np.pipe

def get_shield_transfer_matrix(configuration, frequency, material_properties):
    '''
    This function returns the transfer matrix for a specific shield and source
    configuration.
    '''
    # Get the properties of the shell we are calculating the transfer matrix for
    # N.B. the inner radius and outer radius will be given in units of [cm]
    sigma = material_properties['conductivity']
    mu_r  = material_properties['rel_permeability']
    a     = 1e-2*material_properties['inner_radius']
    b     = 1e-2*material_properties['outer_radius']

    # Calculate the skin depth for this shell [m]
    delta = skin_depth(2*np.pi*frequency, mu_r*mu_0, sigma)

    # Calculate the complex wavenumber for the magnetic field in this shell
    gamma = (1.0 + 1j)/delta

    # Transfer matrix for a linear magnetic material forming a hollow cylinder
    # in a transverse AC field
    if configuration == 'cylinder_transv_ac_field':

        # Initialise the transfer matrix
        T = [[0,0],[0,0]]

        # Calculate parameters used to calculate transfer matrix elements
        ga     = gamma*a
        gb     = gamma*b
        I1_ga  = special.iv(1, ga)
        I1_gb  = special.iv(1, gb)
        I1p_ga = special.ivp(1, ga)
        I1p_gb = special.ivp(1, gb)
        K1_ga  = special.kv(1, ga)
        K1_gb  = special.kv(1, gb)
        K1p_ga = special.kvp(1, ga)
        K1p_gb = special.kvp(1, gb)

        # Set the values of each element
        T[0][0] = gb * (I1p_ga*K1_gb - I1_gb*K1p_ga)
        T[0][1] = (1.0/mu_r) * (gb**2) * (I1p_gb*K1p_ga - I1p_ga*K1p_gb)
        T[1][0] = mu_r * (gb/ga) * (I1_ga*K1_gb - I1_gb*K1_ga)
        T[1][1] = ((gb**2)/(ga)) * (I1p_gb*K1_ga - I1_ga*K1p_gb)

    # Transfer matrix for a linear magnetic material forming a hollow cylinder
    # in a longitudinal AC field
    elif configuration == 'cylinder_long_ac_field':

        # Initialise the transfer matrix
        T = [[0,0],[0,0]]

        # Calculate parameters used to calculate transfer matrix elements
        ga     = gamma*a
        gb     = gamma*b
        I0_ga  = iv(0, ga)
        I0_gb  = iv(0, gb)
        I0p_ga = ivp(0, ga)
        I0p_gb = ivp(0, gb)
        K0_ga  = kv(0, ga)
        K0_gb  = kv(0, gb)
        K0p_ga = kvp(0, ga)
        K0p_gb = kvp(0, gb)

        # Set the values of each element
        T[0][0] = gb * (I0p_gb*K0_ga - I0_ga*K0p_gb)
        T[0][1] = 1j * (1.0/mu_r) * (gb**2) * (I0_ga*K0_gb - I0_gb*K0_ga) 
        T[1][0] = 1j * mu_r * (gb/ga) * (I0p_ga*K0p_gb - I0p_gb*K0p_ga)
        T[1][1] = ((gb**2)/ga) * (I0p_ga*K0_gb - I0_gb*K0p_ga)

    # Any other configuration has not been coded
    else:
        print('the shield-source configuration is unknown')
        exit()

    return T

def H_alpha_B_alpha(T, HBBB):
    '''
    This function transforms the tangential magnetic field and normal flux
    density (or tangential electric field) from an outer surface of a shell
    to an inner surface.
    '''
    # Initialise a list to hold the magnetic field and flux density
    HBBA = [0, 0]

    # Calculate the magnetic field and flux density on the other side
    HBBA[0] = T[0][0]*HBBB[0] + T[0][1]*HBBB[1]
    HBBA[1] = T[1][0]*HBBB[0] + T[1][1]*HBBB[1]

    return HBBA

def lin_mat_cylinder_transv_ac_field(f, shells, complex_num=False, dont_error=False):
    '''
    This function calculates the reduction factor of an infinitely long
    cylindrical with an AC magnetic field in the transverse direction.

    This function calculates the shielding factor for a linear magnetic
    material, i.e. one that obeys B = mu*H, in a time-varying magnetic field.

    The calculation of the shielding factor follows that outlined in
    "A Computational Methodology and Results for Quasistatic Multilayered
    Magnetic Shielding" by J. F. Hoburg, c.f. Sec. VI.
    
    shells is a list of dictionaries containing the material properties of
    each cylindrical shield forming the shield.
    '''
    # Check that we are calculating the shielding factor for an AC field
    if f == 0:
        return 1.0

    # Get the transfer matrix for each shell to calculate the impedance and
    # magnetic fields throughout the shield
    transfer_matrices = []
    for shell in shells:
        transfer_matrices.append(
            get_shield_transfer_matrix('cylinder_transv_ac_field', abs(f), shell)
        )

    # 1) Set the magnetic impedance at the surface of the shielded region.
    Z = 1

    # 2) Transform the magnetic impedance layer by layer to the surface of the 
    #    source region.
    for T in transfer_matrices:
        Z = Z_beta(T, Z)

    # 3) Calculate the (normalised) tangential magnetic field and the (normalised)
    #    normal flux density at the surface of the source region.
    H_H0  = 2.0/(1+Z)
    B_H0  = Z*H_H0
    HB_H0 = [H_H0, B_H0]

    # 4) Use the transfer relations to transform the tangential magnetic field and
    #    and normal flux density to the surface of the shielded region.
    for T in transfer_matrices[::-1]:
        HB_H0 = H_alpha_B_alpha(T, HB_H0)

    # 5) Calculate the shielding factor.
    [H_H0, B_H0] = HB_H0

    # Check that the calculation has been performed correctly
    if not abs(H_H0 - B_H0) < 1e-6:
        if dont_error:
            return None
        else:
            print('computation of the shielding factor is inconsistent. ' + \
                  'Values are %s %s' % (H_H0, B_H0))
            exit()

    # The shielding effectiveness is the modulus of the complex value
    if complex_num:
        s = H_H0
    else:
        s = abs(H_H0)
    
    return s

#
# IMPLEMENTATION OF STRAY FIELD SIMULATIONS
#
def save_lattice(path, lattice):
    '''
    This function saves a lattice file.
    '''
    output_lattice = open(path, 'w')
    for element in lattice:
        output_lattice.write(element)
    output_lattice.close()
    return

def get_lattice_elements(lattice_filename):
    '''
    This function reads a lattice file and returns the elements within it.
    '''
    # Read the original lattice file
    lattice_file = open(lattice_filename, 'r')
    lines = lattice_file.readlines()
    lattice_file.close()
    return lines

def get_all_lattice_files(lattice_directory):
    '''
    This function returns the path to lattice files for each section of CLIC.
    '''
    # Path to the lattice file of each section
    rtml_lattice_file    = '%s/0000_e-_rtml.tcl' % (lattice_directory)
    ml_lattice_file      = '%s/1000_db_ml.tcl'   % (lattice_directory)
    bds6_lattice_file    = '%s/2006_bds.tcl'     % (lattice_directory)

    # Generate a dictionary that can be used to look up the location
    # of each lattice file from the section
    lattice_files = {
        'rtml' : rtml_lattice_file,
        'ml'   : ml_lattice_file,
        'bds6' : bds6_lattice_file
    }

    return lattice_files

def calc_constant_stray_field(amplitude, dipole_spacing):
    '''
    This function calculates the kick of a constant stray field.
    The kick is in the vertical direction in keV from an amplitude in nT.
    '''
    c = 3e8; # speed of light [m/s]
    delta  = 1E-12*c*amplitude*dipole_spacing
    return delta

def calc_distance_and_correction_factor(factor_file_data, s):
    '''
    Returns a correction factor that takes into account the direction the beam
    is moving in.
    '''
    index = argwhere(factor_file_data[0]==round(s,1))[0][0]
    return factor_file_data[1][index], factor_file_data[2][index]

def get_dipole_locations(section, spacing):
    '''
    This function returns the locations along a beamline in which dipoles to
    represent a stray field has been placed.
    '''
    if section == 'rtml':
        s = np.arange(0.5*spacing, L_RTML['e-'], spacing)
    elif section == 'ml':
        s = np.arange(0.5*spacing+L_RTML['e-'], L_RTML['e-']+L_ML, spacing)
    elif section == 'bds43':
        s = np.arange(0.5*spacing+L_RTML['e-']+L_ML, L_RTML['e-']+L_ML+L_BDS[43], spacing)
    elif section == 'bds6':
        s = np.arange(0.5*spacing+L_RTML['e-']+L_ML, L_RTML['e-']+L_ML+L_BDS[6], spacing)
    return s

def set_stray_field_dipole_strength(section, spacing, sf_x, sf_y):
    '''
    This function assigns values to the dummy dipoles that represent stray fields.
    '''
    # Get the lattice for the section we're adding stray fields to
    all_lattice_files = get_all_lattice_files('./Lattices')
    lattice_file = all_lattice_files[section][:-4] + '_dipole_grid' + \
                   all_lattice_files[section][-4:]
    lattice = get_lattice_elements(lattice_file)

    # Loop through each element in the lattice
    count = 0
    final_lattice = []
    for element in lattice:
        element = element.split()
        if len(element) > 1:
            element_name = element[0]

            # Check this line is actually an element
            if element_name not in ['#', 'proc', 'upvar', 'set',
                                    'SetReferenceEnergy', 'TclCall']:

                # Assign a strength to the dipoles
                if element_name == 'Dipole':
                    if len(element) >= 2:
                        if element[2] == '"DIPOLE_GRID"':
                            delta_x = calc_constant_stray_field(sf_x[count], spacing)
                            delta_y = calc_constant_stray_field(sf_y[count], spacing)
                            element[6] = str(delta_x)
                            element[8] = str(delta_y)
                            final_lattice.append(' '.join(element) + '\n')
                            count += 1

                # Otherwise, just add the element to the list
                else:
                    final_lattice.append(' '.join(element) + '\n')
            else:
                final_lattice.append(' '.join(element) + '\n')
        else:
            final_lattice.append(' '.join(element) + '\n')

    # Check the correct number of dipoles were in the lattice
    if count != len(sf_x):
        print('there was a miss match between the number of stray field ' + \
              'strengths passed and the number of dipoles in the lattice')
        exit()

    # Save the final lattice
    save_lattice(lattice_file, final_lattice)

    return

def in_shielded_region(s, shielding):
    if shielding == 'none':
        shield = False
    elif shielding == 'rtml':
        shield = (s > L_RTML_VT_START and s < L_RTML_VT_END) or \
                 (s > L_RTML_TL_START and s < L_RTML_TL_END)
    elif shielding == 'bds':
        shield = (s > L_RTML['e-'] + L_ML + L_BDS_COLL_START and \
                  s < L_RTML['e-'] + L_ML + L_BDS_COLL_END) or \
                 (s > L_RTML['e-'] + L_ML + L_BDS_FF_SBEND_START)
    elif shielding == 'rtml+bds':
        shield = (s > L_RTML_VT_START and s < L_RTML_VT_END) or \
                 (s > L_RTML_TL_START and s < L_RTML_TL_END) or \
                 (s > L_RTML['e-'] + L_ML + L_BDS_COLL_START and \
                  s < L_RTML['e-'] + L_ML + L_BDS_COLL_END) or \
                 (s > L_RTML['e-'] + L_ML + L_BDS_FF_SBEND_START)
    return shield

def generator(model_filename, model_type, section, use_filter, sf_filter,
              sf_filter_ml, delta_T, spacing, sf_seed, iteration, particle,
              geometry_folder, profile_folder=None, shielding='none'):
    '''
    This function is the stray field generator used in PLACET simulations.
    '''
    # Dipole grid parameters
    s    = get_dipole_locations(section, spacing); # Location of dipoles
    n_d  = len(s);                                 # Number of dipoles

    # Stray field to be calculated
    sf_x = np.empty(n_d)
    sf_y = np.empty(n_d)

    # Time for which we are evaluating the stray field
    t = delta_T*iteration; 

    # Set the seed of the generator
    np.random.seed(sf_seed)

    #
    # Geometry of CLIC (changes direction of the kick)
    #
    # Read the file containing the geometry correction factors for the beamline
    geometry = read_file('%s/%s.dat' % (geometry_folder, section), no_message=True)

    # Reflect the geometry if we're generating the stray field for the e+ beam
    if particle == 'e+':
        geometry[1] *= -1

    # Distance away from the IP of each dipole
    z = geometry[1]

    # Correction factor for the kick due to the geometry
    y_corr_factor = geometry[2]

    #
    # Transfer functions of mitigation
    #
    # Beam-based feedback system and/or Beam np.pipe
    if use_filter:
        if section == 'ml':
            [f_tf, tf] = read_file(sf_filter_ml, no_message=True)
        else:
            [f_tf, tf] = read_file(sf_filter, no_message=True)

        # Interpolate the transfer function so that it can be calculated at
        # the same frequencies as the model
        tf_interp = interpolate.interp1d(f_tf, tf, bounds_error=False, fill_value=0.0)

    # Mu-metal shield
    if shielding != 'none':
        mu_metal = setup_beam_pipe([['mu-metal', 0.1]], inner_radius=1.0)
        f_sh_tf  = np.arange(0, 500, 1); # above 500 Hz is effectively zero
        sh_tf    = [lin_mat_cylinder_transv_ac_field(fi, mu_metal) for fi in f_sh_tf]
        sh_tf_interp = interpolate.interp1d(f_sh_tf, sh_tf, bounds_error=False, fill_value=0)

    #
    # 1D PSD models
    #
    if model_type == 'homogeneous':

        # Read the file containing the model
        [f_psd, psd_z, psd_x, psd_y] = read_file(model_filename, no_message=True)

        # Frequency bin of the model (assumed to be constant)
        df = f_psd[1] - f_psd[0]

        # Number of different frequency modes in the model
        n_f = len(f_psd)

        # Give modes a np.random phase
        phi_x = np.random.uniform(0, 2*np.pi, n_f)
        phi_y = np.random.uniform(0, 2*np.pi, n_f)

        # Apply a filter to the model if specified
        if use_filter == 1:
            psd_x *= tf_interp(f_psd)**2
            psd_y *= tf_interp(f_psd)**2

        # Calculate the amplitudes for each frequency mode from the model
        A_x = sqrt(2*psd_x*df)
        A_y = sqrt(2*psd_y*df)

        # Calculate the stray field amplitudes at this instance in time
        sf_x_modes = A_x*np.sin(2*np.pi*f_psd*t + phi_x)
        sf_y_modes = A_y*np.sin(2*np.pi*f_psd*t + phi_y)

        # Calculate the magnetic field at the location of each dipole, which is the
        # sum of all frequency modes.
        # N.B. don't need a correction factor for horizontal kicks because
        # there's not much vertical beam motion
        for d in range(n_d):
            sf_x[d] = np.sum(sf_x_modes)
            sf_y[d] = y_corr_factor[d]*np.sum(sf_y_modes)

    #
    # 2D PSD models
    #
    elif model_type == '2d_psd':
        
        # Read file containing the 2D amplitudes
        [f, k, a] = reconfigure_arrays('uck',
                                       read_file(model_filename, no_message=True))

        # Transpose 'a' because currently the first axis is wavenumber
        a = a.T

        # Convert frequency to angular frequency
        w = 2*np.pi*f

        # Number of frequency modes
        n_f = f.shape[0]

        # Number of wavenumber modes
        n_k = k.shape[0]

        # Random phases
        phi_x   = np.random.uniform(0, 2*np.pi, [n_f, n_k])
        phi_y   = np.random.uniform(0, 2*np.pi, [n_f, n_k])
        theta_x = np.random.uniform(0, 2*np.pi, n_k)
        theta_y = np.random.uniform(0, 2*np.pi, n_k)

        # Apply a filter to the model if specified
        if use_filter == 1:
            filter_tf = tf_interp(f)
            a = a*filter_tf[:,None]

        sin_wt = np.sin(w*t)
        cos_wt = np.cos(w*t)

        # Calculate an effective amplitude for this instance in time
        A_x = np.empty([n_f, n_k])
        A_y = np.empty([n_f, n_k])
        a_sin_phi_x = a*np.sin(phi_x)
        a_cos_phi_x = a*np.cos(phi_x)
        a_sin_phi_y = a*np.sin(phi_y)
        a_cos_phi_y = a*np.cos(phi_y)
        A_x = a_cos_phi_x*cos_wt[:,None] - a_sin_phi_x*sin_wt[:,None]
        A_y = a_cos_phi_y*cos_wt[:,None] - a_sin_phi_y*sin_wt[:,None]

        # Calculate the amplitudes in the shielded region
        if shielding != 'none':
            sh_filter_tf = sh_tf_interp(f)
            sh_a = a*sh_filter_tf[:,None]
            sh_A_x = np.empty([n_f, n_k])
            sh_A_y = np.empty([n_f, n_k])
            sh_a_sin_phi_x = sh_a*np.sin(phi_x)
            sh_a_cos_phi_x = sh_a*np.cos(phi_x)
            sh_a_sin_phi_y = sh_a*np.sin(phi_y)
            sh_a_cos_phi_y = sh_a*np.cos(phi_y)
            sh_A_x = sh_a_cos_phi_x*cos_wt[:,None] - sh_a_sin_phi_x*sin_wt[:,None]
            sh_A_y = sh_a_cos_phi_y*cos_wt[:,None] - sh_a_sin_phi_y*sin_wt[:,None]

        # Loop through dipoles
        for d in range(n_d):
            # use z (distance from the IP) instead of s (position along the machine)
            cos_ks_theta_x = np.cos(k*z[d] + theta_x)
            cos_ks_theta_y = np.cos(k*z[d] + theta_y)
            if in_shielded_region(s[d], shielding):
                B_x = np.sum(sh_A_x*cos_ks_theta_x[None,:])
                B_y = np.sum(sh_A_y*cos_ks_theta_y[None,:])
            else:
                B_x = np.sum(A_x*cos_ks_theta_x[None,:])
                B_y = np.sum(A_y*cos_ks_theta_y[None,:])
            # N.B. don't need a correction factor for horizontal kicks because
            # there's not much vertical beam motion
            sf_x[d] = B_x
            sf_y[d] = y_corr_factor[d]*B_y

    #
    # Specify a profile (or envelop) along the machine
    #
    elif model_type == 'profile':
        if profile_folder == None:
            print('a profile file must be specified')
            exit()
        else:
            profile = read_file('%s/%s_%s.dat' % (profile_folder, particle, section),
                                no_message=True)

        # Read the file containing the model
        [f_psd, psd] = read_file(model_filename, no_message=True)

        # Frequency bin of the model (assumed to be constant)
        df = f_psd[1] - f_psd[0]

        # Number of different frequency modes in the model
        n_f = len(f_psd)

        # Give modes a np.random phase
        phi = np.random.uniform(0, 2*np.pi, n_f)

        # Apply a filter to the model if specified
        if use_filter == 1:
            psd *= tf_interp(f_psd)**2

        # Calculate the amplitudes for each frequency mode from the model
        A = sqrt(2*psd*df)

        # Calculate the stray field amplitudes at this instance in time
        sf_modes = A*np.sin(2*np.pi*f_psd*t + phi)

        # Stray field modes with shielding
        if shielding != 'none':
            sh_psd = psd*sh_tf_interp(f_psd)**2
            sh_A   = sqrt(2*sh_psd*df)
            sh_sf_modes = sh_A*np.sin(2*np.pi*f_psd*t + phi)

        # Calculate the magnetic field at the location of each dipole, which is the
        # sum of all frequency modes.
        for d in range(n_d):
            if in_shielded_region(s[d], shielding):
                B = np.sum(sh_sf_modes)
            else:
                B = np.sum(sf_modes)
            # N.B. don't need a correction factor for horizontal kicks because
            # there's not much vertical beam motion
            sf_x[d] = profile[1][d]*B
            sf_y[d] = profile[2][d]*y_corr_factor[d]*B

    # Any other stray field model hasn't been written
    else:
        print('model type unknown')
        exit()

    # Set dipole strengths in accordance with the model
    set_stray_field_dipole_strength(section, spacing, sf_x, sf_y)

    return
