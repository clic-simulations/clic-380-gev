#
# This script setups the ML lattice and wakefields
#

# Long-range wakefields
source $scriptdir/clic_basic_single.tcl

# Lattice
BeamlineNew
source $latticedir/1001_kly_ml.tcl
BeamlineSet -name ml
