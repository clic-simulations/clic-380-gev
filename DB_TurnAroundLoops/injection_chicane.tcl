##################################################################
## Placet2 lattice file, "clean" version of TurnAround_auto.tcl
## Author: Raul Costa (raul.costa@cern.ch)
## File generated in 2019-02-11 
##################################################################	
Sbend	"PEM_B1"	-len	1.005115441	-angle	-0.1748804447	-E1	0	-E2	-0.1748804447	
Drift	"PEM_D12"	-len	2.538722248	
Sbend	"PEM_B2"	-len	1.005115441	-angle	0.1748804447	-E1	0.1748804447	-E2	0	
Drift	"PEM_D23"	-len	1	
Sbend	"PEM_B3"	-len	1.005115441	-angle	0.1748804447	-E1	0	-E2	0.1748804447	
Drift	"PEM_D34"	-len	2.538722248	
Sbend	"PEM_B4"	-len	1.005115441	-angle	-0.1748804447	-E1	-0.1748804447	-E2	0	
