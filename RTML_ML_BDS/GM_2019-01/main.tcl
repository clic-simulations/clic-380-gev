#
# CLIC 380 GeV: Main Beam (RTML to IP)
#

# Initialise a timer
Octave {initial = tic();}

# Set the number of cores to run in parallel
ParallelThreads -num 4

# Directories containing files needed by the simulation
set latticedir ./Lattices
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set wakedir    ./Wakefields
set beamdir    ./Beams
set resultsdir ./Results
set gmdir      ./Ground_Motion

#
# Command line arguments
#
set particle  [lindex $argv 0]
set beam_seed [lindex $argv 1]
set bds_type  [lindex $argv 2]
set gm_seed   [lindex $argv 3]
set gm_iter   [lindex $argv 4]

#
# Setup the simulation
#
puts   "$particle beam:"
source $scriptdir/octave_functions.tcl
source $scriptdir/beam_setup.tcl

# Set parameters
source $scriptdir/set_parameters.tcl

puts "Generating initial particle distribution"
create_particles_file $beamdir/particles.$particle.in beamparams $beam_seed

puts "Setting up lattice"
source $scriptdir/rtml_beamline_and_wake.tcl
source $scriptdir/ml_beamline_and_wake.tcl
source $scriptdir/bds_beamline.tcl

# Setup the beam
make_particle_beam beam0 beamparams $beamdir/particles.$particle.in $wakedir/zero_wake.dat

#
# Ground motion
#
source $gmdir/ground_motion.tcl
puts "Setting up ground motion:"
set_rtml_ground_motion $gm_seed $gm_iter $particle
set_ml_ground_motion   $gm_seed $gm_iter $particle
set_bds_ground_motion  $gm_seed $gm_iter $particle

#
# Tracking
#
Octave {
    # Display how long it took to set up the lattices
    toc(initial);

    # Track the beam through the RTML
    tic();
    disp("\nTracking RTML");
    [emitt, beam] = placet_test_no_correction("rtml", "beam0", "None",
        "%ex %ey %sex %sey %x %y %xp %yp %Env %sx %sy %sxp %syp %E %dE %s %n %name");
    rtml = get_tracking_results(beam, 5);
    save -text $resultsdir/tracking.$particle.rtml emitt;
    placet_set_beam("beam0", beam);
    toc();

    # Track the beam through the ML
    tic();
    disp("\nTracking ML");
    [emitt, beam] = placet_test_no_correction("ml", "beam0", "None",
        "%ex %ey %sex %sey %x %y %xp %yp %Env %sx %sy %sxp %syp %E %dE %s %n %name");
    ml = get_tracking_results(beam, 5);
    save -text $resultsdir/tracking.$particle.ml emitt;
    placet_set_beam("beam0", beam);
    toc();

    # Track the beam through the BDS
    tic();
    disp("\nTracking BDS");
    [emitt, beam] = placet_test_no_correction("bds", "beam0", "None",
        "%ex %ey %sex %sey %x %y %xp %yp %Env %sx %sy %sxp %syp %E %dE %s %n %name");
    bds = get_tracking_results(beam, 3);
    save -text $resultsdir/tracking.$particle.bds emitt;
    toc();

    # Save the results, final beam and display the total run time
    save_tracking_results("$resultsdir/tracking.$particle.out", rtml, ml, bds);
    save_beam("$beamdir/particles.$particle.out", beam);
    disp("\nTotal:");
    toc(initial);
}

exit
