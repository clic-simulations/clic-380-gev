#
# This script checks the number of the arguments passed to the run script is correct.
#
if {$argc != 4} {
    puts "Error in run.tcl: incorrect number of arguments passed."
    exit
}
