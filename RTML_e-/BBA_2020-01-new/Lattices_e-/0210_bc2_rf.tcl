#
# BC2 RF lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# rfparams=[gradient,phase,lambda,a,g,l,delta,delta_g]
# units=[eV/m,Degree,m,m,m,m,m,m]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_bc2_rf {rfparray bparray} {
    global placetunits energy_wake_bc2
    upvar $rfparray rfparams
    upvar $bparray  beamparams

    set usesixdim     1
    set numthinlenses 100
    set quad_synrad   0

    # Wakefield parameters
    if {$beamparams(usewakefields)==1} {
        set rfparams(gradient) [expr $rfparams(gradientww)*$placetunits(energy)]
        set rfparams(dewake) $energy_wake_bc2
    } else {
        set rfparams(gradient) [expr $rfparams(gradient)*$placetunits(energy)]
        set rfparams(dewake) 0
    }


    set gradient  $rfparams(gradient)
    set refenergy $beamparams(meanenergy)
    set q0        $beamparams(charge)
    set cavphase  $rfparams(phase)

    set lcav 0.9248
    set lquad 0.4
    set lbpm  0.08
	set ldrift 0.04

    set lgirder 3.0496

    # This is required to take into account the energy gain due to the cavities.
    set pi    [expr acos(-1.0)]
    set deacc [expr $gradient*$lcav*cos(($cavphase)/180.0*$pi)]

    # Energy loss due to wake fields
    set dewake $rfparams(dewake)
    set de [expr $deacc+$dewake]

    set ebeam $refenergy
    SetReferenceEnergy $ebeam

    set lcell  [expr 1.0*$lgirder]
    set mu_rad [expr 360/7.5*acos(-1.0)/180.0]

    set Qf_k1l [expr  4.0*sin($mu_rad/2.0)/$lcell]
    set Qd_k1l [expr -4.0*sin($mu_rad/2.0)/$lcell]

    set kq1 [expr $Qf_k1l/$lquad]
    set kq2 [expr $Qd_k1l/$lquad]

    # Remove any longitudinal offset to ensure that the correct RF phase is seen
    TclCall -script {Octave {B=placet_get_beam(); B(:,4)-=mean(B(:,4)); placet_set_beam(B);}}

    for {set sec 1} {$sec<=11} {incr sec} {
		Girder
    	Bpm -length $lbpm
		Drift -length $ldrift -six_dim $usesixdim
		Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
		Dipole -name "D210-$sec-1"
		Drift -length $ldrift -six_dim $usesixdim
		Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.9942
		set ebeam [expr $ebeam+$de]
		SetReferenceEnergy $ebeam
		Drift -length $ldrift -six_dim $usesixdim
		Bpm -length $lbpm
		Drift -length $ldrift -six_dim $usesixdim
		Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq2*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
		Dipole -name "D210-$sec-2"
		Drift -length $ldrift -six_dim $usesixdim
		Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.9942
		set ebeam [expr $ebeam+$de]
		SetReferenceEnergy $ebeam
		Drift -length $ldrift -six_dim $usesixdim
	}

    set beamparams(meanenergy) $ebeam
}