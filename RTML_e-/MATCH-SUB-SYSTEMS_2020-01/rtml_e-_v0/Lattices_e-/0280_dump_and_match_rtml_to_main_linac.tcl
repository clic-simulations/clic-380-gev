#
# Dump and matching of RTML to Main Linac
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_rtml_to_main_linac {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 10
set quad_synrad 0

set refenergy $beamparams(meanenergy)
set q0 $beamparams(charge)

set lquadm 0.3

set kqm1  1.57487861566943
set kqm2 -1.57485246714103

set ld1 0.0212739958530168
set ld2 0.7358397847406376
set ld3 5.6569792937577867

SetReferenceEnergy $refenergy

Girder
Drift -length $ld1 -six_dim $usesixdim
Dipole -name "D280-1" -length 0.0
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld2 -six_dim $usesixdim
Dipole -name "D280-2" -length 0.0
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld3 -six_dim $usesixdim
Dipole -name "D280-3" -length 0.0
Bpm -length 0.0

}
