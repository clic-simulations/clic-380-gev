#
# This script sets up the BDS beamline and wake.
#

# Long-range wakefields
source $scriptdir/clic_basic_single.tcl

# Lattice
BeamlineNew
source $latticedir/2006_bds.tcl
BeamlineSet -name bds
