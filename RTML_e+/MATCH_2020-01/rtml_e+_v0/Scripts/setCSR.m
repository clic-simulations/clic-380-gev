Octave {
      SI = placet_get_name_number_list("0080", "080S");
      placet_element_set_attribute("0080", SI, "thin_lens", 0);
      placet_element_set_attribute("0080", SI, "csr", true);
      placet_element_set_attribute("0080", SI, "csr_charge", $charge);
      placet_element_set_attribute("0080", SI, "csr_nbins", 510);
      placet_element_set_attribute("0080", SI, "csr_nsectors", 100);
      placet_element_set_attribute("0080", SI, "csr_filterorder", 1);
      placet_element_set_attribute("0080", SI, "csr_nhalffilter", 10);
      placet_element_set_attribute("0080", SI, "csr_enforce_steady_state", true);

      SI = placet_get_name_number_list("0230", "230S");
      placet_element_set_attribute("0230", SI, "thin_lens", 0);
      placet_element_set_attribute("0230", SI, "csr", true);
      placet_element_set_attribute("0230", SI, "csr_charge", $charge);
      placet_element_set_attribute("0230", SI, "csr_nbins", 510);
      placet_element_set_attribute("0230", SI, "csr_nsectors", 100);
      placet_element_set_attribute("0230", SI, "csr_filterorder", 1);
      placet_element_set_attribute("0230", SI, "csr_nhalffilter", 10);
      placet_element_set_attribute("0230", SI, "csr_enforce_steady_state", true);

      SI = placet_get_name_number_list("0250", "250S");
      placet_element_set_attribute("0250", SI, "thin_lens", 0);
      placet_element_set_attribute("0250", SI, "csr", true);
      placet_element_set_attribute("0250", SI, "csr_charge", $charge);
      placet_element_set_attribute("0250", SI, "csr_nbins", 510);
      placet_element_set_attribute("0250", SI, "csr_nsectors", 100);
      placet_element_set_attribute("0250", SI, "csr_filterorder", 1);
      placet_element_set_attribute("0250", SI, "csr_nhalffilter", 10);
      placet_element_set_attribute("0250", SI, "csr_enforce_steady_state", true);
}
