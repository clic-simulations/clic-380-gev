set script_dir "."
source lumi.tcl

set e_initial 9.0
set e0 $e_initial

set scale 1.0

source clic_basic_single.tcl
source lattice4a.tcl
Linac::choose_linac lattice.def -1
Linac::put_linac


BeamlineList -file latnew.tcl

BeamlineSet -name test
exit

set match(emitt_x) 0.92
set match(emitt_y) 0.1
set match(e_spread) 1.0
set match(sigma_z) 70
set match(phase) 0.0
set match(charge) 5.2e9

set charge $match(charge)
source clic_beam.tcl

set n_total 500
set n_slice 21
set n 1

make_beam_slice beam0 $n_slice $n
set n 7
make_beam_slice beam1 $n_slice $n

FirstOrder 0

exec rm -f phase.def
exec cp lattice.def phase.def
set fphase [open phase.def a]
puts $fphase ""
foreach target_phase {0 1 2 3 4 5 6 7  8 9 10 11 12} {
#    foreach target_phase {0} {}
    set phases(gradient) [lindex $argv 0]
    puts "phase [optimise_phase_new2 beam0 2579 3869 $target_phase 30.0]"
    puts "grad [optimise_gradient2 beam0 9 190]"
    puts "phase [optimise_phase_new2 beam0 2579 3869 $target_phase 30.0]"
    puts "grad [optimise_gradient2 beam0 9 190]"
    puts "phase [optimise_phase_new2 beam0 2579 3869 $target_phase 30.0]"
    puts "grad [optimise_gradient2 beam0 9 190]"
    puts "phase [optimise_phase_new2 beam0 2579 3869 $target_phase 30.0]"
    puts "grad [optimise_gradient2 beam0 9 190]"
    puts "phase [optimise_phase_new2 beam0 2579 3869 $target_phase 30.0]"
    puts "grad [optimise_gradient2 beam0 9 190]"
    set res1 "[optimise_phase_new2 beam0 2579 3869 $target_phase 30.0]"
    set res2 "[optimise_gradient2 beam0 9 190]"
    puts "phase $res1"
    puts "grad $res2"
    puts $fphase ""
    puts $fphase "set phase_set($target_phase) \{"
    puts $fphase "    0.n 2579 0.ph $target_phase"
    puts $fphase "    1.n 3869 1.ph [lindex $res1 0]"
    puts $fphase "    2.n 10000 2.ph [lindex $res1 1]"
    puts $fphase "    gradient $res2"
    puts $fphase "\}"
    flush $fphase
}
puts $fphase ""
close $fphase

exit
EnergySpreadPlot -beam beam0 -file espread
TwissPlot -beam beam0 -file twiss

exit

BeamSetToOffset -beam beam0 -y [expr sqrt($match(beta_y)*$match(emitt_y)*1e5*0.511e-3/$e_initial)]
TestNoCorrection -beam beam0 -machines 1 -emitt_file auto -survey Zero

SurveyErrorSet -quadrupole_y 0.01 \
    -cavity_y 0.0 \
    -cavity_realign_y 0.0 \
    -cavity_yp 0.0 \
    -bpm_y 0.0

BeamSetToOffset -beam beam0 -y 0.0
TestNoCorrection -beam beam0 -machines 100 -emitt_file quadrupole -survey Clic

Zero
SurveyErrorSet -quadrupole_y 0.0 \
    -cavity_y 10.0 \
    -cavity_realign_y 10.0 \
    -cavity_yp 0.0 \
    -bpm_y 0.0

TestSimpleCorrection -beam beam0 -machines 100 -emitt_file cavity -survey Clic

Zero
SurveyErrorSet -quadrupole_y 0.0 \
    -cavity_y 0.0 \
    -cavity_realign_y 0.0 \
    -cavity_yp 0.0 \
    -bpm_y 10.0

TestSimpleCorrection -beam beam0 -machines 100 -emitt_file bpm -survey Clic
exit
