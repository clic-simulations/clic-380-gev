set tcl_precision 15
ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set wakedir    ./Wakefields

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/plainTracking.tcl

source $scriptdir/beamline_and_wake.tcl

source $scriptdir/load_mag.tcl

BeamlineList -file lattice-$machine.tcl
