#
# This code was developed by C. Gohil.
#
# Runs GUINEA-PIG to calculate the luminosity of a single bunch.
#
# To run use command: placet guinea_pig.tcl
#
puts "Running GUINEA-PIG"

# Directories
set gpdir      ./Guinea_Pig
set beamdir   ../Beams
set scriptdir ../Scripts
set paramsdir ../Parameters

# Move to another directory to store results
cd $gpdir

# Set up the beam
exec cp $beamdir/particles.e-.out electron.ini
exec cp $beamdir/particles.e-.out positron.ini

# Set the number of particles
set n_slice 50
set n       2000
set n_total [expr $n_slice*$n]

# Set up GUINEA-PIG
source clic_guinea.tcl

# Run GUINEA-PIG
run_guinea 0.0 0.0
