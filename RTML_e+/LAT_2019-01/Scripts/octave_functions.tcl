Octave {
    # Read in some useful functions
    wakedir   = "$wakedir/";
    scriptdir = "$scriptdir/";
    source([scriptdir,"octave_beam_creation.m"]);
    source([scriptdir,"octave_beam_statistics.m"]);
    source([scriptdir,"octave_beam_statistics_output.m"]);
    source([scriptdir,"octave_beamline_errors.m"]);
    source([scriptdir,"octave_correction_algorithms.m"]);
    source([scriptdir,"octave_results.m"]);
    source([wakedir,"octave_cavity_wake_setup.m"]);
}
