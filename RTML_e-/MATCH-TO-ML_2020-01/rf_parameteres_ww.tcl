set rfparamsbooster(gradientww)   1.489875320583e+07
set rfparamsbc1(gradientww)   1.327365952113e+07
set rfparamsbc2(gradientww)   7.186961806470e+07
set energy_wake_bc1  -3.528044340138e-05
set energy_wake_booster  -5.772685777510e-05
set energy_wake_bc2  -7.427173195844e-05
