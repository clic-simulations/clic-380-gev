#
# BC2 RF lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# rfparams=[gradient,phase,lambda,a,g,l,delta,delta_g]
# units=[eV/m,Degree,m,m,m,m,m,m]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_bc2_rf {rfparray bparray} {
    global placetunits energy_wake_bc2
    upvar $rfparray rfparams
    upvar $bparray  beamparams

    set usesixdim     1
    set numthinlenses 100
    set quad_synrad   0

    # Wakefield parameters
    if {$beamparams(usewakefields)==1} {
        set rfparams(gradient) [expr $rfparams(gradientww)*$placetunits(energy)]
        set rfparams(dewake) $energy_wake_bc2
    } else {
        set rfparams(gradient) [expr $rfparams(gradient)*$placetunits(energy)]
        set rfparams(dewake) 0
    }

    set lcav 0.23

    set gradient  $rfparams(gradient)
    set refenergy $beamparams(meanenergy)
    set q0        $beamparams(charge)
    set cavphase  $rfparams(phase)

    set lquad 0.4
    set lbpm  0.08

    # This is required to take into account the energy gain due to the cavities.
    set pi    [expr acos(-1.0)]
    set deacc [expr $gradient*$lcav*cos(($cavphase)/180.0*$pi)]

    # Energy loss due to wake fields
    set dewake $rfparams(dewake)
    set de [expr $deacc+$dewake]

    set ebeam $refenergy
    SetReferenceEnergy $ebeam

    set lgirder 2.61
    set lcell  [expr 2.0*$lgirder]
    set mu_rad [expr 72.0*acos(-1.0)/180.0]

    set Qf_k1l [expr  4.0*sin($mu_rad/2.0)/$lcell]
    set Qd_k1l [expr -4.0*sin($mu_rad/2.0)/$lcell]

    set kq1 [expr $Qf_k1l/$lquad]
    set kq2 [expr $Qd_k1l/$lquad]

    # Remove any longitudinal offset to ensure that the correct RF phase is seen
    TclCall -script {Octave {B=placet_get_beam(); B(:,4)-=mean(B(:,4)); placet_set_beam(B);}}

    Bpm -length $lbpm
    Drift -length 0.04 -six_dim $usesixdim
    for {set sec 1} {$sec<=5} {incr sec} {
        Girder
        Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
        Dipole -name "D210-$sec-1"
        Drift -length 0.08 -six_dim $usesixdim
        Girder
        Drift -length 0.03 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length 0.04 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length 0.04 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length 0.04 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length 0.02 -six_dim $usesixdim
        Bpm -length $lbpm
        Drift -length 0.04 -six_dim $usesixdim
        Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq2*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
        Dipole -name "D210-$sec-2"
        Drift -length 0.08 -six_dim $usesixdim
        Girder
        Drift -length 0.03 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length 0.04 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length 0.04 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length 0.04 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0 -frequency 11.99169832
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length 0.02 -six_dim $usesixdim
        Bpm -length $lbpm
        Drift -length 0.04 -six_dim $usesixdim
    }
    Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "D210-3"
    Bpm -length $lbpm
    Drift -length 0.04 -six_dim $usesixdim

    set beamparams(meanenergy) $ebeam
}
