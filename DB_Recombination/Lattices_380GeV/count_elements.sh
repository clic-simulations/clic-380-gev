FILES="DBRC_CLIC_Placet2_v15/DelayLoop_Arc.tcl \
 DBRC_CLIC_Placet2_v13/CombinerRing1_Part1.tcl \
 DBRC_CLIC_Placet2_v13/CombinerRing1_Part1_noinj.tcl \
 DBRC_CLIC_Placet2_v13/CombinerRing1_Part2.tcl \
 DBRC_CLIC_Placet2_v13/CombinerRing2_Part1.tcl \
 DBRC_CLIC_Placet2_v13/CombinerRing2_Part1_noextraction.tcl \
 DBRC_CLIC_Placet2_v13/CombinerRing2_Part2.tcl"

for f in $FILES
do
    echo "$f:"
    for t in Quadrupole Sextupole Sbend
    do
        N=$(grep "$t" "$f" | wc -l)
        echo "$t count = $N"
    done
done
