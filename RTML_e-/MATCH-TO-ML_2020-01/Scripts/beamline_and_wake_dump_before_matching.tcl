set wakelongrangedata(nmodes) 0
set wakelongrangedata(modes) {}
initialize_cavities rfparamsbc1 wakelong1 wakelongrangedata

Girder
Drift -name "MARKER_MATCH_DR_TO_RTML"
lattice_match_dr_to_rtml beamparams

Drift -name "MARKER_DIAGNOSTICS1"
lattice_diagnostics_1 beamparams

Drift -name "MARKER_MATCH_DIAG1_TO_SR"
lattice_dump_and_match_diag_to_sr beamparams

Drift -name "MARKER_SPIN_ROTATOR"
lattice_spin_rotator beamparams

Drift -name "MARKER_MATCH_SR_TO_BC1"
lattice_match_sr_to_bc1_rf beamparams
TclCall -script {
        Octave {
                B0=placet_get_beam();
		B0=sortrows(B0,4);
		placet_set_beam(B0);

                meanz = mean(B0(:,4));
                CAV = placet_get_name_number_list('rtml','060C');

                lambda_bc1 = placet_element_get_attribute("rtml", CAV(1),"lambda");
                dphase = meanz*1e-6*360/lambda_bc1;

                placet_element_vary_attribute("rtml",CAV,'phase',dphase);
		placet_element_set_attribute("rtml",CAV,"six_dim",true);
	}
}
if {$beamparams(usewakefields)==1} {
	set rfparamsbc1(gradient) [expr $rfparamsbc1(gradientww)*$placetunits(energy)]
	set rfparamsbc1(dewake) $energy_wake_bc1
	source $scriptdir/set_wake_bc1.tcl
} else {
	set rfparamsbc1(gradient) [expr $rfparamsbc1(gradient)*$placetunits(energy)]
	set rfparamsbc1(dewake) 0
}

Drift -name "MARKER_BC1_RF"
lattice_bc1_rf rfparamsbc1 beamparams
TclCall -script {
        Octave {
               placet_element_vary_attribute("rtml",CAV,'phase',-dphase);
        }
}

Drift -name "MARKER_MATCH_BC1_RF_TO_CHICANE"
lattice_match_bc1_rf_to_chicane beamparams

Drift -name "MARKER_BC1_CHICANE"
lattice_bc1_chicane beamparams

Drift -name "MARKER_MATCH_BC1_TO_DIAG2"
lattice_match_bc1_to_diag beamparams

Drift -name "MARKER_DIAGNOSTICS2"
lattice_diagnostics_2 beamparams

Drift -name "MARKER_MATCH_DIAG2_TO_BOOSTER"
lattice_dump_and_match_diag_to_booster beamparams

if {$beamparams(usewakefields)==1} {
	set rfparamsbooster(gradient) [expr $rfparamsbooster(gradientww)*$placetunits(energy)]
	set rfparamsbooster(dewake) $energy_wake_booster
	source $scriptdir/set_wake_booster.tcl
} else {
	set rfparamsbooster(gradient) [expr $rfparamsbooster(gradient)*$placetunits(energy)]
	set rfparamsbooster(dewake) 0
}
TclCall -script {
        Octave {
                B0=placet_get_beam();
                B0 = sortrows(B0,4);
                placet_set_beam(B0);
                meanz = mean(B0(:,4));
                CAV = placet_get_name_number_list('rtml','120C');

		lambda_booster = placet_element_get_attribute("rtml", CAV(1),"lambda");
                dphase = meanz*1e-6*360/lambda_booster;

                placet_element_vary_attribute("rtml",CAV,'phase',dphase);
                placet_element_set_attribute("rtml",CAV,"six_dim",true);
	}
}
set beamparams(sigmaz) 300
lattice_booster_linac rfparamsbooster beamparams
TclCall -script {
        Octave {
               placet_element_vary_attribute("rtml",CAV,'phase',-dphase);
        }
}
Drift -name "MARKER_MATCH_BOOSTER_TO_CA"
lattice_dump_and_match_booster_to_ca beamparams

Drift -name "MARKER_CA"
lattice_central_arc beamparams

Drift -name "MARKER_VT"
lattice_vertical_transfer beamparams

Drift -name "MARKER_MATCH_VT_TO_LTL"
lattice_match_vt_to_ltl beamparams

Drift -name "MARKER_LTL"
lattice_long_transfer_line beamparams

Drift -name "MARKER_MATCH_LTL_TO_TAL"
lattice_dump_and_match_ltl_to_tal beamparams

Drift -name "MARKER_MATCH_TAL"
lattice_turn_around_loop beamparams

Drift -name "MARKER_MATCH_TAL_TO_BC2_RF"
lattice_match_tal_to_bc2_rf beamparams

TclCall -script {
        Octave {
                B0 = placet_get_beam();
                B0=sortrows(B0,4);
                placet_set_beam(B0);
                meanz = mean(B0(:,4));

                CAV = placet_get_name_number_list('rtml','210C');
		placet_element_set_attribute("rtml",CAV,"six_dim",true);
		placet_element_set_attribute("rtml",CAV,"frequency",12);

		lambda_bc2 = placet_element_get_attribute("rtml", CAV(1),"lambda");
                dphase = meanz*1e-6*360/lambda_bc2;           
                placet_element_vary_attribute("rtml",CAV,'phase',dphase);
        }
}
if {$beamparams(usewakefields)==1} {
	set rfparamsbc2(gradient) [expr $rfparamsbc2(gradientww)*$placetunits(energy)]
	set rfparamsbc2(dewake) $energy_wake_bc2
	source $scriptdir/set_wake_bc2.tcl
} else {
	set rfparamsbc2(gradient) [expr $rfparamsbc2(gradient)*$placetunits(energy)]
	set rfparamsbc2(dewake) 0
}
lattice_bc2_rf rfparamsbc2 beamparams
TclCall -script {
        Octave {
               placet_element_vary_attribute("rtml",CAV,'phase',-dphase);
        }
}

Drift -name "MARKER_BC2_RF_TO_CHICANE"
lattice_match_bc2_rf_to_chicane_1 beamparams

Drift -name "MARKER_BC2_CHICANE1"
lattice_bc2_chicane_1 beamparams
lattice_match_bc2_chicanes beamparams

Drift -name "MARKER_BC2_CHICANE2"
lattice_bc2_chicane_2 beamparams

Drift -name "MARKER_MATCH_BC2_TO_DIAG3"
lattice_match_bc2_to_diag beamparams

Drift -name "MARKER_DIAGNOSTICS3"
lattice_diagnostics_3 beamparams

#Drift -name "MARKER_MATCH_RTML_TO_ML"
#lattice_dump_and_match_rtml_to_main_linac beamparams
