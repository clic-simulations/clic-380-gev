#
# BC1 RF lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# rfparams=[gradient,phase,lambda,a,g,l,delta,delta_g]
# units=[eV/m,Degree,m,m,m,m,m,m]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_bc1_rf {rfparray bparray} {
    global placetunits energy_wake_bc1
    upvar $rfparray rfparams
    upvar $bparray  beamparams

    set usesixdim     1
    set numthinlenses 100
    set quad_synrad   0

    set lcav 1.5

    set gradient  $rfparams(gradient)
    set refenergy $beamparams(meanenergy)
    set q0        $beamparams(charge)
    set cavphase  $rfparams(phase)

    set lquad 0.3

    set kq1  0.5085726542
    set kq2 -0.5100178364

    set ldrift 0.3

    # This is required to take into account the energy gain due to acceleration
    set pi       3.14159265358979
    set dphase   0
    set deacc    [expr $gradient*$lcav*cos(($cavphase-0*$dphase)/180.0*$pi)]
    set cavphase [expr $cavphase+$dphase]

    # Energy loss due to wake fields
    set dewake $rfparams(dewake)
    set de     [expr $deacc+$dewake]

    set ebeam $refenergy
    SetReferenceEnergy $ebeam

    # Remove any longitudinal offset to ensure the correct RF phase is seen
    #TclCall -script {Octave {B=placet_get_beam(); B(:,4)-=0.1120842419354864; placet_set_beam(B);}}

    Girder
    Drift -length $ldrift -six_dim $usesixdim
    for {set sec 1} {$sec<=1} {incr sec} {
        Girder
        Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length $ldrift -six_dim $usesixdim
        Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length $ldrift -six_dim $usesixdim
        Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length $ldrift -six_dim $usesixdim
        Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length $ldrift -six_dim $usesixdim

        Bpm
        Quadrupole -name "060Q1" -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
        Dipole -name "D060-$sec-1"
        Drift -length $ldrift -six_dim $usesixdim

        Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length $ldrift -six_dim $usesixdim
        Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length $ldrift -six_dim $usesixdim
        Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length $ldrift -six_dim $usesixdim
        Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
        Drift -length $ldrift -six_dim $usesixdim

        Bpm
        Quadrupole -name "060Q2" -synrad $quad_synrad -length $lquad -strength [expr $kq2*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
        Dipole -name "D060-$sec-2"
        Drift -length $ldrift -six_dim $usesixdim
    }
    Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+$de]
    SetReferenceEnergy $ebeam
    Drift -length $ldrift -six_dim $usesixdim
    Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+$de]
    SetReferenceEnergy $ebeam
    Drift -length $ldrift -six_dim $usesixdim
    Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+$de]
    SetReferenceEnergy $ebeam
    Drift -length $ldrift -six_dim $usesixdim
    Cavity -name "060C" -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+$de]
    SetReferenceEnergy $ebeam
    Drift -length $ldrift -six_dim $usesixdim

    set beamparams(meanenergy) $ebeam
}
