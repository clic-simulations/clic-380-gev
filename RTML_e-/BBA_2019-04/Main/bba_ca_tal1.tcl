set tcl_precision 15
ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set beamdir    ./Beams
set wakedir    ./Wakefields

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/getResponseMatrix.tcl
source $scriptdir/disperseEnergy.tcl
source $scriptdir/survey.tcl
source $scriptdir/splitBin.tcl
source $scriptdir/plainTracking.tcl
source $scriptdir/OTO.tcl
source $scriptdir/DFS.tcl
source $scriptdir/DFS-bc1.tcl
source $scriptdir/DFS-boo.tcl
source $scriptdir/DFS-ca.tcl
source $scriptdir/DFS-bc2.tcl
source $scriptdir/DFS-tal.tcl

create_particles_file $beamdir/particles.in beamparams
create_zero_wakes_file $wakedir/zero_wake.dat beamparams rfparamsbc1 $beamdir/particles.in

source $scriptdir/beamline_and_wake.tcl

make_particle_beam beam1 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam6 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam7 beamparams $beamdir/particles.in $wakedir/zero_wake.dat
make_particle_beam beam8 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

source $scriptdir/load_mag.tcl

Octave {
    Start = placet_get_name_number_list("rtml","Marker-SR-start");
    End = placet_get_name_number_list("rtml","Marker-LTL-start");
    [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1, Start, End);
    placet_set_beam("beam6", B);
}

puts "\nApplying BBA to Long Transfer Line"
puts "*****************************************" 
getResponseMatrix LTL rtml Marker-LTL-start Marker-LTL-end beam6 $LTLde
splitBin rtml LTLncarray Marker-LTL-start Marker-LTL-end
otoCorrection LTL rtml Marker-LTL-start Marker-LTL-end beam6 $LTLbeta0 $LTLOTOnloop
dfsCorrection LTL rtml Marker-LTL-start Marker-LTL-end beam6 $LTLde $LTLbeta1 $LTLwgt $LTLDFSnloop

Octave {
    Start = placet_get_name_number_list("rtml","Marker-LTL-start");
    End = placet_get_name_number_list("rtml","Marker-TAL1-start");
    [E,B] = placet_test_no_correction("rtml", "beam6", "None", 1, Start, End);
    placet_set_beam("beam7", B);
    Bins = Bins(1);
}

puts "\nApplying BBA to Turn Around Loop 1"
puts "*****************************************" 
getResponseMatrix TAL1 rtml Marker-TAL1-start Marker-TAL1-end beam7 $TAL1de
splitBin rtml TAL1ncarray Marker-TAL1-start Marker-TAL1-end
otoCorrection TAL1 rtml Marker-TAL1-start Marker-TAL1-end beam7 $TAL1beta0 $TAL1OTOnloop
dfsCorrection TAL1 rtml Marker-TAL1-start Marker-TAL1-end beam7 $TAL1de $TAL1beta1 $TAL1wgt $TAL1DFSnloop

Octave {
    Start = placet_get_name_number_list("rtml","Marker-TAL1-start");
    End = placet_get_name_number_list("rtml","Marker-TAL2-start");
    [E,B] = placet_test_no_correction("rtml", "beam7", "None", 1, Start, End);
    placet_set_beam("beam8", B);
    Bins = Bins(1);
}

source $scriptdir/save.tcl
