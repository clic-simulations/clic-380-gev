##------> Author: Raul Costa [raul.costa@cern.ch] <------------------##
## Placet2 lattice generated from Eduardo Marin's design for Placet1
## Sectors 1 and 5 extended to match ring length
## Injector deflectors are now RF
## Extraction Stripline added
## Later adapted to be defined by K1L/K2L instead of S1/S2
## Sextupole strength optimised for low emittance/T566
## Straight sector quadrupoles optimised to match arcs
## Center of arc cells optimised for periodicity
## Low energy version for 380GeV stage
set Pi [expr acos(-1)] 
if {$Dipole_synrad} { 
set refen 1.904935082
} else { 
set refen 1.906 
} 
Marker "CR2_P2.INJECTION" 
Drift "CR2.SEPTUM" -len 7.486757641 
Quadrupole "QUAD.CR2.QRFB3.EXT" -len 0.5 -K1L -0.2258082330193268 -refen $refen 
Drift "CR2.DRFB3" -len 3.771233433 
Quadrupole "QUAD.CR2.QRFB2.EXT" -len 0.5 -K1L 0.5591986720709291 -refen $refen 
Drift "CR2.DRFB2" -len 1.193240969 
Quadrupole "QUAD.CR2.QRFB1.EXT" -len 0.5 -K1L -1.079720421877582 -refen $refen 
Drift "CR2.DL3IN" -len 2.6172311618 
Quadrupole "QUAD.CR2.QL3IN.EXT" -len 0.5 -K1L -0.2499898253787109 -refen $refen 
Drift "CR2.DL2IN" -len 1.103409345 
Quadrupole "QUAD.CR2.QL2IN.EXT" -len 0.5 -K1L 0.8526680541738718 -refen $refen 
Drift "CR2.DL1IN" -len 0.4000469975 
Quadrupole "QUAD.CR2.QL1IN.EXT" -len 0.5 -K1L -0.6970772319488282 -refen $refen 
Drift "CR2.DL0IN" -len 2.4491971274 
#### Arc 5 Start #### 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.5" -len 0.1 -K2L 5.916770281519848 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -K2L 0.004086039457833162 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.5" -len 0.1 -K2L -2.128115778419815 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.5" -len 0.1 -K2L -0.2923601851150768 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -K2L 0.004086039457833162 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.5" -len 0.1 -K2L -1.882188515983449 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183728 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183728 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.5" -len 0.1 -K2L -1.882188515983449 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -K2L 0.004086039457833162 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.5" -len 0.1 -K2L -0.2923601851150768 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.5" -len 0.1 -K2L -2.128115778419815 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -K2L 0.004086039457833162 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.5" -len 0.1 -K2L 5.916770281519848 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864928 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
#### Arc 5 End ####
Drift "CR2.DTR1A" -len 1.1 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.84671680555554 -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458445 -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Quadrupole "QUAD.CR2.QTR4" -len 0.5 -K1L 0.374011719219854 -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458445 -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.84671680555554 -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
Drift "CR2.DTR1A" -len 1.1 
### Arc 6 Start ### 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864928 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.6" -len 0.1 -K2L 10.46729163081503 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -K2L -0.03075131890845511 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.6" -len 0.1 -K2L -2.335820368364084 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.6" -len 0.1 -K2L -0.1224868329535538 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -K2L -0.03075131890845511 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.6" -len 0.1 -K2L -2.833737318518565 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.6" -len 0.1 -K2L -2.833737318518565 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -K2L -0.03075131890845511 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.6" -len 0.1 -K2L -0.1224868329535538 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.6" -len 0.1 -K2L -2.335820368364084 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -K2L -0.03075131890845511 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.6" -len 0.1 -K2L 10.46729163081503 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
### Arc 6 End ### 
Drift "CR2.DS1C_A" -len 2.179474654 
Sextupole "SEXT.CR2.SXS1C" -len 0.1 -K2L 2.1606775839716965 -refen $refen 
Drift "CR2.DS1C_B" -len 0.3 
Quadrupole "QUAD.CR2.QS1C" -len 0.5 -K1L -0.647356904204382 -refen $refen 
Drift "CR2.DS2C" -len 0.2003907272 
Quadrupole "QUAD.CR2.QS2C" -len 0.5 -K1L 0.7905004870236609 -refen $refen 
Drift "CR2.DS3C_A" -len 0.3 
Sextupole "SEXT.CR2.SXS3C" -len 0.1 -K2L -0.42652765870388576 -refen $refen 
Drift "CR2.DS3C_B" -len 2.777418619 
Quadrupole "QUAD.CR2.QS3C" -len 0.5 -K1L -0.24686896328832697 -refen $refen 
Drift "CR2.DS3C_C" -len 0.5 
Quadrupole "QUAD.CR2.QS4C" -len 0.5 -K1L 0.24225644224167997 -refen $refen 
Drift "CR2.DS4C" -len 2.081432 
Quadrupole "QUAD.CR2.QS4C" -len 0.5 -K1L 0.24225644224167997 -refen $refen 
Drift "CR2.DS3C_C" -len 0.5 
Quadrupole "QUAD.CR2.QS3C" -len 0.5 -K1L -0.24686896328832697 -refen $refen 
Drift "CR2.DS3C_B" -len 2.777418619 
Sextupole "SEXT.CR2.SXS3C" -len 0.1 -K2L -0.42652765870388576 -refen $refen 
Drift "CR2.DS3C_A" -len 0.3 
Quadrupole "QUAD.CR2.QS2C" -len 0.5 -K1L 0.7905004870236609 -refen $refen 
Drift "CR2.DS2C" -len 0.2003907272 
Quadrupole "QUAD.CR2.QS1C" -len 0.5 -K1L -0.647356904204382 -refen $refen 
Drift "CR2.DS1C_B" -len 0.3 
Sextupole "SEXT.CR2.SXS1C" -len 0.1 -K2L 2.1606775839716965 -refen $refen 
Drift "CR2.DS1C_A" -len 2.179474654 
### Arc 7 Start ### 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.7" -len 0.1 -K2L 4.84107203364833 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -K2L 0.08666324940721458 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.7" -len 0.1 -K2L -1.890896958432581 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.7" -len 0.1 -K2L -0.1343915497518794 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -K2L 0.08666324940721458 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.7" -len 0.1 -K2L -10.69759660163174 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.08046148038247242 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.28064251000565993 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.08046148038247242 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.7" -len 0.1 -K2L -10.69759660163174 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -K2L 0.08666324940721458 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.7" -len 0.1 -K2L -0.1343915497518794 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.7" -len 0.1 -K2L -1.890896958432581 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -K2L 0.08666324940721458 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.7" -len 0.1 -K2L 4.84107203364833 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.8215880000000001 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
### Arc 7 End ### 
Drift "CR2.DTR1A" -len 1.1 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.846716805555538 -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458447 -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Quadrupole "QUAD.CR2.QTR4" -len 0.5 -K1L 0.374011719219854 -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -K2L -5.570284467378723 -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -K1L -0.2785882030366049 -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -K2L 15.920430620458447 -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -K1L 0.5868821504395235 -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -K2L -8.846716805555538 -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -K1L -0.3753341264206926 -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -K2L 5.0021777846977145 -refen $refen 
Drift "CR2.DTR1A" -len 1.1 
### Arc 8 Start ### 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864923 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.8" -len 0.1 -K2L 8.910931642429942 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -K2L -0.1051607345916809 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.8" -len 0.1 -K2L -1.807756410813105 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.8" -len 0.1 -K2L -0.4227303832889224 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -K2L -0.1051607345916809 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.8" -len 0.1 -K2L -5.319302666092288 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -K1L 0.2806425100056599 -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -K1L -1.0972460430183726 -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -K2L 0.0804614803824724 -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.8" -len 0.1 -K2L -5.319302666092288 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -K2L -0.1051607345916809 -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.8" -len 0.1 -K2L -0.4227303832889224 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.4 -K1L -0.2376272 -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.8" -len 0.1 -K2L -1.807756410813105 -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -K1L 0.7724967744 -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -K2L -0.1051607345916809 -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -K1L -1.114316 -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.8" -len 0.1 -K2L 8.910931642429942 -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -K1L 0.0399258644 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -K1L 0.821588 -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -K2L -1.8766635419864919 -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -K1L -0.590316 -refen $refen 
#### Arc 8 End ####
Drift "CR2.DL0" -len 1.4491971274 
Quadrupole "QUAD.CR2.QL0IN.IN1" -len 0.5 -K1L -0.3536192234913272 -refen $refen 
Drift "CR2.DL0IN" -len 0.5 
Quadrupole "QUAD.CR2.QL1IN.IN1" -len 0.5 -K1L 0.9809117348720798 -refen $refen 
Drift "CR2.DL1IN" -len 0.4000469975 
Quadrupole "QUAD.CR2.QL2IN.IN1" -len 0.5 -K1L -0.030500513033999792 -refen $refen 
Drift "CR2.DL2IN" -len 1.103409345 
Quadrupole "QUAD.CR2.QL3IN.IN1" -len 0.5 -K1L -0.2614385592457404 -refen $refen 
Drift "CR2.DL3IN" -len 0.4000389758 
#### Artificially centering (longitudinally) to align with the RF #### 
#### In practice we will adjust the RF phase instead (equivalent) ####
#~ Kalign al1 -cz 0 
Kalign al1 -fixed 1 -cz -0.0001357244419 
Marker "CR2.align_RF" -len 0 -node 1 -kick al1 
#### RF DEFLECTOR #### 
Rfmultipole "RFMulti.CR2.RFKICK1" -len 2 -strength [expr -0.004*$refen] -freq [expr 0.9995*3.] -phase [expr -$Pi/2] -refen $refen 
###################### 
Drift "CR2.DRFB1" -len 0.217192186 
Quadrupole "QUAD.CR2.QRFB1.IN1" -len 0.5 -K1L -0.22582311699328744 -refen $refen 
Drift "CR2.DRFB2" -len 1.193240969 
Quadrupole "QUAD.CR2.QRFB2.IN1" -len 0.5 -K1L 0.39708610074064277 -refen $refen 
Drift "CR2.DRFB3" -len 3.771233433 
Quadrupole "QUAD.CR2.QRFB3.IN1" -len 0.5 -K1L -0.08240914765491558 -refen $refen 
Drift "CR2.D1M" -len 1.01
Sextupole "SEXT.CR2.SRFB4.IN1" -len 0.5 -K2L -4.717961074837193 -refen $refen 
Drift "CR2.DS1" -len 0.5 
Sextupole "SEXT.CR2.SRFB5.IN1" -len 0.5 -K2L 2.9954518485987762 -refen $refen 
Drift "CR2.DRFB4A" -len 1.209135479 
Sextupole "SEXT.CR2.SRFB6.IN1" -len 0.4 -K2L 3.0040021747304624 -refen $refen 
Drift "CR2.OLDSEPT" -len 6.735244324  
Sextupole "SEXT.CR2.SRFB6.IN2" -len 0.4 -K2L -3.4377743532350404 -refen $refen 
Drift "CR2.DRFB4A" -len 1.209135479 
Sextupole "SEXT.CR2.SRFB5.IN2" -len 0.5 -K2L 0.29120223716942567 -refen $refen 
Drift "CR2.DS1" -len 0.5 
Sextupole "SEXT.CR2.SRFB4.IN2" -len 0.5 -K2L 1.4264823718127457 -refen $refen 
##################################################################### 
Marker "CR2_P2.EXTRACTION" 
