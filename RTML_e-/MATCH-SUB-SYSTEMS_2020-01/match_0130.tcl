# CLIC Main Beam RTML
# This script assumes Scripts/create_wake.tcl has been run.

set tcl_precision 15
ParallelThreads -num 4

set latticedir ./Lattices_e-
set scriptdir  ./Scripts
set paramsdir  ./Parameters
set wakedir    ./Wakefields
set beamdir    ./Beams

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
if {$beamparams(usewakefields)==1} {
   source $paramsdir/rf_parameters_ww.tcl
}
create_particles_file $beamdir/particles.in beamparams
source $scriptdir/beamline_and_wake.tcl
make_particle_beam beam0 beamparams $beamdir/particles.in $wakedir/zero_wake.dat

# Track to matching section
Octave {
    disp("0010");
    [E,B] = placet_test_no_correction("0010", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0020");
    [E,B] = placet_test_no_correction("0020", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0030");
    [E,B] = placet_test_no_correction("0030", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0040");
    [E,B] = placet_test_no_correction("0040", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0050");
    [E,B] = placet_test_no_correction("0050", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0060");
    [E,B] = placet_test_no_correction("0060", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0070");
    [E,B] = placet_test_no_correction("0070", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0080");
    [E,B] = placet_test_no_correction("0080", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0090");
    [E,B] = placet_test_no_correction("0090", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0100");
    [E,B] = placet_test_no_correction("0100", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0110");
    [E,B] = placet_test_no_correction("0110", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);

    disp("0120");
    [E,B] = placet_test_no_correction("0120", "beam0", "None");
    disp_results(B);
    placet_set_beam("beam0", B);
}

# Target twiss parameters at the end of the matching section
array set twiss1 {
  beta_x 36.51641943684712
  beta_y 5.965648460480047
  alpha_x 7.172165519844627
  alpha_y -1.183909125026760
}

# Matching
Octave {
    pkg load optim;
    format long;

    global Correctors1 = placet_get_name_number_list("0130","matching-quad");
    global Correctors2 = placet_get_name_number_list("0130","matching-drift");
    global refenergy  = 9.00992576402593;

    global twiss1 = zeros(4,4);
    twiss1(1:2,1:2) = [  $twiss1(beta_x), -($twiss1(alpha_x)); -($twiss1(alpha_x)), (1+($twiss1(alpha_x))*($twiss1(alpha_x)))/($twiss1(beta_x)) ];
    twiss1(3:4,3:4) = [  $twiss1(beta_y), -($twiss1(alpha_y)); -($twiss1(alpha_y)), (1+($twiss1(alpha_y))*($twiss1(alpha_y)))/($twiss1(beta_y)) ];
                                                         
    function merit = TwissMatching(beamline)
        global Correctors;                  
        global Correctors1;
        global Correctors2;
        global refenergy;
        global twiss1;
                      
        Quads  = placet_element_get_attribute("0130",Correctors1,"strength")./refenergy
        Drifts = placet_element_get_attribute("0130",Correctors2,"length")
                
        [E, B] = placet_test_no_correction("0130", "beam0", "None"); 
        emitt   = placet_get_emittance(B)
        twiss   = placet_get_twiss_matrix(B);
        beta_x  = twiss(1,1)/twiss1(1,1)     
        beta_y  = twiss(3,3)/twiss1(3,3)
        alpha_x = twiss(1,2)/twiss1(1,2)
        alpha_y = twiss(3,4)/twiss1(3,4)
        x       = mean(B(:,2))          
        y       = mean(B(:,3))    
        xp      = mean(B(:,5))    
        yp      = mean(B(:,6))    
                                
        #merit = sum((1.0 - [beta_x, beta_y, alpha_x, alpha_y]).**2) + (1-1.568379674898096/x)**2 + xp**2;
        merit = sum((1.0 - [beta_x, beta_y, alpha_x, alpha_y]).**2) + x**2 + xp**2 + y**2 + yp**2;
        #merit = sum((1.0 - [beta_x, beta_y]).**2) + (1-1.568379674898096/x)**2;
        #merit = sum((1.0 - [alpha_x, alpha_y]).**2) + (1-1.568379674898096/x)**2;
        #merit = sum((1.0 - [beta_x, beta_y, alpha_x, alpha_y]).**2);
        #merit = sum((1.0 - [beta_x, beta_y]).**2);
        #merit = sum((1.0 - [alpha_x, alpha_y]).**2);
        #merit = sum((1.0 - [beta_x, beta_y]).**2) + 1e-3*sum((1.0 - [alpha_x, alpha_y]).**2);
                                                   
    endfunction
               
    Leverages1   = ["strength"; "strength"; "strength"; "strength"];
    Constraints1 = [0  0; 0  0; 0  0; 0  0];
    Leverages2   = ["length"; "length"; "length"];
    Constraints2 = [0.02  30; 0.02  30; 0.02  30];

    Correctors  = [Correctors1, Correctors2];
    Leverages   = [Leverages1; Leverages2];
    Constraints = [Constraints1; Constraints2];
    
    #Correctors  = Correctors2;
    #Leverages   = Leverages2;
    #Constraints = Constraints2;

    [optimum, merit] = placet_optimize_constraint("0130", "TwissMatching",
                                                  Correctors, Leverages, Constraints);
    Quads  = placet_element_get_attribute("0130",Correctors1,"strength") ./ refenergy
    Drifts = placet_element_get_attribute("0130",Correctors2,"length")
}
