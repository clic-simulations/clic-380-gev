#
# BC2 RF lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# rfparams=[gradient,phase,lambda,a,g,l,delta,delta_g]
# units=[eV/m,Degree,m,m,m,m,m,m]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_bc2_rf {rfparray bparray} {
    upvar $rfparray rfparams
    upvar $bparray beamparams

    #puts [array get rfparams]
    #puts [array get beamparams]

    set usesixdim 1
    set numthinlenses 10
    set quad_synrad 0

    set gradient $rfparams(gradient)
    set refenergy $beamparams(meanenergy)
    set q0 $beamparams(charge)
    set cavphase $rfparams(phase)
    set lcav 0.23

    set lquad 0.4
    set lbpm 0.08

    # This is required to take into account the energy gain due to the cavities.
    # Attention! Since there will be a longitudinal offset of the bunch in these cavities
    # they will induce a small acceleration.
    set pi [expr acos(-1.0) ]
    #set dphase [expr $beamparams(meanz)*1e-6*360/$rfparams(lambda)]
    set dphase 0
    set deacc [expr $gradient*$lcav*cos(($cavphase-0*$dphase)/180.0*$pi)]
    set cavphase [expr $cavphase+$dphase]
    #energy loss due to wake fields
    #set dewake [expr $rfparams(dewake)*1e6/1e9*$lcav]
    set dewake $rfparams(dewake)

    set de [expr $deacc+$dewake]

    set ebeam $refenergy
    SetReferenceEnergy $ebeam

    set lgirder 2.61
    set lcell [expr 2.0 * $lgirder]
    set mu_rad [expr 72.0 * acos(-1.0) / 180.0 ]
    
    set Qf_k1l [expr  4.0 * sin($mu_rad / 2.0) / $lcell]
    set Qd_k1l [expr -4.0 * sin($mu_rad / 2.0) / $lcell]

    set kq1 [expr $Qf_k1l / $lquad]
    set kq2 [expr $Qd_k1l / $lquad]

    #array set fodo_twiss [MatchFodo -l1 $lquad -l2 $lquad -K1 $Qf_k1l -K2 $Qd_k1l -L [expr $lcell / 2.0 ] ]
    #puts "beta_x = $fodo_twiss(beta_x)"
    #puts "beta_y = $fodo_twiss(beta_y)"
    #puts "alpha_x = $fodo_twiss(alpha_x)"
   # puts "alpha_y = $fodo_twiss(alpha_y)"
    
    # new Module
    Bpm -length $lbpm
    Drift -length 0.04 -six_dim $usesixdim

    for {set sec 1} {$sec<=5} {incr sec} {
	Girder
	Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length 0.08 -six_dim $usesixdim
	Girder
	Drift -length 0.03 -six_dim $usesixdim
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Drift -length 0.04 -six_dim $usesixdim
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Drift -length 0.04 -six_dim $usesixdim
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Drift -length 0.04 -six_dim $usesixdim
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Drift -length 0.02 -six_dim $usesixdim
	Bpm -length $lbpm
	Drift -length 0.04 -six_dim $usesixdim
	Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq2*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length 0.08 -six_dim $usesixdim
	Girder
	Drift -length 0.03 -six_dim $usesixdim
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Drift -length 0.04 -six_dim $usesixdim
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Drift -length 0.04 -six_dim $usesixdim
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Drift -length 0.04 -six_dim $usesixdim
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
	Drift -length 0.02 -six_dim $usesixdim
	Bpm -length $lbpm
	Drift -length 0.04 -six_dim $usesixdim
    }

    Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses

    #Bpm -length $lbpm
    Drift -length $lbpm
    Drift -length 0.04 -six_dim $usesixdim
    set beamparams(meanenergy) $ebeam
    #puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}






