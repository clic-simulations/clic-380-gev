#
# These scripts were developed by C. Gohil.
#
# Optimises the luminosity of two beams by performing a beam-beam offset scan,
# a crossing angle scan and waist scan.
#
# The beam-beam offset and cross angle scan are only performed in the vertical
# direction.
#
# To run: placet run.tcl <machine1> <machine2>
#

# Directories
set gpdir      ./Guinea_Pig
set scriptdir ../Scripts
set paramsdir ../Parameters

# Directory containing beams from PLACET tracking simulations.
set beamdir "/eos/user/c/cgohil/simulations/clic_380_gev/tuned/one_beam/beams"

cd $gpdir

# Command line arguments
# machine1 and machine2 are IDs for the beams
set machine1 [lindex $argv 0]
set machine2 [lindex $argv 1]

puts "machine1=$machine1, machine2=$machine2"

# Prepare beams for GUINEA-PIG
Octave {
    disp("Settings up electron.ini and positron.ini");
    electron_beam = load("$beamdir/beam-$machine1.out");
    electron_beam(:,2) -= mean(electron_beam(:,2));
    electron_beam(:,3) -= mean(electron_beam(:,3));
    electron_beam(:,5) -= mean(electron_beam(:,5));
    electron_beam(:,6) -= mean(electron_beam(:,6));
    save_beam("electron.ini", electron_beam);

    positron_beam = load("$beamdir/beam-$machine2.out");
    positron_beam(:,2) -= mean(positron_beam(:,2));
    positron_beam(:,3) -= mean(positron_beam(:,3));
    positron_beam(:,5) -= mean(positron_beam(:,5));
    positron_beam(:,6) -= mean(positron_beam(:,6));
    save_beam("positron.ini", positron_beam);
}

# Set the number of particles
set n_slice 50
set n       2000
set n_total [expr $n_slice*$n]

# Set up GUINEA-PIG
source clic_guinea.tcl

puts "Running without optimisation"
run_guinea 0 0 0
set L0 [get_lumi]
puts "L0=$L0"

set L0_no $L0

# Beam-beam offset scan
puts "\nOptimising beam-beam offset:"
set ro_opt 0

puts "Checking if we need positive or negative offsets"
run_guinea -0.05 0 0
set L [get_lumi]
puts "ro=-0.05, L=$L"

if {$L < $L0} {
    puts "Using positive offsets"
    for {set i 1} {$i < 30} {incr i} {
        set ro [expr 0.05*$i]
        run_guinea $ro 0 0
        set L [get_lumi]
        puts "ro=$ro, L=$L"
        if {$L >= $L0} {
            set L0 $L
            set ro_opt $ro
        } else {
            puts "optimised: ro_opt=$ro_opt, L0=$L0"
            break
        }
    }
} else {
    puts "Using negative offsets"
    for {set i 2} {$i < 20} {incr i} {
        set ro [expr -0.05*$i]
        run_guinea $ro 0 0
        set L [get_lumi]
        puts "ro=$ro, L=$L"
        if {$L > $L0} {
            set L0 $L
            set ro_opt $ro
        } else {
            puts "optimised: ro_opt=$ro_opt, L0=$L0"
            break
        }
    }

}

set L0_ro $L0

# Crossing angle scan
puts "\nOptimising collision angle:"
set a_opt 0

puts "Checking if we need positive or negative angle"
run_guinea $ro_opt -0.2 0
set L [get_lumi]
puts "ro=$ro_opt, a=-0.2, L=$L"

if {$L < $L0} {
    puts "Using positive angles"
    for {set i 1} {$i < 30} {incr i} {
        set a [expr 0.2*$i]
        run_guinea $ro_opt $a 0
        set L [get_lumi]
        puts "a=$a, L=$L"
        if {$L >= $L0} {
            set L0 $L
            set a_opt $a
        } else {
            puts "optimised: ro_opt=$ro_opt, a=$a_opt, L0=$L0"
            break
        }
    }
} else {
    puts "Using negative angles"
    for {set i 2} {$i < 25} {incr i} {
        set a [expr -0.2*$i]
        run_guinea $ro_opt $a 0
        set L [get_lumi]
        puts "a=$a, L=$L"
        if {$L >= $L0} {
            set L0 $L
            set a_opt $a
        } else {
            puts "optimised: ro_opt=$ro_opt, a=$a_opt, L0=$L0"
            break
        }
    }

}

set L0_ro_a $L0

# Waist scan
puts "\nOptimising waist:"
set w_opt 0
for {set i 1} {$i < 10} {incr i} {
    set w [expr -4*$i]
    run_guinea $ro_opt $a_opt $w
    set L [get_lumi]
    puts "w=$w, L=$L"
    if {$L >= $L0} {
        set L0 $L
        set w_opt $w
    } else {
        puts "optimised: ro_opt=$ro_opt, a=$a_opt, w=$w_opt, L0=$L0"
        break
    }
}

set L0_ro_a_w $L0

# Write results file
set fileId [open "../results.out" "w"]
puts $fileId "$L0_no $L0_ro $L0_ro_a $L0_ro_a_w $ro_opt $a_opt $w_opt"
close $fileId
