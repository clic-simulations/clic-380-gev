#
# This simulation was developed by C. Gohil from code inherited from
# N. Blaskovic Kraljevic
#
# Implements static imperfections and performs 1-2-1, DFS and RF realignment on the ML.
#

To run: "placet run.tcl <num-machines>"
where <num-machines> is the number of machines to simulate.

The final offsets of all elements are saved to "results/bx.1"
This file was saved with "SaveAllPositions -file bx.1 -cav_bpm 1"
To read use "ReadAllPositions -file bx.1 -cav_bpm 1"
