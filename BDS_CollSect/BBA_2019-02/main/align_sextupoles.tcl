# Pre-aligning the sextupoles
##########################################################
foreach axis { x y } {
  foreach knob { 1 2 3 4 5 6 } {
    Octave {
      function move_sextupole${knob}${axis}(x)
        global Sind;
        placet_element_vary_attribute("test", Sind($knob), "$axis", x);
      end

      function L = test_move_sextupole${knob}${axis}(x)
        global IPind;
        move_sextupole${knob}${axis}(+x);
	  try
        [E,B] = placet_test_no_correction("test", "beam0t", "None", 1, 0, IPind);
      	sxrms=std(B(:,2))
      	syrms=std(B(:,3))
      	L = -get_lumi(B);
      catch
	    L = -1e29;
      end
      move_sextupole${knob}${axis}(-x);
      printf("test_move_sextupole${knob}${axis}(%g) =  %g\n", x, L);
      # write to file
      knobfile = fopen("Tuning_log_pre_alignment.dat","a");
      printf(knobfile,"${axis} ${knob} %g %g\n", x, L);
      fclose(knobfile);        
      end
    }
  }
}

# Align sextupoles one-by-one, direction downstream
proc align_sextupoles { range } {
   Octave {
     MStemp = placet_element_get_attribute("test", Sind, "strength");
     placet_element_set_attribute("test", Sind, "strength", 0.0);
   }
   foreach knob { 1 2 3 4 5 6 } {
      Octave {
        placet_element_set_attribute("test", Sind($knob), "strength", MStemp($knob));
      }
      foreach axis { x y } {
        Octave {
          [x, fmin, eval_num] = fmin_inverse_parabola("test_move_sextupole${knob}${axis}", -$range, 0.0, $range, 3);
          move_sextupole${knob}${axis}(x);
          Lmeas += eval_num
        }
      }

      foreach axis { x y } {
        Octave {
          [x, fmin, eval_num] = fmin_inverse_parabola("test_move_sextupole${knob}${axis}", -$range/20, 0.0, $range/20, 5);
          move_sextupole${knob}${axis}(x);
          Lmeas += eval_num;
        }
      }
   }
}

# Align sextupoles one-by-one, direction upstream
proc align_sextupoles_reverse { range } {
   Octave {
     MStemp = placet_element_get_attribute("test", Sind, "strength");
     placet_element_set_attribute("test", Sind, "strength", 0.0);
   }
   foreach knob { 6 5 4 3 2 1 } {
      Octave {
        placet_element_set_attribute("test", Sind($knob), "strength", MStemp($knob));
      }
      foreach axis { x y } {
        Octave {
          [x, fmin, eval_num] = fmin_inverse_parabola("test_move_sextupole${knob}${axis}", -$range, 0.0, $range, 3);
          move_sextupole${knob}${axis}(x);
          Lmeas += eval_num
        }
      }

      foreach axis { x y } {
        Octave {
          [x, fmin, eval_num] = fmin_inverse_parabola("test_move_sextupole${knob}${axis}", -$range/20, 0.0, $range/20, 5);
          move_sextupole${knob}${axis}(x);
          Lmeas += eval_num;
        }
      }
   }
}

# Align sextupoles one-by-one, direction upstream
proc align_sextupoles_optimum { range } {
   Octave {
     MStemp = placet_element_get_attribute("test", Sind, "strength");
     placet_element_set_attribute("test", Sind, "strength", 0.0);
   }
   foreach knob { 6 5 4 2 3 1 } {
      Octave {
        placet_element_set_attribute("test", Sind($knob), "strength", MStemp($knob));
      }
      foreach axis { x y } {
        Octave {
          [x, fmin, eval_num] = fmin_inverse_parabola("test_move_sextupole${knob}${axis}", -$range, 0.0, $range, 3);
          move_sextupole${knob}${axis}(x);
          Lmeas += eval_num
        }
      }

      foreach axis { x y } {
        Octave {
          [x, fmin, eval_num] = fmin_inverse_parabola("test_move_sextupole${knob}${axis}", -$range/20, 0.0, $range/20, 5);
          move_sextupole${knob}${axis}(x);
          Lmeas += eval_num;
        }
      }
   }
}
