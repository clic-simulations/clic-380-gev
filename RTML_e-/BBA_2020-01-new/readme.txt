#
# These files are updated for CLIC-RTML BBA test by Xingguang Liu
based on the version of BBA-2020-01
May 8, 2020
#

Major changes are:
a) new design for BC2 RF (Lattices_e-/0210_bc2_rf.tcl)
b) new match section (Lattices_e-/0220_match_bc2_rf_to_chicane_1.tcl)
c) new RF related parameters in: 
    - Parameters/rf_parameters_e-.tcl 
      (rf_parameters_ww.tcl will be updated after running create_wake.tcl)
d) 'E_total' for BC1 and BC2 modified to be the same value as that in use
    - ResponseMatrix/disperseEnergy-responseMatrix.tcl
e) new corresponding matrix for BC1 and BC2
f) sextupoles tuning scripts updated to speed up the tuning process:
    - Scripts/sextupole_tuning_functions.m # commonly used functions
    - Scripts/sextupole_tuning_*.tcl
    - Scripts/sextupole_tuning_*_x.tcl
    - Scripts/sextupole_tuning_*_y.tcl
    (where * stands for ca, tal1 or tal2.
    all the * scripts can be simplified to a single scripts by:
    making marker_start and marker_end set as command line input paramters.)

g) new run.tcl script to reflect the BBA test process

# ---------------------------------#
Belows are original readme.txt before May 8, 2020
# ---------------------------------#

#
# These files were developed by C. Gohil from files inherited from Y. Han.
#
# Static errors are applied to the RTML and Beam-Based Alignment (BBA) techniques
# are applied.
#

#-----------------------------------#
# STEP 1 : generate wakefield files #
#-----------------------------------#
These are the files Wt_*.dat and Wl_*.dat.
They are already present in ./Wakefields, so this step can be skipped.
However, this would need to be re-run if RF parameters change.

Execute: placet Wakefields/create_wake.tcl

#-------------------------------------#
# STEP 2 : generate response matrices #
#-------------------------------------#
These are the files R_parm_*.dat.
These are already present in ./ResponseMatrix, so this step can be skipped.
This would need to be re-run if lattices change.
Generating the response matrices takes a few hours.

Execute: placet Main/response_matrix.tcl

#-----------------------------------------------------------#
# STEP 3: Apply the BBA for different random seed (machine) #
#-----------------------------------------------------------#
Execute: placet run.tcl <machine>
<machine> is an integer passed as a command line argument to run.tcl

E.g. "placet run.tcl 22" would perform BBA on machine number 22.
