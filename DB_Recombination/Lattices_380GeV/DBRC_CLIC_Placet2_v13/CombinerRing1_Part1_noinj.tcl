##------> Author: Raul Costa [raul.costa@cern.ch] <------------------##
## Placet2 lattice generated from Eduardo Marin's design for Placet1
## Sectors 1 and 4 extended to match ring length
## Injector deflectors are now RF
## Extraction Stripline added
## Later adapted to be defined by K1L/K2L instead of S1/S2
## Sextupole strength optimised for low emittance/T566
## Straight sector quadrupoles optimised to match arcs
## Low energy version for 380GeV stage
set Pi [expr acos(-1)] 
if {$Dipole_synrad} { 
set refen 1.905381993 
} else { 
set refen 1.906
} 
Marker "CR1_P1.INJECTION" 
Drift "CR1.INJSEPTUM" -len 1.5 
Quadrupole "QUAD.CR1.QL6IN" -len 0.25 -K1L 0.5016200926487812 -refen $refen 
Drift "CR1.DL6IN" -len 0.553 
Quadrupole "QUAD.CR1.QL5IN" -len 0.5 -K1L -0.906597728673643 -refen $refen 
Drift "CR1.DL5IN" -len 1 
Quadrupole "QUAD.CR1.QL4IN" -len 0.5 -K1L 0.6388416037583273 -refen $refen 
Drift "CR1.DL4IN" -len 0.802646062 
##### Static Deflector ##### 
Rfmultipole "RFMulti.CR1.RFDEFLCOMP2" -len 0.2 -strength [expr 0.00095*$refen] -freq 0 -refen $refen 
Drift "CR1.DRRFL1" -len 0.1 
####    RF Deflector   #####
#~ Rfmultipole "RFMulti.CR1.RFDEFLB" -len 1.147353938 -strength [expr 0.0038*$refen] -freq 0.9995 -refen $refen 
Rfmultipole "RFMulti.CR1.RFDEFLB" -len 1.147353938 -strength [expr -0.0019*$refen] -freq 0 -refen $refen 
Drift "CR1.DRRFL1" -len 0.1 
##### Static Deflector ##### 
Rfmultipole "RFMulti.CR1.RFDEFLCOMP2" -len 0.2 -strength [expr 0.00095*$refen] -freq 0 -refen $refen 
Drift "CR1.DL3IN" -len 1.399258 
Quadrupole "QUAD.CR1.QL3IN" -len 0.5 -K1L -0.4780806892244219 -refen $refen 
Drift "CR1.DL2IN" -len 0.3010372 
Quadrupole "QUAD.CR1.QL2IN" -len 0.5 -K1L 1.1940230580948872 -refen $refen 
Drift "CR1.DL1IN" -len 0.3098482 
Quadrupole "QUAD.CR1.QL1IN" -len 0.5 -K1L -0.7350190844328781 -refen $refen 
Drift "CR1.DL0IN" -len 2.66137098404886 
### Arc 1 Start ### 
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.3358875195 -refen $refen 
Drift "CR1.DBA.D11A" -len 0.4072203145  
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN0.1" -len 0.1 -K2L -0.4584481441890126 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.1" -len 0.1 -K2L 1.895664473523004 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.4 -K1L 0.4647092472 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.1" -len 0.1 -K2L -0.08845453430102207 -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN1.1" -len 0.1 -K2L -8.06723748784385 -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D11B1" -len 0.5133083062 
Quadrupole "QUAD.CR1.DBA.QF1" -len 0.4 -K1L -0.664141578 -refen $refen 
Drift "CR1.DBA.DM1_A" -len 1.106904053 
Sextupole "SEXT.CR1.DBA.SNCH1" -len 0.1 -K2L -6.611960070044437 -refen $refen 
Drift "CR1.DBA.DM1_B" -len 0.1069040525 
Quadrupole "QUAD.CR1.DBA.QF2" -len 0.4 -K1L 0.5637241356 -refen $refen 
Drift "CR1.DBA.DM2" -len 1.299116763 
Quadrupole "QUAD.CR1.DBA.QF3" -len 0.4 -K1L -0.6662844456 -refen $refen 
Drift "CR1.DBA.DM3" -len 0.5053146596 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN1.1" -len 0.1 -K2L -8.067237487843848 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.1" -len 0.1 -K2L -0.08845453430102207 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.4 -K1L 0.4647092472 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.1" -len 0.1 -K2L 1.8956644735230037 -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN0.1" -len 0.1 -K2L -0.4584481441890126 -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D11A" -len 0.4072203145
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.3358875195 -refen $refen 
### Arc 1 End ###
Drift "CR1.DTR1A" -len 0.35 
Sextupole "SEXT.CR1.SXTR1" -len 0.1 -K2L -52.5692689579194 -refen $refen 
Drift "CR1.DTR1B" -len 0.15 
Quadrupole "QUAD.CR1.QTR1" -len 0.5 -K1L -0.8561234623524756 -refen $refen 
Drift "CR1.DTR2" -len 1.2 
Quadrupole "QUAD.CR1.QTR2" -len 0.5 -K1L 0.8508215606095942 -refen $refen 
Drift "CR1.DTR3A" -len 0.45 
Sextupole "SEXT.CR1.SXTR3" -len 0.1 -K2L -1.1506746154350833 -refen $refen 
Drift "CR1.DTR3B" -len 0.45 
Quadrupole "QUAD.CR1.QTR3" -len 0.5 -K1L -0.5277167345565399 -refen $refen 
Drift "CR1.DTR4" -len 0.6 
Quadrupole "QUAD.CR1.QTR4" -len 0.5 -K1L -0.09358466556668753 -refen $refen 
Drift "CR1.DTR5A" -len 0.75 
Sextupole "SEXT.CR1.SXTR5" -len 0.1 -K2L 2.0853311178434613 -refen $refen 
Drift "CR1.DTR5B" -len 0.35 
Drift "CR1.DTR5B" -len 0.35 
Sextupole "SEXT.CR1.SXTR5" -len 0.1 -K2L 2.0853311178434613 -refen $refen 
Drift "CR1.DTR5A" -len 0.75 
Quadrupole "QUAD.CR1.QTR4" -len 0.5 -K1L -0.09358466556668753 -refen $refen 
Drift "CR1.DTR4" -len 0.6 
Quadrupole "QUAD.CR1.QTR3" -len 0.5 -K1L -0.5277167345565399 -refen $refen 
Drift "CR1.DTR3B" -len 0.45 
Sextupole "SEXT.CR1.SXTR3" -len 0.1 -K2L -1.1506746154350833 -refen $refen 
Drift "CR1.DTR3A" -len 0.45 
Quadrupole "QUAD.CR1.QTR2" -len 0.5 -K1L 0.8508215606095942 -refen $refen 
Drift "CR1.DTR2" -len 1.2 
Quadrupole "QUAD.CR1.QTR1" -len 0.5 -K1L -0.8561234623524756 -refen $refen 
Drift "CR1.DTR1B" -len 0.15 
Sextupole "SEXT.CR1.SXTR1" -len 0.1 -K2L -52.5692689579194 -refen $refen 
Drift "CR1.DTR1A" -len 0.35 
### Arc 2 Start ###
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.3358875195 -refen $refen 
Drift "CR1.DBA.D11A" -len 0.4072203145 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN0.2" -len 0.1 -K2L 0.3864109005993865 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.2" -len 0.1 -K2L 0.43449341273905634 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.4 -K1L 0.4647092472 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.2" -len 0.1 -K2L 1.1136161088301622 -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN1.2" -len 0.1 -K2L 4.116095710882976 -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D11B1" -len 0.5133083062 
Quadrupole "QUAD.CR1.DBA.QF1" -len 0.4 -K1L -0.664141578 -refen $refen 
Drift "CR1.DBA.DM1_A" -len 1.106904053 
Sextupole "SEXT.CR1.DBA.SNCH1" -len 0.1 -K2L -6.611960070044437 -refen $refen 
Drift "CR1.DBA.DM1_B" -len 0.1069040525 
Quadrupole "QUAD.CR1.DBA.QF2" -len 0.4 -K1L 0.5637241356 -refen $refen 
Drift "CR1.DBA.DM2" -len 1.299116763 
Quadrupole "QUAD.CR1.DBA.QF3" -len 0.4 -K1L -0.6662844456 -refen $refen 
Drift "CR1.DBA.DM3" -len 0.5053146596 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN1.2" -len 0.1 -K2L 4.116095710882976 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.2" -len 0.1 -K2L 1.1136161088301622 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.4 -K1L 0.4647092472 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.2" -len 0.1 -K2L 0.43449341273905645 -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN0.2" -len 0.1 -K2L 0.3864109005993865 -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D11A" -len 0.4072203145  
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.3358875195 -refen $refen 
### Arc 2 End ###
Drift "CR1.DS1C_A" -len 1.95 
Sextupole "SEXT.CR1.SX1CC" -len 0.1 -K2L -2.6486187130003453 -refen $refen 
Drift "CR1.DS1C_B" -len 0.95 
Quadrupole "QUAD.CR1.QS1C" -len 0.5 -K1L -0.48249140494984327 -refen $refen 
Drift "CR1.DS2C" -len 0.5 
Quadrupole "QUAD.CR1.QS2C" -len 0.5 -K1L 0.0 -refen $refen 
Drift "CR1.DS3C_A" -len 0.4 
Sextupole "SEXT.CR1.SX3CC" -len 0.1 -K2L 3.8582402335406933 -refen $refen 
Drift "CR1.DS3C_B" -len 0.4 
Quadrupole "QUAD.CR1.QS3C" -len 0.5 -K1L 0.6734170259611983 -refen $refen 
Drift "CR1.DS4C" -len 0.5 
Quadrupole "QUAD.CR1.QS4C" -len 0.5 -K1L 0.0 -refen $refen 
Drift "CR1.DS5C" -len 0.5 
Quadrupole "QUAD.CR1.QS5C" -len 0.5 -K1L -0.735718561962932 -refen $refen 
Drift "CR1.WIG" -len 2.6 
Quadrupole "QUAD.CR1.QS5C" -len 0.5 -K1L -0.735718561962932 -refen $refen 
Drift "CR1.DS5C" -len 0.5 
Quadrupole "QUAD.CR1.QS4C" -len 0.5 -K1L 0.0 -refen $refen 
Drift "CR1.DS4C" -len 0.5 
Quadrupole "QUAD.CR1.QS3C" -len 0.5 -K1L 0.6734170259611983 -refen $refen 
Drift "CR1.DS3C_B" -len 0.4 
Sextupole "SEXT.CR1.SX3CC" -len 0.1 -K2L 3.8582402335406933 -refen $refen 
Drift "CR1.DS3C_A" -len 0.4 
Quadrupole "QUAD.CR1.QS2C" -len 0.5 -K1L 0.0 -refen $refen 
Drift "CR1.DS2C" -len 0.5 
Quadrupole "QUAD.CR1.QS1C" -len 0.5 -K1L -0.48249140494984327 -refen $refen 
Drift "CR1.DS1C_B" -len 0.95 
Sextupole "SEXT.CR1.SX1CC" -len 0.1 -K2L -2.6486187130003453 -refen $refen 
Drift "CR1.DS1C_A" -len 1.95 
### Arc 3 Start ###
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.3358875195 -refen $refen 
Drift "CR1.DBA.D11A" -len 0.4072203145 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN0.3" -len 0.1 -K2L -3.8025374831036793 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.3" -len 0.1 -K2L 2.601756478584562 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.4 -K1L 0.4647092472 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.3" -len 0.1 -K2L -0.06285053528477778 -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN1.3" -len 0.1 -K2L -6.583534167937967 -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D11B1" -len 0.5133083062 
Quadrupole "QUAD.CR1.DBA.QF1" -len 0.4 -K1L -0.664141578 -refen $refen 
Drift "CR1.DBA.DM1_A" -len 1.106904053 
Sextupole "SEXT.CR1.DBA.SNCH1" -len 0.1 -K2L -6.6119600700444385 -refen $refen 
Drift "CR1.DBA.DM1_B" -len 0.1069040525 
Quadrupole "QUAD.CR1.DBA.QF2" -len 0.4 -K1L 0.5637241356 -refen $refen 
Drift "CR1.DBA.DM2" -len 1.299116763 
Quadrupole "QUAD.CR1.DBA.QF3" -len 0.4 -K1L -0.6662844456 -refen $refen 
Drift "CR1.DBA.DM3" -len 0.5053146596 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN1.3" -len 0.1 -K2L -6.583534167937967 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.3" -len 0.1 -K2L -0.06285053528477778 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.4 -K1L 0.4647092472 -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.3" -len 0.1 -K2L 2.601756478584562 -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.450384 -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN0.3" -len 0.1 -K2L -3.8025374831036793 -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.051254996039999993 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
Drift "CR1.DBA.D11A" -len 0.4072203145 
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.3358875195 -refen $refen 
### Arc 3 End ###
Drift "CR1.DL0IN" -len 2.66137098404886 
Quadrupole "QUAD.CR1.QL1EXT" -len 0.5 -K1L -0.7350190844328781 -refen $refen 
Drift "CR1.DL1IN" -len 0.3098482 
Quadrupole "QUAD.CR1.QL2EXT" -len 0.5 -K1L 1.1940230580948872 -refen $refen 
Drift "CR1.DL2IN" -len 0.3010372 
Quadrupole "QUAD.CR1.QL3EXT" -len 0.5 -K1L -0.4780806892244219 -refen $refen 
Drift "CR1.DL3IN" -len 1.2729349688 
#### STEP EXTRACTOR - kicks on 3*244 bunches at a time ####  
#~ Stepdipole "CR1.EXTKICKER" -len 2. -strength [expr 0.0057*$refen] -freq [expr 0.49975/(244*3)] -phase [expr (-3./12.)*2.*$Pi] -duty [expr 1./3.] -refen $refen 
Stepdipole "CR1.EXTKICKER" -len 2. -strength 0 -freq 0 -duty 0 -refen $refen 
Drift "CR1.DL4IN" -len 0.6763230308 
Quadrupole "QUAD.CR1.QL4EXT" -len 0.5 -K1L 0.6388416037583273 -refen $refen 
Drift "CR1.DL5IN" -len 1 
Quadrupole "QUAD.CR1.QL5EXT" -len 0.5 -K1L -0.906597728673643 -refen $refen 
Drift "CR1.DL6IN" -len 0.553 
Quadrupole "QUAD.CR1.QL6EXT" -len 0.25 -K1L 0.5016200926487812 -refen $refen 
Drift "CR1.EXTSEPTUM" -len 1.5 
Marker "CR1_P1.EXTRACTION" 
