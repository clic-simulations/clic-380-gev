#
# matching of spin rotator to BC1 RF
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_sr_to_bc1_rf {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 10
    set quad_synrad   0

    set lquadm 0.3

    set kqm1  0.676808031
    set kqm2 -0.5958566715
    set kqm3  0.4751261008
    set kqm4  0.2739727001
    set kqm5 -0.162808699
    set kqm6 -0.5100178364

    set ldm1 5.5
    set ldm2 5.5
    set ldm3 2.0
    set ldm4 2.0
    set ldm5 0.5

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole
    Drift -length $ldm1 -six_dim $usesixdim
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole
    Drift -length $ldm2 -six_dim $usesixdim
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole
    Drift -length $ldm3 -six_dim $usesixdim
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole
    Drift -length $ldm4 -six_dim $usesixdim
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm5*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole
    Drift -length $ldm5 -six_dim $usesixdim
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm6*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole
}
