##------> Author: Raul Costa [raul.costa@cern.ch]  (Dec 2023 version) <------------------##
## Placet2 lattice generated from Eduardo Marin's design for Placet1
## Later adapted to be defined by K1L/K2L instead of S1/S2
## Sextupole strength optimised for low emittance/T566
## Low energy version for 380GeV stage
#~ if {$Dipole_synrad} {
#~ set refen 1.905993977 
#~ } else { 
#~ set refen 1.906
#~ } 

## Entry Quads and Sexts
## -0.70538    1.12129   -1.86800	-2.60383    5.36953   -0.46287

## Aligning the bunch after separation
Kalign DL_inj -fixed 1 -cx 0.0200233146 -cxp 0.01368718617
Marker "DL_LONG.CENTERINJ" -len 0. -nodes 1 -kick DL_inj 
#~ Sbend "BEND.SEPTQ" -len 0.500001425441655 -angle -0.005849061169 -E1 -0.002924530584 -E2 -0.002924530584 -K1L -0.5000007127 -refen $refen 
#~ set refen [expr $refen-14.1e-6*-0.005849061169*-0.005849061169/0.500001425441655*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DL1IN" -len 1.1 
Sbend "BEND.SEPT1" -len 1.00063486103645 -angle -0.0872664626 -E1 -0.0436332313 -E2 -0.0436332313 -K1L 0.9956481989 -refen $refen 
set refen [expr $refen-14.1e-6*-0.0872664626*-0.0872664626/1.00063486103645*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DL2IN" -len 0.3 
Quadrupole "QUAD.QL1IN" -len 0.4 -K1L -0.70538 -refen $refen 
Drift "DL3IN" -len 0.4 
Sbend "BEND.SEPT2" -len 2.58202788155376 -angle -0.2714151852 -E1 -0.1357075926 -E2 -0.1357075926 -K1L -1.249456954 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2714151852*-0.2714151852/2.58202788155376*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D33" -len 0.192971 
Quadrupole "QUAD.QL2IN" -len 0.4 -K1L 1.12129 -refen $refen 
Drift "D32A" -len 0.11111 
Sextupole "SEXT.DL.SXL2" -len 0.1 -K2L -2.60383 -refen $refen 
Drift "D32B" -len 0.43333 
Quadrupole "QUAD.QL3IN" -len 0.4 -K1L -1.86800 -refen $refen 
Drift "D33A" -len 0.0464855 
Sextupole "SEXT.DL.SXL3" -len 0.1 -K2L 5.36953 -refen $refen 
Drift "D33B" -len 0.0464855 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS3CA" -len 0.08511145 
Sextupole "SEXT.DL.SXL4" -len 0.1 -K2L -0.46287 -refen $refen 
Drift "DS3CB" -len 0.08511145 
Quadrupole "QUAD.QS2C" -len 0.4 -K1L -0.3333352 -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -K2L 1.02713 -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -K1L 0.9511964998 -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -K2L -43.72410 -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -K1L -1.209056 -refen $refen 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -K2L -35.79306 -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -K1L 0.04695405543 -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -K1L 0.824612 -refen $refen 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -K2L -2.60468 -refen $refen 
Drift "D11A" -len 0.1 
#~ Quadrupole "QUAD.QI1" -len 0.4 -K1L -1.493024 -refen $refen 
Quadrupole "QUAD.QI1" -len [expr 0.4/2.] -K1L [expr -1.493024/2.] -refen $refen  
