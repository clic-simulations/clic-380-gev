FILES="BBA/files/dbl_lat_1_3.0.tcl"

for f in $FILES
do
    echo "$f:"
    for t in Quadrupole Sextupole Sbend Cavity Bpm
    do
        N=$(grep "$t" "$f" | wc -l)
        echo "$t count = $N"
    done
done
