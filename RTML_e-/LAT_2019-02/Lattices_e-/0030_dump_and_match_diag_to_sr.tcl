#
# Dump and matching of diagnostics to spin rotator
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_diag_to_sr {bparray} {
    upvar $bparray beamparams

    set usesixdim     1
    set numthinlenses 10
    set quad_synrad   0

    set lquadm 0.3

    set kqm1 0.676841094
    set kqm2 -0.6610440419
    set kqm3 0.7750555182
    set kqm4 -0.6743661213

    set ldm1 4.0
    set ldm2 2.3
    set ldm3 3.6

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
    Bpm
    Quadrupole -name "SRmatch1" -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "DIQ-SRmatch"
    Drift -length $ldm1 -six_dim $usesixdim
    Bpm
    Quadrupole -name "SRmatch1" -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "DIQ-SRmatch"
    Drift -length $ldm2 -six_dim $usesixdim
    Bpm
    Quadrupole -name "SRmatch1" -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "DIQ-SRmatch"
    Drift -length $ldm3 -six_dim $usesixdim
    Bpm
    Quadrupole -name "SRmatch1" -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "DIQ-SRmatch"
}
