Performs 1-2-1, DFS and RF realignment on the ML.

The final offsets of all elements are saved to "results/bx.1"
This file was saved with "SaveAllPositions -file bx.1 -cav_bpm 1"
To read use "ReadAllPositions -file bx.1 -cav_bpm 1"

The number of machines to perform tuning on is set using a command line argument:
placet run.tcl num-machines
