TclCall -script {
     Octave {
         B0 = placet_get_beam();
         x = B0(:,2);
         y = B0(:,3);
         xp = B0(:,5);
         yp = B0(:,6);
        
         l = 0.3;

         HALFPI = [0,1,0,0;-1,0,0,0;0,0,0,1;0,0,-1,0];
         PI = [-1,0,0,0;0,-1,0,0;0,0,-1,0;0,0,0,-1];
         PIHALFPI = [-1,0,0,0;0,-1,0,0;0,0,0,1;0,0,-1,0];
         ZeroHALFPI = [1,0,0,0;0,1,0,0;0,0,0,1;0,0,-1,0];

         load X1.dat


         k1 = X(1);
         k2 = X(2);
         k3 = X(3);
         k4 = X(4);
 
         M1 = [1,0,0,0;0,1,-k1*l,0;0,0,1,0;-k1*l,0,0,1];
         M2 = [1,0,0,0;0,1,-k2*l,0;0,0,1,0;-k2*l,0,0,1];
         M3 = [1,0,0,0;0,1,-k3*l,0;0,0,1,0;-k3*l,0,0,1];
         M4 = [1,0,0,0;0,1,-k4*l,0;0,0,1,0;-k4*l,0,0,1];



         vec_xy = [x xp y yp];
         vec_xy = vec_xy * (M1');
         vec_xy = vec_xy*(HALFPI')*(M2')*(PIHALFPI');
         vec_xy = vec_xy * (M3');
         vec_xy = vec_xy * (HALFPI')*(M4')*(ZeroHALFPI');
        
         B0(:,2) = vec_xy(:,1);
         B0(:,3) = vec_xy(:,3);
         B0(:,5) = vec_xy(:,2);
         B0(:,6) = vec_xy(:,4);
       
 
         placet_set_beam(B0);

     }
}
