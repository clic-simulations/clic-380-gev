#
# matching of BC2 RF to BC2 chicane 1
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_bc2_rf_to_chicane_1 {bparray} {
    upvar $bparray beamparams

    set usesixdim 1
    set numthinlenses 10
    set quad_synrad 0

    set lquadm 0.3

    set kqm1 -0.110482562118430
    set kqm2 -0.179162534075576
    set kqm3  0.268887322217520
    set kqm4 -0.127374568691316
    
    set ldm1 0.8
    set ldm2 8.2
    set ldm3 3.9
    set ldm4 0.5
    set ldm5 0.5

    set refenergy $beamparams(meanenergy)
    SetReferenceEnergy $refenergy

    Girder
    Drift -length $ldm1 -six_dim $usesixdim
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "D220-1"
    Drift -length $ldm2 -six_dim $usesixdim
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "D220-2"
    Drift -length $ldm3 -six_dim $usesixdim
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "D220-3"
    Bpm
    Drift -length $ldm4 -six_dim $usesixdim
    Bpm
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "D220-4"
    Drift -length $ldm5 -six_dim $usesixdim
}
