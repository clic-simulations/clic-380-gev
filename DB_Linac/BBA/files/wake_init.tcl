

SplineCreate "acc_cav_Wt" -file "acc_cav_Wt.dat"
SplineCreate "acc_cav_Wl" -file "acc_cav_Wl.dat"

# 
ShortRangeWake "acc_cav_SR_W" -type 2 -wx "acc_cav_Wt" -wy "acc_cav_Wt" -wz "acc_cav_Wl"
	 

#
# use this list to create fields
 WakeSet Wake_acc_cav_long_range {1. 1 0 0}
#
# define structure
InjectorCavityDefine -name 0 -lambda $acc_cav(lambda) -wakelong Wake_acc_cav_long_range 
