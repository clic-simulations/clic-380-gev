These files can be used to generate a lattice for the L*=4.3 m BDS.

MAD-X is used to generate the lattice (can be used to match) and the output is converted to a PLACET lattice file.

Steps:
- ./madx job.madx - this generates the a twiss.bds file.
- perl madx2placet.pl twiss.bds - this generates the PLACET lattice file.
