##------> Author: Raul Costa [raul.costa@cern.ch] <------------------##
## Placet2 lattice generated from Eduardo Marin's design for Placet1
## RF kicker added to be compatible with  Placet2's recombination
## Later adapted to be defined by K1L/K2L instead of S1/S2
## Low energy version for 380GeV stage
set pi [expr acos(-1)] 
if {$Dipole_synrad} { 
set refen 1.906
} else { 
set refen 1.906 
} 
Marker "DBRC_Start.INJECTION" 
Sbend "BEND.BRB1B" -len 4.4 -angle -0.267035375600000 -E1 -0.06675884389 -E2 -0.06675884389 -refen $refen 
set refen [expr $refen-14.1e-6*-0.267035375600000*-0.267035375600000/4.4*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DG.L1" -len 2 
Sextupole "SEXT.DG.SEXT1" -len 0.1 -K2L -0.7527053170837354 -refen $refen 
Drift "DG.L11" -len 0.9 
Quadrupole "QUAD.QFDOG1" -len 0.2 -K1L 0.28289 -refen $refen 
Drift "DG.L2" -len 5 
Quadrupole "QUAD.QDDOG1" -len 0.2 -K1L -0.21502 -refen $refen  
Drift "DG.L3" -len 5 
Quadrupole "QUAD.QFDOG2" -len 0.2 -K1L 0.15917 -refen $refen 
Drift "DG.L4" -len 8 
Drift "DG.L4" -len 8 
Quadrupole "QUAD.QFDOG3" -len 0.2 -K1L 0.15917 -refen $refen 
Drift "DG.L3" -len 5 
Quadrupole "QUAD.QDDOG2" -len 0.2 -K1L -0.21502 -refen $refen 
Drift "DG.L2" -len 5 
Quadrupole "QUAD.QFDOG4" -len 0.2 -K1L 0.28289 -refen $refen 
Drift "DG.L11" -len 0.9 
Sextupole "SEXT.DG.SEXT2" -len 0.1 -K2L 0.7552874900369622 -refen $refen 
Drift "DG.L1" -len 2 
Sbend "BEND.BRB1B" -len 4.4 -angle 0.267035375600000 -E1 0.06675884389 -E2 0.06675884389 -refen $refen 
set refen [expr $refen-14.1e-6*0.267035375600000*0.267035375600000/4.4*$refen*$refen*$refen*$refen*$Dipole_synrad] 
Drift "DGDL.D1" -len 1.03396146 
Quadrupole "QUAD.DGDL.Q1" -len 0.2 -K1L 0.369215290202011 -refen $refen 
Drift "DGDL.D2" -len 0.99977365 
Quadrupole "QUAD.DGDL.Q2" -len 0.2 -K1L -0.373101787518819 -refen $refen 
Drift "DGDL.D3" -len 5.26626489 
Quadrupole "QUAD.DGDL.Q3" -len 0.2 -K1L 0.477815730530590 -refen $refen 
Drift "DGDL.D4" -len 1.01854763 
Quadrupole "QUAD.DGDL.Q4" -len 0.2 -K1L -0.268040170753416 -refen $refen  
Drift "DGDL.D5" -len 1.39261465 
Rfmultipole "RFMulti.DLINJSEPTUM" -len 0.2 -freq 0.49975 -strength [expr 0.0075*$refen] -phase $pi -refen $refen 
Drift "DRRF" -len 1.8 
Marker "DBRC_Start.EXTRACTION" 
