Octave {
    function decrease_strength(beamline,ds,Start,End)

        load movement_all.dat; 

        if ( ds == 0.05)
        sigma_move = 0.35;
        else
        sigma_move = 0.7;
        endif

        sbend_all = placet_get_number_list(beamline,"sbend");
        quadrupole_all = placet_get_number_list(beamline,"quadrupole");
        dipole_all = placet_get_number_list(beamline,"dipole");
        multipole_all = placet_get_number_list(beamline,"multipole");
    
        sbends = sbend_all((sbend_all > Start) & (sbend_all<End));
        quadrupoles = quadrupole_all((quadrupole_all > Start) & (quadrupole_all<End));
        dipoles = dipole_all((dipole_all > Start) & (dipole_all<End));
        multipoles = multipole_all((multipole_all > Start) & (multipole_all<End));

        quad_mask = (quadrupole_all > Start) & (quadrupole_all<End);
        x_quad = x_quad_all(quad_mask) * sigma_move;
        y_quad = y_quad_all(quad_mask) * sigma_move;

        multi_mask = (multipole_all > Start) & (multipole_all<End);
        x_multi = x_multi_all(multi_mask) * sigma_move;
        y_multi = y_multi_all(multi_mask) * sigma_move;
   
        if ( length(sbends)>0) 
        SA = placet_element_get_attribute(beamline, sbends, "e0"); 
        placet_element_set_attribute(beamline, sbends,"e0", SA/(1 + ds));
        endif
    
        if ( length(quadrupoles)>0) 
        QA = placet_element_get_attribute(beamline, quadrupoles, "strength");
        placet_element_set_attribute(beamline, quadrupoles,"strength", QA/(1 + ds));
        placet_element_vary_attribute(beamline, quadrupoles,"x",x_quad);
        placet_element_vary_attribute(beamline, quadrupoles,"y",y_quad);
        endif

        if ( length(dipoles)>0) 
        DAx = placet_element_get_attribute(beamline, dipoles, "strength_x");
        DAy = placet_element_get_attribute(beamline, dipoles, "strength_y");
        placet_element_set_attribute(beamline, dipoles ,"strength_x", DAx/(1 + ds));
        placet_element_set_attribute(beamline, dipoles ,"strength_y", DAy/(1 + ds));
        endif

        if ( length(multipoles)>0) 
        MA = placet_element_get_attribute(beamline, multipoles,"strength");
        placet_element_set_attribute(beamline, multipoles,"strength",MA/(1 + ds));
        placet_element_vary_attribute(beamline, multipoles,"x",x_multi);
        placet_element_vary_attribute(beamline, multipoles,"y",y_multi);
        endif
       save -text movement.dat x_quad y_quad x_multi y_multi sigma_move;

    end

    function reset_strength(beamline,ds,Start,End)
        load movement.dat;

        sbend_all = placet_get_number_list(beamline,"sbend");
        quadrupole_all = placet_get_number_list(beamline,"quadrupole");
        dipole_all = placet_get_number_list(beamline,"dipole");
        multipole_all = placet_get_number_list(beamline,"multipole");

        sbends = sbend_all((sbend_all > Start) & (sbend_all<End));
        quadrupoles = quadrupole_all((quadrupole_all > Start) & (quadrupole_all<End));
        dipoles = dipole_all((dipole_all > Start) & (dipole_all<End));
        multipoles = multipole_all((multipole_all > Start) & (multipole_all<End));

        if ( length(sbends)>0) 
        SA = placet_element_get_attribute(beamline, sbends, "e0");
        placet_element_set_attribute(beamline, sbends,"e0", SA*(1 + ds));
        endif

        if ( length(quadrupoles)>0) 
        QA = placet_element_get_attribute(beamline, quadrupoles, "strength");
        placet_element_set_attribute(beamline, quadrupoles,"strength", QA*(1 + ds));
        placet_element_vary_attribute(beamline, quadrupoles,"x",-1*x_quad);
        placet_element_vary_attribute(beamline, quadrupoles,"y",-1*y_quad);
        endif

        if ( length(dipoles)>0) 
        DAx = placet_element_get_attribute(beamline, dipoles, "strength_x");
        DAy = placet_element_get_attribute(beamline, dipoles, "strength_y");
        placet_element_set_attribute(beamline, dipoles ,"strength_x", DAx*(1 + ds));
        placet_element_set_attribute(beamline, dipoles ,"strength_y", DAy*(1 + ds));
        endif

        if ( length(multipoles)>0) 
        MA = placet_element_get_attribute(beamline, multipoles,"strength");
        placet_element_set_attribute(beamline, multipoles,"strength",MA*(1 + ds));
        placet_element_vary_attribute(beamline, multipoles,"x",-1*x_multi);
        placet_element_vary_attribute(beamline, multipoles,"y",-1*y_multi);
        endif

    end

    function decrease_strength_tal(beamline,ds,Start,End)

        sbend_all = placet_get_number_list(beamline,"sbend");
        quadrupole_all = placet_get_number_list(beamline,"quadrupole");
        dipole_all = placet_get_number_list(beamline,"dipole");
        multipole_all = placet_get_number_list(beamline,"multipole");

        sbends = sbend_all((sbend_all > Start) & (sbend_all<End));
        quadrupoles = quadrupole_all((quadrupole_all > Start) & (quadrupole_all<End));
        dipoles = dipole_all((dipole_all > Start) & (dipole_all<End));
        multipoles = multipole_all((multipole_all > Start) & (multipole_all<End));

        if ( length(sbends)>0)
        SA = placet_element_get_attribute(beamline, sbends, "e0");
        placet_element_set_attribute(beamline, sbends,"e0", SA/(1 + ds));
        endif
        if ( length(quadrupoles)>0)
        QA = placet_element_get_attribute(beamline, quadrupoles, "strength");
        placet_element_set_attribute(beamline, quadrupoles,"strength", QA/(1 + ds));
        endif

        if ( length(dipoles)>0)
        DAx = placet_element_get_attribute(beamline, dipoles, "strength_x");
        DAy = placet_element_get_attribute(beamline, dipoles, "strength_y");
        placet_element_set_attribute(beamline, dipoles ,"strength_x", DAx/(1 + ds));
        placet_element_set_attribute(beamline, dipoles ,"strength_y", DAy/(1 + ds));
        endif

        if ( length(multipoles)>0)
        MA = placet_element_get_attribute(beamline, multipoles,"strength");
        placet_element_set_attribute(beamline, multipoles,"strength",MA/(1 + ds));
        endif

    end


    function reset_strength_tal(beamline,ds,Start,End)

        sbend_all = placet_get_number_list(beamline,"sbend");
        quadrupole_all = placet_get_number_list(beamline,"quadrupole");
        dipole_all = placet_get_number_list(beamline,"dipole");
        multipole_all = placet_get_number_list(beamline,"multipole");

        sbends = sbend_all((sbend_all > Start) & (sbend_all<End));
        quadrupoles = quadrupole_all((quadrupole_all > Start) & (quadrupole_all<End));
        dipoles = dipole_all((dipole_all > Start) & (dipole_all<End));
        multipoles = multipole_all((multipole_all > Start) & (multipole_all<End));

        if ( length(sbends)>0)
        SA = placet_element_get_attribute(beamline, sbends, "e0");
        placet_element_set_attribute(beamline, sbends,"e0", SA*(1 + ds));
        endif
        if ( length(quadrupoles)>0)
        QA = placet_element_get_attribute(beamline, quadrupoles, "strength");
        placet_element_set_attribute(beamline, quadrupoles,"strength", QA*(1 + ds));
        endif

        if ( length(dipoles)>0)
        DAx = placet_element_get_attribute(beamline, dipoles, "strength_x");
        DAy = placet_element_get_attribute(beamline, dipoles, "strength_y");
        placet_element_set_attribute(beamline, dipoles ,"strength_x", DAx*(1 + ds));
        placet_element_set_attribute(beamline, dipoles ,"strength_y", DAy*(1 + ds));
        endif

        if ( length(multipoles)>0)
        MA = placet_element_get_attribute(beamline, multipoles,"strength");
        placet_element_set_attribute(beamline, multipoles,"strength",MA*(1 + ds));
        endif

    end



    function decrease_grad(beamline,ds,Start,End)
    CA = placet_get_number_list(beamline,"cavity");
    G = placet_element_get_attribute(beamline, CA, "gradient");
    placet_element_set_attribute(beamline, CA, "gradient", G/(1+ds));
    end


    function reset_grad(beamline,ds,Start,End)
    CA = placet_get_number_list(beamline,"cavity");
    G = placet_element_get_attribute(beamline, CA, "gradient");
    placet_element_set_attribute(beamline, CA, "gradient", G*(1+ds));
    end

    function change_phase(beamline,ds,Start,End)
        denergy = 2.86*ds;
        E_total = 0.01323 * 1.5 *20;
        dphase_bc1 = acos(-denergy/E_total)*180/pi - 90;

        CA_all = placet_get_number_list(beamline,"cavity");
        CAs = CA_all((CA_all > Start) & (CA_all<End));
        placet_element_vary_attribute(beamline, CAs, "phase",dphase_bc1);

    end

    function reset_phase(beamline,ds,Start,End)
        denergy = 2.86*ds;
        E_total = 0.01323 * 1.5 *20;
        dphase_bc1 = acos(-denergy/E_total)*180/pi - 90;

        CA_all = placet_get_number_list(beamline,"cavity");
        CAs = CA_all((CA_all > Start) & (CA_all<End));
        placet_element_vary_attribute(beamline, CAs, "phase",-dphase_bc1);

    end

    function change_phase_bc2(beamline,ds,Start,End)
        denergy = 9.0*ds;
        E_total = 0.0927 * 0.23 * 78;
        dphase_bc2 = acos(-denergy/E_total)*180/pi - 90;

        CA_all = placet_get_number_list(beamline,"cavity");
        CAs = CA_all((CA_all > Start) & (CA_all<End));
        placet_element_vary_attribute(beamline, CAs, "phase",dphase_bc2);

    end

    function reset_phase_bc2(beamline,ds,Start,End)
        denergy = 9.0*ds;
        E_total = 0.0927 * 0.23 * 78;
        dphase_bc2 = acos(-denergy/E_total)*180/pi - 90;

        CA_all = placet_get_number_list(beamline,"cavity");
        CAs = CA_all((CA_all > Start) & (CA_all<End));
        placet_element_vary_attribute(beamline, CAs, "phase",-dphase_bc2);

    end



}
