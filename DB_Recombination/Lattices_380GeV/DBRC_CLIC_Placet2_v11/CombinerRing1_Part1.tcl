## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
 ## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
 ## Which means there will be a slight magnet missfocus for the other bunches. 
 ## This was set to the 3nd turn of CR1. 
 set Pi [expr acos(-1)] 
 if {$Dipole_synrad} { 
 set refen 1.905381993 
 } else { 
 set refen 1.906
 } 
 ####START#### 
 Drift "CR1.INJSEPTUM" -len 1 
 Drift "CR1.DL7IN" -len 0.5 
 Quadrupole "QUAD.CR1.QL6IN" -len 0.25 -K1L 0.5016200926487812 -refen $refen 
 Bpm "BPM.CR1_1.1" -len 0 
 Drift "CR1.DL6IN" -len 0.553 
 Quadrupole "QUAD.CR1.QL5IN" -len 0.5 -K1L -0.906597728673643 -refen $refen 
 Bpm "BPM.CR1_1.2" -len 0 
 Drift "CR1.DL5IN" -len 1 
 Quadrupole "QUAD.CR1.QL4IN" -len 0.5 -K1L 0.6388416037583273 -refen $refen 
 Bpm "BPM.CR1_1.3" -len 0 
 Drift "CR1.DL4IN" -len 0.349996062 
 Drift "CR1.DRRFL2" -len 0.45265 
 ##### Static Dipole ####### 
 Rfmultipole "RFMulti.CR1.RFDEFLCOMP2" -len 0.2 -strength [expr 0.00095*$refen] -freq 0 -refen $refen 
 Drift "CR1.DRRFL1" -len 0.1 
 #### RF Corrector 
 Rfmultipole "RFMulti.CR1.RFDEFLB" -len 1.147353938 -strength [expr 0.0038*$refen] -freq 0.9995 -refen $refen 
 Drift "CR1.DRRFL1" -len 0.1 
 ##### Static Dipole ####### 
 Rfmultipole "RFMulti.CR1.RFDEFLCOMP2" -len 0.2 -strength [expr 0.00095*$refen] -freq 0 -refen $refen 
 Drift "CR1.DRRFL" -len 0.3 
 Drift "CR1.DL3IN" -len 1.099258 
 Quadrupole "QUAD.CR1.QL3IN" -len 0.5 -K1L -0.4780806892244219 -refen $refen 
 Bpm "BPM.CR1_1.4" -len 0 
 Drift "CR1.DL2IN" -len 0.3010372 
 Quadrupole "QUAD.CR1.QL2IN" -len 0.5 -K1L 1.1940230580948872 -refen $refen 
 Bpm "BPM.CR1_1.5" -len 0 
 Drift "CR1.DL1IN" -len 0.3098482 
 Quadrupole "QUAD.CR1.QL1IN" -len 0.5 -K1L -0.7350190844328781 -refen $refen 
 Bpm "BPM.CR1_1.6" -len 0 
 ### Missing length correction ### 
 Drift "CR1.DL0IN" -len 0.5 
 Quadrupole "QUAD.CR1.QL0IN" -len 0.5 -K1L 0.0 -refen $refen 
 Drift "CR1.DMISSLEN" -len 1.66137098404886 
 ### Start of arc 1 ### 
 Marker "CR1MDBACELLSTART" 
 Marker "CR1.DBA.MCELL" 
 Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.33588751949999995 -refen $refen 
 Bpm "BPM.CR1_1.7" -len 0 
 Drift "CR1.DBA.D11A" -len 0.2082887778 
 Drift "CR1.DBA.D11B" -len 0.1989315367 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.8" -len 0 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sextupole "SEXT.CR1.DBA.SN0.1" -len 0.1 -K2L -0.4584481441890126 -refen $refen 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Marker "CR1.DBA.HCM0" 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038400000000006 -refen $refen 
 Bpm "BPM.CR1_1.9" -len 0 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Marker "CR1.DBA.HBM" 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SN3.1" -len 0.1 -K2L 1.895664473523004 -refen $refen 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.10" -len 0 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.11" -len 0 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SNN3.1" -len 0.1 -K2L -0.08845453430102207 -refen $refen 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Marker "CR1.DBA.HBM2" 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038399999999995 -refen $refen 
 Bpm "BPM.CR1_1.12" -len 0 
 Marker "CR1.DBA.HCM" 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Sextupole "SEXT.CR1.DBA.SN1.1" -len 0.1 -K2L -8.06723748784385 -refen $refen 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.6692279500000001 -refen $refen 
 Bpm "BPM.CR1_1.13" -len 0 
 Drift "CR1.DBA.D11B1" -len 0.3133083062 
 Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -K2L 0.0 -refen $refen 
 Drift "CR1.DBA.DM0" -len 0.1 
 Quadrupole "QUAD.CR1.DBA.QF1" -len 0.4 -K1L -0.664141578 -refen $refen 
 Bpm "BPM.CR1_1.14" -len 0 
 Drift "CR1.DBA.DM1_A" -len 1.106904053 
 Sextupole "SEXT.CR1.DBA.SNCH1" -len 0.1 -K2L -6.611960070044437 -refen $refen 
 Drift "CR1.DBA.DM1_B" -len 0.1069040525 
 Quadrupole "QUAD.CR1.DBA.QF2" -len 0.4 -K1L 0.5637241356 -refen $refen 
 Bpm "BPM.CR1_1.15" -len 0 
 Drift "CR1.DBA.DM2" -len 1.099116763 
 Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -K2L 0.0 -refen $refen 
 Drift "CR1.DBA.DMSEXT1" -len 0.1 
 Quadrupole "QUAD.CR1.DBA.QF3" -len 0.4 -K1L -0.6662844456000001 -refen $refen 
 Bpm "BPM.CR1_1.16" -len 0 
 Drift "CR1.DBA.DM3" -len 0.2563831229 
 Drift "CR1.DBA.DMSEXT1" -len 0.1 
 Drift "CR1.DBA.D11B2" -len 0.1489315367 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.6692279500000001 -refen $refen 
 Bpm "BPM.CR1_1.17" -len 0 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.051254996039999993 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sextupole "SEXT.CR1.DBA.SN1.1" -len 0.1 -K2L -8.067237487843848 -refen $refen 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038400000000006 -refen $refen 
 Bpm "BPM.CR1_1.18" -len 0 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SNN3.1" -len 0.1 -K2L -0.08845453430102207 -refen $refen 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.19" -len 0 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.20" -len 0 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SN3.1" -len 0.1 -K2L 1.8956644735230037 -refen $refen 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038400000000006 -refen $refen 
 Bpm "BPM.CR1_1.21" -len 0 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Sextupole "SEXT.CR1.DBA.SN0.1" -len 0.1 -K2L -0.4584481441890126 -refen $refen 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.22" -len 0 
 Drift "CR1.DBA.D11B" -len 0.1989315367 
 Drift "CR1.DBA.D11A" -len 0.2082887778 
 Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.3358875195 -refen $refen 
 Bpm "BPM.CR1_1.23" -len 0 
 Marker "CR1.DBA.MCELL" 
 Marker "CR1MDBACELLEND" 
 Drift "CR1.DTR1A" -len 0.35 
 Sextupole "SEXT.CR1.SXTR1" -len 0.1 -K2L -52.56926895791942 -refen $refen 
 Drift "CR1.DTR1B" -len 0.15 
 Quadrupole "QUAD.CR1.QTR1" -len 0.5 -K1L -0.8561234623524756 -refen $refen 
 Bpm "BPM.CR1_1.24" -len 0 
 Drift "CR1.DTR2" -len 1.2 
 Quadrupole "QUAD.CR1.QTR2" -len 0.5 -K1L 0.8508215606095942 -refen $refen 
 Bpm "BPM.CR1_1.25" -len 0 
 Drift "CR1.DTR3A" -len 0.45 
 Sextupole "SEXT.CR1.SXTR3" -len 0.1 -K2L -1.1506746154350833 -refen $refen 
 Drift "CR1.DTR3B" -len 0.45 
 Quadrupole "QUAD.CR1.QTR3" -len 0.5 -K1L -0.5277167345565399 -refen $refen 
 Bpm "BPM.CR1_1.26" -len 0 
 Drift "CR1.DTR4" -len 0.6 
 Quadrupole "QUAD.CR1.QTR4" -len 0.5 -K1L -0.09358466556668753 -refen $refen 
 Bpm "BPM.CR1_1.27" -len 0 
 Drift "CR1.DTR5A" -len 0.75 
 Sextupole "SEXT.CR1.SXTR5" -len 0.1 -K2L 2.0853311178434613 -refen $refen 
 Drift "CR1.DTR5B" -len 0.35 
 Drift "CR1.DTR5B" -len 0.35 
 Sextupole "SEXT.CR1.SXTR5" -len 0.1 -K2L 2.0853311178434613 -refen $refen 
 Drift "CR1.DTR5A" -len 0.75 
 Quadrupole "QUAD.CR1.QTR4" -len 0.5 -K1L -0.09358466556668753 -refen $refen 
 Bpm "BPM.CR1_1.28" -len 0 
 Drift "CR1.DTR4" -len 0.6 
 Quadrupole "QUAD.CR1.QTR3" -len 0.5 -K1L -0.5277167345565399 -refen $refen 
 Bpm "BPM.CR1_1.29" -len 0 
 Drift "CR1.DTR3B" -len 0.45 
 Sextupole "SEXT.CR1.SXTR3" -len 0.1 -K2L -1.1506746154350833 -refen $refen 
 Drift "CR1.DTR3A" -len 0.45 
 Quadrupole "QUAD.CR1.QTR2" -len 0.5 -K1L 0.8508215606095942 -refen $refen 
 Bpm "BPM.CR1_1.30" -len 0 
 Drift "CR1.DTR2" -len 1.2 
 Quadrupole "QUAD.CR1.QTR1" -len 0.5 -K1L -0.8561234623524756 -refen $refen 
 Bpm "BPM.CR1_1.31" -len 0 
 Drift "CR1.DTR1B" -len 0.15 
 Sextupole "SEXT.CR1.SXTR1" -len 0.1 -K2L -52.56926895791942 -refen $refen 
 Drift "CR1.DTR1A" -len 0.35 
 Marker "CR1MDBACELLSTART" 
 Marker "CR1.DBA.MCELL" 
 Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.3358875195 -refen $refen 
 Bpm "BPM.CR1_1.32" -len 0 
 Drift "CR1.DBA.D11A" -len 0.2082887778 
 Drift "CR1.DBA.D11B" -len 0.1989315367 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.33" -len 0 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.051254996039999993 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sextupole "SEXT.CR1.DBA.SN0.2" -len 0.1 -K2L 0.3864109005993865 -refen $refen 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Marker "CR1.DBA.HCM0" 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038399999999995 -refen $refen 
 Bpm "BPM.CR1_1.34" -len 0 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Marker "CR1.DBA.HBM" 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SN3.2" -len 0.1 -K2L 0.43449341273905634 -refen $refen 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.23235462360000003 -refen $refen 
 Bpm "BPM.CR1_1.35" -len 0 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.23235462360000003 -refen $refen 
 Bpm "BPM.CR1_1.36" -len 0 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SNN3.2" -len 0.1 -K2L 1.1136161088301622 -refen $refen 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Marker "CR1.DBA.HBM2" 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.19959458320000004 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038399999999995 -refen $refen 
 Bpm "BPM.CR1_1.37" -len 0 
 Marker "CR1.DBA.HCM" 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Sextupole "SEXT.CR1.DBA.SN1.2" -len 0.1 -K2L 4.116095710882976 -refen $refen 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.38" -len 0 
 Drift "CR1.DBA.D11B1" -len 0.3133083062 
 Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -K2L 0.0 -refen $refen 
 Drift "CR1.DBA.DM0" -len 0.1 
 Quadrupole "QUAD.CR1.DBA.QF1" -len 0.4 -K1L -0.664141578 -refen $refen 
 Bpm "BPM.CR1_1.39" -len 0 
 Drift "CR1.DBA.DM1_A" -len 1.106904053 
 Sextupole "SEXT.CR1.DBA.SNCH1" -len 0.1 -K2L -6.611960070044437 -refen $refen 
 Drift "CR1.DBA.DM1_B" -len 0.1069040525 
 Quadrupole "QUAD.CR1.DBA.QF2" -len 0.4 -K1L 0.5637241356 -refen $refen 
 Bpm "BPM.CR1_1.40" -len 0 
 Drift "CR1.DBA.DM2" -len 1.099116763 
 Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -K2L 0.0 -refen $refen 
 Drift "CR1.DBA.DMSEXT1" -len 0.1 
 Quadrupole "QUAD.CR1.DBA.QF3" -len 0.4 -K1L -0.6662844456000001 -refen $refen 
 Bpm "BPM.CR1_1.41" -len 0 
 Drift "CR1.DBA.DM3" -len 0.2563831229 
 Drift "CR1.DBA.DMSEXT1" -len 0.1 
 Drift "CR1.DBA.D11B2" -len 0.1489315367 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.42" -len 0 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sextupole "SEXT.CR1.DBA.SN1.2" -len 0.1 -K2L 4.116095710882976 -refen $refen 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038399999999995 -refen $refen 
 Bpm "BPM.CR1_1.43" -len 0 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SNN3.2" -len 0.1 -K2L 1.1136161088301622 -refen $refen 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.44" -len 0 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.45" -len 0 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SN3.2" -len 0.1 -K2L 0.43449341273905645 -refen $refen 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038400000000006 -refen $refen 
 Bpm "BPM.CR1_1.46" -len 0 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Sextupole "SEXT.CR1.DBA.SN0.2" -len 0.1 -K2L 0.3864109005993865 -refen $refen 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.47" -len 0 
 Drift "CR1.DBA.D11B" -len 0.1989315367 
 Drift "CR1.DBA.D11A" -len 0.2082887778 
 Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.33588751949999995 -refen $refen 
 Bpm "BPM.CR1_1.48" -len 0 
 Marker "CR1.DBA.MCELL" 
 Marker "CR1MDBACELLEND" 
 Drift "CR1.DS1C_A" -len 1.95 
 Sextupole "SEXT.CR1.SX1CC" -len 0.1 -K2L -2.6486187130003453 -refen $refen 
 Drift "CR1.DS1C_B" -len 0.95 
 Quadrupole "QUAD.CR1.QS1C" -len 0.5 -K1L -0.48249140494984327 -refen $refen 
 Bpm "BPM.CR1_1.49" -len 0 
 Drift "CR1.DS2C" -len 0.5 
 Quadrupole "QUAD.CR1.QS2C" -len 0.5 -K1L 0.0 -refen $refen 
 Bpm "BPM.CR1_1.50" -len 0 
 Drift "CR1.DS3C_A" -len 0.4 
 Sextupole "SEXT.CR1.SX3CC" -len 0.1 -K2L 3.8582402335406933 -refen $refen 
 Drift "CR1.DS3C_B" -len 0.4 
 Quadrupole "QUAD.CR1.QS3C" -len 0.5 -K1L 0.6734170259611983 -refen $refen 
 Bpm "BPM.CR1_1.51" -len 0 
 Drift "CR1.DS4C" -len 0.5 
 Quadrupole "QUAD.CR1.QS4C" -len 0.5 -K1L 0.0 -refen $refen 
 Bpm "BPM.CR1_1.52" -len 0 
 Drift "CR1.DS5C" -len 0.5 
 Quadrupole "QUAD.CR1.QS5C" -len 0.5 -K1L -0.735718561962932 -refen $refen 
 Bpm "BPM.CR1_1.53" -len 0 
 Drift "CR1.DS6C" -len 0.5 
 Drift "CR1.WIG1" -len 0.8 
 Drift "CR1.WIG2" -len 0.8 
 Drift "CR1.DS6C" -len 0.5 
 Quadrupole "QUAD.CR1.QS5C" -len 0.5 -K1L -0.735718561962932 -refen $refen 
 Bpm "BPM.CR1_1.54" -len 0 
 Drift "CR1.DS5C" -len 0.5 
 Quadrupole "QUAD.CR1.QS4C" -len 0.5 -K1L 0.0 -refen $refen 
 Bpm "BPM.CR1_1.55" -len 0 
 Drift "CR1.DS4C" -len 0.5 
 Quadrupole "QUAD.CR1.QS3C" -len 0.5 -K1L 0.6734170259611983 -refen $refen 
 Bpm "BPM.CR1_1.56" -len 0 
 Drift "CR1.DS3C_B" -len 0.4 
 Sextupole "SEXT.CR1.SX3CC" -len 0.1 -K2L 3.8582402335406933 -refen $refen 
 Drift "CR1.DS3C_A" -len 0.4 
 Quadrupole "QUAD.CR1.QS2C" -len 0.5 -K1L 0.0 -refen $refen 
 Bpm "BPM.CR1_1.57" -len 0 
 Drift "CR1.DS2C" -len 0.5 
 Quadrupole "QUAD.CR1.QS1C" -len 0.5 -K1L -0.48249140494984327 -refen $refen 
 Bpm "BPM.CR1_1.58" -len 0 
 Drift "CR1.DS1C_B" -len 0.95 
 Sextupole "SEXT.CR1.SX1CC" -len 0.1 -K2L -2.6486187130003453 -refen $refen 
 Drift "CR1.DS1C_A" -len 1.95 
 Marker "CR1MDBACELLSTART" 
 Marker "CR1.DBA.MCELL" 
 Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.33588751949999995 -refen $refen 
 Bpm "BPM.CR1_1.59" -len 0 
 Drift "CR1.DBA.D11A" -len 0.2082887778 
 Drift "CR1.DBA.D11B" -len 0.1989315367 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.60" -len 0 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sextupole "SEXT.CR1.DBA.SN0.3" -len 0.1 -K2L -3.8025374831036793 -refen $refen 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Marker "CR1.DBA.HCM0" 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038400000000006 -refen $refen 
 Bpm "BPM.CR1_1.61" -len 0 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Marker "CR1.DBA.HBM" 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SN3.3" -len 0.1 -K2L 2.601756478584562 -refen $refen 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.62" -len 0 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.63" -len 0 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SNN3.3" -len 0.1 -K2L -0.06285053528477778 -refen $refen 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Marker "CR1.DBA.HBM2" 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038400000000006 -refen $refen 
 Bpm "BPM.CR1_1.64" -len 0 
 Marker "CR1.DBA.HCM" 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Sextupole "SEXT.CR1.DBA.SN1.3" -len 0.1 -K2L -6.583534167937967 -refen $refen 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.65" -len 0 
 Drift "CR1.DBA.D11B1" -len 0.3133083062 
 Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -K2L 0.0 -refen $refen 
 Drift "CR1.DBA.DM0" -len 0.1 
 Quadrupole "QUAD.CR1.DBA.QF1" -len 0.4 -K1L -0.664141578 -refen $refen 
 Bpm "BPM.CR1_1.66" -len 0 
 Drift "CR1.DBA.DM1_A" -len 1.106904053 
 Sextupole "SEXT.CR1.DBA.SNCH1" -len 0.1 -K2L -6.6119600700444385 -refen $refen 
 Drift "CR1.DBA.DM1_B" -len 0.1069040525 
 Quadrupole "QUAD.CR1.DBA.QF2" -len 0.4 -K1L 0.5637241356 -refen $refen 
 Bpm "BPM.CR1_1.67" -len 0 
 Drift "CR1.DBA.DM2" -len 1.099116763 
 Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -K2L 0.0 -refen $refen 
 Drift "CR1.DBA.DMSEXT1" -len 0.1 
 Quadrupole "QUAD.CR1.DBA.QF3" -len 0.4 -K1L -0.6662844456000001 -refen $refen 
 Bpm "BPM.CR1_1.68" -len 0 
 Drift "CR1.DBA.DM3" -len 0.2563831229 
 Drift "CR1.DBA.DMSEXT1" -len 0.1 
 Drift "CR1.DBA.D11B2" -len 0.1489315367 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.69" -len 0 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.05125499604 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sextupole "SEXT.CR1.DBA.SN1.3" -len 0.1 -K2L -6.583534167937967 -refen $refen 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038399999999995 -refen $refen 
 Bpm "BPM.CR1_1.70" -len 0 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.1995945832 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SNN3.3" -len 0.1 -K2L -0.06285053528477778 -refen $refen 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.71" -len 0 
 Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -K1L 0.2323546236 -refen $refen 
 Bpm "BPM.CR1_1.72" -len 0 
 Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
 Sextupole "SEXT.CR1.DBA.SN3.3" -len 0.1 -K2L 2.601756478584562 -refen $refen 
 Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
 Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -K1L 0.19959458320000004 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.DS2C" -len 0.5664710132 
 Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -K1L -0.45038399999999995 -refen $refen 
 Bpm "BPM.CR1_1.73" -len 0 
 Drift "CR1.DBA.DS1C_B" -len 0.1 
 Sextupole "SEXT.CR1.DBA.SN0.3" -len 0.1 -K2L -3.8025374831036793 -refen $refen 
 Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
 Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -K1L 0.051254996039999993 -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$refen*$refen*$refen*$refen*$Dipole_synrad] 
 Drift "CR1.DBA.D12" -len 0.2467904078 
 Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -K1L 0.66922795 -refen $refen 
 Bpm "BPM.CR1_1.74" -len 0 
 Drift "CR1.DBA.D11B" -len 0.1989315367 
 Drift "CR1.DBA.D11A" -len 0.2082887778 
 Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -K1L -0.3358875195 -refen $refen 
 Bpm "BPM.CR1_1.75" -len 0 
 Marker "CR1.DBA.MCELL" 
 Marker "CR1MDBACELLEND" 
 ### End of arc 3 ### 
 ### Missing length correction ### 
 Drift "CR1.DMISSLEN" -len 1.66137098404886 
 Sextupole "SEXT.CR1.SXL0EXT" -len 0.5 -K2L 0.0 -refen $refen 
 Drift "CR1.DL0IN" -len 0.5 
 ### 
 Quadrupole "QUAD.CR1.QL1EXT" -len 0.5 -K1L -0.7350190844328781 -refen $refen 
 Bpm "BPM.CR1_1.76" -len 0 
 Drift "CR1.DL1IN" -len 0.3098482 
 Quadrupole "QUAD.CR1.QL2EXT" -len 0.5 -K1L 1.1940230580948872 -refen $refen 
 Bpm "BPM.CR1_1.77" -len 0 
 Drift "CR1.DL2IN" -len 0.3010372 
 Quadrupole "QUAD.CR1.QL3EXT" -len 0.5 -K1L -0.4780806892244219 -refen $refen 
 Bpm "BPM.CR1_1.78" -len 0 
 Drift "CR1.DL3IN" -len 1.099258 
 Drift "CR1.DRIFTEXT1" -len 0.1736769688 
 #### STEP EXTRACTOR - kicks on 3*244 bunches at a time #### 
 #Marker "CR1.EXTK.SHOUT" -shout 1 
 Stepdipole "CR1.EXTKICKER" -len 2. -strength [expr 0.0057*$refen] -freq [expr 0.49975/(244*3)] -phase [expr (-3./12.)*2.*$Pi] -duty [expr 1./3.] -refen $refen 
 Drift "CR1.DRIFTEXT2" -len 0.3263269688 
 Drift "CR1.DL4IN" -len 0.349996062 
 Quadrupole "QUAD.CR1.QL4EXT" -len 0.5 -K1L 0.6388416037583273 -refen $refen 
 Bpm "BPM.CR1_1.79" -len 0 
 Drift "CR1.DL5IN" -len 1 
 Quadrupole "QUAD.CR1.QL5EXT" -len 0.5 -K1L -0.906597728673643 -refen $refen 
 Bpm "BPM.CR1_1.80" -len 0 
 Drift "CR1.DL6IN" -len 0.553 
 Quadrupole "QUAD.CR1.QL6EXT" -len 0.25 -K1L 0.5016200926487812 -refen $refen 
 Bpm "BPM.CR1_1.81" -len 0 
 Drift "CR1.DL7IN" -len 0.5 
 Drift "CR1.EXTSEPTUM" -len 1 
 Marker "CR1_P1.EXTRACTION" 
 
